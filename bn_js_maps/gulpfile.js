// bn-build configurations

var BNB = {
    current : "tests",

    packages : {

        bnmaps : {

            // custom js
            js : {
                res : [
                    'src/main.js'
                ],

                dest : 'dist/js',
                name : 'bnmaps.js'
            }

        },
        tests : {

            // custom js
            js : {
                res : [
                    'example/main.js'
                ]
            }

        }
    }
};

var gulp = require('gulp'),
    gulpLoadPlugins = require('gulp-load-plugins'),
    gutil = require('gulp-util'),
    babel = require('gulp-babel'),
    fs = require('fs'),
    plugins = gulpLoadPlugins();
var argv = require('yargs').argv;

if (argv.test === undefined) BNB.current = "bnmaps";

// custom js
gulp.task('js', function () {
    return gulp.src(BNB.packages[BNB.current].js.res)
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(plugins.webpack({
            output: {
                filename: 'bnmaps.js'
            }
        }))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('js:common', function () {
    return gulp.src(BNB.packages[BNB.current].js.res)
        .pipe(plugins.webpack({
            output: {
                libraryTarget: 'commonjs2',
                filename: 'bnmaps.js'
            }
        }))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('dist/common/js'));
});
// custom js
gulp.task('test', function () {
    return gulp.src('example/main.js')
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(plugins.webpack({
            output: {
                filename: 'main.js'
            }
        }))
            // .pipe(plugins.uglify())
        .pipe(gulp.dest('example/common'));
});

// build
gulp.task('build', ['js', 'js:common', 'test'], function () { console.log("Building entire project"); });