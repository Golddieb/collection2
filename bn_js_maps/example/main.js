const bnMaps = require("../src/main.js");
/*
    In Live Production wird as JS Module mit folgender Zeile required: require("bn.maps/dist/common/js/bnmaps");
 */

/**
 * Example 1 Config
 */
let optionsExample1 = {
    apiKey: "AIzaSyAyZuub8mckP7u0SNQ_xeum9n1eHGwY2_0",
    language: 'de',
    containerMap: 'example1',
    mapConfig:{
        marker: {
            lat: 46.5671285,
            lng: 11.5613681,
            title: 'Wanderhotel Europa',
            icon: 'https://www.brandnamic.com/fileadmin/web/img/marker.png'
        },
        options: {
            zoom: 12,
            scrollwheel: true,
            draggable: true,
            mapTypeControl: true
        }
    },
};
let example1 = new bnMaps(optionsExample1);

/**
 * Example 2 Config
 */

let optionsExample2 = {
    apiKey: "AIzaSyAyZuub8mckP7u0SNQ_xeum9n1eHGwY2_0",
    language: 'de',
    containerMap: 'example2',
    mapConfig:{
        route: {
            fieldInputId: "routeStart",
            autoComplete: true,
            geoLocation: false,
            // travelMode: 'WALKING', //Default driving
            removeMarkerOnResult: false,
            containerId: "directionsPanel"
        },
        marker: {
            lat: 46.5671285,
            lng: 11.5613681,
            title: 'Wanderhotel Europa',
            icon: 'https://www.brandnamic.com/fileadmin/web/img/marker.png'
        },
        options: {
            style: [{'stylers': [{'hue': '#332318'}, {'gamma': 0.4}]}],
            zoom: 12,
            scrollwheel: false,
            draggable: false,
            mapTypeControl: false
        }
    },
};
let example2 = new bnMaps(optionsExample2);
$('#routeForm').on('submit', function(e){
    e.preventDefault();
    example2.calcRoute();
});
/**
 * Example 3 Config
 */

let optionsExample3 = {
    apiKey: "AIzaSyAyZuub8mckP7u0SNQ_xeum9n1eHGwY2_0",
    language: 'de',
    containerMap: 'example3',
    mapConfig:{
        streetView:{
            lat: 46.5671285,
            lng: 11.5613681,
            heading: 0, //Rotationswinkel um den geometrischen Kameraort
            pitch: 0 //Horizontaler Neigungswinkel
        },
        marker: {
            lat: 46.5671285,
            lng: 11.5613681,
            title: 'Wanderhotel Europa',
            icon: 'https://www.brandnamic.com/fileadmin/web/img/marker.png'
        },
    },
};

let example3 = new bnMaps(optionsExample3);

/**
 * Example 4 Config
 */

let optionsExample4 = {
    language: 'de',
    containerMap: 'example4',
    mapConfig:{
        route: {
            fieldInputId: "routeStart4",
            autoComplete: true,
            geoLocation: false,
            removeMarkerOnResult: true
            // travelMode: 'WALKING', //Default driving
        },
        marker: {
            lat: 46.5671285,
            lng: 11.5613681,
            title: 'Wanderhotel Europa',
            icon: 'https://www.brandnamic.com/fileadmin/web/img/marker.png'
        },
        options: {
            style: [{'stylers': [{'hue': '#332318'}, {'gamma': 0.4}]}],
            zoom: 12,
            scrollwheel: false,
            draggable: false,
            mapTypeControl: false
        }
    },
};
let example4 = new bnMaps(optionsExample4);

$('#routeForm4').on('submit', function(e){
    e.preventDefault();
    example4.calcRoute();
});

/**
 * Example 5 Config
 */

let optionsExample5 = {
    language: 'de',
    containerMap: 'example5',
    mapConfig:{
        options: {
            zoom: 7,
            scrollwheel: true,
            draggable: true,
            mapTypeControl: true,
            lat: 47.6173226,
            lng: 10.9189516
        },
        marker: [
            ['Brandnamic Brixen', 46.6956387, 11.6426861,"https://www.brandnamic.com/fileadmin/web/img/marker.png"],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic München', 48.1178941, 11.597513],
        ],
    },
};

let example5 = new bnMaps(optionsExample5);

/**
 * Example 6 Config
 */

let optionsExample6 = {
    language: 'de',
    containerMap: 'example6',
    mapConfig:{
        options: {
            zoom: 7,
            maxZoom: 15,
            scrollwheel: true,
            draggable: true,
            mapTypeControl: true,
            lat: 47.6173226,
            lng: 10.9189516,
            markerCluster: true,
            //Take Snippet below to use individual marker cluster
            // markerIcons: [
            //     {
            //         textColor: '#fff',
            //         url: '../images/m5.png',
            //         height: 89,
            //         width: 90
            //     },
            //     {
            //         textColor: 'white',
            //         url: '../images/m3.png',
            //         height: 66,
            //         width: 65
            //     }
            // ]
        },
        marker: [
            ['Wirt an der Mahr', 46.697008, 11.644028,"https://www.brandnamic.com/fileadmin/web/img/marker.png"],
            ['Brandnamic Brixen', 46.6956387, 11.6426861,"https://www.brandnamic.com/fileadmin/web/img/marker.png"],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic München', 48.1178941, 11.597513],
          ['Brandnamic München', 48.1178941, 11.597513],
          ['Brandnamic München', 48.1178941, 11.597513],
          ['Brandnamic München', 48.1178941, 11.597513],
          ['Brandnamic München', 48.1178941, 11.597513],
          ['Brandnamic München', 48.1178941, 11.597513],
          ['Brandnamic München', 48.1178941, 11.597513],
        ],
    },
};

let example6 = new bnMaps(optionsExample6);

let optionsExample6_2 = {
    language: 'de',
    containerMap: 'example6_2',
    mapConfig:{
        options: {
            zoom: 7,
            scrollwheel: true,
            draggable: true,
            mapTypeControl: true,
            lat: 47.6173226,
            lng: 10.9189516,
            markerCluster: true,
        },
        marker: [
            ['Wirt an der Mahr', 46.697008, 11.644028,"https://www.brandnamic.com/fileadmin/web/img/marker.png"],
            ['Brandnamic Brixen', 46.6956387, 11.6426861,"https://www.brandnamic.com/fileadmin/web/img/marker.png"],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic München', 48.1178941, 11.597513],
        ],
    },
};

let example6_2 = new bnMaps(optionsExample6_2);

/**
 * Example 7 Config
 */

let optionsExample7 = {
    language: 'de',
    containerMap: 'example7',
    mapConfig:{
        route: {
            fieldInputId: "routeStart7",
            autoComplete: true,
            geoLocation: false,
            removeMarkerOnResult: true
            // travelMode: 'WALKING', //Default driving
        },
        options: {
            zoom: 7,
            maxZoom: 12,
            scrollwheel: false,
            draggable: true,
            mapTypeControl: true,
            lat: 47.6173226,
            lng: 10.9189516,
            markerCluster: true,
            showMarkerInSelectField: '.markerDropdown'
        },
        marker: [
            ['Wirt an der Mahr', 46.697008, 11.644028,"https://www.brandnamic.com/fileadmin/web/img/marker.png"],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Cyprianerhof', 46.4711, 11.5595103],
          ['Ritterhof', 46.5410376, 11.5604435],
          ['La Maiena', 46.6633989, 11.1365077],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic Meran', 46.6780594, 11.2556943],
          ['Brandnamic München', 48.1178941, 11.597513],
          ['Brandnamic München', 48.1178941, 11.597513],
          ['Brandnamic München', 48.1178941, 11.597513],
          ['Brandnamic München', 48.1178941, 11.597513],
        ],
    },
};

let example7 = new bnMaps(optionsExample7);
$('#routeForm7').on('submit', function(e){
    e.preventDefault();
    example7.calcRoute();
});