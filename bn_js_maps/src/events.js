class EventListeners {
    onMapLoaded() {
        let _self = this;
        google.maps.event.addListenerOnce(this.map, 'idle', function () {
            document.getElementById(_self.containerMap).className += " bn-maps-initDone";
            // do something only the first time the map is loaded
        });
    }

    disablePinterestCrawl() {
        let _self = this;
        google.maps.event.addListener(this.map, 'tilesloaded', () => {
            let images = document.getElementById(_self.containerMap).querySelectorAll('img');
            images.forEach(function (el) {
                el.setAttribute('nopin', 'nopin');
            });
        });
    }
}
module.exports = EventListeners;