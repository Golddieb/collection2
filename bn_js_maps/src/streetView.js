class StreetView {
    constructor(options) {
        this.options = options;
        this.map = options.map;
        this.mapConfig = options.mapConfig;
        this.containerMap = options.containerMap;
        this.render();
    }
    render() {
        let mapConfig = this.mapConfig,
            streetView = mapConfig.streetView,
            options = mapConfig.options,
            heading = streetView.heading || 0,
            pitch = streetView.pitch || 0,
            lat = streetView.lat || options.lat,
            lng = streetView.lng || options.lng;

        let location = {lat: lat, lng: lng};

        /**
         * When the map isn´t intialized yet, make map
         */
        if (typeof this.map === "undefined") {
            let map = new google.maps.Map(document.getElementById(this.containerMap), {
                center: new google.maps.LatLng(lat, lng),
                zoom: 10
            });
            this.map = map
        }
        let panorama = new google.maps.StreetViewPanorama(
            document.getElementById(this.containerMap), {
                position: location,
                pov: {
                    heading: heading,
                    pitch: pitch
                }
            });
        this.map.setStreetView(panorama);
    }
}
module.exports = StreetView;