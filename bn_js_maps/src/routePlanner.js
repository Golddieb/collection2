class RoutePlanner {
    constructor(options) {
        this.options = options;
        this.map = options.map;
        this.mapConfig = options.mapConfig;
        this.mapOptions = options.mapConfig.options;
        /**
         * Disable Marker Cluster otherwise the markers will be reinitialized on delete
         */
        if (options.markerCluster !== undefined) {
           this.options.markerCluster.clearMarkers();
        }

        if (this.mapOptions.showMarkerInSelectField === undefined) {
            /**
             Init Single RoutePlanner
             */
            this.calcSingleRoute();
        }
        else {
            /**
             Init Multiple RoutePlanner
             */
            this.calcMultipleRoute();
        }
    }

    calcSingleRoute() {
        let mapConfig = this.mapConfig,
            options = mapConfig.options,
            marker = mapConfig.marker,
            route = mapConfig.route,
            lat = marker.lat || options.lat,
            lng = marker.lng || options.lng,
            drivingMode = route.travelMode || 'DRIVING',
            self = this,
            services = this.options.services,
            directionsDisplay = services.directionsDisplay,
            directionsService = services.directionsService;


        directionsDisplay.setMap(this.map);
        directionsDisplay.setPanel(document.getElementById(route.containerId));

        let start = document.getElementById(route.fieldInputId).value;
        let request = {
            origin: start,
            destination: lat + ',' + lng,
            travelMode: google.maps.DirectionsTravelMode[drivingMode]
        };
        let thisObject = this.options;
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                /**
                 * Remove marker when route results are displayed
                 */
                if (route.removeMarkerOnResult !== undefined && route.removeMarkerOnResult !== false) {
                    self.options.marker.setMap(null)
                }
                else {
                    directionsDisplay.setOptions({suppressMarkers: true});
                }
            } else {
                if (status == 'ZERO_RESULTS') {
                    alert(thisObject.getTranslation("ZERO_RESULTS"));
                } else if (status == 'UNKNOWN_ERROR') {
                    alert(thisObject.getTranslation("UNKNOWN_ERROR"));
                } else if (status == 'REQUEST_DENIED') {
                    alert(thisObject.getTranslation("REQUEST_DENIED"));
                } else if (status == 'OVER_QUERY_LIMIT') {
                    alert(thisObject.getTranslation("OVER_QUERY_LIMIT"));
                } else if (status == 'NOT_FOUND') {
                    alert(thisObject.getTranslation("NOT_FOUND"));
                } else if (status == 'INVALID_REQUEST') {
                    alert(thisObject.getTranslation("INVALID_REQUEST"));
                } else {
                    alert(thisObject.getTranslation("REQUESTSTATUS") + " \n\n" + status);
                }
            }
        });
    }

    calcMultipleRoute() {
        let mapConfig = this.mapConfig,
            options = mapConfig.options,
            marker = mapConfig.marker,
            route = mapConfig.route,
            drivingMode = route.travelMode || 'DRIVING',
            self = this,
            services = this.options.services,
            directionsDisplay = services.directionsDisplay,
            directionsService = services.directionsService;
        debugger;
        /**
         * Get current selected option
         */
        let selectField = document.querySelector(options.showMarkerInSelectField).getElementsByClassName('selectField')[0],
            currIndex = selectField.selectedIndex,
            currValue = selectField.options[currIndex].value;

        let lat = marker[currValue][1],
            lng = marker[currValue][2];

        directionsDisplay.setMap(this.map);
        directionsDisplay.setPanel(document.getElementById(route.containerId));

        let start = document.getElementById(route.fieldInputId).value;
        let request = {
            origin: start,
            destination: lat + ',' + lng,
            travelMode: google.maps.DirectionsTravelMode[drivingMode]
        };
        let thisObject = this.options;
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                /**
                 * Remove marker when route results are displayed
                 */
                if (route.removeMarkerOnResult !== undefined && route.removeMarkerOnResult !== false) {
                    for (let i = 0; i < self.options.markers.length; i++) {
                        self.options.markers[i].setMap(null);
                    }
                    self.options.markers = new Array();
                }
                else {
                    directionsDisplay.setOptions({suppressMarkers: true});
                }
            } else {
                if (status == 'ZERO_RESULTS') {
                    alert(thisObject.getTranslation("ZERO_RESULTS"));
                } else if (status == 'UNKNOWN_ERROR') {
                    alert(thisObject.getTranslation("UNKNOWN_ERROR"));
                } else if (status == 'REQUEST_DENIED') {
                    alert(thisObject.getTranslation("REQUEST_DENIED"));
                } else if (status == 'OVER_QUERY_LIMIT') {
                    alert(thisObject.getTranslation("OVER_QUERY_LIMIT"));
                } else if (status == 'NOT_FOUND') {
                    alert(thisObject.getTranslation("NOT_FOUND"));
                } else if (status == 'INVALID_REQUEST') {
                    alert(thisObject.getTranslation("INVALID_REQUEST"));
                } else {
                    alert(thisObject.getTranslation("REQUESTSTATUS") + " \n\n" + status);
                }
            }
        });
        debugger;
    }
}

module.exports = RoutePlanner;