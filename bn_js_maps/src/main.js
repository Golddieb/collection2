const {Promise} = require('es6-promise');
const EventListeners = require('./events');
const RoutePlanner = require('./routePlanner');
const StreetView = require('./streetView');


class BnMaps extends EventListeners {
    constructor(options) {
        super();
        this.containerMap = options.containerMap;
        //Use users api key - fallback to default one
        this.apiKey = options.apiKey || 'AIzaSyDaUUlhlgiJM9T3hfbKMXTujcMTetgbMvQ';
        //Default language is en
        this.language = options.language || 'en';
        this.mapStyle = options.mapStyle;
        this.mapConfig = options.mapConfig;
        //Create object for bundling all services
        this.services = {};
        this.loadLibrary();
    }

    initMap() {
        /**
         * Initialize Map
         */
        let mapConfig = this.mapConfig,
            marker = mapConfig.marker,
            route = mapConfig.route,
            streetView = mapConfig.streetView,
            options = mapConfig.options;

        if (typeof streetView !== "undefined") {
            new StreetView(this);
            // this.streetView();
        }
        if (typeof options !== "undefined") {

            let lat = options.lat || marker.lat,
                lng = options.lng || marker.lng;
            //Change Options before intialization of the map
            if (typeof options.style !== "undefined") {
                options.mapTypeId = 'Styled';
                options.disableDefaultUI = true
            }
            options.center = new google.maps.LatLng(lat, lng);

            let map = new google.maps.Map(document.getElementById(this.containerMap), options);

            //Save map object to use it global
            this.map = map;
            /**
             * Set map style
             */
            if (typeof options.style !== "undefined") {
                //If User select custom style, disable UI
                options.mapTypeId = 'Styled';
                options.disableDefaultUI = true;
                this.setMapStyle(map);
            }
            /**
             * Set class when map is loaded
             */
            this.onMapLoaded();
            /**
             * Disable crawling google maps images in Pinterest
             */
            this.disablePinterestCrawl();

        }
        if (typeof marker !== "undefined") {
            /**
             * Set Marker in google maps
             */
            if (Object.prototype.toString.call(marker) === '[object Array]') {
                this.initMultipleMarker();
            }
            else {
                this.initSingleMarker();
            }
        }

        if (typeof route === "undefined") {
        } else {
            /**
             * Set directionsService and directionsDisplay as global variable to init new routes with a blank map
             */
            this.services = {
                directionsService: new google.maps.DirectionsService,
                directionsDisplay: new google.maps.DirectionsRenderer

            };
            /**
             * AutoComplete on input field
             */
            if (typeof route.autoComplete !== "undefined") {
                if (typeof route.fieldInputId !== "undefined") {
                    let input = /** @type {!HTMLInputElement} */ (document.getElementById(route.fieldInputId));
                    let autocomplete = new google.maps.places.Autocomplete(input);
                    autocomplete.bindTo('bounds', this.map);
                }
                else {
                    console.error('Google Maps - fieldInputId is missing in route');
                }
            }

            /**
             * Calculate users start point with geolocation
             */
            if (typeof route.geoLocation !== "undefined" && route.geoLocation !== false) {
                this.geoLocation();
            }
        }
        if (options.markerCluster === true) {
            this.loadMarkClusterLibrary();
        }


    }

    initMarkerCluster() {
        let clusterOptions = {};
        if (typeof this.mapConfig.options.markerIcons !== "undefined") {
            clusterOptions.styles = this.mapConfig.options.markerIcons;
        }
        else {
            clusterOptions.imagePath = 'https://cdn.bnamic.com/brandnamic_files/bn-maps/images/m';
        }
        this.markerCluster = new MarkerClusterer(this.map, this.markers, clusterOptions);
    }

    setMapStyle(map) {
        /**
         * Set Style for Google Maps
         */
        let mapConfig = this.mapConfig;
        let mapType = new google.maps.StyledMapType(mapConfig.options.style, {name: 'Styled'});
        map.mapTypes.set('Styled', mapType);
    }

    /**
     * Render multiple markers in select form
     */
    renderMarkersInSelectForm() {
        let markerConfig = this.mapConfig.marker,
            mapConfiguration = this.mapConfig.options,
            i;

        let selectList = document.createElement('select'); // Create Select Field
        selectList.className = 'selectField'; //Add class to select list
        document.querySelector(mapConfiguration.showMarkerInSelectField).appendChild(selectList);

        for (i = 0; i < markerConfig.length; i++) {
            let option = document.createElement("option");
            option.value = i;
            option.setAttribute('data-lat', markerConfig[i][1]);
            option.setAttribute('data-lng', markerConfig[i][2]);
            //If no marker is set, replace it with empty value
            option.setAttribute('data-icon', markerConfig[i][3] || '');
            option.text = markerConfig[i][0];
            selectList.appendChild(option);
        }
    }

    /**
     * Initialize the multiple markers that are passed from the marker array
     */
    initMultipleMarker() {
        let markerConfig = this.mapConfig.marker,
            infowindow = new google.maps.InfoWindow(),
            marker, i;
        let markers = [];
        let mapConfig = this.mapConfig.options;

        for (i = 0; i < markerConfig.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(markerConfig[i][1], markerConfig[i][2]),
                map: this.map,
                icon: markerConfig[i][3]
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(markerConfig[i][0]);
                    infowindow.open(this.map, marker);
                }
            })(marker, i));
            markers.push(marker);
        }

        if (mapConfig.showMarkerInSelectField !== undefined) {
            this.renderMarkersInSelectForm();
        }
        this.markers = markers;
    }

    initSingleMarker() {
        let markerConfig = this.mapConfig.marker,
            mapConfig = this.mapConfig,
            options = mapConfig.options,
            lat = markerConfig.lat || options.lat,
            lng = markerConfig.lng || options.lng;

        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: this.map,
            title: markerConfig.title,
            icon: markerConfig.icon
        });
        this.marker = marker;
    }

    geoLocation() {
        let selfConfig = this.mapConfig,
            self = this;
        // Check if html5 geolocation is supported
        if (navigator.geolocation) {
            // Wait after user accept geoLocation Term
            navigator.geolocation.getCurrentPosition(function (position) {
                let pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                document.getElementById(selfConfig.route.fieldInputId).value = pos.lat + ',' + pos.lng;
                self.calcRoute();
            });
        }
    }

    calcRoute() {
        new RoutePlanner(this);
    }

    getTranslation(word) {
        let translations = {
            'de': {
                'ZERO_RESULTS': "Zwischen Ausgangspunkt und Zielpunkt wurde kein Pfad gefunden.",
                'UNKNOWN_ERROR': "Die Directions-Anfrage konnte aufgrund eines Serverfehlers nicht durchgeführt werden. Bitte versuchen Sie es erneut.",
                'REQUEST_DENIED': "Die Webseite ist nicht berechtigt, den Directions-Dienst zu verwenden.",
                'OVER_QUERY_LIMIT': "Die Webseite hat die Höchstgrenze der Anfragen in zu kurzer Zeit überschritten.",
                'NOT_FOUND': "Ausgangspunkt, Zielpunkt oder Wegpunkte konnten nicht geokodiert werden.",
                'INVALID_REQUEST': "Die angegebene Directions-Anfrage ist ungültig.",
                'REQUESTSTATUS': "Es gab einen unbekannten Fehler in Ihrer Anfrage. Requeststatus:"
            },
            'it': {
                'ZERO_RESULTS': "Non è stato individuato alcun percorso tra punto di partenza e punto di arrivo.",
                'UNKNOWN_ERROR': "Non è stato possibile eseguire la richiesta di direzione a causa di un errore di server. Si prega di riprovare.",
                'REQUEST_DENIED': "Il sito web non è autorizzato a utilizzare il servizio di direzione.",
                'OVER_QUERY_LIMIT': "Il sito web ha superato il limite massimo delle richieste in un tempo troppo breve.",
                'NOT_FOUND': "Non è stato possibile geocodificare il punto di partenza, il punto di arrivo e il punto di itinerario.",
                'INVALID_REQUEST': "La richiesta di direzione fornita non è valida.",
                'REQUESTSTATUS': "Si è verificato un errore non identificato nella richiesta inviata. Stato della richiesta:"
            },
            'default': {
                'ZERO_RESULTS': "No route could be found between the origin and destination.",
                'UNKNOWN_ERROR': "A directions request could not be processed due to a server error. The request may succeed if you try again.",
                'REQUEST_DENIED': "This webpage is not allowed to use the directions service.",
                'OVER_QUERY_LIMIT': "The webpage has gone over the requests limit in too short a period of time.",
                'NOT_FOUND': "At least one of the origin, destination, or waypoints could not be geocoded.",
                'INVALID_REQUEST': "The DirectionsRequest provided was invalid.",
                'REQUESTSTATUS': "There was an unknown error in your request. Requeststatus:"
            }
        };
        return translations[this.language][word] ? translations[this.language][word] : translations["default"][word];
    }

    /**
     * Load MarkerCluster API if needed
     */
    loadMarkClusterLibrary() {
        /**
         * Require Library
         */
        require('../libs/markerclusterer.js');
        /**
         Init Cluster
         */
        this.initMarkerCluster();
    }

    loadLibrary() {
        let self = this;
        let BN = window.BN = window.brandnamic = window.brandnamic || window.BN || {};
        BN.maps = BN.maps || {};

        let proms = BN.maps.promises = BN.maps.promises || {};

        if (!proms.library || typeof proms.library.then !== typeof function () {
            }) {
            proms.library = new Promise((resolve, reject) => {
                let script = document.createElement('script');
                let apiKey = '&key=' + this.apiKey;

                script.src = "https://maps.googleapis.com/maps/api/js?libraries=places&v=3" + apiKey + "&language=" + this.language;
                let wrapper = () => {
                    resolve(true);
                };

                if (script.readyState) {
                    script.onreadystatechange = function () {
                        if (script.readyState === "loaded" || script.readyState === "complete") {
                            script.onreadystatechange = null;
                            wrapper.call(script);
                        }
                    };
                } else if (script.addEventListener) {
                    script.addEventListener('load', wrapper, false);
                } else if (script.attachEvent) {
                    script.attachEvent('onload', wrapper);
                } else {
                    script.onload = wrapper;
                }
                document.head.appendChild(script);
            });
        }

        proms.library.then(self.initMap.bind(self));
    }
}

// class RoutePlanner {
//     constructor(options) {
//         this.options = options;
//         this.map = options.map;
//         this.mapConfig = options.mapConfig;
//         this.mapOptions = options.mapConfig.options;
//         /**
//          * Disable Marker Cluster otherwise the markers will be reinitialized on delete
//          */
//         if (options.markerCluster !== undefined) {
//            this.options.markerCluster.clearMarkers();
//         }
//
//         if (this.mapOptions.showMarkerInSelectField === undefined) {
//             /**
//              Init Single RoutePlanner
//              */
//             this.calcSingleRoute();
//         }
//         else {
//             /**
//              Init Multiple RoutePlanner
//              */
//             this.calcMultipleRoute();
//         }
//     }
//
//     calcSingleRoute() {
//         let mapConfig = this.mapConfig,
//             options = mapConfig.options,
//             marker = mapConfig.marker,
//             route = mapConfig.route,
//             lat = marker.lat || options.lat,
//             lng = marker.lng || options.lng,
//             drivingMode = route.travelMode || 'DRIVING',
//             self = this,
//             services = this.options.services,
//             directionsDisplay = services.directionsDisplay,
//             directionsService = services.directionsService;
//
//
//         directionsDisplay.setMap(this.map);
//         directionsDisplay.setPanel(document.getElementById(route.containerId));
//
//         let start = document.getElementById(route.fieldInputId).value;
//         let request = {
//             origin: start,
//             destination: lat + ',' + lng,
//             travelMode: google.maps.DirectionsTravelMode[drivingMode]
//         };
//         let thisObject = this.options;
//         directionsService.route(request, function (response, status) {
//             if (status == google.maps.DirectionsStatus.OK) {
//                 directionsDisplay.setDirections(response);
//                 /**
//                  * Remove marker when route results are displayed
//                  */
//                 if (route.removeMarkerOnResult !== undefined && route.removeMarkerOnResult !== false) {
//                     self.options.marker.setMap(null)
//                 }
//                 else {
//                     directionsDisplay.setOptions({suppressMarkers: true});
//                 }
//             } else {
//                 if (status == 'ZERO_RESULTS') {
//                     alert(thisObject.getTranslation("ZERO_RESULTS"));
//                 } else if (status == 'UNKNOWN_ERROR') {
//                     alert(thisObject.getTranslation("UNKNOWN_ERROR"));
//                 } else if (status == 'REQUEST_DENIED') {
//                     alert(thisObject.getTranslation("REQUEST_DENIED"));
//                 } else if (status == 'OVER_QUERY_LIMIT') {
//                     alert(thisObject.getTranslation("OVER_QUERY_LIMIT"));
//                 } else if (status == 'NOT_FOUND') {
//                     alert(thisObject.getTranslation("NOT_FOUND"));
//                 } else if (status == 'INVALID_REQUEST') {
//                     alert(thisObject.getTranslation("INVALID_REQUEST"));
//                 } else {
//                     alert(thisObject.getTranslation("REQUESTSTATUS") + " \n\n" + status);
//                 }
//             }
//         });
//     }
//
//     calcMultipleRoute() {
//         let mapConfig = this.mapConfig,
//             options = mapConfig.options,
//             marker = mapConfig.marker,
//             route = mapConfig.route,
//             drivingMode = route.travelMode || 'DRIVING',
//             self = this,
//             services = this.options.services,
//             directionsDisplay = services.directionsDisplay,
//             directionsService = services.directionsService;
//         debugger;
//         /**
//          * Get current selected option
//          */
//         let selectField = document.querySelector(options.showMarkerInSelectField).getElementsByClassName('selectField')[0],
//             currIndex = selectField.selectedIndex,
//             currValue = selectField.options[currIndex].value;
//
//         let lat = marker[currValue][1],
//             lng = marker[currValue][2];
//
//         directionsDisplay.setMap(this.map);
//         directionsDisplay.setPanel(document.getElementById(route.containerId));
//
//         let start = document.getElementById(route.fieldInputId).value;
//         let request = {
//             origin: start,
//             destination: lat + ',' + lng,
//             travelMode: google.maps.DirectionsTravelMode[drivingMode]
//         };
//         let thisObject = this.options;
//         directionsService.route(request, function (response, status) {
//             if (status == google.maps.DirectionsStatus.OK) {
//                 directionsDisplay.setDirections(response);
//                 /**
//                  * Remove marker when route results are displayed
//                  */
//                 if (route.removeMarkerOnResult !== undefined && route.removeMarkerOnResult !== false) {
//                     for (let i = 0; i < self.options.markers.length; i++) {
//                         self.options.markers[i].setMap(null);
//                     }
//                     self.options.markers = new Array();
//                 }
//                 else {
//                     directionsDisplay.setOptions({suppressMarkers: true});
//                 }
//             } else {
//                 if (status == 'ZERO_RESULTS') {
//                     alert(thisObject.getTranslation("ZERO_RESULTS"));
//                 } else if (status == 'UNKNOWN_ERROR') {
//                     alert(thisObject.getTranslation("UNKNOWN_ERROR"));
//                 } else if (status == 'REQUEST_DENIED') {
//                     alert(thisObject.getTranslation("REQUEST_DENIED"));
//                 } else if (status == 'OVER_QUERY_LIMIT') {
//                     alert(thisObject.getTranslation("OVER_QUERY_LIMIT"));
//                 } else if (status == 'NOT_FOUND') {
//                     alert(thisObject.getTranslation("NOT_FOUND"));
//                 } else if (status == 'INVALID_REQUEST') {
//                     alert(thisObject.getTranslation("INVALID_REQUEST"));
//                 } else {
//                     alert(thisObject.getTranslation("REQUESTSTATUS") + " \n\n" + status);
//                 }
//             }
//         });
//         debugger;
//     }
// }
if (typeof module != "undefined") module.exports = BnMaps;
