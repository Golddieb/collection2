'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

module.exports =
/******/function (modules) {
	// webpackBootstrap
	/******/ // The module cache
	/******/var installedModules = {};

	/******/ // The require function
	/******/function __webpack_require__(moduleId) {

		/******/ // Check if module is in cache
		/******/if (installedModules[moduleId])
			/******/return installedModules[moduleId].exports;

		/******/ // Create a new module (and put it into the cache)
		/******/var module = installedModules[moduleId] = {
			/******/exports: {},
			/******/id: moduleId,
			/******/loaded: false
			/******/ };

		/******/ // Execute the module function
		/******/modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

		/******/ // Flag the module as loaded
		/******/module.loaded = true;

		/******/ // Return the exports of the module
		/******/return module.exports;
		/******/
	}

	/******/ // expose the modules object (__webpack_modules__)
	/******/__webpack_require__.m = modules;

	/******/ // expose the module cache
	/******/__webpack_require__.c = installedModules;

	/******/ // __webpack_public_path__
	/******/__webpack_require__.p = "";

	/******/ // Load entry module and return exports
	/******/return __webpack_require__(0);
	/******/
}(
/************************************************************************/
/******/[
/* 0 */
/***/function (module, exports, __webpack_require__) {
	var _webpack_require__ = __webpack_require__(1),
	    Promise = _webpack_require__.Promise;

	var EventListeners = __webpack_require__(4);
	var RoutePlanner = __webpack_require__(5);
	var StreetView = __webpack_require__(6);

	var BnMaps = function (_EventListeners) {
		_inherits(BnMaps, _EventListeners);

		function BnMaps(options) {
			_classCallCheck(this, BnMaps);

			var _this = _possibleConstructorReturn(this, (BnMaps.__proto__ || Object.getPrototypeOf(BnMaps)).call(this));

			_this.containerMap = options.containerMap;
			//Use users api key - fallback to default one
			_this.apiKey = options.apiKey || 'AIzaSyDaUUlhlgiJM9T3hfbKMXTujcMTetgbMvQ';
			//Default language is en
			_this.language = options.language || 'en';
			_this.mapStyle = options.mapStyle;
			_this.mapConfig = options.mapConfig;
			//Create object for bundling all services
			_this.services = {};
			_this.loadLibrary();
			return _this;
		}

		_createClass(BnMaps, [{
			key: 'initMap',
			value: function initMap() {
				/**
     * Initialize Map
     */
				var mapConfig = this.mapConfig,
				    marker = mapConfig.marker,
				    route = mapConfig.route,
				    streetView = mapConfig.streetView,
				    options = mapConfig.options;

				if (typeof streetView !== "undefined") {
					new StreetView(this);
					// this.streetView();
				}
				if (typeof options !== "undefined") {

					var lat = options.lat || marker.lat,
					    lng = options.lng || marker.lng;
					//Change Options before intialization of the map
					if (typeof options.style !== "undefined") {
						options.mapTypeId = 'Styled';
						options.disableDefaultUI = true;
					}
					options.center = new google.maps.LatLng(lat, lng);

					var map = new google.maps.Map(document.getElementById(this.containerMap), options);

					//Save map object to use it global
					this.map = map;
					/**
      * Set map style
      */
					if (typeof options.style !== "undefined") {
						//If User select custom style, disable UI
						options.mapTypeId = 'Styled';
						options.disableDefaultUI = true;
						this.setMapStyle(map);
					}
					/**
      * Set class when map is loaded
      */
					this.onMapLoaded();
					/**
      * Disable crawling google maps images in Pinterest
      */
					this.disablePinterestCrawl();
				}
				if (typeof marker !== "undefined") {
					/**
      * Set Marker in google maps
      */
					if (Object.prototype.toString.call(marker) === '[object Array]') {
						this.initMultipleMarker();
					} else {
						this.initSingleMarker();
					}
				}

				if (typeof route === "undefined") {} else {
					/**
      * Set directionsService and directionsDisplay as global variable to init new routes with a blank map
      */
					this.services = {
						directionsService: new google.maps.DirectionsService(),
						directionsDisplay: new google.maps.DirectionsRenderer()

					};
					/**
      * AutoComplete on input field
      */
					if (typeof route.autoComplete !== "undefined") {
						if (typeof route.fieldInputId !== "undefined") {
							var input = /** @type {!HTMLInputElement} */document.getElementById(route.fieldInputId);
							var autocomplete = new google.maps.places.Autocomplete(input);
							autocomplete.bindTo('bounds', this.map);
						} else {
							console.error('Google Maps - fieldInputId is missing in route');
						}
					}

					/**
      * Calculate users start point with geolocation
      */
					if (typeof route.geoLocation !== "undefined" && route.geoLocation !== false) {
						this.geoLocation();
					}
				}
				if (options.markerCluster === true) {
					this.loadMarkClusterLibrary();
				}
			}
		}, {
			key: 'initMarkerCluster',
			value: function initMarkerCluster() {
				var clusterOptions = {};
				if (typeof this.mapConfig.options.markerIcons !== "undefined") {
					clusterOptions.styles = this.mapConfig.options.markerIcons;
				} else {
					clusterOptions.imagePath = 'https://cdn.bnamic.com/brandnamic_files/bn-maps/images/m';
				}
				this.markerCluster = new MarkerClusterer(this.map, this.markers, clusterOptions);
			}
		}, {
			key: 'setMapStyle',
			value: function setMapStyle(map) {
				/**
     * Set Style for Google Maps
     */
				var mapConfig = this.mapConfig;
				var mapType = new google.maps.StyledMapType(mapConfig.options.style, { name: 'Styled' });
				map.mapTypes.set('Styled', mapType);
			}

			/**
    * Render multiple markers in select form
    */

		}, {
			key: 'renderMarkersInSelectForm',
			value: function renderMarkersInSelectForm() {
				var markerConfig = this.mapConfig.marker,
				    mapConfiguration = this.mapConfig.options,
				    i = void 0;

				var selectList = document.createElement('select'); // Create Select Field
				selectList.className = 'selectField'; //Add class to select list
				document.querySelector(mapConfiguration.showMarkerInSelectField).appendChild(selectList);

				for (i = 0; i < markerConfig.length; i++) {
					var option = document.createElement("option");
					option.value = i;
					option.setAttribute('data-lat', markerConfig[i][1]);
					option.setAttribute('data-lng', markerConfig[i][2]);
					//If no marker is set, replace it with empty value
					option.setAttribute('data-icon', markerConfig[i][3] || '');
					option.text = markerConfig[i][0];
					selectList.appendChild(option);
				}
			}

			/**
    * Initialize the multiple markers that are passed from the marker array
    */

		}, {
			key: 'initMultipleMarker',
			value: function initMultipleMarker() {
				var markerConfig = this.mapConfig.marker,
				    infowindow = new google.maps.InfoWindow(),
				    marker = void 0,
				    i = void 0;
				var markers = [];
				var mapConfig = this.mapConfig.options;

				for (i = 0; i < markerConfig.length; i++) {
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(markerConfig[i][1], markerConfig[i][2]),
						map: this.map,
						icon: markerConfig[i][3]
					});

					google.maps.event.addListener(marker, 'click', function (marker, i) {
						return function () {
							infowindow.setContent(markerConfig[i][0]);
							infowindow.open(this.map, marker);
						};
					}(marker, i));
					markers.push(marker);
				}

				if (mapConfig.showMarkerInSelectField !== undefined) {
					this.renderMarkersInSelectForm();
				}
				this.markers = markers;
			}
		}, {
			key: 'initSingleMarker',
			value: function initSingleMarker() {
				var markerConfig = this.mapConfig.marker,
				    mapConfig = this.mapConfig,
				    options = mapConfig.options,
				    lat = markerConfig.lat || options.lat,
				    lng = markerConfig.lng || options.lng;

				var marker = new google.maps.Marker({
					position: new google.maps.LatLng(lat, lng),
					map: this.map,
					title: markerConfig.title,
					icon: markerConfig.icon
				});
				this.marker = marker;
			}
		}, {
			key: 'geoLocation',
			value: function geoLocation() {
				var selfConfig = this.mapConfig,
				    self = this;
				// Check if html5 geolocation is supported
				if (navigator.geolocation) {
					// Wait after user accept geoLocation Term
					navigator.geolocation.getCurrentPosition(function (position) {
						var pos = {
							lat: position.coords.latitude,
							lng: position.coords.longitude
						};
						document.getElementById(selfConfig.route.fieldInputId).value = pos.lat + ',' + pos.lng;
						self.calcRoute();
					});
				}
			}
		}, {
			key: 'calcRoute',
			value: function calcRoute() {
				new RoutePlanner(this);
			}

			// streetView() {
			//     let mapConfig = this.mapConfig,
			//         streetView = mapConfig.streetView,
			//         options = mapConfig.options,
			//         heading = streetView.heading || 0,
			//         pitch = streetView.pitch || 0,
			//         lat = streetView.lat || options.lat,
			//         lng = streetView.lng || options.lng;
			//
			//     let location = {lat: lat, lng: lng};
			//
			//     /**
			//      * When the map isn´t intialized yet, make map
			//      */
			//     if (typeof this.map === "undefined") {
			//         let map = new google.maps.Map(document.getElementById(this.containerMap), {
			//             center: new google.maps.LatLng(lat, lng),
			//             zoom: 10
			//         });
			//         this.map = map
			//     }
			//     let panorama = new google.maps.StreetViewPanorama(
			//         document.getElementById(this.containerMap), {
			//             position: location,
			//             pov: {
			//                 heading: heading,
			//                 pitch: pitch
			//             }
			//         });
			//     this.map.setStreetView(panorama);
			// }

		}, {
			key: 'getTranslation',
			value: function getTranslation(word) {
				var translations = {
					'de': {
						'ZERO_RESULTS': "Zwischen Ausgangspunkt und Zielpunkt wurde kein Pfad gefunden.",
						'UNKNOWN_ERROR': "Die Directions-Anfrage konnte aufgrund eines Serverfehlers nicht durchgeführt werden. Bitte versuchen Sie es erneut.",
						'REQUEST_DENIED': "Die Webseite ist nicht berechtigt, den Directions-Dienst zu verwenden.",
						'OVER_QUERY_LIMIT': "Die Webseite hat die Höchstgrenze der Anfragen in zu kurzer Zeit überschritten.",
						'NOT_FOUND': "Ausgangspunkt, Zielpunkt oder Wegpunkte konnten nicht geokodiert werden.",
						'INVALID_REQUEST': "Die angegebene Directions-Anfrage ist ungültig.",
						'REQUESTSTATUS': "Es gab einen unbekannten Fehler in Ihrer Anfrage. Requeststatus:"
					},
					'it': {
						'ZERO_RESULTS': "Non è stato individuato alcun percorso tra punto di partenza e punto di arrivo.",
						'UNKNOWN_ERROR': "Non è stato possibile eseguire la richiesta di direzione a causa di un errore di server. Si prega di riprovare.",
						'REQUEST_DENIED': "Il sito web non è autorizzato a utilizzare il servizio di direzione.",
						'OVER_QUERY_LIMIT': "Il sito web ha superato il limite massimo delle richieste in un tempo troppo breve.",
						'NOT_FOUND': "Non è stato possibile geocodificare il punto di partenza, il punto di arrivo e il punto di itinerario.",
						'INVALID_REQUEST': "La richiesta di direzione fornita non è valida.",
						'REQUESTSTATUS': "Si è verificato un errore non identificato nella richiesta inviata. Stato della richiesta:"
					},
					'default': {
						'ZERO_RESULTS': "No route could be found between the origin and destination.",
						'UNKNOWN_ERROR': "A directions request could not be processed due to a server error. The request may succeed if you try again.",
						'REQUEST_DENIED': "This webpage is not allowed to use the directions service.",
						'OVER_QUERY_LIMIT': "The webpage has gone over the requests limit in too short a period of time.",
						'NOT_FOUND': "At least one of the origin, destination, or waypoints could not be geocoded.",
						'INVALID_REQUEST': "The DirectionsRequest provided was invalid.",
						'REQUESTSTATUS': "There was an unknown error in your request. Requeststatus:"
					}
				};
				return translations[this.language][word] ? translations[this.language][word] : translations["default"][word];
			}

			/**
    * Load MarkerCluster API if needed
    */

		}, {
			key: 'loadMarkClusterLibrary',
			value: function loadMarkClusterLibrary() {
				/**
     * Require Library
     */
				__webpack_require__(7);
				/**
     Init Cluster
     */
				this.initMarkerCluster();
			}
		}, {
			key: 'loadLibrary',
			value: function loadLibrary() {
				var _this2 = this;

				var self = this;
				var BN = window.BN = window.brandnamic = window.brandnamic || window.BN || {};
				BN.maps = BN.maps || {};

				var proms = BN.maps.promises = BN.maps.promises || {};

				if (!proms.library || _typeof(proms.library.then) !== _typeof(function () {})) {
					proms.library = new Promise(function (resolve, reject) {
						var script = document.createElement('script');
						var apiKey = '&key=' + _this2.apiKey;

						script.src = "https://maps.googleapis.com/maps/api/js?libraries=places&v=3" + apiKey + "&language=" + _this2.language;
						var wrapper = function wrapper() {
							resolve(true);
						};

						if (script.readyState) {
							script.onreadystatechange = function () {
								if (script.readyState === "loaded" || script.readyState === "complete") {
									script.onreadystatechange = null;
									wrapper.call(script);
								}
							};
						} else if (script.addEventListener) {
							script.addEventListener('load', wrapper, false);
						} else if (script.attachEvent) {
							script.attachEvent('onload', wrapper);
						} else {
							script.onload = wrapper;
						}
						document.head.appendChild(script);
					});
				}

				proms.library.then(self.initMap.bind(self));
			}
		}]);

		return BnMaps;
	}(EventListeners);

	// class RoutePlanner {
	//     constructor(options) {
	//         this.options = options;
	//         this.map = options.map;
	//         this.mapConfig = options.mapConfig;
	//         this.mapOptions = options.mapConfig.options;
	//         /**
	//          * Disable Marker Cluster otherwise the markers will be reinitialized on delete
	//          */
	//         if (options.markerCluster !== undefined) {
	//            this.options.markerCluster.clearMarkers();
	//         }
	//
	//         if (this.mapOptions.showMarkerInSelectField === undefined) {
	//             /**
	//              Init Single RoutePlanner
	//              */
	//             this.calcSingleRoute();
	//         }
	//         else {
	//             /**
	//              Init Multiple RoutePlanner
	//              */
	//             this.calcMultipleRoute();
	//         }
	//     }
	//
	//     calcSingleRoute() {
	//         let mapConfig = this.mapConfig,
	//             options = mapConfig.options,
	//             marker = mapConfig.marker,
	//             route = mapConfig.route,
	//             lat = marker.lat || options.lat,
	//             lng = marker.lng || options.lng,
	//             drivingMode = route.travelMode || 'DRIVING',
	//             self = this,
	//             services = this.options.services,
	//             directionsDisplay = services.directionsDisplay,
	//             directionsService = services.directionsService;
	//
	//
	//         directionsDisplay.setMap(this.map);
	//         directionsDisplay.setPanel(document.getElementById(route.containerId));
	//
	//         let start = document.getElementById(route.fieldInputId).value;
	//         let request = {
	//             origin: start,
	//             destination: lat + ',' + lng,
	//             travelMode: google.maps.DirectionsTravelMode[drivingMode]
	//         };
	//         let thisObject = this.options;
	//         directionsService.route(request, function (response, status) {
	//             if (status == google.maps.DirectionsStatus.OK) {
	//                 directionsDisplay.setDirections(response);
	//                 /**
	//                  * Remove marker when route results are displayed
	//                  */
	//                 if (route.removeMarkerOnResult !== undefined && route.removeMarkerOnResult !== false) {
	//                     self.options.marker.setMap(null)
	//                 }
	//                 else {
	//                     directionsDisplay.setOptions({suppressMarkers: true});
	//                 }
	//             } else {
	//                 if (status == 'ZERO_RESULTS') {
	//                     alert(thisObject.getTranslation("ZERO_RESULTS"));
	//                 } else if (status == 'UNKNOWN_ERROR') {
	//                     alert(thisObject.getTranslation("UNKNOWN_ERROR"));
	//                 } else if (status == 'REQUEST_DENIED') {
	//                     alert(thisObject.getTranslation("REQUEST_DENIED"));
	//                 } else if (status == 'OVER_QUERY_LIMIT') {
	//                     alert(thisObject.getTranslation("OVER_QUERY_LIMIT"));
	//                 } else if (status == 'NOT_FOUND') {
	//                     alert(thisObject.getTranslation("NOT_FOUND"));
	//                 } else if (status == 'INVALID_REQUEST') {
	//                     alert(thisObject.getTranslation("INVALID_REQUEST"));
	//                 } else {
	//                     alert(thisObject.getTranslation("REQUESTSTATUS") + " \n\n" + status);
	//                 }
	//             }
	//         });
	//     }
	//
	//     calcMultipleRoute() {
	//         let mapConfig = this.mapConfig,
	//             options = mapConfig.options,
	//             marker = mapConfig.marker,
	//             route = mapConfig.route,
	//             drivingMode = route.travelMode || 'DRIVING',
	//             self = this,
	//             services = this.options.services,
	//             directionsDisplay = services.directionsDisplay,
	//             directionsService = services.directionsService;
	//         debugger;
	//         /**
	//          * Get current selected option
	//          */
	//         let selectField = document.querySelector(options.showMarkerInSelectField).getElementsByClassName('selectField')[0],
	//             currIndex = selectField.selectedIndex,
	//             currValue = selectField.options[currIndex].value;
	//
	//         let lat = marker[currValue][1],
	//             lng = marker[currValue][2];
	//
	//         directionsDisplay.setMap(this.map);
	//         directionsDisplay.setPanel(document.getElementById(route.containerId));
	//
	//         let start = document.getElementById(route.fieldInputId).value;
	//         let request = {
	//             origin: start,
	//             destination: lat + ',' + lng,
	//             travelMode: google.maps.DirectionsTravelMode[drivingMode]
	//         };
	//         let thisObject = this.options;
	//         directionsService.route(request, function (response, status) {
	//             if (status == google.maps.DirectionsStatus.OK) {
	//                 directionsDisplay.setDirections(response);
	//                 /**
	//                  * Remove marker when route results are displayed
	//                  */
	//                 if (route.removeMarkerOnResult !== undefined && route.removeMarkerOnResult !== false) {
	//                     for (let i = 0; i < self.options.markers.length; i++) {
	//                         self.options.markers[i].setMap(null);
	//                     }
	//                     self.options.markers = new Array();
	//                 }
	//                 else {
	//                     directionsDisplay.setOptions({suppressMarkers: true});
	//                 }
	//             } else {
	//                 if (status == 'ZERO_RESULTS') {
	//                     alert(thisObject.getTranslation("ZERO_RESULTS"));
	//                 } else if (status == 'UNKNOWN_ERROR') {
	//                     alert(thisObject.getTranslation("UNKNOWN_ERROR"));
	//                 } else if (status == 'REQUEST_DENIED') {
	//                     alert(thisObject.getTranslation("REQUEST_DENIED"));
	//                 } else if (status == 'OVER_QUERY_LIMIT') {
	//                     alert(thisObject.getTranslation("OVER_QUERY_LIMIT"));
	//                 } else if (status == 'NOT_FOUND') {
	//                     alert(thisObject.getTranslation("NOT_FOUND"));
	//                 } else if (status == 'INVALID_REQUEST') {
	//                     alert(thisObject.getTranslation("INVALID_REQUEST"));
	//                 } else {
	//                     alert(thisObject.getTranslation("REQUESTSTATUS") + " \n\n" + status);
	//                 }
	//             }
	//         });
	//         debugger;
	//     }
	// }


	if (true) module.exports = BnMaps;

	/***/
},
/* 1 */
/***/function (module, exports, __webpack_require__) {

	var require; /* WEBPACK VAR INJECTION */(function (process, global) {
		/*!
  * @overview es6-promise - a tiny implementation of Promises/A+.
  * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
  * @license   Licensed under MIT license
  *            See https://raw.githubusercontent.com/stefanpenner/es6-promise/master/LICENSE
  * @version   4.1.1
  */

		(function (global, factory) {
			true ? module.exports = factory() : typeof define === 'function' && define.amd ? define(factory) : global.ES6Promise = factory();
		})(this, function () {
			'use strict';

			function objectOrFunction(x) {
				var type = typeof x === 'undefined' ? 'undefined' : _typeof(x);
				return x !== null && (type === 'object' || type === 'function');
			}

			function isFunction(x) {
				return typeof x === 'function';
			}

			var _isArray = undefined;
			if (Array.isArray) {
				_isArray = Array.isArray;
			} else {
				_isArray = function _isArray(x) {
					return Object.prototype.toString.call(x) === '[object Array]';
				};
			}

			var isArray = _isArray;

			var len = 0;
			var vertxNext = undefined;
			var customSchedulerFn = undefined;

			var asap = function asap(callback, arg) {
				queue[len] = callback;
				queue[len + 1] = arg;
				len += 2;
				if (len === 2) {
					// If len is 2, that means that we need to schedule an async flush.
					// If additional callbacks are queued before the queue is flushed, they
					// will be processed by this flush that we are scheduling.
					if (customSchedulerFn) {
						customSchedulerFn(flush);
					} else {
						scheduleFlush();
					}
				}
			};

			function setScheduler(scheduleFn) {
				customSchedulerFn = scheduleFn;
			}

			function setAsap(asapFn) {
				asap = asapFn;
			}

			var browserWindow = typeof window !== 'undefined' ? window : undefined;
			var browserGlobal = browserWindow || {};
			var BrowserMutationObserver = browserGlobal.MutationObserver || browserGlobal.WebKitMutationObserver;
			var isNode = typeof self === 'undefined' && typeof process !== 'undefined' && {}.toString.call(process) === '[object process]';

			// test for web worker but not in IE10
			var isWorker = typeof Uint8ClampedArray !== 'undefined' && typeof importScripts !== 'undefined' && typeof MessageChannel !== 'undefined';

			// node
			function useNextTick() {
				// node version 0.10.x displays a deprecation warning when nextTick is used recursively
				// see https://github.com/cujojs/when/issues/410 for details
				return function () {
					return process.nextTick(flush);
				};
			}

			// vertx
			function useVertxTimer() {
				if (typeof vertxNext !== 'undefined') {
					return function () {
						vertxNext(flush);
					};
				}

				return useSetTimeout();
			}

			function useMutationObserver() {
				var iterations = 0;
				var observer = new BrowserMutationObserver(flush);
				var node = document.createTextNode('');
				observer.observe(node, { characterData: true });

				return function () {
					node.data = iterations = ++iterations % 2;
				};
			}

			// web worker
			function useMessageChannel() {
				var channel = new MessageChannel();
				channel.port1.onmessage = flush;
				return function () {
					return channel.port2.postMessage(0);
				};
			}

			function useSetTimeout() {
				// Store setTimeout reference so es6-promise will be unaffected by
				// other code modifying setTimeout (like sinon.useFakeTimers())
				var globalSetTimeout = setTimeout;
				return function () {
					return globalSetTimeout(flush, 1);
				};
			}

			var queue = new Array(1000);
			function flush() {
				for (var i = 0; i < len; i += 2) {
					var callback = queue[i];
					var arg = queue[i + 1];

					callback(arg);

					queue[i] = undefined;
					queue[i + 1] = undefined;
				}

				len = 0;
			}

			function attemptVertx() {
				try {
					var r = require;
					var vertx = __webpack_require__(3);
					vertxNext = vertx.runOnLoop || vertx.runOnContext;
					return useVertxTimer();
				} catch (e) {
					return useSetTimeout();
				}
			}

			var scheduleFlush = undefined;
			// Decide what async method to use to triggering processing of queued callbacks:
			if (isNode) {
				scheduleFlush = useNextTick();
			} else if (BrowserMutationObserver) {
				scheduleFlush = useMutationObserver();
			} else if (isWorker) {
				scheduleFlush = useMessageChannel();
			} else if (browserWindow === undefined && "function" === 'function') {
				scheduleFlush = attemptVertx();
			} else {
				scheduleFlush = useSetTimeout();
			}

			function then(onFulfillment, onRejection) {
				var _arguments = arguments;

				var parent = this;

				var child = new this.constructor(noop);

				if (child[PROMISE_ID] === undefined) {
					makePromise(child);
				}

				var _state = parent._state;

				if (_state) {
					(function () {
						var callback = _arguments[_state - 1];
						asap(function () {
							return invokeCallback(_state, child, callback, parent._result);
						});
					})();
				} else {
					subscribe(parent, child, onFulfillment, onRejection);
				}

				return child;
			}

			/**
     `Promise.resolve` returns a promise that will become resolved with the
     passed `value`. It is shorthand for the following:
   	  ```javascript
     let promise = new Promise(function(resolve, reject){
       resolve(1);
     });
   	  promise.then(function(value){
       // value === 1
     });
     ```
   	  Instead of writing the above, your code now simply becomes the following:
   	  ```javascript
     let promise = Promise.resolve(1);
   	  promise.then(function(value){
       // value === 1
     });
     ```
   	  @method resolve
     @static
     @param {Any} value value that the returned promise will be resolved with
     Useful for tooling.
     @return {Promise} a promise that will become fulfilled with the given
     `value`
   */
			function resolve$1(object) {
				/*jshint validthis:true */
				var Constructor = this;

				if (object && (typeof object === 'undefined' ? 'undefined' : _typeof(object)) === 'object' && object.constructor === Constructor) {
					return object;
				}

				var promise = new Constructor(noop);
				resolve(promise, object);
				return promise;
			}

			var PROMISE_ID = Math.random().toString(36).substring(16);

			function noop() {}

			var PENDING = void 0;
			var FULFILLED = 1;
			var REJECTED = 2;

			var GET_THEN_ERROR = new ErrorObject();

			function selfFulfillment() {
				return new TypeError("You cannot resolve a promise with itself");
			}

			function cannotReturnOwn() {
				return new TypeError('A promises callback cannot return that same promise.');
			}

			function getThen(promise) {
				try {
					return promise.then;
				} catch (error) {
					GET_THEN_ERROR.error = error;
					return GET_THEN_ERROR;
				}
			}

			function tryThen(then$$1, value, fulfillmentHandler, rejectionHandler) {
				try {
					then$$1.call(value, fulfillmentHandler, rejectionHandler);
				} catch (e) {
					return e;
				}
			}

			function handleForeignThenable(promise, thenable, then$$1) {
				asap(function (promise) {
					var sealed = false;
					var error = tryThen(then$$1, thenable, function (value) {
						if (sealed) {
							return;
						}
						sealed = true;
						if (thenable !== value) {
							resolve(promise, value);
						} else {
							fulfill(promise, value);
						}
					}, function (reason) {
						if (sealed) {
							return;
						}
						sealed = true;

						reject(promise, reason);
					}, 'Settle: ' + (promise._label || ' unknown promise'));

					if (!sealed && error) {
						sealed = true;
						reject(promise, error);
					}
				}, promise);
			}

			function handleOwnThenable(promise, thenable) {
				if (thenable._state === FULFILLED) {
					fulfill(promise, thenable._result);
				} else if (thenable._state === REJECTED) {
					reject(promise, thenable._result);
				} else {
					subscribe(thenable, undefined, function (value) {
						return resolve(promise, value);
					}, function (reason) {
						return reject(promise, reason);
					});
				}
			}

			function handleMaybeThenable(promise, maybeThenable, then$$1) {
				if (maybeThenable.constructor === promise.constructor && then$$1 === then && maybeThenable.constructor.resolve === resolve$1) {
					handleOwnThenable(promise, maybeThenable);
				} else {
					if (then$$1 === GET_THEN_ERROR) {
						reject(promise, GET_THEN_ERROR.error);
						GET_THEN_ERROR.error = null;
					} else if (then$$1 === undefined) {
						fulfill(promise, maybeThenable);
					} else if (isFunction(then$$1)) {
						handleForeignThenable(promise, maybeThenable, then$$1);
					} else {
						fulfill(promise, maybeThenable);
					}
				}
			}

			function resolve(promise, value) {
				if (promise === value) {
					reject(promise, selfFulfillment());
				} else if (objectOrFunction(value)) {
					handleMaybeThenable(promise, value, getThen(value));
				} else {
					fulfill(promise, value);
				}
			}

			function publishRejection(promise) {
				if (promise._onerror) {
					promise._onerror(promise._result);
				}

				publish(promise);
			}

			function fulfill(promise, value) {
				if (promise._state !== PENDING) {
					return;
				}

				promise._result = value;
				promise._state = FULFILLED;

				if (promise._subscribers.length !== 0) {
					asap(publish, promise);
				}
			}

			function reject(promise, reason) {
				if (promise._state !== PENDING) {
					return;
				}
				promise._state = REJECTED;
				promise._result = reason;

				asap(publishRejection, promise);
			}

			function subscribe(parent, child, onFulfillment, onRejection) {
				var _subscribers = parent._subscribers;
				var length = _subscribers.length;

				parent._onerror = null;

				_subscribers[length] = child;
				_subscribers[length + FULFILLED] = onFulfillment;
				_subscribers[length + REJECTED] = onRejection;

				if (length === 0 && parent._state) {
					asap(publish, parent);
				}
			}

			function publish(promise) {
				var subscribers = promise._subscribers;
				var settled = promise._state;

				if (subscribers.length === 0) {
					return;
				}

				var child = undefined,
				    callback = undefined,
				    detail = promise._result;

				for (var i = 0; i < subscribers.length; i += 3) {
					child = subscribers[i];
					callback = subscribers[i + settled];

					if (child) {
						invokeCallback(settled, child, callback, detail);
					} else {
						callback(detail);
					}
				}

				promise._subscribers.length = 0;
			}

			function ErrorObject() {
				this.error = null;
			}

			var TRY_CATCH_ERROR = new ErrorObject();

			function tryCatch(callback, detail) {
				try {
					return callback(detail);
				} catch (e) {
					TRY_CATCH_ERROR.error = e;
					return TRY_CATCH_ERROR;
				}
			}

			function invokeCallback(settled, promise, callback, detail) {
				var hasCallback = isFunction(callback),
				    value = undefined,
				    error = undefined,
				    succeeded = undefined,
				    failed = undefined;

				if (hasCallback) {
					value = tryCatch(callback, detail);

					if (value === TRY_CATCH_ERROR) {
						failed = true;
						error = value.error;
						value.error = null;
					} else {
						succeeded = true;
					}

					if (promise === value) {
						reject(promise, cannotReturnOwn());
						return;
					}
				} else {
					value = detail;
					succeeded = true;
				}

				if (promise._state !== PENDING) {
					// noop
				} else if (hasCallback && succeeded) {
					resolve(promise, value);
				} else if (failed) {
					reject(promise, error);
				} else if (settled === FULFILLED) {
					fulfill(promise, value);
				} else if (settled === REJECTED) {
					reject(promise, value);
				}
			}

			function initializePromise(promise, resolver) {
				try {
					resolver(function resolvePromise(value) {
						resolve(promise, value);
					}, function rejectPromise(reason) {
						reject(promise, reason);
					});
				} catch (e) {
					reject(promise, e);
				}
			}

			var id = 0;
			function nextId() {
				return id++;
			}

			function makePromise(promise) {
				promise[PROMISE_ID] = id++;
				promise._state = undefined;
				promise._result = undefined;
				promise._subscribers = [];
			}

			function Enumerator$1(Constructor, input) {
				this._instanceConstructor = Constructor;
				this.promise = new Constructor(noop);

				if (!this.promise[PROMISE_ID]) {
					makePromise(this.promise);
				}

				if (isArray(input)) {
					this.length = input.length;
					this._remaining = input.length;

					this._result = new Array(this.length);

					if (this.length === 0) {
						fulfill(this.promise, this._result);
					} else {
						this.length = this.length || 0;
						this._enumerate(input);
						if (this._remaining === 0) {
							fulfill(this.promise, this._result);
						}
					}
				} else {
					reject(this.promise, validationError());
				}
			}

			function validationError() {
				return new Error('Array Methods must be provided an Array');
			}

			Enumerator$1.prototype._enumerate = function (input) {
				for (var i = 0; this._state === PENDING && i < input.length; i++) {
					this._eachEntry(input[i], i);
				}
			};

			Enumerator$1.prototype._eachEntry = function (entry, i) {
				var c = this._instanceConstructor;
				var resolve$$1 = c.resolve;

				if (resolve$$1 === resolve$1) {
					var _then = getThen(entry);

					if (_then === then && entry._state !== PENDING) {
						this._settledAt(entry._state, i, entry._result);
					} else if (typeof _then !== 'function') {
						this._remaining--;
						this._result[i] = entry;
					} else if (c === Promise$2) {
						var promise = new c(noop);
						handleMaybeThenable(promise, entry, _then);
						this._willSettleAt(promise, i);
					} else {
						this._willSettleAt(new c(function (resolve$$1) {
							return resolve$$1(entry);
						}), i);
					}
				} else {
					this._willSettleAt(resolve$$1(entry), i);
				}
			};

			Enumerator$1.prototype._settledAt = function (state, i, value) {
				var promise = this.promise;

				if (promise._state === PENDING) {
					this._remaining--;

					if (state === REJECTED) {
						reject(promise, value);
					} else {
						this._result[i] = value;
					}
				}

				if (this._remaining === 0) {
					fulfill(promise, this._result);
				}
			};

			Enumerator$1.prototype._willSettleAt = function (promise, i) {
				var enumerator = this;

				subscribe(promise, undefined, function (value) {
					return enumerator._settledAt(FULFILLED, i, value);
				}, function (reason) {
					return enumerator._settledAt(REJECTED, i, reason);
				});
			};

			/**
     `Promise.all` accepts an array of promises, and returns a new promise which
     is fulfilled with an array of fulfillment values for the passed promises, or
     rejected with the reason of the first passed promise to be rejected. It casts all
     elements of the passed iterable to promises as it runs this algorithm.
   	  Example:
   	  ```javascript
     let promise1 = resolve(1);
     let promise2 = resolve(2);
     let promise3 = resolve(3);
     let promises = [ promise1, promise2, promise3 ];
   	  Promise.all(promises).then(function(array){
       // The array here would be [ 1, 2, 3 ];
     });
     ```
   	  If any of the `promises` given to `all` are rejected, the first promise
     that is rejected will be given as an argument to the returned promises's
     rejection handler. For example:
   	  Example:
   	  ```javascript
     let promise1 = resolve(1);
     let promise2 = reject(new Error("2"));
     let promise3 = reject(new Error("3"));
     let promises = [ promise1, promise2, promise3 ];
   	  Promise.all(promises).then(function(array){
       // Code here never runs because there are rejected promises!
     }, function(error) {
       // error.message === "2"
     });
     ```
   	  @method all
     @static
     @param {Array} entries array of promises
     @param {String} label optional string for labeling the promise.
     Useful for tooling.
     @return {Promise} promise that is fulfilled when all `promises` have been
     fulfilled, or rejected if any of them become rejected.
     @static
   */
			function all$1(entries) {
				return new Enumerator$1(this, entries).promise;
			}

			/**
     `Promise.race` returns a new promise which is settled in the same way as the
     first passed promise to settle.
   	  Example:
   	  ```javascript
     let promise1 = new Promise(function(resolve, reject){
       setTimeout(function(){
         resolve('promise 1');
       }, 200);
     });
   	  let promise2 = new Promise(function(resolve, reject){
       setTimeout(function(){
         resolve('promise 2');
       }, 100);
     });
   	  Promise.race([promise1, promise2]).then(function(result){
       // result === 'promise 2' because it was resolved before promise1
       // was resolved.
     });
     ```
   	  `Promise.race` is deterministic in that only the state of the first
     settled promise matters. For example, even if other promises given to the
     `promises` array argument are resolved, but the first settled promise has
     become rejected before the other promises became fulfilled, the returned
     promise will become rejected:
   	  ```javascript
     let promise1 = new Promise(function(resolve, reject){
       setTimeout(function(){
         resolve('promise 1');
       }, 200);
     });
   	  let promise2 = new Promise(function(resolve, reject){
       setTimeout(function(){
         reject(new Error('promise 2'));
       }, 100);
     });
   	  Promise.race([promise1, promise2]).then(function(result){
       // Code here never runs
     }, function(reason){
       // reason.message === 'promise 2' because promise 2 became rejected before
       // promise 1 became fulfilled
     });
     ```
   	  An example real-world use case is implementing timeouts:
   	  ```javascript
     Promise.race([ajax('foo.json'), timeout(5000)])
     ```
   	  @method race
     @static
     @param {Array} promises array of promises to observe
     Useful for tooling.
     @return {Promise} a promise which settles in the same way as the first passed
     promise to settle.
   */
			function race$1(entries) {
				/*jshint validthis:true */
				var Constructor = this;

				if (!isArray(entries)) {
					return new Constructor(function (_, reject) {
						return reject(new TypeError('You must pass an array to race.'));
					});
				} else {
					return new Constructor(function (resolve, reject) {
						var length = entries.length;
						for (var i = 0; i < length; i++) {
							Constructor.resolve(entries[i]).then(resolve, reject);
						}
					});
				}
			}

			/**
     `Promise.reject` returns a promise rejected with the passed `reason`.
     It is shorthand for the following:
   	  ```javascript
     let promise = new Promise(function(resolve, reject){
       reject(new Error('WHOOPS'));
     });
   	  promise.then(function(value){
       // Code here doesn't run because the promise is rejected!
     }, function(reason){
       // reason.message === 'WHOOPS'
     });
     ```
   	  Instead of writing the above, your code now simply becomes the following:
   	  ```javascript
     let promise = Promise.reject(new Error('WHOOPS'));
   	  promise.then(function(value){
       // Code here doesn't run because the promise is rejected!
     }, function(reason){
       // reason.message === 'WHOOPS'
     });
     ```
   	  @method reject
     @static
     @param {Any} reason value that the returned promise will be rejected with.
     Useful for tooling.
     @return {Promise} a promise rejected with the given `reason`.
   */
			function reject$1(reason) {
				/*jshint validthis:true */
				var Constructor = this;
				var promise = new Constructor(noop);
				reject(promise, reason);
				return promise;
			}

			function needsResolver() {
				throw new TypeError('You must pass a resolver function as the first argument to the promise constructor');
			}

			function needsNew() {
				throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
			}

			/**
     Promise objects represent the eventual result of an asynchronous operation. The
     primary way of interacting with a promise is through its `then` method, which
     registers callbacks to receive either a promise's eventual value or the reason
     why the promise cannot be fulfilled.
   	  Terminology
     -----------
   	  - `promise` is an object or function with a `then` method whose behavior conforms to this specification.
     - `thenable` is an object or function that defines a `then` method.
     - `value` is any legal JavaScript value (including undefined, a thenable, or a promise).
     - `exception` is a value that is thrown using the throw statement.
     - `reason` is a value that indicates why a promise was rejected.
     - `settled` the final resting state of a promise, fulfilled or rejected.
   	  A promise can be in one of three states: pending, fulfilled, or rejected.
   	  Promises that are fulfilled have a fulfillment value and are in the fulfilled
     state.  Promises that are rejected have a rejection reason and are in the
     rejected state.  A fulfillment value is never a thenable.
   	  Promises can also be said to *resolve* a value.  If this value is also a
     promise, then the original promise's settled state will match the value's
     settled state.  So a promise that *resolves* a promise that rejects will
     itself reject, and a promise that *resolves* a promise that fulfills will
     itself fulfill.
   
     Basic Usage:
     ------------
   	  ```js
     let promise = new Promise(function(resolve, reject) {
       // on success
       resolve(value);
   	    // on failure
       reject(reason);
     });
   	  promise.then(function(value) {
       // on fulfillment
     }, function(reason) {
       // on rejection
     });
     ```
   	  Advanced Usage:
     ---------------
   	  Promises shine when abstracting away asynchronous interactions such as
     `XMLHttpRequest`s.
   	  ```js
     function getJSON(url) {
       return new Promise(function(resolve, reject){
         let xhr = new XMLHttpRequest();
   	      xhr.open('GET', url);
         xhr.onreadystatechange = handler;
         xhr.responseType = 'json';
         xhr.setRequestHeader('Accept', 'application/json');
         xhr.send();
   	      function handler() {
           if (this.readyState === this.DONE) {
             if (this.status === 200) {
               resolve(this.response);
             } else {
               reject(new Error('getJSON: `' + url + '` failed with status: [' + this.status + ']'));
             }
           }
         };
       });
     }
   	  getJSON('/posts.json').then(function(json) {
       // on fulfillment
     }, function(reason) {
       // on rejection
     });
     ```
   	  Unlike callbacks, promises are great composable primitives.
   	  ```js
     Promise.all([
       getJSON('/posts'),
       getJSON('/comments')
     ]).then(function(values){
       values[0] // => postsJSON
       values[1] // => commentsJSON
   	    return values;
     });
     ```
   	  @class Promise
     @param {function} resolver
     Useful for tooling.
     @constructor
   */
			function Promise$2(resolver) {
				this[PROMISE_ID] = nextId();
				this._result = this._state = undefined;
				this._subscribers = [];

				if (noop !== resolver) {
					typeof resolver !== 'function' && needsResolver();
					this instanceof Promise$2 ? initializePromise(this, resolver) : needsNew();
				}
			}

			Promise$2.all = all$1;
			Promise$2.race = race$1;
			Promise$2.resolve = resolve$1;
			Promise$2.reject = reject$1;
			Promise$2._setScheduler = setScheduler;
			Promise$2._setAsap = setAsap;
			Promise$2._asap = asap;

			Promise$2.prototype = {
				constructor: Promise$2,

				/**
      The primary way of interacting with a promise is through its `then` method,
      which registers callbacks to receive either a promise's eventual value or the
      reason why the promise cannot be fulfilled.
    
      ```js
      findUser().then(function(user){
        // user is available
      }, function(reason){
        // user is unavailable, and you are given the reason why
      });
      ```
    
      Chaining
      --------
    
      The return value of `then` is itself a promise.  This second, 'downstream'
      promise is resolved with the return value of the first promise's fulfillment
      or rejection handler, or rejected if the handler throws an exception.
    
      ```js
      findUser().then(function (user) {
        return user.name;
      }, function (reason) {
        return 'default name';
      }).then(function (userName) {
        // If `findUser` fulfilled, `userName` will be the user's name, otherwise it
        // will be `'default name'`
      });
    
      findUser().then(function (user) {
        throw new Error('Found user, but still unhappy');
      }, function (reason) {
        throw new Error('`findUser` rejected and we're unhappy');
      }).then(function (value) {
        // never reached
      }, function (reason) {
        // if `findUser` fulfilled, `reason` will be 'Found user, but still unhappy'.
        // If `findUser` rejected, `reason` will be '`findUser` rejected and we're unhappy'.
      });
      ```
      If the downstream promise does not specify a rejection handler, rejection reasons will be propagated further downstream.
    
      ```js
      findUser().then(function (user) {
        throw new PedagogicalException('Upstream error');
      }).then(function (value) {
        // never reached
      }).then(function (value) {
        // never reached
      }, function (reason) {
        // The `PedgagocialException` is propagated all the way down to here
      });
      ```
    
      Assimilation
      ------------
    
      Sometimes the value you want to propagate to a downstream promise can only be
      retrieved asynchronously. This can be achieved by returning a promise in the
      fulfillment or rejection handler. The downstream promise will then be pending
      until the returned promise is settled. This is called *assimilation*.
    
      ```js
      findUser().then(function (user) {
        return findCommentsByAuthor(user);
      }).then(function (comments) {
        // The user's comments are now available
      });
      ```
    
      If the assimliated promise rejects, then the downstream promise will also reject.
    
      ```js
      findUser().then(function (user) {
        return findCommentsByAuthor(user);
      }).then(function (comments) {
        // If `findCommentsByAuthor` fulfills, we'll have the value here
      }, function (reason) {
        // If `findCommentsByAuthor` rejects, we'll have the reason here
      });
      ```
    
      Simple Example
      --------------
    
      Synchronous Example
    
      ```javascript
      let result;
    
      try {
        result = findResult();
        // success
      } catch(reason) {
        // failure
      }
      ```
    
      Errback Example
    
      ```js
      findResult(function(result, err){
        if (err) {
          // failure
        } else {
          // success
        }
      });
      ```
    
      Promise Example;
    
      ```javascript
      findResult().then(function(result){
        // success
      }, function(reason){
        // failure
      });
      ```
    
      Advanced Example
      --------------
    
      Synchronous Example
    
      ```javascript
      let author, books;
    
      try {
        author = findAuthor();
        books  = findBooksByAuthor(author);
        // success
      } catch(reason) {
        // failure
      }
      ```
    
      Errback Example
    
      ```js
    
      function foundBooks(books) {
    
      }
    
      function failure(reason) {
    
      }
    
      findAuthor(function(author, err){
        if (err) {
          failure(err);
          // failure
        } else {
          try {
            findBoooksByAuthor(author, function(books, err) {
              if (err) {
                failure(err);
              } else {
                try {
                  foundBooks(books);
                } catch(reason) {
                  failure(reason);
                }
              }
            });
          } catch(error) {
            failure(err);
          }
          // success
        }
      });
      ```
    
      Promise Example;
    
      ```javascript
      findAuthor().
        then(findBooksByAuthor).
        then(function(books){
          // found books
      }).catch(function(reason){
        // something went wrong
      });
      ```
    
      @method then
      @param {Function} onFulfilled
      @param {Function} onRejected
      Useful for tooling.
      @return {Promise}
    */
				then: then,

				/**
      `catch` is simply sugar for `then(undefined, onRejection)` which makes it the same
      as the catch block of a try/catch statement.
    
      ```js
      function findAuthor(){
        throw new Error('couldn't find that author');
      }
    
      // synchronous
      try {
        findAuthor();
      } catch(reason) {
        // something went wrong
      }
    
      // async with promises
      findAuthor().catch(function(reason){
        // something went wrong
      });
      ```
    
      @method catch
      @param {Function} onRejection
      Useful for tooling.
      @return {Promise}
    */
				'catch': function _catch(onRejection) {
					return this.then(null, onRejection);
				}
			};

			/*global self*/
			function polyfill$1() {
				var local = undefined;

				if (typeof global !== 'undefined') {
					local = global;
				} else if (typeof self !== 'undefined') {
					local = self;
				} else {
					try {
						local = Function('return this')();
					} catch (e) {
						throw new Error('polyfill failed because global object is unavailable in this environment');
					}
				}

				var P = local.Promise;

				if (P) {
					var promiseToString = null;
					try {
						promiseToString = Object.prototype.toString.call(P.resolve());
					} catch (e) {
						// silently ignored
					}

					if (promiseToString === '[object Promise]' && !P.cast) {
						return;
					}
				}

				local.Promise = Promise$2;
			}

			// Strange compat..
			Promise$2.polyfill = polyfill$1;
			Promise$2.Promise = Promise$2;

			return Promise$2;
		});

		//# sourceMappingURL=es6-promise.map

		/* WEBPACK VAR INJECTION */
	}).call(exports, __webpack_require__(2), function () {
		return this;
	}());

	/***/
},
/* 2 */
/***/function (module, exports) {

	// shim for using process in browser
	var process = module.exports = {};

	// cached from whatever global is present so that test runners that stub it
	// don't break things.  But we need to wrap it in a try catch in case it is
	// wrapped in strict mode code which doesn't define any globals.  It's inside a
	// function because try/catches deoptimize in certain engines.

	var cachedSetTimeout;
	var cachedClearTimeout;

	function defaultSetTimout() {
		throw new Error('setTimeout has not been defined');
	}
	function defaultClearTimeout() {
		throw new Error('clearTimeout has not been defined');
	}
	(function () {
		try {
			if (typeof setTimeout === 'function') {
				cachedSetTimeout = setTimeout;
			} else {
				cachedSetTimeout = defaultSetTimout;
			}
		} catch (e) {
			cachedSetTimeout = defaultSetTimout;
		}
		try {
			if (typeof clearTimeout === 'function') {
				cachedClearTimeout = clearTimeout;
			} else {
				cachedClearTimeout = defaultClearTimeout;
			}
		} catch (e) {
			cachedClearTimeout = defaultClearTimeout;
		}
	})();
	function runTimeout(fun) {
		if (cachedSetTimeout === setTimeout) {
			//normal enviroments in sane situations
			return setTimeout(fun, 0);
		}
		// if setTimeout wasn't available but was latter defined
		if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
			cachedSetTimeout = setTimeout;
			return setTimeout(fun, 0);
		}
		try {
			// when when somebody has screwed with setTimeout but no I.E. maddness
			return cachedSetTimeout(fun, 0);
		} catch (e) {
			try {
				// When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
				return cachedSetTimeout.call(null, fun, 0);
			} catch (e) {
				// same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
				return cachedSetTimeout.call(this, fun, 0);
			}
		}
	}
	function runClearTimeout(marker) {
		if (cachedClearTimeout === clearTimeout) {
			//normal enviroments in sane situations
			return clearTimeout(marker);
		}
		// if clearTimeout wasn't available but was latter defined
		if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
			cachedClearTimeout = clearTimeout;
			return clearTimeout(marker);
		}
		try {
			// when when somebody has screwed with setTimeout but no I.E. maddness
			return cachedClearTimeout(marker);
		} catch (e) {
			try {
				// When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
				return cachedClearTimeout.call(null, marker);
			} catch (e) {
				// same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
				// Some versions of I.E. have different rules for clearTimeout vs setTimeout
				return cachedClearTimeout.call(this, marker);
			}
		}
	}
	var queue = [];
	var draining = false;
	var currentQueue;
	var queueIndex = -1;

	function cleanUpNextTick() {
		if (!draining || !currentQueue) {
			return;
		}
		draining = false;
		if (currentQueue.length) {
			queue = currentQueue.concat(queue);
		} else {
			queueIndex = -1;
		}
		if (queue.length) {
			drainQueue();
		}
	}

	function drainQueue() {
		if (draining) {
			return;
		}
		var timeout = runTimeout(cleanUpNextTick);
		draining = true;

		var len = queue.length;
		while (len) {
			currentQueue = queue;
			queue = [];
			while (++queueIndex < len) {
				if (currentQueue) {
					currentQueue[queueIndex].run();
				}
			}
			queueIndex = -1;
			len = queue.length;
		}
		currentQueue = null;
		draining = false;
		runClearTimeout(timeout);
	}

	process.nextTick = function (fun) {
		var args = new Array(arguments.length - 1);
		if (arguments.length > 1) {
			for (var i = 1; i < arguments.length; i++) {
				args[i - 1] = arguments[i];
			}
		}
		queue.push(new Item(fun, args));
		if (queue.length === 1 && !draining) {
			runTimeout(drainQueue);
		}
	};

	// v8 likes predictible objects
	function Item(fun, array) {
		this.fun = fun;
		this.array = array;
	}
	Item.prototype.run = function () {
		this.fun.apply(null, this.array);
	};
	process.title = 'browser';
	process.browser = true;
	process.env = {};
	process.argv = [];
	process.version = ''; // empty string to avoid regexp issues
	process.versions = {};

	function noop() {}

	process.on = noop;
	process.addListener = noop;
	process.once = noop;
	process.off = noop;
	process.removeListener = noop;
	process.removeAllListeners = noop;
	process.emit = noop;
	process.prependListener = noop;
	process.prependOnceListener = noop;

	process.listeners = function (name) {
		return [];
	};

	process.binding = function (name) {
		throw new Error('process.binding is not supported');
	};

	process.cwd = function () {
		return '/';
	};
	process.chdir = function (dir) {
		throw new Error('process.chdir is not supported');
	};
	process.umask = function () {
		return 0;
	};

	/***/
},
/* 3 */
/***/function (module, exports) {

	/* (ignored) */

	/***/},
/* 4 */
/***/function (module, exports) {
	var EventListeners = function () {
		function EventListeners() {
			_classCallCheck(this, EventListeners);
		}

		_createClass(EventListeners, [{
			key: 'onMapLoaded',
			value: function onMapLoaded() {
				var _self = this;
				google.maps.event.addListenerOnce(this.map, 'idle', function () {
					document.getElementById(_self.containerMap).className += " bn-maps-initDone";
					// do something only the first time the map is loaded
				});
			}
		}, {
			key: 'disablePinterestCrawl',
			value: function disablePinterestCrawl() {
				var _self = this;
				google.maps.event.addListener(this.map, 'tilesloaded', function () {
					var images = document.getElementById(_self.containerMap).querySelectorAll('img');
					images.forEach(function (el) {
						el.setAttribute('nopin', 'nopin');
					});
				});
			}
		}]);

		return EventListeners;
	}();

	module.exports = EventListeners;

	/***/
},
/* 5 */
/***/function (module, exports) {
	var RoutePlanner = function () {
		function RoutePlanner(options) {
			_classCallCheck(this, RoutePlanner);

			this.options = options;
			this.map = options.map;
			this.mapConfig = options.mapConfig;
			this.mapOptions = options.mapConfig.options;
			/**
    * Disable Marker Cluster otherwise the markers will be reinitialized on delete
    */
			if (options.markerCluster !== undefined) {
				this.options.markerCluster.clearMarkers();
			}

			if (this.mapOptions.showMarkerInSelectField === undefined) {
				/**
     Init Single RoutePlanner
     */
				this.calcSingleRoute();
			} else {
				/**
     Init Multiple RoutePlanner
     */
				this.calcMultipleRoute();
			}
		}

		_createClass(RoutePlanner, [{
			key: 'calcSingleRoute',
			value: function calcSingleRoute() {
				var mapConfig = this.mapConfig,
				    options = mapConfig.options,
				    marker = mapConfig.marker,
				    route = mapConfig.route,
				    lat = marker.lat || options.lat,
				    lng = marker.lng || options.lng,
				    drivingMode = route.travelMode || 'DRIVING',
				    self = this,
				    services = this.options.services,
				    directionsDisplay = services.directionsDisplay,
				    directionsService = services.directionsService;

				directionsDisplay.setMap(this.map);
				directionsDisplay.setPanel(document.getElementById(route.containerId));

				var start = document.getElementById(route.fieldInputId).value;
				var request = {
					origin: start,
					destination: lat + ',' + lng,
					travelMode: google.maps.DirectionsTravelMode[drivingMode]
				};
				var thisObject = this.options;
				directionsService.route(request, function (response, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(response);
						/**
       * Remove marker when route results are displayed
       */
						if (route.removeMarkerOnResult !== undefined && route.removeMarkerOnResult !== false) {
							self.options.marker.setMap(null);
						} else {
							directionsDisplay.setOptions({ suppressMarkers: true });
						}
					} else {
						if (status == 'ZERO_RESULTS') {
							alert(thisObject.getTranslation("ZERO_RESULTS"));
						} else if (status == 'UNKNOWN_ERROR') {
							alert(thisObject.getTranslation("UNKNOWN_ERROR"));
						} else if (status == 'REQUEST_DENIED') {
							alert(thisObject.getTranslation("REQUEST_DENIED"));
						} else if (status == 'OVER_QUERY_LIMIT') {
							alert(thisObject.getTranslation("OVER_QUERY_LIMIT"));
						} else if (status == 'NOT_FOUND') {
							alert(thisObject.getTranslation("NOT_FOUND"));
						} else if (status == 'INVALID_REQUEST') {
							alert(thisObject.getTranslation("INVALID_REQUEST"));
						} else {
							alert(thisObject.getTranslation("REQUESTSTATUS") + " \n\n" + status);
						}
					}
				});
			}
		}, {
			key: 'calcMultipleRoute',
			value: function calcMultipleRoute() {
				var mapConfig = this.mapConfig,
				    options = mapConfig.options,
				    marker = mapConfig.marker,
				    route = mapConfig.route,
				    drivingMode = route.travelMode || 'DRIVING',
				    self = this,
				    services = this.options.services,
				    directionsDisplay = services.directionsDisplay,
				    directionsService = services.directionsService;
				debugger;
				/**
     * Get current selected option
     */
				var selectField = document.querySelector(options.showMarkerInSelectField).getElementsByClassName('selectField')[0],
				    currIndex = selectField.selectedIndex,
				    currValue = selectField.options[currIndex].value;

				var lat = marker[currValue][1],
				    lng = marker[currValue][2];

				directionsDisplay.setMap(this.map);
				directionsDisplay.setPanel(document.getElementById(route.containerId));

				var start = document.getElementById(route.fieldInputId).value;
				var request = {
					origin: start,
					destination: lat + ',' + lng,
					travelMode: google.maps.DirectionsTravelMode[drivingMode]
				};
				var thisObject = this.options;
				directionsService.route(request, function (response, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						directionsDisplay.setDirections(response);
						/**
       * Remove marker when route results are displayed
       */
						if (route.removeMarkerOnResult !== undefined && route.removeMarkerOnResult !== false) {
							for (var i = 0; i < self.options.markers.length; i++) {
								self.options.markers[i].setMap(null);
							}
							self.options.markers = new Array();
						} else {
							directionsDisplay.setOptions({ suppressMarkers: true });
						}
					} else {
						if (status == 'ZERO_RESULTS') {
							alert(thisObject.getTranslation("ZERO_RESULTS"));
						} else if (status == 'UNKNOWN_ERROR') {
							alert(thisObject.getTranslation("UNKNOWN_ERROR"));
						} else if (status == 'REQUEST_DENIED') {
							alert(thisObject.getTranslation("REQUEST_DENIED"));
						} else if (status == 'OVER_QUERY_LIMIT') {
							alert(thisObject.getTranslation("OVER_QUERY_LIMIT"));
						} else if (status == 'NOT_FOUND') {
							alert(thisObject.getTranslation("NOT_FOUND"));
						} else if (status == 'INVALID_REQUEST') {
							alert(thisObject.getTranslation("INVALID_REQUEST"));
						} else {
							alert(thisObject.getTranslation("REQUESTSTATUS") + " \n\n" + status);
						}
					}
				});
				debugger;
			}
		}]);

		return RoutePlanner;
	}();

	module.exports = RoutePlanner;

	/***/
},
/* 6 */
/***/function (module, exports) {
	var StreetView = function () {
		function StreetView(options) {
			_classCallCheck(this, StreetView);

			this.options = options;
			this.map = options.map;
			this.mapConfig = options.mapConfig;
			this.containerMap = options.containerMap;
			this.render();
		}

		_createClass(StreetView, [{
			key: 'render',
			value: function render() {
				var mapConfig = this.mapConfig,
				    streetView = mapConfig.streetView,
				    options = mapConfig.options,
				    heading = streetView.heading || 0,
				    pitch = streetView.pitch || 0,
				    lat = streetView.lat || options.lat,
				    lng = streetView.lng || options.lng;

				var location = { lat: lat, lng: lng };

				/**
     * When the map isn´t intialized yet, make map
     */
				if (typeof this.map === "undefined") {
					var map = new google.maps.Map(document.getElementById(this.containerMap), {
						center: new google.maps.LatLng(lat, lng),
						zoom: 10
					});
					this.map = map;
				}
				var panorama = new google.maps.StreetViewPanorama(document.getElementById(this.containerMap), {
					position: location,
					pov: {
						heading: heading,
						pitch: pitch
					}
				});
				this.map.setStreetView(panorama);
			}
		}]);

		return StreetView;
	}();

	module.exports = StreetView;

	/***/
},
/* 7 */
/***/function (module, exports) {

	// ==ClosureCompiler==
	// @compilation_level ADVANCED_OPTIMIZATIONS
	// @externs_url https://raw.githubusercontent.com/google/closure-compiler/master/contrib/externs/maps/google_maps_api_v3.js
	// ==/ClosureCompiler==

	/**
  * @name MarkerClusterer for Google Maps v3
  * @version version 1.0
  * @author Luke Mahe
  * @fileoverview
  * The library creates and manages per-zoom-level clusters for large amounts of
  * markers.
  * <br/>
  * This is a v3 implementation of the
  * <a href="http://gmaps-utility-library-dev.googlecode.com/svn/tags/markerclusterer/"
  * >v2 MarkerClusterer</a>.
  */

	/**
  * @license
  * Copyright 2010 Google Inc. All Rights Reserved.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *     http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

	/**
  * A Marker Clusterer that clusters markers.
  *
  * @param {google.maps.Map} map The Google map to attach to.
  * @param {Array.<google.maps.Marker>=} opt_markers Optional markers to add to
  *   the cluster.
  * @param {Object=} opt_options support the following options:
  *     'gridSize': (number) The grid size of a cluster in pixels.
  *     'maxZoom': (number) The maximum zoom level that a marker can be part of a
  *                cluster.
  *     'zoomOnClick': (boolean) Whether the default behaviour of clicking on a
  *                    cluster is to zoom into it.
  *     'averageCenter': (boolean) Whether the center of each cluster should be
  *                      the average of all markers in the cluster.
  *     'minimumClusterSize': (number) The minimum number of markers to be in a
  *                           cluster before the markers are hidden and a count
  *                           is shown.
  *     'styles': (object) An object that has style properties:
  *       'url': (string) The image url.
  *       'height': (number) The image height.
  *       'width': (number) The image width.
  *       'anchor': (Array) The anchor position of the label text.
  *       'textColor': (string) The text color.
  *       'textSize': (number) The text size.
  *       'backgroundPosition': (string) The position of the backgound x, y.
  *       'iconAnchor': (Array) The anchor position of the icon x, y.
  * @constructor
  * @extends google.maps.OverlayView
  */
	function MarkerClusterer(map, opt_markers, opt_options) {
		// MarkerClusterer implements google.maps.OverlayView interface. We use the
		// extend function to extend MarkerClusterer with google.maps.OverlayView
		// because it might not always be available when the code is defined so we
		// look for it at the last possible moment. If it doesn't exist now then
		// there is no point going ahead :)
		this.extend(MarkerClusterer, google.maps.OverlayView);
		this.map_ = map;

		/**
   * @type {Array.<google.maps.Marker>}
   * @private
   */
		this.markers_ = [];

		/**
   *  @type {Array.<Cluster>}
   */
		this.clusters_ = [];

		this.sizes = [53, 56, 66, 78, 90];

		/**
   * @private
   */
		this.styles_ = [];

		/**
   * @type {boolean}
   * @private
   */
		this.ready_ = false;

		var options = opt_options || {};

		/**
   * @type {number}
   * @private
   */
		this.gridSize_ = options['gridSize'] || 60;

		/**
   * @private
   */
		this.minClusterSize_ = options['minimumClusterSize'] || 2;

		/**
   * @type {?number}
   * @private
   */
		this.maxZoom_ = options['maxZoom'] || null;

		this.styles_ = options['styles'] || [];

		/**
   * @type {string}
   * @private
   */
		this.imagePath_ = options['imagePath'] || this.MARKER_CLUSTER_IMAGE_PATH_;

		/**
   * @type {string}
   * @private
   */
		this.imageExtension_ = options['imageExtension'] || this.MARKER_CLUSTER_IMAGE_EXTENSION_;

		/**
   * @type {boolean}
   * @private
   */
		this.zoomOnClick_ = true;

		if (options['zoomOnClick'] != undefined) {
			this.zoomOnClick_ = options['zoomOnClick'];
		}

		/**
   * @type {boolean}
   * @private
   */
		this.averageCenter_ = false;

		if (options['averageCenter'] != undefined) {
			this.averageCenter_ = options['averageCenter'];
		}

		this.setupStyles_();

		this.setMap(map);

		/**
   * @type {number}
   * @private
   */
		this.prevZoom_ = this.map_.getZoom();

		// Add the map event listeners
		var that = this;
		google.maps.event.addListener(this.map_, 'zoom_changed', function () {
			var zoom = that.map_.getZoom();

			if (that.prevZoom_ != zoom) {
				that.prevZoom_ = zoom;
				that.resetViewport();
			}
		});

		google.maps.event.addListener(this.map_, 'idle', function () {
			that.redraw();
		});

		// Finally, add the markers
		if (opt_markers && opt_markers.length) {
			this.addMarkers(opt_markers, false);
		}
	}

	/**
  * The marker cluster image path.
  *
  * @type {string}
  * @private
  */
	MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_PATH_ = '../images/m';

	/**
  * The marker cluster image path.
  *
  * @type {string}
  * @private
  */
	MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_EXTENSION_ = 'png';

	/**
  * Extends a objects prototype by anothers.
  *
  * @param {Object} obj1 The object to be extended.
  * @param {Object} obj2 The object to extend with.
  * @return {Object} The new extended object.
  * @ignore
  */
	MarkerClusterer.prototype.extend = function (obj1, obj2) {
		return function (object) {
			for (var property in object.prototype) {
				this.prototype[property] = object.prototype[property];
			}
			return this;
		}.apply(obj1, [obj2]);
	};

	/**
  * Implementaion of the interface method.
  * @ignore
  */
	MarkerClusterer.prototype.onAdd = function () {
		this.setReady_(true);
	};

	/**
  * Implementaion of the interface method.
  * @ignore
  */
	MarkerClusterer.prototype.draw = function () {};

	/**
  * Sets up the styles object.
  *
  * @private
  */
	MarkerClusterer.prototype.setupStyles_ = function () {
		if (this.styles_.length) {
			return;
		}

		for (var i = 0, size; size = this.sizes[i]; i++) {
			this.styles_.push({
				url: this.imagePath_ + (i + 1) + '.' + this.imageExtension_,
				height: size,
				width: size
			});
		}
	};

	/**
  *  Fit the map to the bounds of the markers in the clusterer.
  */
	MarkerClusterer.prototype.fitMapToMarkers = function () {
		var markers = this.getMarkers();
		var bounds = new google.maps.LatLngBounds();
		for (var i = 0, marker; marker = markers[i]; i++) {
			bounds.extend(marker.getPosition());
		}

		this.map_.fitBounds(bounds);
	};

	/**
  *  Sets the styles.
  *
  *  @param {Object} styles The style to set.
  */
	MarkerClusterer.prototype.setStyles = function (styles) {
		this.styles_ = styles;
	};

	/**
  *  Gets the styles.
  *
  *  @return {Object} The styles object.
  */
	MarkerClusterer.prototype.getStyles = function () {
		return this.styles_;
	};

	/**
  * Whether zoom on click is set.
  *
  * @return {boolean} True if zoomOnClick_ is set.
  */
	MarkerClusterer.prototype.isZoomOnClick = function () {
		return this.zoomOnClick_;
	};

	/**
  * Whether average center is set.
  *
  * @return {boolean} True if averageCenter_ is set.
  */
	MarkerClusterer.prototype.isAverageCenter = function () {
		return this.averageCenter_;
	};

	/**
  *  Returns the array of markers in the clusterer.
  *
  *  @return {Array.<google.maps.Marker>} The markers.
  */
	MarkerClusterer.prototype.getMarkers = function () {
		return this.markers_;
	};

	/**
  *  Returns the number of markers in the clusterer
  *
  *  @return {Number} The number of markers.
  */
	MarkerClusterer.prototype.getTotalMarkers = function () {
		return this.markers_.length;
	};

	/**
  *  Sets the max zoom for the clusterer.
  *
  *  @param {number} maxZoom The max zoom level.
  */
	MarkerClusterer.prototype.setMaxZoom = function (maxZoom) {
		this.maxZoom_ = maxZoom;
	};

	/**
  *  Gets the max zoom for the clusterer.
  *
  *  @return {number} The max zoom level.
  */
	MarkerClusterer.prototype.getMaxZoom = function () {
		return this.maxZoom_;
	};

	/**
  *  The function for calculating the cluster icon image.
  *
  *  @param {Array.<google.maps.Marker>} markers The markers in the clusterer.
  *  @param {number} numStyles The number of styles available.
  *  @return {Object} A object properties: 'text' (string) and 'index' (number).
  *  @private
  */
	MarkerClusterer.prototype.calculator_ = function (markers, numStyles) {
		var index = 0;
		var count = markers.length;
		var dv = count;
		while (dv !== 0) {
			dv = parseInt(dv / 10, 10);
			index++;
		}

		index = Math.min(index, numStyles);
		return {
			text: count,
			index: index
		};
	};

	/**
  * Set the calculator function.
  *
  * @param {function(Array, number)} calculator The function to set as the
  *     calculator. The function should return a object properties:
  *     'text' (string) and 'index' (number).
  *
  */
	MarkerClusterer.prototype.setCalculator = function (calculator) {
		this.calculator_ = calculator;
	};

	/**
  * Get the calculator function.
  *
  * @return {function(Array, number)} the calculator function.
  */
	MarkerClusterer.prototype.getCalculator = function () {
		return this.calculator_;
	};

	/**
  * Add an array of markers to the clusterer.
  *
  * @param {Array.<google.maps.Marker>} markers The markers to add.
  * @param {boolean=} opt_nodraw Whether to redraw the clusters.
  */
	MarkerClusterer.prototype.addMarkers = function (markers, opt_nodraw) {
		for (var i = 0, marker; marker = markers[i]; i++) {
			this.pushMarkerTo_(marker);
		}
		if (!opt_nodraw) {
			this.redraw();
		}
	};

	/**
  * Pushes a marker to the clusterer.
  *
  * @param {google.maps.Marker} marker The marker to add.
  * @private
  */
	MarkerClusterer.prototype.pushMarkerTo_ = function (marker) {
		marker.isAdded = false;
		if (marker['draggable']) {
			// If the marker is draggable add a listener so we update the clusters on
			// the drag end.
			var that = this;
			google.maps.event.addListener(marker, 'dragend', function () {
				marker.isAdded = false;
				that.repaint();
			});
		}
		this.markers_.push(marker);
	};

	/**
  * Adds a marker to the clusterer and redraws if needed.
  *
  * @param {google.maps.Marker} marker The marker to add.
  * @param {boolean=} opt_nodraw Whether to redraw the clusters.
  */
	MarkerClusterer.prototype.addMarker = function (marker, opt_nodraw) {
		this.pushMarkerTo_(marker);
		if (!opt_nodraw) {
			this.redraw();
		}
	};

	/**
  * Removes a marker and returns true if removed, false if not
  *
  * @param {google.maps.Marker} marker The marker to remove
  * @return {boolean} Whether the marker was removed or not
  * @private
  */
	MarkerClusterer.prototype.removeMarker_ = function (marker) {
		var index = -1;
		if (this.markers_.indexOf) {
			index = this.markers_.indexOf(marker);
		} else {
			for (var i = 0, m; m = this.markers_[i]; i++) {
				if (m == marker) {
					index = i;
					break;
				}
			}
		}

		if (index == -1) {
			// Marker is not in our list of markers.
			return false;
		}

		marker.setMap(null);

		this.markers_.splice(index, 1);

		return true;
	};

	/**
  * Remove a marker from the cluster.
  *
  * @param {google.maps.Marker} marker The marker to remove.
  * @param {boolean=} opt_nodraw Optional boolean to force no redraw.
  * @return {boolean} True if the marker was removed.
  */
	MarkerClusterer.prototype.removeMarker = function (marker, opt_nodraw) {
		var removed = this.removeMarker_(marker);

		if (!opt_nodraw && removed) {
			this.resetViewport();
			this.redraw();
			return true;
		} else {
			return false;
		}
	};

	/**
  * Removes an array of markers from the cluster.
  *
  * @param {Array.<google.maps.Marker>} markers The markers to remove.
  * @param {boolean=} opt_nodraw Optional boolean to force no redraw.
  */
	MarkerClusterer.prototype.removeMarkers = function (markers, opt_nodraw) {
		var removed = false;

		for (var i = 0, marker; marker = markers[i]; i++) {
			var r = this.removeMarker_(marker);
			removed = removed || r;
		}

		if (!opt_nodraw && removed) {
			this.resetViewport();
			this.redraw();
			return true;
		}
	};

	/**
  * Sets the clusterer's ready state.
  *
  * @param {boolean} ready The state.
  * @private
  */
	MarkerClusterer.prototype.setReady_ = function (ready) {
		if (!this.ready_) {
			this.ready_ = ready;
			this.createClusters_();
		}
	};

	/**
  * Returns the number of clusters in the clusterer.
  *
  * @return {number} The number of clusters.
  */
	MarkerClusterer.prototype.getTotalClusters = function () {
		return this.clusters_.length;
	};

	/**
  * Returns the google map that the clusterer is associated with.
  *
  * @return {google.maps.Map} The map.
  */
	MarkerClusterer.prototype.getMap = function () {
		return this.map_;
	};

	/**
  * Sets the google map that the clusterer is associated with.
  *
  * @param {google.maps.Map} map The map.
  */
	MarkerClusterer.prototype.setMap = function (map) {
		this.map_ = map;
	};

	/**
  * Returns the size of the grid.
  *
  * @return {number} The grid size.
  */
	MarkerClusterer.prototype.getGridSize = function () {
		return this.gridSize_;
	};

	/**
  * Sets the size of the grid.
  *
  * @param {number} size The grid size.
  */
	MarkerClusterer.prototype.setGridSize = function (size) {
		this.gridSize_ = size;
	};

	/**
  * Returns the min cluster size.
  *
  * @return {number} The grid size.
  */
	MarkerClusterer.prototype.getMinClusterSize = function () {
		return this.minClusterSize_;
	};

	/**
  * Sets the min cluster size.
  *
  * @param {number} size The grid size.
  */
	MarkerClusterer.prototype.setMinClusterSize = function (size) {
		this.minClusterSize_ = size;
	};

	/**
  * Extends a bounds object by the grid size.
  *
  * @param {google.maps.LatLngBounds} bounds The bounds to extend.
  * @return {google.maps.LatLngBounds} The extended bounds.
  */
	MarkerClusterer.prototype.getExtendedBounds = function (bounds) {
		var projection = this.getProjection();

		// Turn the bounds into latlng.
		var tr = new google.maps.LatLng(bounds.getNorthEast().lat(), bounds.getNorthEast().lng());
		var bl = new google.maps.LatLng(bounds.getSouthWest().lat(), bounds.getSouthWest().lng());

		// Convert the points to pixels and the extend out by the grid size.
		var trPix = projection.fromLatLngToDivPixel(tr);
		trPix.x += this.gridSize_;
		trPix.y -= this.gridSize_;

		var blPix = projection.fromLatLngToDivPixel(bl);
		blPix.x -= this.gridSize_;
		blPix.y += this.gridSize_;

		// Convert the pixel points back to LatLng
		var ne = projection.fromDivPixelToLatLng(trPix);
		var sw = projection.fromDivPixelToLatLng(blPix);

		// Extend the bounds to contain the new bounds.
		bounds.extend(ne);
		bounds.extend(sw);

		return bounds;
	};

	/**
  * Determins if a marker is contained in a bounds.
  *
  * @param {google.maps.Marker} marker The marker to check.
  * @param {google.maps.LatLngBounds} bounds The bounds to check against.
  * @return {boolean} True if the marker is in the bounds.
  * @private
  */
	MarkerClusterer.prototype.isMarkerInBounds_ = function (marker, bounds) {
		return bounds.contains(marker.getPosition());
	};

	/**
  * Clears all clusters and markers from the clusterer.
  */
	MarkerClusterer.prototype.clearMarkers = function () {
		this.resetViewport(true);

		// Set the markers a empty array.
		this.markers_ = [];
	};

	/**
  * Clears all existing clusters and recreates them.
  * @param {boolean} opt_hide To also hide the marker.
  */
	MarkerClusterer.prototype.resetViewport = function (opt_hide) {
		// Remove all the clusters
		for (var i = 0, cluster; cluster = this.clusters_[i]; i++) {
			cluster.remove();
		}

		// Reset the markers to not be added and to be invisible.
		for (var i = 0, marker; marker = this.markers_[i]; i++) {
			marker.isAdded = false;
			if (opt_hide) {
				marker.setMap(null);
			}
		}

		this.clusters_ = [];
	};

	/**
  *
  */
	MarkerClusterer.prototype.repaint = function () {
		var oldClusters = this.clusters_.slice();
		this.clusters_.length = 0;
		this.resetViewport();
		this.redraw();

		// Remove the old clusters.
		// Do it in a timeout so the other clusters have been drawn first.
		window.setTimeout(function () {
			for (var i = 0, cluster; cluster = oldClusters[i]; i++) {
				cluster.remove();
			}
		}, 0);
	};

	/**
  * Redraws the clusters.
  */
	MarkerClusterer.prototype.redraw = function () {
		this.createClusters_();
	};

	/**
  * Calculates the distance between two latlng locations in km.
  * @see http://www.movable-type.co.uk/scripts/latlong.html
  *
  * @param {google.maps.LatLng} p1 The first lat lng point.
  * @param {google.maps.LatLng} p2 The second lat lng point.
  * @return {number} The distance between the two points in km.
  * @private
 */
	MarkerClusterer.prototype.distanceBetweenPoints_ = function (p1, p2) {
		if (!p1 || !p2) {
			return 0;
		}

		var R = 6371; // Radius of the Earth in km
		var dLat = (p2.lat() - p1.lat()) * Math.PI / 180;
		var dLon = (p2.lng() - p1.lng()) * Math.PI / 180;
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(p1.lat() * Math.PI / 180) * Math.cos(p2.lat() * Math.PI / 180) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c;
		return d;
	};

	/**
  * Add a marker to a cluster, or creates a new cluster.
  *
  * @param {google.maps.Marker} marker The marker to add.
  * @private
  */
	MarkerClusterer.prototype.addToClosestCluster_ = function (marker) {
		var distance = 40000; // Some large number
		var clusterToAddTo = null;
		var pos = marker.getPosition();
		for (var i = 0, cluster; cluster = this.clusters_[i]; i++) {
			var center = cluster.getCenter();
			if (center) {
				var d = this.distanceBetweenPoints_(center, marker.getPosition());
				if (d < distance) {
					distance = d;
					clusterToAddTo = cluster;
				}
			}
		}

		if (clusterToAddTo && clusterToAddTo.isMarkerInClusterBounds(marker)) {
			clusterToAddTo.addMarker(marker);
		} else {
			var cluster = new Cluster(this);
			cluster.addMarker(marker);
			this.clusters_.push(cluster);
		}
	};

	/**
  * Creates the clusters.
  *
  * @private
  */
	MarkerClusterer.prototype.createClusters_ = function () {
		if (!this.ready_) {
			return;
		}

		// Get our current map view bounds.
		// Create a new bounds object so we don't affect the map.
		var mapBounds = new google.maps.LatLngBounds(this.map_.getBounds().getSouthWest(), this.map_.getBounds().getNorthEast());
		var bounds = this.getExtendedBounds(mapBounds);

		for (var i = 0, marker; marker = this.markers_[i]; i++) {
			if (!marker.isAdded && this.isMarkerInBounds_(marker, bounds)) {
				this.addToClosestCluster_(marker);
			}
		}
	};

	/**
  * A cluster that contains markers.
  *
  * @param {MarkerClusterer} markerClusterer The markerclusterer that this
  *     cluster is associated with.
  * @constructor
  * @ignore
  */
	function Cluster(markerClusterer) {
		this.markerClusterer_ = markerClusterer;
		this.map_ = markerClusterer.getMap();
		this.gridSize_ = markerClusterer.getGridSize();
		this.minClusterSize_ = markerClusterer.getMinClusterSize();
		this.averageCenter_ = markerClusterer.isAverageCenter();
		this.center_ = null;
		this.markers_ = [];
		this.bounds_ = null;
		this.clusterIcon_ = new ClusterIcon(this, markerClusterer.getStyles(), markerClusterer.getGridSize());
	}

	/**
  * Determins if a marker is already added to the cluster.
  *
  * @param {google.maps.Marker} marker The marker to check.
  * @return {boolean} True if the marker is already added.
  */
	Cluster.prototype.isMarkerAlreadyAdded = function (marker) {
		if (this.markers_.indexOf) {
			return this.markers_.indexOf(marker) != -1;
		} else {
			for (var i = 0, m; m = this.markers_[i]; i++) {
				if (m == marker) {
					return true;
				}
			}
		}
		return false;
	};

	/**
  * Add a marker the cluster.
  *
  * @param {google.maps.Marker} marker The marker to add.
  * @return {boolean} True if the marker was added.
  */
	Cluster.prototype.addMarker = function (marker) {
		if (this.isMarkerAlreadyAdded(marker)) {
			return false;
		}

		if (!this.center_) {
			this.center_ = marker.getPosition();
			this.calculateBounds_();
		} else {
			if (this.averageCenter_) {
				var l = this.markers_.length + 1;
				var lat = (this.center_.lat() * (l - 1) + marker.getPosition().lat()) / l;
				var lng = (this.center_.lng() * (l - 1) + marker.getPosition().lng()) / l;
				this.center_ = new google.maps.LatLng(lat, lng);
				this.calculateBounds_();
			}
		}

		marker.isAdded = true;
		this.markers_.push(marker);

		var len = this.markers_.length;
		if (len < this.minClusterSize_ && marker.getMap() != this.map_) {
			// Min cluster size not reached so show the marker.
			marker.setMap(this.map_);
		}

		if (len == this.minClusterSize_) {
			// Hide the markers that were showing.
			for (var i = 0; i < len; i++) {
				this.markers_[i].setMap(null);
			}
		}

		if (len >= this.minClusterSize_) {
			marker.setMap(null);
		}

		this.updateIcon();
		return true;
	};

	/**
  * Returns the marker clusterer that the cluster is associated with.
  *
  * @return {MarkerClusterer} The associated marker clusterer.
  */
	Cluster.prototype.getMarkerClusterer = function () {
		return this.markerClusterer_;
	};

	/**
  * Returns the bounds of the cluster.
  *
  * @return {google.maps.LatLngBounds} the cluster bounds.
  */
	Cluster.prototype.getBounds = function () {
		var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
		var markers = this.getMarkers();
		for (var i = 0, marker; marker = markers[i]; i++) {
			bounds.extend(marker.getPosition());
		}
		return bounds;
	};

	/**
  * Removes the cluster
  */
	Cluster.prototype.remove = function () {
		this.clusterIcon_.remove();
		this.markers_.length = 0;
		delete this.markers_;
	};

	/**
  * Returns the center of the cluster.
  *
  * @return {number} The cluster center.
  */
	Cluster.prototype.getSize = function () {
		return this.markers_.length;
	};

	/**
  * Returns the center of the cluster.
  *
  * @return {Array.<google.maps.Marker>} The cluster center.
  */
	Cluster.prototype.getMarkers = function () {
		return this.markers_;
	};

	/**
  * Returns the center of the cluster.
  *
  * @return {google.maps.LatLng} The cluster center.
  */
	Cluster.prototype.getCenter = function () {
		return this.center_;
	};

	/**
  * Calculated the extended bounds of the cluster with the grid.
  *
  * @private
  */
	Cluster.prototype.calculateBounds_ = function () {
		var bounds = new google.maps.LatLngBounds(this.center_, this.center_);
		this.bounds_ = this.markerClusterer_.getExtendedBounds(bounds);
	};

	/**
  * Determines if a marker lies in the clusters bounds.
  *
  * @param {google.maps.Marker} marker The marker to check.
  * @return {boolean} True if the marker lies in the bounds.
  */
	Cluster.prototype.isMarkerInClusterBounds = function (marker) {
		return this.bounds_.contains(marker.getPosition());
	};

	/**
  * Returns the map that the cluster is associated with.
  *
  * @return {google.maps.Map} The map.
  */
	Cluster.prototype.getMap = function () {
		return this.map_;
	};

	/**
  * Updates the cluster icon
  */
	Cluster.prototype.updateIcon = function () {
		var zoom = this.map_.getZoom();
		var mz = this.markerClusterer_.getMaxZoom();

		if (mz && zoom > mz) {
			// The zoom is greater than our max zoom so show all the markers in cluster.
			for (var i = 0, marker; marker = this.markers_[i]; i++) {
				marker.setMap(this.map_);
			}
			return;
		}

		if (this.markers_.length < this.minClusterSize_) {
			// Min cluster size not yet reached.
			this.clusterIcon_.hide();
			return;
		}

		var numStyles = this.markerClusterer_.getStyles().length;
		var sums = this.markerClusterer_.getCalculator()(this.markers_, numStyles);
		this.clusterIcon_.setCenter(this.center_);
		this.clusterIcon_.setSums(sums);
		this.clusterIcon_.show();
	};

	/**
  * A cluster icon
  *
  * @param {Cluster} cluster The cluster to be associated with.
  * @param {Object} styles An object that has style properties:
  *     'url': (string) The image url.
  *     'height': (number) The image height.
  *     'width': (number) The image width.
  *     'anchor': (Array) The anchor position of the label text.
  *     'textColor': (string) The text color.
  *     'textSize': (number) The text size.
  *     'backgroundPosition: (string) The background postition x, y.
  * @param {number=} opt_padding Optional padding to apply to the cluster icon.
  * @constructor
  * @extends google.maps.OverlayView
  * @ignore
  */
	function ClusterIcon(cluster, styles, opt_padding) {
		cluster.getMarkerClusterer().extend(ClusterIcon, google.maps.OverlayView);

		this.styles_ = styles;
		this.padding_ = opt_padding || 0;
		this.cluster_ = cluster;
		this.center_ = null;
		this.map_ = cluster.getMap();
		this.div_ = null;
		this.sums_ = null;
		this.visible_ = false;

		this.setMap(this.map_);
	}

	/**
  * Triggers the clusterclick event and zoom's if the option is set.
  *
  * @param {google.maps.MouseEvent} event The event to propagate
  */
	ClusterIcon.prototype.triggerClusterClick = function (event) {
		var markerClusterer = this.cluster_.getMarkerClusterer();

		// Trigger the clusterclick event.
		google.maps.event.trigger(markerClusterer, 'clusterclick', this.cluster_, event);

		if (markerClusterer.isZoomOnClick()) {
			// Zoom into the cluster.
			this.map_.fitBounds(this.cluster_.getBounds());
		}
	};

	/**
  * Adding the cluster icon to the dom.
  * @ignore
  */
	ClusterIcon.prototype.onAdd = function () {
		this.div_ = document.createElement('DIV');
		if (this.visible_) {
			var pos = this.getPosFromLatLng_(this.center_);
			this.div_.style.cssText = this.createCss(pos);
			this.div_.innerHTML = this.sums_.text;
		}

		var panes = this.getPanes();
		panes.overlayMouseTarget.appendChild(this.div_);

		var that = this;
		var isDragging = false;
		google.maps.event.addDomListener(this.div_, 'click', function (event) {
			// Only perform click when not preceded by a drag
			if (!isDragging) {
				that.triggerClusterClick(event);
			}
		});
		google.maps.event.addDomListener(this.div_, 'mousedown', function () {
			isDragging = false;
		});
		google.maps.event.addDomListener(this.div_, 'mousemove', function () {
			isDragging = true;
		});
	};

	/**
  * Returns the position to place the div dending on the latlng.
  *
  * @param {google.maps.LatLng} latlng The position in latlng.
  * @return {google.maps.Point} The position in pixels.
  * @private
  */
	ClusterIcon.prototype.getPosFromLatLng_ = function (latlng) {
		var pos = this.getProjection().fromLatLngToDivPixel(latlng);

		if (_typeof(this.iconAnchor_) === 'object' && this.iconAnchor_.length === 2) {
			pos.x -= this.iconAnchor_[0];
			pos.y -= this.iconAnchor_[1];
		} else {
			pos.x -= parseInt(this.width_ / 2, 10);
			pos.y -= parseInt(this.height_ / 2, 10);
		}
		return pos;
	};

	/**
  * Draw the icon.
  * @ignore
  */
	ClusterIcon.prototype.draw = function () {
		if (this.visible_) {
			var pos = this.getPosFromLatLng_(this.center_);
			this.div_.style.top = pos.y + 'px';
			this.div_.style.left = pos.x + 'px';
		}
	};

	/**
  * Hide the icon.
  */
	ClusterIcon.prototype.hide = function () {
		if (this.div_) {
			this.div_.style.display = 'none';
		}
		this.visible_ = false;
	};

	/**
  * Position and show the icon.
  */
	ClusterIcon.prototype.show = function () {
		if (this.div_) {
			var pos = this.getPosFromLatLng_(this.center_);
			this.div_.style.cssText = this.createCss(pos);
			this.div_.style.display = '';
		}
		this.visible_ = true;
	};

	/**
  * Remove the icon from the map
  */
	ClusterIcon.prototype.remove = function () {
		this.setMap(null);
	};

	/**
  * Implementation of the onRemove interface.
  * @ignore
  */
	ClusterIcon.prototype.onRemove = function () {
		if (this.div_ && this.div_.parentNode) {
			this.hide();
			this.div_.parentNode.removeChild(this.div_);
			this.div_ = null;
		}
	};

	/**
  * Set the sums of the icon.
  *
  * @param {Object} sums The sums containing:
  *   'text': (string) The text to display in the icon.
  *   'index': (number) The style index of the icon.
  */
	ClusterIcon.prototype.setSums = function (sums) {
		this.sums_ = sums;
		this.text_ = sums.text;
		this.index_ = sums.index;
		if (this.div_) {
			this.div_.innerHTML = sums.text;
		}

		this.useStyle();
	};

	/**
  * Sets the icon to the the styles.
  */
	ClusterIcon.prototype.useStyle = function () {
		var index = Math.max(0, this.sums_.index - 1);
		index = Math.min(this.styles_.length - 1, index);
		var style = this.styles_[index];
		this.url_ = style['url'];
		this.height_ = style['height'];
		this.width_ = style['width'];
		this.textColor_ = style['textColor'];
		this.anchor_ = style['anchor'];
		this.textSize_ = style['textSize'];
		this.backgroundPosition_ = style['backgroundPosition'];
		this.iconAnchor_ = style['iconAnchor'];
	};

	/**
  * Sets the center of the icon.
  *
  * @param {google.maps.LatLng} center The latlng to set as the center.
  */
	ClusterIcon.prototype.setCenter = function (center) {
		this.center_ = center;
	};

	/**
  * Create the css text based on the position of the icon.
  *
  * @param {google.maps.Point} pos The position.
  * @return {string} The css style text.
  */
	ClusterIcon.prototype.createCss = function (pos) {
		var style = [];
		style.push('background-image:url(' + this.url_ + ');');
		var backgroundPosition = this.backgroundPosition_ ? this.backgroundPosition_ : '0 0';
		style.push('background-position:' + backgroundPosition + ';');

		if (_typeof(this.anchor_) === 'object') {
			if (typeof this.anchor_[0] === 'number' && this.anchor_[0] > 0 && this.anchor_[0] < this.height_) {
				style.push('height:' + (this.height_ - this.anchor_[0]) + 'px; padding-top:' + this.anchor_[0] + 'px;');
			} else if (typeof this.anchor_[0] === 'number' && this.anchor_[0] < 0 && -this.anchor_[0] < this.height_) {
				style.push('height:' + this.height_ + 'px; line-height:' + (this.height_ + this.anchor_[0]) + 'px;');
			} else {
				style.push('height:' + this.height_ + 'px; line-height:' + this.height_ + 'px;');
			}
			if (typeof this.anchor_[1] === 'number' && this.anchor_[1] > 0 && this.anchor_[1] < this.width_) {
				style.push('width:' + (this.width_ - this.anchor_[1]) + 'px; padding-left:' + this.anchor_[1] + 'px;');
			} else {
				style.push('width:' + this.width_ + 'px; text-align:center;');
			}
		} else {
			style.push('height:' + this.height_ + 'px; line-height:' + this.height_ + 'px; width:' + this.width_ + 'px; text-align:center;');
		}

		var txtColor = this.textColor_ ? this.textColor_ : 'black';
		var txtSize = this.textSize_ ? this.textSize_ : 11;

		style.push('cursor:pointer; top:' + pos.y + 'px; left:' + pos.x + 'px; color:' + txtColor + '; position:absolute; font-size:' + txtSize + 'px; font-family:Arial,sans-serif; font-weight:bold');
		return style.join('');
	};

	// Export Symbols for Closure
	// If you are not going to compile with closure then you can remove the
	// code below.
	window['MarkerClusterer'] = MarkerClusterer;
	MarkerClusterer.prototype['addMarker'] = MarkerClusterer.prototype.addMarker;
	MarkerClusterer.prototype['addMarkers'] = MarkerClusterer.prototype.addMarkers;
	MarkerClusterer.prototype['clearMarkers'] = MarkerClusterer.prototype.clearMarkers;
	MarkerClusterer.prototype['fitMapToMarkers'] = MarkerClusterer.prototype.fitMapToMarkers;
	MarkerClusterer.prototype['getCalculator'] = MarkerClusterer.prototype.getCalculator;
	MarkerClusterer.prototype['getGridSize'] = MarkerClusterer.prototype.getGridSize;
	MarkerClusterer.prototype['getExtendedBounds'] = MarkerClusterer.prototype.getExtendedBounds;
	MarkerClusterer.prototype['getMap'] = MarkerClusterer.prototype.getMap;
	MarkerClusterer.prototype['getMarkers'] = MarkerClusterer.prototype.getMarkers;
	MarkerClusterer.prototype['getMaxZoom'] = MarkerClusterer.prototype.getMaxZoom;
	MarkerClusterer.prototype['getStyles'] = MarkerClusterer.prototype.getStyles;
	MarkerClusterer.prototype['getTotalClusters'] = MarkerClusterer.prototype.getTotalClusters;
	MarkerClusterer.prototype['getTotalMarkers'] = MarkerClusterer.prototype.getTotalMarkers;
	MarkerClusterer.prototype['redraw'] = MarkerClusterer.prototype.redraw;
	MarkerClusterer.prototype['removeMarker'] = MarkerClusterer.prototype.removeMarker;
	MarkerClusterer.prototype['removeMarkers'] = MarkerClusterer.prototype.removeMarkers;
	MarkerClusterer.prototype['resetViewport'] = MarkerClusterer.prototype.resetViewport;
	MarkerClusterer.prototype['repaint'] = MarkerClusterer.prototype.repaint;
	MarkerClusterer.prototype['setCalculator'] = MarkerClusterer.prototype.setCalculator;
	MarkerClusterer.prototype['setGridSize'] = MarkerClusterer.prototype.setGridSize;
	MarkerClusterer.prototype['setMaxZoom'] = MarkerClusterer.prototype.setMaxZoom;
	MarkerClusterer.prototype['onAdd'] = MarkerClusterer.prototype.onAdd;
	MarkerClusterer.prototype['draw'] = MarkerClusterer.prototype.draw;

	Cluster.prototype['getCenter'] = Cluster.prototype.getCenter;
	Cluster.prototype['getSize'] = Cluster.prototype.getSize;
	Cluster.prototype['getMarkers'] = Cluster.prototype.getMarkers;

	ClusterIcon.prototype['onAdd'] = ClusterIcon.prototype.onAdd;
	ClusterIcon.prototype['draw'] = ClusterIcon.prototype.draw;
	ClusterIcon.prototype['onRemove'] = ClusterIcon.prototype.onRemove;

	/***/
}]
/******/);