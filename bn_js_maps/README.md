# BN JS Maps

Dieses JS/Node Module erleichtert die Verwendung von Google Maps und enthält aktuell folgende Funktionen:

- Routen Planer
- Streetmode

Wenn weitere Features benötigt werden, kann diese Bibliothek beliebig erweitert werden mit einem entsprechenden Pull Request.
Das gleiche gilt auch für Bugs, da dieses Modul nicht auf jede Eventualität getestet worden ist. 

### Domain in der Google API Konsole hinzufügen
https://console.developers.google.com/apis/credentials/key/221?project=bn-maps-defaulta-1497442582498

### Benutzung
==================
##### - JS Package installieren
- Yarn

yarn add  git+ssh://git@bitbucket.org:brandnamic/bn-maps#v<TAG> <= Läd das Modul mit der angegebenen Tag Version herunter (<TAG> muss mit der Tagversion ersetzt werden)

- NPM

npm install git+ssh://git@bitbucket.org:brandnamic/bn-maps#<TAG>

##### - JS Einbindung
Einbindung des Node/JS Module:

- Bei nicht Verwendung von ES6 (transpiled Version wird zurückgeliefert):


```

var bnMaps = require("bn-maps/dist/common/js/bnmaps");

```
- Bei Verwendung von ES6:

```
var bnMaps = require("bn-maps");
```

##### - Erstellung des Config Objektes

Vor dem Initialisieren der Karte muss ein Config Objekt zusammengestellt werden, wo die benötigten Informationen mitgegeben werden:

Beispiel 
(mehrere Anwendungsbeispiele befinden sich in der example/index.html ):

```javascript
let options = {
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXX", 
    language: 'de', 
    containerMap: 'example2',
    mapConfig:{
        marker: {
            lat: 46.5671285,
            lng: 11.5613681,
            title: 'Wanderhotel Europa',
            icon: 'https://www.brandnamic.com/fileadmin/web/img/marker.png'
        },
        options: {
            style: [{'stylers': [{'hue': '#332318'}, {'gamma': 0.4}]}],
            zoom: 12,
            scrollwheel: false,
            draggable: false,
            mapTypeControl: false
        }
    },
};
```

Beschreibung der einzelnen Key´s:

```js
apiKey = Es kann individuell ein API KEY vergeben werden, ansonsten wird der Default API KEY verwendet
language = Aktuell genutzte Sprache (Default: en)
containerMap: Id des Containers, wo die Google Maps Karte hineingeladen werden soll
mapConfig:{
        route{
            fieldInputId = Id des Input Feldes, von wo der Startpunkt berechnet werden soll
            autocomplete = Legt auf dem "fieldInputId" eine Autocomplete Funktion drauf, wo Suchvorschläge für die Orte angezeigt werden
            travelMode: Auswahl des Fortbewegungsmittel für die Routenberechnung ("DRIVING";"WALKING";"TRANSIT";"BICYCLING")
            removeMarkerOnResult: Entfernt Marker bei der Berechnung der Route (Default : true)
            containerId = Id des Containers, wo anschließend die Routenbeschreibung hineingeparsed werden soll
            geoLocation = Aktiviert die automatische Erkennung des aktuellen Ortes und löst beim Initialisieren die Routenberechnung automatisch aus(nur möglich mit HTTPS)
        }
        marker{
             lat = Längengrad des Logos
             lng = Breitengrad des Logos
             title = Titel des Logos beim Hovern
             icon = Verwendet folgendes Logo als Marker
             
             Dieses Object kann auch vom Typ Array sein, um mehrere Marker gleichzeitig initialisieren zu können (siehe example.html)
        }
        options{
             //In diesem Knoten können die ganz normalen Option Felder  von Google Maps verwendet werden
             style: [{'stylers': [{'hue': '#332318'}, {'gamma': 0.4}]}],
             zoom: 12,
             scrollwheel: false,
             draggable: false,
             mapTypeControl: false,
             markerCluster= aktiviert den Marker Cluster, wo Markers die sich überlappen zusammengefasst werden und erst beim Zoom sichtbar werden
             markerIcons = Wird für die Ausgabe des markerCluster verwendet, indem ein Array mitgegeben wird, wo individuelle Markers verwendet werden, je nach Anzahl der Einträge
        }
    },
}
```
##### - Initialisation der Karte:
```
var example = new bnMaps(options);
```