<?php

$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'] = '/index.php?id=8';

// GraphicsMagick yields better results on Google PageSpeed Insights
$GLOBALS['TYPO3_CONF_VARS']['GFX']['im_version_5'] = 'gm';
$GLOBALS['TYPO3_CONF_VARS']['GFX']['colorspace'] = 'RGB';
// Quality of 70 is enough
$GLOBALS['TYPO3_CONF_VARS']['GFX']['jpg_quality'] = '70';
