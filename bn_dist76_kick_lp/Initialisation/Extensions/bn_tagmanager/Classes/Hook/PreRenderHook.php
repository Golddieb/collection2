<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 18.12.2014
 * Time: 18:52
 */

namespace BN\BnTagmanager\Hook;

use BN\BnTagmanager\Service\TagManagerService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class PreRenderHook {

    /**
     * @var array
     */
    protected $settings;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager = NULL;

    /**
     * @var \BN\BnTagmanager\Domain\Repository\PageRepository
     */
    protected $pageRepository = NULL;

    /**
     * initialise objectManager
     */
    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $this->pageRepository = $this->objectManager->get('BN\\BnTagmanager\\Domain\\Repository\\PageRepository');
        $this->settings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager')->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS, 'BnTagmanager');
    }

    /**
     * @param array $conf
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $parent
     */
    public function addTagManager($conf, $parent)
    {
        if (defined('TYPO3_MODE') && TYPO3_MODE === 'FE') {
            $container_id = $this->settings['gtm_container_id'];
            if (!empty($container_id) && strpos($conf['pObj']->content, '#id1443539507#') === FALSE) {
                /**
                 * @var \BN\BnTagmanager\Domain\Model\Page $page
                 */
                $page = $this->pageRepository->findByUid($GLOBALS['TSFE']->id);

                $tagManager = new TagManagerService($container_id, $page->getGtmDataLayer());
                $conf['pObj']->content = preg_replace('/(<head>[\s\S]*?<\/head>[\s\S]*?<body[^>]*>)/i', '$1 ' . $tagManager->getCode(), $conf['pObj']->content, 1);
            }
        }
    }
}