# Google Tag Manager

Für die Einbindung des Google Tag Managers (fortan: GTM) ist die Eingabe der GTM-Container ID zwingend erforderlich.  
Sobald die Extension installiert ist einfach das statische Template `Tag Manager` in den Template-Eigenschaften dazuklicken und im Konstanteneditor die Container ID eintragen.

## ContainerPreset
In der Extension wird ein `ContainerPreset.json` File mitversioniert das als eine Art Container-Kickstarter verstanden werden kann.
Diese Datei kann importiert werden um einen neuen Container zu überschreiben und so einige gängige Funktionen sofort verfügbar zu machen, oder um einen bestehenden Container zu erweitern.

## Known issues
Das GTM Snippet wird eingefügt bevor das HTML zum Zweck des Cachings in der Datenbank abgelegt wird.  
Sollte das GTM Snippet nicht mehr aufscheint sobald man nicht mehr im Typo3-Dashboard angemeldet ist, dann wird warscheinlich eine gecachte Version der Seite geladen die vor der Installation der Extension generiert wurde.  
Um dies zu beheben reicht es die Typo3-Caches vollständig zu löschen (`Installation -> Important actions -> Clear all cache`).

## ToDos
* Diese Dokumentation mit Details zum Import von GTM Containern und eventuell der Einrichtung derselben ergänzen.
* Ordentliche Sprachversionierung für DataLayer-Konfiguration in den Seiteneigenschaften (Templatevariablen ersetzen, Languagefallback, usw.)