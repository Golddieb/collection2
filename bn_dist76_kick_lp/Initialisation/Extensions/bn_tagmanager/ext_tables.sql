#
# Table structure for table 'pages'
#
CREATE TABLE pages (
  gtm_data_layer longtext NOT NULL
);

#
# Table structure for table 'pages_language_overlay'
#
CREATE TABLE pages_language_overlay (
  gtm_data_layer longtext NOT NULL
);