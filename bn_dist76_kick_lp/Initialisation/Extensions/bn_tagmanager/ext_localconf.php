<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

$overlayFields = &$GLOBALS['TYPO3_CONF_VARS']['FE']['pageOverlayFields'];
if (!empty($overlayFields)) $overlayFields .= ',';
$overlayFields .= 'gtm_data_layer';

$TYPO3_CONF_VARS['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-all'][$_EXTKEY] = 'EXT:' . $_EXTKEY . '/Classes/Hook/PreRenderHook.php:&BN\\BnTagmanager\\Hook\\PreRenderHook->addTagManager';