# TS to JS Constant Converter _(bn_ts2js)_

The **bn_ts2js** extension converts all `TypoScript` constants to a `JavaScript` object on the site.
This means, all `TypoScript` constants are also available in `JavaScript`.

## Installation

Upload the extension to the `typo3conf/ext` folder and activate it in the Typo3 backend (`Admin Tools` > `Extension Manager`).
Include the `bn_ts2js` extension in your `TypoScript` template.

## Configuration possibilities

You can configure the following parameters to change the behavior of the **TS2JS Converter**.

- **Path to output file**: define a path where the constant `JavaScript` file is going to be saved
- **Path to input folder**: path of the folder where the constant files are stored
- **Parse folder recursively**: if `true`, parse files in subfolders
- **JS locales property path**: set the `JavaScript` object path for accessing all constants (default: `BN.lang.locales`)
- **JS globals property path**: set the `JavaScript` object root (default: `BN.lang`)

## Available functions in frontend

* Access all constants in all languages:

```javascript
    var allConstants = BN.lang.locales;
```

* Get current language code:

```javascript
    var currLang = BN.lang.currLang;
```

* Translation module:

```javascript
    var i18n = BN.lang.i18n;
    i18n('mehr_lesen'); // => without language code = translation in actual language
    i18n('mehr_lesen', 'en');
```

## Known issues

At the moment the usage of constants in constant values does not work "cross file".
That means, you can only use constants from the same file as constant value

**Example**

```typoscript
# base.ts
    test = value
    anotherTest = {$test} // works
```

```typoscript
# base.ts
    test = value
    
# en.ts
    anotherTest = {$test} // does not work
```

## Supported Typo3 versions

At the moment following Typo3 versions are supported:
* _Typo3 6.2.x_
* _Typo3 7.6.x_

## Contributing

Please use [Git Flow](http://jeffkreeftmeijer.com/2010/why-arent-you-using-git-flow/) for contributing to this project. Thanks!
