<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 18.12.2014
 * Time: 18:52
 */

namespace BN\BnTs2js\Hooks;

use BN\BnTs2js\Services\ConstantParserService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Configuration\Exception;

class PreRenderHook {
    /**
     * @var array
     */
    protected $settings;

    /**
     * @param array $jsLibs
     * @param \TYPO3\CMS\Core\Page\PageRenderer $pageRenderer
     *
     * @throws Exception
     *
     * @return void
     */
    public function addDynamicJsFiles($jsLibs, $pageRenderer)
    {
        if (defined('TYPO3_MODE') && TYPO3_MODE === 'FE') {
            $this->initSettings();

            $filepath = GeneralUtility::getFileAbsFileName($this->getSetting('output_file', 'EXT:bn_ts2js/Resources/Public/Js/locales.js'));
            $constRoot = GeneralUtility::getFileAbsFileName($this->getSetting('input_folder', 'fileadmin/web/ts/constants'));
            $localesPath = $this->getSetting('js_locales_property', 'BN.lang.locales');
            $localesPathStatement = $this->generateJsPathStatement($localesPath);
            $globalsPath = $this->getSetting('js_globals_property', 'BN.lang');
            $globalsPathStatement = $this->generateJsPathStatement($globalsPath);

            if (is_file($filepath) && preg_match('/###tstamp:([\d]+)###/i', file_get_contents($filepath), $matches)) {
                $jsTStamp = (int) $matches[1];
            }

            if (is_dir($constRoot)) {
                $tsTStamp = $this->checkLastModified($constRoot, $this->getSetting('input_recursive', 0));
            } else {
                throw new Exception('input_folder: ' .$constRoot . ' is not a valid directory.', 1429101350);
            }

            if (empty($jsTStamp) || ($tsTStamp > $jsTStamp)) {
                $parser = new ConstantParserService();

                $locales = "/*###tstamp:{$tsTStamp}###*/(function(window){'use strict';";
                $locales .= $localesPathStatement . '=';
                $locales .= $parser->parsePathToJson($constRoot, $this->getSetting('input_recursive', 0));
                $locales .= ";})(window);";

                file_put_contents($filepath, $locales);
            }

            if (is_file($filepath)) {
                $relPath = GeneralUtility::removePrefixPathFromList(array($filepath), PATH_site);
                $jsLibs['jsLibs'][$relPath[0]] = array(
                    'file' => $relPath[0],
                    'type' => 'text/javascript',
                    'section' => 2,
                    'compress' => true,
                    'forceOnTop' => false,
                    'allWrap' => null,
                    'excludeFromConcatenation' => false,
                    'splitChar' => null
                );
            }

            array_unshift($jsLibs['headerData'], <<<EOT
<script>
    (function(window, document, undefined){
        'use strict';
        $globalsPathStatement=$globalsPath||{};
        $globalsPath.currLang='{$GLOBALS['TSFE']->config['config']['language']}';
        $globalsPath.intLang={$GLOBALS['TSFE']->sys_language_uid};
        $globalsPath.i18n=function(string, language) {
            var lang = language || $globalsPath.currLang,
                locale = {$localesPath},
                path = string.split('.'),
                currLoc = locale;

            if (currLoc.hasOwnProperty(lang)) {
                currLoc = currLoc[lang];

                for (var i = 0; i < path.length; i++) {
                    if (currLoc.hasOwnProperty(path[i])) {
                        currLoc = currLoc[path[i]];
                    } else {
                        return '';
                    }
                }

                return currLoc;
            }

            return '';
        };
    })(window, document);
</script>
EOT
            );
        }
    }

    /**
     * @param string $dirName
     * @param bool $recursive
     * @return int
     */
    function checkLastModified($dirName, $recursive = false) {
        $dirName = rtrim($dirName, '/');
        $dir = @dir($dirName);
        $lastModified = 0;
        $currentModified = 0;
        while($entry = $dir->read()) {
            if ($entry != '.' && $entry != '..') {
                if (!is_dir($dirName . '/' . $entry)) {
                    $currentModified = filemtime($dirName . '/' . $entry);
                } else if ($recursive) {
                    $currentModified = $this->checkLastModified($dirName . '/' . $entry, 1);
                }

                if ($currentModified > $lastModified){
                    $lastModified = $currentModified;
                }
            }
        }
        $dir->close();

        return $lastModified;
    }

    /**
     * @return void
     */
    protected function initSettings()
    {
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        /**
         * @var ConfigurationManagerInterface $configurationManager
         */
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
        $framework = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);

        $this->settings = $framework['tx_bnts2js']['settings'];
    }

    /**
     * @param string $key
     * @param string $defaultValue
     * @return string
     */
    protected function getSetting($key, $defaultValue = '')
    {
        return (!empty($this->settings[$key]) ? $this->settings[$key] : $defaultValue);
    }

    /**
     * @param string $property_path
     * @return string
     */
    protected function generateJsPathStatement($property_path)
    {
        $properties = explode('.', $property_path);
        $jsPath = 'var ' . $properties[0] . '=window.' . $properties[0] . '=window.' . $properties[0] . '||{};';
        for ($i = 1; $i < count($properties); $i++) {
            $part = '';
            for ($j = 0; $j < $i; $j++) {
                $part .= $properties[$j] . '.';
            }

            $jsPath .= $part . $properties[$i];

            if ($i != count($properties)-1) {
                $jsPath .= '=' . $part . $properties[$i] . '||{};';
            }
        }

        return $jsPath;
    }
}