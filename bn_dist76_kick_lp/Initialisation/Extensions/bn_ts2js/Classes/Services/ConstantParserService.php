<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 09.01.2015
 * Time: 14:18
 */

namespace BN\BnTs2js\Services;

use \TYPO3\CMS\Core\TypoScript\Parser\TypoScriptParser;
use \TYPO3\CMS\Extbase\Service\TypoScriptService;

class ConstantParserService {
    /**
     * @param string $constRoot
     * @param bool $recursive
     *
     * @return string
     */
    public function parsePath($constRoot, $recursive = false)
    {
        $content = '';
        $constRoot = rtrim($constRoot, '/');
        $dir = @dir($constRoot);
        while ($entry = $dir->read()) {
            if ($entry != "." && $entry != "..") {
                if (!is_dir($constRoot . '/' . $entry)) {
                    $content .= $this->parseFile($constRoot, $entry);
                } else if ($recursive) {
                    $content .= "'" . $entry . "':{" . $this->parsePath($constRoot . '/' . $entry, 1) . "},";
                }
            }
        }
        $dir->close();

        return trim($content, ",");
    }

    /**
     * @param string $constRoot
     * @param bool $recursive
     *
     * @return string
     */
    public function parsePathToJson($constRoot, $recursive = false)
    {
        $content = array();
        $constRoot = rtrim($constRoot, '/');
        $dir = @dir($constRoot);
        while ($entry = $dir->read()) {
            if ($entry != "." && $entry != "..") {
                if (!is_dir($constRoot . '/' . $entry)) {
                    $content = array_merge($content, $this->parseFileToArray($constRoot, $entry));
                } else if ($recursive) {
                    $content[$entry] = $this->parsePathToJson($constRoot . '/' . $entry, 1);
                }
            }
        }
        $dir->close();

        $jsonString = json_encode($this->parseConstants($content), JSON_FORCE_OBJECT + JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE);
        
        return $jsonString;
    }

    /**
     * @param string $filename
     * @param string $path
     *
     * @return string
     */
    public function parseFile($path, $filename)
    {
        // Nur valide .ts-Files werden eingelesen
        if (is_file($path . '/' . $filename) && preg_match('/(.*)\.ts/', $filename, $match)) {
            // Der Dateiname wird als property name verwendet (z.B.: de)
            $content = "'" . $match[1] . "':";

            $file = file_get_contents($path . '/' . $filename);
            $tsParser = new TypoScriptParser();
            $tsParser->parse($file);
            $tsService = new TypoScriptService();
            $content .= json_encode($tsService->convertTypoScriptArrayToPlainArray($tsParser->setup), JSON_FORCE_OBJECT + JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE) . ',';
            return $content;
        } else {
            return '';
        }
    }

    /**
     * @param string $filename
     * @param string $path
     *
     * @return string
     */
    public function parseFileToArray($path, $filename)
    {
        // Nur valide .ts-Files werden eingelesen
        if (is_file($path . '/' . $filename) && preg_match('/(.*)\.ts/', $filename, $match)) {
            // Der Dateiname wird als property name verwendet (z.B.: de)
            $content[$match[1]] = array();

            $file = file_get_contents($path . '/' . $filename);
            $tsParser = new TypoScriptParser();
            $tsParser->parse($file);
            $tsService = new TypoScriptService();
            $content[$match[1]] = $tsService->convertTypoScriptArrayToPlainArray($tsParser->setup);

            return $content;
        } else {
            return '';
        }
    }

    /**
     * @param array $constantArray
     *
     * @author Julian Wiedenhofer <julian.wiedenhofer@brandnamic.com>
     *
     * @return array
     */
    public function parseConstants(array &$constantArray) {
        $keys = array();

        $this->arrayWalkRecursiveExt($constantArray, function ($value, $index, $userData, $parentKey) use ($constantArray, &$keys) {
            if(!is_array($value)) {
                if(preg_match('/({\$)([^}]*)(})/', $value, $match)) {
                    $paramArray = array();

                    foreach(array_keys($constantArray) as $filenameKey) {
                        $paramArray[$filenameKey] = null;
                    }

                    foreach($this->searchArrayByKeyRecursive($constantArray, $match[2]) as $keyConst => $value) {
                        if(!empty($value)) {
                            foreach($value as $key => $param) {
                                if($key !== '__parentKey') {
                                    $paramArray[$value['__parentKey']] = $param;
                                }
                            }
                        }
                    }
                    $keys[$match[2]] = $paramArray;
                }
            }
        });

        foreach ($constantArray as $fileName => &$fileConstants) {
            $this->replaceConstants($fileConstants, $keys, $fileName);
        }

        return $constantArray;
    }

    protected function replaceConstants(&$constantArray, &$keys, $parentKey = '') {
        foreach ($constantArray as $key => &$item) {
            if(is_array($item)) {
                $this->replaceConstants($item, $keys, $parentKey);
            }
            else if(preg_match('/({\$)([^}]*)(})/', $item, $match)) {
                $item = preg_replace('/({\$)([^}]*)(})/', $keys[$match[2]][$parentKey], $item);
            }
        }
    }

    /**
     * @param array $array
     * @param string|int $key
     *
     * @author Julian Wiedenhofer <julian.wiedenhofer@brandnamic.com>
     *
     * @return array
     */
    public function searchArrayByKeyRecursive (array $array, $key, $parentKey = null) {
        $arr = array();
        if( array_key_exists($key, $array) ){
            if( !preg_match('/({\$)([^}]*)(})/', $array[$key])) {
                $arr[] = $array[$key];
                $arr['__parentKey'] = $parentKey;

                return $arr;
            }

        } else if( !isset($array[$key]) || !array_key_exists($key, $array) ) {
            foreach ($array as $index => $subarray) {
                if( is_array($subarray) ) {
                    foreach ($this->searchArrayByKeyRecursive($subarray, $key, $index) as $item) {
                        if(isset($item) && !empty($item)) {
                            $arr[] = $this->searchArrayByKeyRecursive($subarray, $key, $index);
                        }
                    }
                }
            }
        }

        return $arr;
    }

    /**
     * @param array $array
     * @param mixed $needle
     *
     * @author Julian Wiedenhofer <julian.wiedenhofer@brandnamic.com>
     *
     * @return string|bool
     */

    function findParentKey($array, $needle) {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (in_array($needle, array_keys($value), true)) {
                    return $key;
                } elseif (false !== $key = $this->findParentKey($value, $needle)) {
                    return $key;
                }
            }
        }

        return false;
    }

    /**
     * Apply a user defined function recursively to every member of an array
     * - Allows the key of an array to be used
     * @param array $array
     * @param string userFunction
     * @param mixed $userData [optional]
     * @param mixed $parentKey [optional]
     * @see array_walk_recursive()
     * @since version 1.0
     */
    function arrayWalkRecursiveExt(&$input, $userFunction, $userData = null, $parentKey = null)
    {
        foreach ($input as $key => $value)
        {
            if (is_array($value))
            {
                /*
                    call the user function and pass all the arguments
                    this is what array_walk_recursive() is missing
                    $value will be an array but we can still use $key
                    and perhaps you want to do something with each array
                */
                call_user_func_array($userFunction,
                    array(
                        $value, $key, $userData, $parentKey
                    )
                );
                // recuse though the next level
                $this->arrayWalkRecursiveExt($value, $userFunction, $userData, $key);
            }
            else
            {
                // call the user function and pass all the arguments
                call_user_func_array($userFunction,
                    array(
                        $value, $key, $userData, $parentKey
                    )
                );
            }
        }
    }
}