<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-preProcess'][] = 'EXT:bn_ts2js/Classes/Hooks/PreRenderHook.php:BN\\BnTs2js\\Hooks\\PreRenderHook->addDynamicJsFiles';