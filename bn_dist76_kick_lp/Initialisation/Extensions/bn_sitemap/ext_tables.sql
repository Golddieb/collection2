#
# Table structure for table 'pages'
#
CREATE TABLE pages (
  sitemap_change_frequency varchar(7) DEFAULT '' NOT NULL,
  sitemap_priority varchar(3) DEFAULT '' NOT NULL,
  sitemap_exclude tinyint(1) unsigned DEFAULT '0' NOT NULL,
  language_overlay int(11) unsigned DEFAULT '0' NOT NULL
);