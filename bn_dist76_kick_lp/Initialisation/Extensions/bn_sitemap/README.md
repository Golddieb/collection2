# BN Sitemap

## Kurze Beschreibung:

Dies bn_sitemap-Extension generiert eine Google-Sitemap wobei die **aktuellen Seite** als Startseite gewählt wird.  
Wird die aktuelle Seite nicht durch die Übergabe des GET-Parameters "id" bestimmt,  
wird automatisch die Rootpage (Seite auf der das Basistemplate eingebunden wurde) gewählt.

**Weiterleitungen** werden für die Startseite interpretiert, d.h. es wird automatisch die Seite gewählt auf die der Verweis zeigt.  
Alle weiteren Weiterleitungen werden ausgelesen, doch werden sie im Template ausgeschlossen um doppelte URLs zu vermeiden.

Dabei werden Seiten, die als **im Menü verbergen (nav_hide)** oder **ausgeblendet (hidden)** markiert sind,  
oder solche die durch die **Zeitsteuerung abgelaufen (endtime)** sind, sowie deren Unterseiten automatisch ausgeschlossen.

## INSTALLATION und KONFIGURATION:

1. Erweiterung installieren
2. Statisches Template in Templatedatensatz hinzufügen
3. Im Konstanteneditor globale Anpassungen vornehmen: **(plugin.tx_bnsitemap.settings)**
  - **Pagetype (pagetype):**
     Integer-Wert der als type-ID für die Google-Sitemap verwendet werden soll
     Standardwert: 100
  - **Custom Rootpage (customRootPage):**
     Kann verwendet werden um eine fixe Rootpage festzulegen.
     Ist zum Beispiel die Rootpage eine Weiterleitung, die auf eine Seite verweist, die nicht den ganzen Seitenbaum einschliesst,
     kann über diesen Parameter das auslesen aller Kindseiten forciert werden.
     Standardwert: $GLOBALS['TSFE']->id
  - **Force absolute paths (forceAbsolutePath):**
     Forciert das rendern von absoluten Pfaden, d.h. inklusive Domain.
     Kann auf Wunsch deaktiviert werden, ist SEO-technisch jedoch nicht zu empfehlen
     Standardwert: true
  - **Include Rootpage (includeRootPage):**
     Sofern die Rootpage selbst Inhalte hat sollte diese Option aktiviert werden,
     damit auch die Rootpage selbst in der Sitemap aufscheint.
     Standardwert: false
  - **Exclude UID List (excludeUidList):**
     UID-Liste der Seiten die expliziet nicht in der Google-sitemap aufscheinen sollen.
     Als Folge werden auch sämtliche Unterseiten ausgeschlossen.
     Standardwert: null
  - **Include PID List (includePidList):**
     PID-Liste von Seiten deren Unterseiten in der Google-Sitemap aufscheinen sollen.
     "Exclude UID List" gilt auch für diese Option.
     Standardwert: null
  - **Include single UID List (includeSingleUidList):**
     UID-Liste von Seiten die explizit als Einzelseite eingebunden werden sollen.
     Im Menü verborgene Seiten werden bei dieser Option angezeigt.
     "Exclude UID List" gilt auch für diese Option.
     Standardwert: null
4. Ist die Erweiterung erst installiert wird jeder Seite ein weiterer Reiter "Google Sitemap" hinzugefügt unter dem man folgende Einstellungen pro Seite vornehmen kann:
  - **Change fequency:**
     Ungefährer Zeitraum in dem Änderungen auf der Seite vorgenommen werden.
     Standardwert: monthly
     Folgende Optionen stehen zur Verfügung:
     * `always`:  Die Seite wird bei jedem Zugriff neu indexiert
     * `hourly`:  Die Seite wird bei Zugriffen im Abstand von min. einer Stunde neu indexiert
     * `daily`:   Die Seite wird bei Zugriffen im Abstand von min. einem Tag neu indexiert
     * `weekly`:  Die Seite wird bei Zugriffen im Abstand von min. einer Woche neu indexiert
     * `monthly`: Die Seite wird bei Zugriffen im Abstand von min. einem Monat neu indexiert
     * `yearly`:  Die Seite wird bei Zugriffen im Abstand von min. einem Jahr neu indexiert
     * `never`:   Die Seite wird nicht mehr neu indexiert, z.B.: für Archivierte Seiten
  - **Priority:**
     Priorität der Indexierung.
     Wert von `0.1` - `1.0`
     Standardwert: `0.5`
  - **Exclude:**
     Dieser Parameter wird im Template zur Verfügung gestellt und kann verwendet werden um einzelne Seiten in der Ausgabe auszuschliessen.
