<?php
if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'bn.' . $_EXTKEY,
    'sitemap',
    array(
        'Page' => 'generateSitemap'
    ),
    array(
        'Page' => 'generateSitemap'
    )
);