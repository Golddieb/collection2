<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$tempColumns = array(
	'sitemap_change_frequency' => array(
		'exclude' => 0,
		'label' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_changefreq',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_changefreq.I.0', ''),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_changefreq.I.1', 'always'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_changefreq.I.2', 'hourly'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_changefreq.I.3', 'daily'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_changefreq.I.4', 'weekly'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_changefreq.I.5', 'monthly'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_changefreq.I.6', 'yearly'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_changefreq.I.7', 'never'),
			),
			'size' => 1,
			'maxitems' => 1,
		)
	),
	'sitemap_priority' => array(
		'exclude' => 0,
		'label' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.0', ''),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.1', '1.0'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.2', '0.9'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.3', '0.8'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.4', '0.7'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.5', '0.6'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.6', '0.5'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.7', '0.4'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.8', '0.3'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.9', '0.2'),
				array('LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_priority.I.10', '0.1'),
			),
			'size' => 1,
			'maxitems' => 1,
		)
	),
	'sitemap_exclude' => array(
		'exclude' => 0,
		'label' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_db.xml:pages.tx_bnsitemap_exclude',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	)
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages',$tempColumns,1);

$palette = max(array_keys($TCA['pages']['palettes'])) + 1;
$TCA['pages']['palettes'][$palette] = array();
$TCA['pages']['palettes'][$palette]['showitem'] ='sitemap_change_frequency;;;;1-1-1, sitemap_priority, sitemap_no_prefix';
$TCA['pages']['palettes'][$palette]['canNotCollapse']='1';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages','--div--;Google Sitemap,--palette--;Page specific configuration;'.$palette.'','');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY,'Configuration/Typoscript/', 'BN Sitemap');
?>