<?php

if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$tmpConfiguration = array(
    'language_overlay' => array(
        'label' => 'Inhalte',
        'exclude' => 1,
        'config' => array(
            'type' => 'select',
            'foreign_table' => 'pages_language_overlay',
            'foreign_field' => 'pid',
            'foreign_table_where' => ' AND tt_content.sys_language_uid="###SYS_LANGUAGE_UID###"',
            'maxitems' => 1
        )
    )
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'pages',
    $tmpConfiguration,
    1
);