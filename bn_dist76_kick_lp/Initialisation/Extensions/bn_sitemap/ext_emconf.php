<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "bn_googlesitemap".
 *
 * Auto generated 04-07-2014 16:23
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Custom Google Sitemap',
	'description' => 'A configurable custom google sitemap',
	'category' => 'services',
	'author' => 'Michael Marcenich | Brandnamic',
	'author_email' => 'michael.marcenich@brandnamic.com',
	'author_company' => 'Brandnamic',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'beta',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '1.1.2',
	'constraints' => array(
		'depends' => array(
            'typo3' => '6.2'
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	)
);

?>