<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 04.03.2015
 * Time: 10:10
 */

namespace BN\BnSitemap\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class PageController extends ActionController
{
    /**
     * @var array
     */
    protected $sitemapInfo = array();

    /**
     * @var \BN\BnSitemap\Domain\Repository\PageRepository
     * @inject
     */
    protected $pageRepository;

    /**
     * @return array
     */
    public function generateSitemapAction()
    {
        $rootpage_uid = (empty($this->settings['customRootPage']) ? $GLOBALS['TSFE']->id : $this->settings['customRootPage']);
        if ($this->settings['includeRootPage']) {
            $pageArray = array($this->pageRepository->findByUid($rootpage_uid));
        } else {
            $pageArray = $this->pageRepository->findByPidNavHideDokTypeAndExcludeList($rootpage_uid, $this->settings['excludeUidList'])->toArray();
        }
        $this->addPagetree($pageArray);

        if (isset($this->settings['includePidList']) && $this->settings['includePidList'] != '') {
            $pidList = array_map('trim', explode(',', $this->settings['includePidList']));
            foreach ($pidList as $pid) {
                $pageArray = $this->pageRepository->findByPidNavHideDokTypeAndExcludeList($pid, $this->settings['excludeUidList'])->toArray();
                $this->addPagetree($pageArray);
            }
        }

        if (isset($this->settings['includeSingleUidList']) && $this->settings['includeSingleUidList'] != '') {
            $uidList = array_map('trim', explode(',', $this->settings['includeSingleUidList']));
            foreach ($uidList as $uid) {
                $pageArray = array($this->pageRepository->findByUid($uid));
                $this->addPagetree($pageArray, false);
            }
        }

        $this->view->assign('pagetree', $this->sitemapInfo);
    }

    /**
     * @param array $page
     * @param bool $includeSubpages
     */
    protected function addPagetree($pages, $includeSubpages = true)
    {
        /**
         * @var \BN\BnSitemap\Domain\Model\Page $page
         */
        foreach ($pages as $page) {
            $this->sitemapInfo[] = $page;
            if ($includeSubpages) {
                $this->addPagetree($this->pageRepository->findByPidNavHideDokTypeAndExcludeList($page->getUid(), $this->settings['excludeUidList'])->toArray());
            }
        }
    }
}