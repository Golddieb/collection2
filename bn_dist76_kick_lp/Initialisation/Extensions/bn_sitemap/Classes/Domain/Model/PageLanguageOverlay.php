<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 04.03.2015
 * Time: 10:12
 */

namespace BN\BnSitemap\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class PageLanguageOverlay extends AbstractEntity
{
    /**
     * @var integer
     */
    protected $doktype;

    /**
     * @var bool
     */
    protected $navHide;

    /**
     * @var int
     */
    protected $tStamp;

    /**
     * @var int
     */
    protected $crDate;

    /**
     * @var int
     */
    protected $sysLanguageUid;

    /**
     * @return int
     */
    public function getTStamp()
    {
        return $this->tStamp;
    }

    /**
     * @param int $tStamp
     */
    public function setTStamp($tStamp)
    {
        $this->tStamp = $tStamp;
    }

    /**
     * @return int
     */
    public function getCrDate()
    {
        return $this->crDate;
    }

    /**
     * @param int $crDate
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;
    }

    /**
     * @return int
     */
    public function getLastMod() {
        if (isset($this->tStamp) && $this->tStamp != 0) {
            return $this->tStamp;
        } else {
            return $this->crDate;
        }
    }

    /**
     * @return integer
     */
    public function getDoktype()
    {
        return $this->doktype;
    }

    /**
     * @param integer $doktype
     */
    public function setDoktype($doktype)
    {
        $this->doktype = $doktype;
    }

    /**
     * @return boolean
     */
    public function isNavHide()
    {
        return $this->navHide;
    }

    /**
     * @param boolean $navHide
     */
    public function setNavHide($navHide)
    {
        $this->navHide = $navHide;
    }

    /**
     * @return int
     */
    public function getSysLanguageUid()
    {
        return $this->sysLanguageUid;
    }

    /**
     * @param int $sysLanguageUid
     */
    public function setSysLanguageUid($sysLanguageUid)
    {
        $this->sysLanguageUid = $sysLanguageUid;
    }
}