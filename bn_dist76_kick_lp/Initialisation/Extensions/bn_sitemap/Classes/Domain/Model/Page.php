<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 04.03.2015
 * Time: 10:12
 */

namespace BN\BnSitemap\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class Page extends AbstractEntity
{
    /**
     * @var string
     */
    protected $changeFrequency;

    /**
     * @var float
     */
    protected $priority;

    /**
     * @var bool
     */
    protected $exclude;

    /**
     * @var integer
     */
    protected $doktype;

    /**
     * @var bool
     */
    protected $navHide;

    /**
     * @var int
     */
    protected $lastUpdated;

    /**
     * @var int
     */
    protected $sysLastChanged;

    /**
     * @var int
     */
    protected $tStamp;

    /**
     * @var int
     */
    protected $crDate;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BN\BnSitemap\Domain\Model\PageLanguageOverlay>
     */
    protected $languageOverlay;

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BN\BnSitemap\Domain\Model\PageLanguageOverlay>
     */
    public function getLanguageOverlay()
    {
        return $this->languageOverlay;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\BN\BnSitemap\Domain\Model\PageLanguageOverlay> $languageOverlay
     */
    public function setLanguageOverlay($languageOverlay)
    {
        $this->languageOverlay = $languageOverlay;
    }

    /**
     * @param \BN\BnSitemap\Domain\Model\PageLanguageOverlay $languageOverlay
     */
    public function addLanguageOverlay($languageOverlay)
    {
        $this->languageOverlay->attach($languageOverlay);
    }

    /**
     * @param \BN\BnSitemap\Domain\Model\PageLanguageOverlay $languageOverlay
     */
    public function removeLanguageOverlay($languageOverlay)
    {
        $this->languageOverlay->detach($languageOverlay);
    }

    public function isLocalized()
    {
        if($GLOBALS['TSFE']->sys_language_uid !== 0) {
            foreach ($this->languageOverlay as $lo) {
                if ($lo->getSysLanguageUid() === $GLOBALS['TSFE']->sys_language_uid) {
                    return true;
                }
            }
        } else {
            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param int $lastUpdated
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
    }

    /**
     * @return int
     */
    public function getSysLastChanged()
    {
        return $this->sysLastChanged;
    }

    /**
     * @param int $sysLastChanged
     */
    public function setSysLastChanged($sysLastChanged)
    {
        $this->sysLastChanged = $sysLastChanged;
    }

    /**
     * @return int
     */
    public function getTStamp()
    {
        return $this->tStamp;
    }

    /**
     * @param int $tStamp
     */
    public function setTStamp($tStamp)
    {
        $this->tStamp = $tStamp;
    }

    /**
     * @return int
     */
    public function getCrDate()
    {
        return $this->crDate;
    }

    /**
     * @param int $crDate
     */
    public function setCrDate($crDate)
    {
        $this->crDate = $crDate;
    }

    /**
     * @return int
     */
    public function getLastMod() {
        if (isset($this->lastUpdated) && $this->lastUpdated != 0) {
            return $this->lastUpdated;
        } elseif (isset($this->sysLastChanged) && $this->sysLastChanged != 0) {
            return $this->sysLastChanged;
        } elseif (isset($this->tStamp) && $this->tStamp != 0) {
            return $this->tStamp;
        } else {
            return $this->crDate;
        }
    }

    /**
     * @return integer
     */
    public function getDoktype()
    {
        return $this->doktype;
    }

    /**
     * @param integer $doktype
     */
    public function setDoktype($doktype)
    {
        $this->doktype = $doktype;
    }

    /**
     * @return boolean
     */
    public function isNavHide()
    {
        return $this->navHide;
    }

    /**
     * @param boolean $navHide
     */
    public function setNavHide($navHide)
    {
        $this->navHide = $navHide;
    }

    /**
     * @return string
     */
    public function getChangeFrequency()
    {
        return $this->changeFrequency;
    }

    /**
     * @param string $changeFrequency
     */
    public function setChangeFrequency($changeFrequency)
    {
        $this->changeFrequency = $changeFrequency;
    }

    /**
     * @return float
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param float $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return bool
     */
    public function isExclude()
    {
        return $this->exclude;
    }

    /**
     * @param bool $exclude
     */
    public function setExclude($exclude)
    {
        $this->exclude = $exclude;
    }
}