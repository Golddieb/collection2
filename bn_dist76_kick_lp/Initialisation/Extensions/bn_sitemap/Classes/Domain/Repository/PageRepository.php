<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 04.03.2015
 * Time: 10:15
 */

namespace BN\BnSitemap\Domain\Repository;


use TYPO3\CMS\Extbase\Persistence\Repository;

class PageRepository extends Repository
{
    public function initializeObject() {
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(FALSE);
        $querySettings->setRespectSysLanguage(FALSE);
        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * @param int $pid
     */
    public function findByPidNavHideDokTypeAndExcludeList($pid, $excludeList)
    {
        $query = $this->createQuery();
        //*
        $constraints = array(
            $query->equals('pid', $pid),
            $query->equals('navHide', 0),
            $query->lessThanOrEqual('doktype', 4)
        );

        if ($excludeList != '') {
            $excludeArray = array_map('trim', explode(',', $excludeList));
            $constraints[] = $query->logicalNot($query->in('uid', $excludeArray));
        }
        return $query->matching($query->logicalAnd($constraints))->execute();
        /*/

        if(is_array($excludeList)) {
            $excludeList = implode(',', $excludeList);
        }
        $excludeQuery = "";
        if($excludeList != '') {
            $excludeQuery = ' AND uid NOT IN ('.$excludeList.')';
        }
        
        $queryString = 'SELECT p.*, plo.uid, plo.pid, plo.sys_language_uid FROM pages AS p LEFT JOIN pages_language_overlay AS plo ON p.uid = plo.pid WHERE p.pid = '.$pid.' AND p.nav_hide = 0 AND p.doktype <= 4'.$excludeQuery.';';
        $query->statement($queryString);

        return $query->execute();
        //*/
    }
}

