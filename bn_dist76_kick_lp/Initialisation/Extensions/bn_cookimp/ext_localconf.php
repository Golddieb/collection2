<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'BN.' . $_EXTKEY,
    'Fe',
    array(
        'Cookie' => 'list'
    ),
    array()
);

$overlayFields = &$GLOBALS['TYPO3_CONF_VARS']['FE']['pageOverlayFields'];
if (!empty($overlayFields)) $overlayFields .= ',';
$overlayFields .= 'gtm_data_layer';

$TYPO3_CONF_VARS['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-all'][$_EXTKEY] = 'EXT:' . $_EXTKEY . '/Classes/Hook/PreRenderHook.php:&BN\\BnCookimp\\Hook\\PreRenderHook->addTagManager';
$TYPO3_CONF_VARS['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-preProcess'][$_EXTKEY] = 'EXT:' . $_EXTKEY . '/Classes/Hook/PreRenderHook.php:&BN\\BnCookimp\\Hook\\PreRenderHook->addJsFiles';