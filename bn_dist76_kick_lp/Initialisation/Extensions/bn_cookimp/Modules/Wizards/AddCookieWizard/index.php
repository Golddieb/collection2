<?php

/**
 * Wizard to add new records to a group/select TCEform formfield
 */
$editController = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('BN\\BnCookimp\\Controller\\Wizard\\AddCookieController');
$editController->main();
