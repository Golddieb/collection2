<?php

if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

$tmpConfiguration = array(
    'gtm_data_layer' => array(
        'label' => 'dataLayer Konfiguration',
        'exclude' => 1,
        'defaultExtras' => 'fixed-font:enable-tab',
        'config' => array(
            'type' => 'text',
            'cols' => 50,
            'rows' => 10,
            'wrap' => 'OFF'
        )
    )
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'pages_language_overlay',
    $tmpConfiguration,
    1
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages_language_overlay',
    '--div--;Tag Manager,gtm_data_layer'
);