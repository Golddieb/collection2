<?php

if (!defined ('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bncookimp_domain_model_cookie', 'EXT:bn_cookimp/Resources/Private/Language/locallang_csh_tx_bncookimp_domain_model_cookie.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bncookimp_domain_model_cookie');

return array(
    'ctrl' => array(
        'title'	=> 'LLL:EXT:bn_cookimp/Resources/Private/Language/locallang_db.xml:tx_bncookimp_domain_model_cookie',
        'label' => 'name',
        'rootLevel' => 1,
        'dividers2tabs' => TRUE,
        'sortby' => 'sorting',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'searchFields' => 'name,description',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('bn_cookimp') . 'Resources/Public/Icons/tx_bncookimp_domain_model_cookie.gif'
    ),
    'interface' => array(
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, name, description, origin, duration, category',
    ),
    'types' => array(
        '1' => array('showitem' => 'name;;1, description, origin, duration, category'),
    ),
    'palettes' => array(
        '1' => array('showitem' => 'sys_language_uid, l10n_parent')
    ),
    'columns' => array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
                ),
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_bncookimp_domain_model_cookie',
                'foreign_table_where' => 'AND tx_bncookimp_domain_model_cookie.pid=###CURRENT_PID### AND tx_bncookimp_domain_model_cookie.sys_language_uid IN (-1,0)',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
        'external_id' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:bn_cookimp/Resources/Private/Language/locallang_db.xml:tx_bncookimp_domain_model_cookie.external_id',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,unique'
            ),
        ),
        'name' => array(
            'exclude' => 1,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:bn_cookimp/Resources/Private/Language/locallang_db.xml:tx_bncookimp_domain_model_cookie.name',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'description' => array(
            'exclude' => 1,
            'l10n_display' => 'prefixLangTitle',
            'label' => 'LLL:EXT:bn_cookimp/Resources/Private/Language/locallang_db.xml:tx_bncookimp_domain_model_cookie.description',
            'config' => array(
                'type' => 'text',
                'cols' => 80,
                'rows' => 15,
                'eval' => 'trim'
            )
        ),
        'origin' => array(
            'exclude' => 1,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:bn_cookimp/Resources/Private/Language/locallang_db.xml:tx_bncookimp_domain_model_cookie.origin',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            )
        ),
        'duration' => array(
            'exclude' => 1,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:bn_cookimp/Resources/Private/Language/locallang_db.xml:tx_bncookimp_domain_model_cookie.duration',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            )
        ),
        'category' => array(
            'exclude' => 1,
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:bn_cookimp/Resources/Private/Language/locallang_db.xml:tx_bncookimp_domain_model_cookie.category',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('LLL:EXT:bn_cookimp/Resources/Private/Language/locallang_db.xml:tx_bncookimp_domain_model_cookie.category.I.0', 0),
                    array('LLL:EXT:bn_cookimp/Resources/Private/Language/locallang_db.xml:tx_bncookimp_domain_model_cookie.category.I.1', 1)
                )
            )
        )
    ),
);