<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Fe',
    'Cookiebeschreibungen'
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_fe';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_fe.xml');

/**
 * @TODO: add a wizard to export local data to external datasource
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModulePath(
    'wizard_add_cookie',
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Modules/Wizards/AddCookieWizard/'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/TagManager', 'Tag Manager');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/CookieBanner', 'Cookie Banner');

$TYPO3_CONF_VARS['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][$_EXTKEY] = 'EXT:' . $_EXTKEY . '/Classes/Hook/DatamapProcessorHook.php:&BN\\BnCookimp\\Hook\\DatamapProcessorHook';