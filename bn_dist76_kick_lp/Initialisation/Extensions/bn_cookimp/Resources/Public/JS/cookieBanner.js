// Drittanbieter-Profilierungen werden nur instanziert und somit gesetzt bei ausdruecklicher Zustimmung
// Laut Gesetz nicht noetig, zusaetzlicher Schutz.
(function($, window, document, undefined) {
    var BN = window.BN = window.BN || {};

    BN.CookieBanner = function (options) {
        var self = this,
            defaults = {
                id: 'cookiebanner',
                cookieName: 't3pcok',
                excludes: '',
                techcookies: [
                    "_ga",
                    "_gat",
                    "__utma",
                    "__utma",
                    "__utmt",
                    "__utmb",
                    "__utmc",
                    "__utmz",
                    "__utmv",
                    "fe_typo_user",
                    "be_typo_user",
                    "PHPSESSID",
                    "Typo3InstallTool",
                    "t3psob",
                    "t3pref",
                    "t3pfilter"
                ],
                activationSnippets: {},
                activationSnippetCallbacks: {},
                infolinks: {}
            };

        self.auxFunctions = {
            createCookie: function(name,value,days) {
                var expires = "";
                if (days) {
                    var date = new Date();
                    date.setTime(date.getTime()+(days*24*60*60*1000));
                    expires = "; expires="+date.toGMTString();
                }
                document.cookie = name+"="+value+expires+"; path=/";
            },
            readCookie: function(name) {
                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for(var i=0;i < ca.length;i++) {
                    var c = ca[i];
                    while (c.charAt(0)==' ') c = c.substring(1,c.length);
                    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                }
                return null;
            },
            eraseCookie: function(name) {
                self.auxFunctions.createCookie(name, "", -1);
            },
            createImageTag: function(path) {
                var img = new Image();

                img.src = path;

                img.onload = function() {
                    $("body").prepend(img);
                };
            },
            createDOMElement: function(tagname, attributes) {
                if (attributes.hasOwnProperty('id') && document.getElementById(attributes.id)) {
                    if (console) console.log('Tag with the ID ' + attributes.id + ' already exists.');
                    return false;
                }

                el = document.createElement(tagname);
                for (var key in attributes) {
                    if (attributes.hasOwnProperty(key)) {
                        el[key] = attributes[key];
                    }
                }

                return el;
            },
            insertElement: function (el, insertPoint, after) {
                if (after) {
                    insertPoint.parentNode.insertBefore(el, insertPoint.nextSibling);
                } else {
                    insertPoint.parentNode.insertBefore(el, insertPoint);
                }
            }
        };

        self.config = $.extend(true, {}, defaults, options);

        self.init();
    };

    BN.CookieBanner.prototype.reinit = function(options) {
        this.config = $.extend(true, {}, this.config, options);

        this.init();
    };

    BN.CookieBanner.prototype.init = function() {
        var self = this;

        self.container = $('#' + self.config.id);
        self.container.hide();

        self.cookieName = self.config.cookieName || 't3pcok';

        if (self.auxFunctions.readCookie(self.cookieName) !== '1') {
            self.deleteNonTrustedCookies();
            self.container.show();

            function cookieBannerPageClick(e) {
                if (!$(e.target).is(self.config.excludes)) {
                    self.auxFunctions.createCookie(self.cookieName, '1', 100);
                    self.executeActivationSnippets();
                    if (typeof self.config.accept === 'function') {
                        self.config.accept();
                    }
                    self.container.hide();
                    $(document).off('click', cookieBannerPageClick);
                }
            }

            $(document).on('click', cookieBannerPageClick);

            self.applyLinks();
        }
    };

    BN.CookieBanner.prototype.deleteNonTrustedCookies = function() {
        var self = this,
            excludeRegEx = new RegExp(self.config.techcookies.join('|'), 'i'),
            ca = document.cookie.split(';');

        for(var i=0;i < ca.length;i++) {
            var c = ca[i],
                name = c.split('=')[0].trim();

            if (!excludeRegEx.test(name)) {
                self.auxFunctions.eraseCookie(name);
            }
        }
    };

    BN.CookieBanner.prototype.applyLinks = function() {
        var self = this,
            links = self.config.infolinks;

        for (var key in links) {
            if (links.hasOwnProperty(key)) {
                (function (target) {
                    self.container.on('click', key, function(e) {
                        e.preventDefault();
                        window.location.href = target;
                    });
                }) (links[key]);
            }
        }
    };

    BN.CookieBanner.prototype.executeActivationSnippets = function() {
        var self = this,
            as = self.config.activationSnippets,
            asc = self.config.activationSnippetCallbacks;

        for (var key in as) {
            if (as.hasOwnProperty(key) && typeof as[key] === 'function') {
                as[key](self);

                if (asc.hasOwnProperty(key) && typeof asc[key] === 'function') {
                    asc[key](self);
                }
            }
        }
    };
})(jQuery, window, document);
