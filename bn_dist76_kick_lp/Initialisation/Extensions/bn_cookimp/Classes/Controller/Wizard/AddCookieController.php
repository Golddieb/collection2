<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 22.07.2015
 * Time: 14:04
 */

namespace BN\BnCookimp\Controller\Wizard;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class AddCookieController extends \TYPO3\CMS\Backend\Controller\Wizard\AddController {
    /**
     * @var \BN\BnCookimp\Service\CookieDataService
     */
    protected $cookieDataService = NULL;

    public function __construct()
    {
        $this->cookieDataService = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager')->get('BN\\BnCookimp\\Service\\CookieDataService');
        parent::__construct();
    }
    /**
     * Extend main functionality to write into remote DB if parameters are set
     *
     * @return void
     */
    public function main()
    {
        if ($this->returnEditConf && $this->processDataFlag && $this->cookieDataService->isCustomDb()) {
            $this->cookieDataService->insertRemote($this->id);
            $this->processDataFlag = 0;
        }
        parent::main();
    }
}