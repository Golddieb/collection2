<?php
namespace BN\BnCookimp\Controller;

class CookieController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
        /**
     * @var \BN\BnCookimp\Domain\Repository\CookieRepository
     * @inject
     */
    protected $cookieRepository = NULL;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        if (empty($this->settings['flexform']['cookies'])) {
            $cookies = $this->cookieRepository->findAll();
        } else {
            $cookies = $this->cookieRepository->findByUidList($this->settings['flexform']['cookies']);
        }

        $this->view->assign('category', $this->settings['flexform']['category']);
        $this->view->assign('cookies', $cookies);
    }
}