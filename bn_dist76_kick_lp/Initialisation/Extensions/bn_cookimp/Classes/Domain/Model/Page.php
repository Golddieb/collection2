<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 16.06.2015
 * Time: 15:16
 */

namespace BN\BnCookimp\Domain\Model;


class Page extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
    /**
     * @var string
     */
    protected $gtmDataLayer;

    /**
     * @return string
     */
    public function getGtmDataLayer()
    {
        return $this->gtmDataLayer;
    }

    /**
     * @param string $gtmDataLayer
     */
    public function setGtmDataLayer($gtmDataLayer)
    {
        $this->gtmDataLayer = $gtmDataLayer;
    }
}