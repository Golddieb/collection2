<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 20.07.2015
 * Time: 11:39
 */

namespace BN\BnCookimp\Domain\Model;


class Cookie extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
    /**
     * @var string
     */
    protected $externalId = '';
    /**
     * @var int
     */
    protected $sysLanguageUid = 0;
    /**
     * @var string
     */
    protected $name = '';
    /**
     * @var string
     */
    protected $description ='';
    /**
     * @var string
     */
    protected $origin ='';
    /**
     * @var string
     */
    protected $duration ='';
    /**
     * @var int
     */
    protected $category = 0;

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return int
     */
    public function getSysLanguageUid()
    {
        return $this->sysLanguageUid;
    }

    /**
     * @param int $sysLanguageUid
     */
    public function setSysLanguageUid($sysLanguageUid)
    {
        $this->sysLanguageUid = $sysLanguageUid;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param string $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param int $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }
}