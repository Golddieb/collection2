<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 20.07.2015
 * Time: 11:41
 */

namespace BN\BnCookimp\Domain\Repository;


class CookieRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

    /**
     * @param string|array $list
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByUidList($list) {
        if (is_string($list)) {
            $list = explode(',', $list);
        }
        $query = $this->createQuery();
        return $query->matching(
            $query->in(
                'uid',
                $list
            )
        )->execute();
    }
}