<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 22.07.2015
 * Time: 20:47
 */

namespace BN\BnCookimp\Hook;


use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class DatamapProcessorHook {

    /**
     * @var \BN\BnCookimp\Service\CookieDataService
     */
    protected $cookieDataService = NULL;

    /**
     * @var \BN\BnCookimp\Domain\Repository\CookieRepository
     */
    protected $cookieRepository;

    /**
     * initialise cookieDataService
     */
    public function __construct()
    {
        $this->cookieDataService = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager')->get('BN\\BnCookimp\\Service\\CookieDataService');
    }

    /**
     * @param array $row
     * @param string $table
     * @param string $id
     * @param DataHandler $tce
     */
    public function processDatamap_preProcessFieldArray(&$row, $table, $id, DataHandler $tce)
    {
        if ($table === 'tt_content' && $row['list_type'] === 'bncookimp_fe') {
            if ($this->cookieDataService->isCustomDb()) {
                if (!empty($row['pi_flexform']['data']['sDEF']['lDEF']['settings.flexform.cookies']['vDEF'])) {
                    $foreignCookies = $this->cookieDataService->getData(
                        array(
                            'external_id',
                            'name',
                            'description',
                            'origin',
                            'duration',
                            'category'
                        ),
                        array(
                            'sys_language_uid' => $row['sys_language_uid'],
                            'external_id' => $row['pi_flexform']['data']['sDEF']['lDEF']['settings.flexform.cookies']['vDEF']
                        )
                    );

                    foreach ($foreignCookies as $foreignCookie) {
                        $localCookie = $this->getDatabaseConnection()->exec_SELECTgetSingleRow(
                            'uid',
                            'tx_bncookimp_domain_model_cookie',
                            'external_id="' . $foreignCookie['external_id'] . '" AND sys_language_uid=' . $row['sys_language_uid']
                        );
                        $cUid = (empty($localCookie) ? 'NEW' . $foreignCookie['external_id'] : $localCookie['uid']);

                        $tce->datamap['tx_bncookimp_domain_model_cookie'][$cUid] = array();
                        foreach ($foreignCookie as $field => $value) {
                            $tce->datamap['tx_bncookimp_domain_model_cookie'][$cUid][$field] = $value;
                        }
                    }
                }
            }
        }

        if ($table === 'tx_bncookimp_domain_model_cookie') {
            $row['external_id'] = md5($row['name'] . $row['sys_language_uid']);
            if ($row['sys_language_uid'] > 0) {
                $parent = $this->getDatabaseConnection()->exec_SELECTgetSingleRow(
                    'uid',
                    'tx_bncookimp_domain_model_cookie',
                    'name="' . $row['name'] . '" AND sys_language_uid=0'
                );
                if (!empty($parent)) {
                    $row['l10n_parent'] = $parent['uid'];
                }
            }

            if ($this->cookieDataService->isCustomDb()) {
                $this->cookieDataService->insertRemote($row['external_id']);
            }
        }
    }

    /**
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}