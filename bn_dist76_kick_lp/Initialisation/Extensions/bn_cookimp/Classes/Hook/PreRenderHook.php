<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 18.12.2014
 * Time: 18:52
 */

namespace BN\BnCookimp\Hook;

use BN\BnCookimp\Service\TagManagerService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class PreRenderHook {

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager = NULL;

    /**
     * @var \BN\BnCookimp\Service\ConfigurationService
     */
    protected $configurationService = NULL;

    /**
     * @var \BN\BnCookimp\Domain\Repository\PageRepository
     */
    protected $pageRepository = NULL;

    /**
     * initialise objectManager
     */
    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
    }

    /**
     * @return \BN\BnCookimp\Service\ConfigurationService
     */
    protected function getConfigurationService()
    {
        if (empty($this->configurationManager)) {
            $this->configurationService = $this->objectManager->get('BN\\BnCookimp\\Service\\ConfigurationService');
        }
        return $this->configurationService;
    }

    /**
     * @return \BN\BnCookimp\Domain\Repository\PageRepository
     */
    protected function getPageRepository()
    {
        if (empty($this->pageRepository)) {
            $this->pageRepository = $this->objectManager->get('BN\\BnCookimp\\Domain\\Repository\\PageRepository');
        }
        return $this->pageRepository;
    }

    /**
     * @param array $conf
     * @param \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $parent
     */
    public function addTagManager($conf, $parent)
    {
        if (defined('TYPO3_MODE') && TYPO3_MODE === 'FE') {
            /**
             * @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $GLOBALS['TSFE']
             */
            $container_id = $this->getConfigurationService()->getSetting('gtm_container_id');

            if (!empty($container_id) && strpos($conf['pObj']->content, '#id1443539507#') === FALSE) {
                /**
                 * @var \BN\BnCookimp\Domain\Model\Page $page
                 */
                $page = $this->getPageRepository()->findByUid($GLOBALS['TSFE']->id);

                $tagManager = new TagManagerService($container_id, $page->getGtmDataLayer());
                $conf['pObj']->content = preg_replace('/(<head>[\s\S]*?<\/head>[\s\S]*?<body[^>]*>)/i', '$1 ' . $tagManager->getCode(), $conf['pObj']->content, 1);
            }
        }
    }

    /**
     * @param array $jsLibs
     * @param \TYPO3\CMS\Core\Page\PageRenderer $pageRenderer
     */
    public function addJSFiles($jsLibs, $pageRenderer) {
        if (defined('TYPO3_MODE') && TYPO3_MODE === 'FE') {
            $filepath = GeneralUtility::getFileAbsFileName($this->getConfigurationService()->getSetting('js_library', 'EXT:bn_cookimp/Resources/Public/JS/cookieBanner.js'));

            if (is_file($filepath)) {
                $relPath = GeneralUtility::removePrefixPathFromList(array($filepath), PATH_site);
                $jsLibs['jsFiles'][$relPath[0]] = array(
                    'file' => $relPath[0],
                    'type' => 'text/javascript',
                    'section' => 2,
                    'compress' => true,
                    'forceOnTop' => true,
                    'allWrap' => null,
                    'excludeFromConcatenation' => false,
                    'splitChar' => null
                );
            }
        }
    }
}