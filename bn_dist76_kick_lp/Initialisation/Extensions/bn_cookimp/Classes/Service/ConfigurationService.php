<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 22.07.2015
 * Time: 10:35
 */

namespace BN\BnCookimp\Service;

use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class ConfigurationService implements \TYPO3\CMS\Core\SingletonInterface {
    /**
     * @var array
     */
    protected $settings;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @inject
     */
    protected $configurationManager = NULL;

    /**
     * @return array
     */
    public function getSettings()
    {
        if (empty($this->settings)) {
            $framework = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
            $this->settings = (empty($framework['tx_bncookimp']['settings']) ?
                array()
            :
                $framework['tx_bncookimp']['settings']
            );
        }

        return $this->settings;
    }

    /**
     * @param string $path
     * @param string $defaultValue
     * @return array|string
     */
    public function getSetting($path, $defaultValue = '')
    {
        $settings = $this->getSettings();

        $steps = explode('.', $path);
        $result = $settings;
        foreach ($steps as $step) {
            if (empty($result[$step])) {
                return $defaultValue;
            } else {
                $result = $result[$step];
            }
        }
        return $result;
    }

    /**
     * @param string $default
     * @return array
     */
    public function getLanguageMap($default = '0:0,1:1,2:2')
    {
        $languages = explode(',', $this->getSetting('language_mapping', $default));
        $languageMap = array();
        foreach ($languages as $language) {
            list($local, $foreign) = array_map('trim', explode(':', $language));
            $languageMap[$local] = $foreign;
        }
        return $languageMap;
    }
}