<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 09.01.2015
 * Time: 14:18
 */

namespace BN\BnCookimp\Service;

use \TYPO3\CMS\Core\TypoScript\Parser\TypoScriptParser;
use \TYPO3\CMS\Extbase\Service\TypoScriptService;
use \TYPO3\CMS\Extbase\Configuration\Exception;

class TagManagerService {

    /**
     * @var string
     */
    protected $containerId;

    /**
     * @var array
     */
    protected $dataLayer;

    /**
     * @param string $containerId
     * @param string|array $dataLayer
     */
    public function __construct($containerId, $dataLayer = '')
    {
        $this->containerId = $containerId;
        $this->setDataLayer($dataLayer);
    }

    /**
     * @param string|array $dataLayer
     */
    public function setDataLayer($dataLayer)
    {
        if (is_array($dataLayer)) {
            $this->dataLayer = $dataLayer;
        } else {
            $this->dataLayer = $this->parseText($dataLayer);
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getCode()
    {
        return <<<EOT

<!-- Google Tag Manager -->
    <script>
        dataLayer = [{$this->getDataLayerString()}];
    </script>
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id={$this->containerId}{$this->getDataLayerString('query')}" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>
        (function (w,d,s,l,i) {
            w[l]=w[l]||[];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event:'gtm.js'
            });
            var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),
                dl=l!='dataLayer'?'&l='+l:'';
                j.async=true;
            j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;
            f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','{$this->containerId}');
    </script>
<!-- End Google Tag Manager -->
<!-- added by bn_cookimp extension #id1443539507# -->

EOT;
    }

    /**
     * @param string $format
     * @return string
     * @throws \Exception
     */
    protected function getDataLayerString($format = 'json')
    {
        if (empty($this->dataLayer)) {
            return '';
        }

        switch ($format) {
            case 'json':
                return json_encode($this->dataLayer, JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE);
                break;
            case 'query':
                return $this->generateQueryString($this->dataLayer);
                break;
            default:
                throw new Exception('There is no format ' . $format . ' for the dataLayer string', 1434393320);
        }
    }

    /**
     * @param array $data
     * @param string $prefix
     * @return string
     */
    protected function generateQueryString($data, $prefix = '')
    {
        $queryString = '';
        foreach ($data as $key => $value) {
            $fullKey = (empty($prefix) ? '&' . $key : $prefix . '[' . $key . ']');
            if (is_array($value)) {
                $queryString .= $this->generateQueryString($value, $fullKey);
            } else {
                $queryString .= $fullKey . '=' . $value;
            }
        }

        return $queryString;
    }

    /**
     * @param string $input
     * @return null|array
     */
    protected function parseText($input)
    {
        if (!empty($input)) {
            $tsParser = new TypoScriptParser();
            $tsParser->parse($input);
            $tsService = new TypoScriptService();
            return $tsService->convertTypoScriptArrayToPlainArray($tsParser->setup);
        } else {
            return NULL;
        }
    }
}