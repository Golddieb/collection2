<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 21.07.2015
 * Time: 20:21
 */

namespace BN\BnCookimp\Service;

use TYPO3\CMS\Core\Utility\GeneralUtility;

class CookieDataService {
    /**
     * @var \BN\BnCookimp\Service\ConfigurationService
     * @inject
     */
    protected $configurationService = NULL;

    /**
     * @var \BN\BnCookimp\Domain\Repository\CookieRepository
     * @inject
     */
    protected $cookieRepository = NULL;

    /**
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected $databaseHandle = NULL;

    /**
     * @param array $fields
     * @param array $constraints
     *
     * @return array
     */
    public function getData($fields, $constraints = NULL)
    {
        $fieldMap = array();
        foreach ($fields as $field) {
            $fieldMap[$field] = $this->configurationService->getSetting('field_mapping.' . $field, $field);
        }

        $where = '1=1';
        if (isset($constraints)) {
            foreach ($constraints as $field => $constraint) {
                if ($field === 'sys_language_uid') {
                    $constraint = $this->mapLanguageValues($constraint);
                }
                $mappedField = $this->configurationService->getSetting('field_mapping.' . $field, $field);

                $where .= ' AND ' . $mappedField . ' IN (' . (is_array($constraint) ? implode(',', $constraint) : $constraint) . ')';
            }
        }

        $table = $this->configurationService->getSetting('table', 'tx_bncookimp_domain_model_cookie');

        $cookies = $this->getDatabaseConnection()->exec_SELECTquery(implode(',', $fieldMap), $table, $where);

        $result = array();
        while ($cookie = $cookies->fetch_assoc()) {
            $data = array();
            foreach ($fieldMap as $field => $key) {
                $data[$field] = $cookie[$key];
            }
            $result[] = $data;
        }

        return $result;
    }

    /**
     * @param int $externalId
     */
    public function insertRemote($externalId)
    {
        $mapping = $this->configurationService->getSetting(
            'field_mapping',
            array(
                'external_id' => 'external_id',
                'sys_language_uid' => 'sys_language_uid',
                'name' => 'name',
                'description' => 'description',
                'origin' => 'origin',
                'duration' => 'duration',
                'category' => 'category'
            )
        );
        $table = $this->configurationService->getSetting('table', 'tx_bncookimp_domain_model_cookie');

        /**
         * @var \BN\BnCookimp\Domain\Model\Cookie
         */
        $cookie = $this->cookieRepository->findOneByExternalId($externalId);
        if (!empty($cookie)) {
            $insertValues = array();
            foreach ($mapping as $local => $foreign) {
                $value = $cookie->{'get' . GeneralUtility::underscoredToUpperCamelCase($local)}();
                if ($local === 'sys_language_uid') {
                    $langMap = $this->configurationService->getLanguageMap();
                    $value = $langMap[$value];
                }
                $insertValues[$foreign] = $value;
            }
            if (!$this->getDatabaseConnection()->exec_UPDATEquery($table, $mapping['external_id'] . '=' . $externalId, $insertValues)) {
                $this->getDatabaseConnection()->exec_INSERTquery($table, $insertValues);
            };
        }
    }

    /**
     * @return bool
     */
    public function isCustomDb()
    {
        $dbInfo = $this->checkCustomDBInfo();
        return (!empty($dbInfo));
    }

    /**
     * @param array|int $input
     * @return array
     */
    protected function mapLanguageValues($input)
    {
        $langMap = $this->configurationService->getLanguageMap();
        if (is_array($input)) {
            $output = array();
            foreach ($input as $value) {
                $output[] = $langMap[$value];
            }
        } else {
            $output = $langMap[$input];
        }

        return $output;
    }

    /**
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        if (empty($this->databaseHandle)) {
            if ($dbInfo = $this->checkCustomDBInfo()) {
                /**
                 * @var $databaseConnection \TYPO3\CMS\Core\Database\DatabaseConnection
                 */
                $databaseConnection = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Database\\DatabaseConnection');
                $databaseConnection->setDatabaseName($dbInfo['name']);
                $databaseConnection->setDatabaseUsername($dbInfo['user']);
                $databaseConnection->setDatabasePassword($dbInfo['password']);

                $host = $dbInfo['host'];
                if (empty($dbInfo['port'])) {
                    list($host, $port) = explode(':', $host);
                    $databaseConnection->setDatabasePort($port);
                } elseif (strpos($host, ':') > 0) {
                    $databaseConnection->setDatabasePort($dbInfo['port']);
                }
                $databaseConnection->setDatabaseHost($host);

                if (empty($dbInfo['socket'])) {
                    $databaseConnection->setDatabaseSocket($dbInfo['socket']);
                }

                $databaseConnection->debugOutput = $dbInfo['debug'];

                if (!($host === 'localhost' || $host === '127.0.0.1' || $host === '::1')) {
                    $databaseConnection->setConnectionCompression(TRUE);
                }

                $this->databaseHandle = $databaseConnection;
                $this->databaseHandle->initialize();
            } else {
                $this->databaseHandle = $GLOBALS['TYPO3_DB'];
            }
        }

        return $this->databaseHandle;
    }

    /**
     * @return array|bool
     */
    protected function checkCustomDBInfo()
    {
        $dbSettings = $this->configurationService->getSetting('database');
        if (empty($dbSettings)) {
            return FALSE;
        } else {
            $dbInfo = array();
            foreach ($dbSettings as $info => $value) {
                if ($info !== 'socket' && $info !== 'port' && $info !== 'debug' && empty($value)) {
                    return FALSE;
                }
                $dbInfo[$info] = $value;
            }

            return $dbInfo;
        }
    }
}