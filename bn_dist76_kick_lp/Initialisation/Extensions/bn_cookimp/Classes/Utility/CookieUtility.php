<?php
/**
 * Created by PhpStorm.
 * User: Michael Marcenich
 * Date: 21.07.2015
 * Time: 20:21
 */

namespace BN\BnCookimp\Utility;


use TYPO3\CMS\Core\Utility\GeneralUtility;

class CookieUtility {
    /**
     * @var array
     */
    protected $settings = array();

    /**
     * @var \BN\BnCookimp\Service\CookieDataService
     */
    protected $cookieDataService = NULL;

    /**
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected $databaseHandle = NULL;

    /**
     * instantiate objectManager and settings
     */
    public function __construct()
    {
        $this->cookieDataService = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager')->get('BN\\BnCookimp\\Service\\CookieDataService');
    }

    /**
     * @return array
     */
    public function getCookieSelectionItems(&$PA, $fObj)
    {
        $piValues = GeneralUtility::xml2array($PA['row']['pi_flexform']);
        $category = $piValues['data']['sDEF']['lDEF']['settings.flexform.category']['vDEF'];
        $cookies = $this->cookieDataService->getData(
            array('external_id','name'),
            array(
                'category' => (empty($category) ? 0 : $category),
                'sys_language_uid' => $PA['row']['sys_language_uid']
            )
        );
        foreach ($cookies as $cookie) {
            $PA['items'][] = array($cookie['name'], $cookie['external_id']);
        }
    }
}