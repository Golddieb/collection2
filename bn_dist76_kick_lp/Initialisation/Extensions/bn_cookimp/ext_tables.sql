#
# Table structure for table 'tx_bncookimp_domain_model_cookie'
#
CREATE TABLE tx_bncookimp_domain_model_cookie (
  uid int(11) NOT NULL auto_increment,
  pid int(11) DEFAULT '0' NOT NULL,
  external_id varchar(255) DEFAULT '' NOT NULL,

  name varchar(255) DEFAULT '' NOT NULL,
  description longtext NOT NULL,
  origin varchar(255) DEFAULT '' NOT NULL,
  duration varchar(255) DEFAULT '' NOT NULL,
  category int(11) DEFAULT '0' NOT NULL,

  sorting int(11) DEFAULT '0' NOT NULL,

  sys_language_uid int(11) DEFAULT '0' NOT NULL,
  l10n_parent int(11) DEFAULT '0' NOT NULL,
  l10n_diffsource mediumblob,

  PRIMARY KEY (uid),
  KEY parent (pid),
  UNIQUE KEY identity (external_id),
  KEY language (l10n_parent,sys_language_uid)
);

#
# Table structure for table 'pages'
#
CREATE TABLE pages (
  gtm_data_layer longtext NOT NULL
);

#
# Table structure for table 'pages_language_overlay'
#
CREATE TABLE pages_language_overlay (
  gtm_data_layer longtext NOT NULL
);