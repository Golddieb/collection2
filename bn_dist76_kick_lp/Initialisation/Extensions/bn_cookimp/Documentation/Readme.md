# Google Tag Manager und Cookiebanner

Nach der Installation der Extension ist die cookiebanner.js Bibliothek (fortan: CBB) sofort verfügbar.
Für die Einbindung des Google Tag Managers (fortan: GTM) die angabe der GTM-Container ID zwingend erforderlich.
Alles weitere wird in den nächsten Abschnitten beschrieben.

## Statische Templates

### Tag Manager: Base

Nach Einbindung sind im Konstant-Editor folgende Konfigurationsmöglichkeiten verfügbar:

* __Container ID:__ Die ContainerID aus dem Backend des GTM
* __Cookie Banner Library:__ Möglichkeit eine alternative CBB einzubinden

### Tag Manager: Example configuration

Nach Einbindung ist eine rudimentäre Implementierung des Cookiebanners verfügbar.
Es muss lediglich im Template ein `COOKIEBANNER`-Subpart vorgesehen werden.