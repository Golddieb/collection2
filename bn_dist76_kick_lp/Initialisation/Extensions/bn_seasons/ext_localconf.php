<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Brandnamic.' . $_EXTKEY,
	'Fp',
	array(
		'Page' => 'pics, autopics, css',
	),
	// non-cacheable actions
	array(
		'Page' => '',
		'Seasonmedia' => '',
	)
);
