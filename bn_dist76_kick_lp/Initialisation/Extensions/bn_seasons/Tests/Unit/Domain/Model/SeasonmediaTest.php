<?php

namespace Brandnamic\BnSeasons\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *           Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Brandnamic\BnSeasons\Domain\Model\Seasonmedia.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Alex Complojer <alex.complojer@brandnamic.com>
 * @author Alex Complojer <alex.complojer@brandnamic.com>
 */
class SeasonmediaTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {
	/**
	 * @var \Brandnamic\BnSeasons\Domain\Model\Seasonmedia
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = new \Brandnamic\BnSeasons\Domain\Model\Seasonmedia();
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getHeaderimageReturnsInitialValueForFileReference() {
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getHeaderimage()
		);
	}

	/**
	 * @test
	 */
	public function setHeaderimageForFileReferenceSetsHeaderimage() {
		$headerimage = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$objectStorageHoldingExactlyOneHeaderimage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneHeaderimage->attach($headerimage);
		$this->subject->setHeaderimage($objectStorageHoldingExactlyOneHeaderimage);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneHeaderimage,
			'headerimage',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addHeaderimageToObjectStorageHoldingHeaderimage() {
		$headerimage = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$headerimageObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$headerimageObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($headerimage));
		$this->inject($this->subject, 'headerimage', $headerimageObjectStorageMock);

		$this->subject->addHeaderimage($headerimage);
	}

	/**
	 * @test
	 */
	public function removeHeaderimageFromObjectStorageHoldingHeaderimage() {
		$headerimage = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
		$headerimageObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$headerimageObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($headerimage));
		$this->inject($this->subject, 'headerimage', $headerimageObjectStorageMock);

		$this->subject->removeHeaderimage($headerimage);

	}

	/**
	 * @test
	 */
	public function getHeadertextReturnsInitialValueForString() {
		$this->assertSame(
			'',
			$this->subject->getHeadertext()
		);
	}

	/**
	 * @test
	 */
	public function setHeadertextForStringSetsHeadertext() {
		$this->subject->setHeadertext('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'headertext',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSeasonReturnsInitialValueForSeason() {
		$this->assertEquals(
			NULL,
			$this->subject->getSeason()
		);
	}

	/**
	 * @test
	 */
	public function setSeasonForSeasonSetsSeason() {
		$seasonFixture = new \Brandnamic\BnSeasons\Domain\Model\Season();
		$this->subject->setSeason($seasonFixture);

		$this->assertAttributeEquals(
			$seasonFixture,
			'season',
			$this->subject
		);
	}
}
