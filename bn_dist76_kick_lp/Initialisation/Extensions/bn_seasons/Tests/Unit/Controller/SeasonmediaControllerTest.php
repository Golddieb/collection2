<?php
namespace Brandnamic\BnSeasons\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *  			Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class Brandnamic\BnSeasons\Controller\SeasonmediaController.
 *
 * @author Alex Complojer <alex.complojer@brandnamic.com>
 * @author Alex Complojer <alex.complojer@brandnamic.com>
 */
class SeasonmediaControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @var \Brandnamic\BnSeasons\Controller\SeasonmediaController
	 */
	protected $subject = NULL;

	protected function setUp() {
		$this->subject = $this->getMock('Brandnamic\\BnSeasons\\Controller\\SeasonmediaController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	protected function tearDown() {
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllSeasonmediasFromRepositoryAndAssignsThemToView() {

		$allSeasonmedias = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$seasonmediaRepository = $this->getMock('Brandnamic\\BnSeasons\\Domain\\Repository\\SeasonmediaRepository', array('findAll'), array(), '', FALSE);
		$seasonmediaRepository->expects($this->once())->method('findAll')->will($this->returnValue($allSeasonmedias));
		$this->inject($this->subject, 'seasonmediaRepository', $seasonmediaRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('seasonmedias', $allSeasonmedias);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenSeasonmediaToView() {
		$seasonmedia = new \Brandnamic\BnSeasons\Domain\Model\Seasonmedia();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('seasonmedia', $seasonmedia);

		$this->subject->showAction($seasonmedia);
	}
}
