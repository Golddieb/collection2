<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_bnseasons_domain_model_season'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_bnseasons_domain_model_season']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, name, startmonth, endmonth, styleclass',
	),
	'types' => array(
		'1' => array('showitem' => 'name, startmonth, endmonth, styleclass'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'renderType' => 'selectSingle',
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_bnseasons_domain_model_season',
				'foreign_table_where' => 'AND tx_bnseasons_domain_model_season.pid=###CURRENT_PID### AND tx_bnseasons_domain_model_season.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'startmonth' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:bn_seasons/Resources/Private/Language/locallang_db.xlf:tx_bnseasons_domain_model_season.startmonth',
			'config' => array(
				'renderType' => 'selectSingle',
				'type' => 'select',
				'items' => array(
                    array('Jan', 1),
                    array('Feb', 2),
                    array('Mar', 3),
                    array('Apr', 4),
                    array('Mai', 5),
                    array('Jun', 6),
                    array('Jul', 7),
                    array('Aug', 8),
                    array('Sep', 9),
                    array('Okt', 10),
                    array('Nov', 11),
                    array('Dez', 12)
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'endmonth' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:bn_seasons/Resources/Private/Language/locallang_db.xlf:tx_bnseasons_domain_model_season.endmonth',
			'config' => array(
				'renderType' => 'selectSingle',
				'type' => 'select',
				'items' => array(
                    array('Jan', 1),
                    array('Feb', 2),
                    array('Mar', 3),
                    array('Apr', 4),
                    array('Mai', 5),
                    array('Jun', 6),
                    array('Jul', 7),
                    array('Aug', 8),
                    array('Sep', 9),
                    array('Okt', 10),
                    array('Nov', 11),
                    array('Dez', 12)
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'styleclass' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:bn_seasons/Resources/Private/Language/locallang_db.xlf:tx_bnseasons_domain_model_season.styleclass',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
        'name' => array(
            'exclude' => 1,
            'label' => 'Name',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
		
	),
);
