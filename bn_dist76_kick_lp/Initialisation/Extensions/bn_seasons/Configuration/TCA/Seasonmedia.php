<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_bnseasons_domain_model_seasonmedia'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_bnseasons_domain_model_seasonmedia']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, headerimage, headertext, season',
	),
	'types' => array(
		'1' => array('showitem' => 'season, headerimage, headertext'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'iconfile' => 'EXT:bn_seasons/Resources/Public/Icons/tx_bnseasons_domain_model_season.gif',
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'renderType' => 'selectSingle',
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'renderType' => 'selectSingle',
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_bnseasons_domain_model_seasonmedia',
				'foreign_table_where' => 'AND tx_bnseasons_domain_model_seasonmedia.pid=###CURRENT_PID### AND tx_bnseasons_domain_model_seasonmedia.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),

		'headerimage' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:seasons/Resources/Private/Language/locallang_db.xlf:tx_seasons_domain_model_seasonmedia.headerimage',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'headerimage',
				array(
					'maxitems' => 10,
					'foreign_types' => array(
						'0' => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
							'showitem' => '
								--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
								--palette--;;filePalette'
						),
					)
				),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		'headertext' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:bn_seasons/Resources/Private/Language/locallang_db.xlf:tx_bnseasons_domain_model_seasonmedia.headertext',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'season' => array(
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
			'exclude' => 1,
			'label' => 'LLL:EXT:bn_seasons/Resources/Private/Language/locallang_db.xlf:tx_bnseasons_domain_model_seasonmedia.season',
			'config' => array(
				'renderType' => 'selectSingle',
				'type' => 'select',
				'foreign_table' => 'tx_bnseasons_domain_model_season',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		
		'page' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
	),
);
