<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Fp',
	'Saisonale Headerbilder'
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_fp';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_fp.xml');

if (TYPO3_MODE === 'BE') {

	/**
	 * Registers a Backend Module
	 */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'Brandnamic.' . $_EXTKEY,
		'tools',	 // Make module a submodule of 'tools'
		'bm',	// Submodule key
		'',						// Position
		array(
			'Page' => 'list, show, pics, autopics, css','Seasonmedia' => 'list, show',
		),
		array(
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/ext_icon.gif',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_bm.xlf',
		)
	);

}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Saisonale Medien');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bnseasons_domain_model_seasonmedia', 'EXT:bn_seasons/Resources/Private/Language/locallang_csh_tx_bnseasons_domain_model_seasonmedia.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bnseasons_domain_model_seasonmedia');
$GLOBALS['TCA']['tx_bnseasons_domain_model_seasonmedia'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:bn_seasons/Resources/Private/Language/locallang_db.xlf:tx_bnseasons_domain_model_seasonmedia',
		'label' => 'headerimage',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',

		),
		'searchFields' => 'headerimage,headertext,season,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Seasonmedia.php',
		'iconfile' => 'EXT:bn_seasons/Resources/Public/Icons/tx_bnseasons_domain_model_seasonmedia.gif'
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_bnseasons_domain_model_season', 'EXT:bn_seasons/Resources/Private/Language/locallang_csh_tx_bnseasons_domain_model_season.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_bnseasons_domain_model_season');
$GLOBALS['TCA']['tx_bnseasons_domain_model_season'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:bn_seasons/Resources/Private/Language/locallang_db.xlf:tx_bnseasons_domain_model_season',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',

		'enablecolumns' => array(

		),
		'searchFields' => 'startmonth,endmonth,styleclass,',
		'dynamicConfigFile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TCA/Season.php',
		'iconfile' => 'EXT:bn_seasons/Resources/Public/Icons/tx_bnseasons_domain_model_season.gif'
	),
);

$tmp_seasons_columns = array(

	'seasonmedias' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:bn_seasons/Resources/Private/Language/locallang_db.xlf:tx_bnseasons_domain_model_page.seasonmedias',
		'config' => array(
			'type' => 'inline',
			'foreign_table' => 'tx_bnseasons_domain_model_seasonmedia',
			'foreign_field' => 'page',
			'maxitems'      => 9999,
			'appearance' => array(
				'collapseAll' => 0,
				'levelLinksPosition' => 'top',
				'showSynchronizationLink' => 1,
				'showPossibleLocalizationRecords' => 1,
				'useSortable' => 1,
				'showAllLocalizationLink' => 1
			),
		),

	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages',$tmp_seasons_columns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages_language_overlay',$tmp_seasons_columns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages', '--div--;Saisonale Medien,seasonmedias');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('pages_language_overlay', '--div--;Saisonale Medien,seasonmedias');

//$GLOBALS['TCA']['pages']['types']['Tx_BnSeasons_Page']['showitem'] = $TCA['pages']['types']['1']['showitem'];
//$GLOBALS['TCA']['pages']['types']['Tx_BnSeasons_Page']['showitem'] .= ',--div--;LLL:EXT:bn_seasons/Resources/Private/Language/locallang_db.xlf:tx_bnseasons_domain_model_page,';
//$GLOBALS['TCA']['pages']['types']['Tx_BnSeasons_Page']['showitem'] .= 'seasonmedias';
//$GLOBALS['TCA']['pages']['columns'][$TCA['pages']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:bn_seasons/Resources/Private/Language/locallang_db.xlf:pages.tx_extbase_type.Tx_BnSeasons_Page','Tx_BnSeasons_Page');


