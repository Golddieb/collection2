<?php
namespace Brandnamic\BnSeasons\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *           Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Season
 */
class Season extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * Startmonat
	 *
	 * @var integer
	 */
	protected $startmonth = 0;

	/**
	 * Endmonat
	 *
	 * @var integer
	 */
	protected $endmonth = 0;

	/**
	 * Style-Id oder Klasse
	 *
	 * @var string
	 */
	protected $styleclass = '';


    /**
     * Name
     *
     * @var string
     */
    protected $name = '';

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

	/**
	 * Returns the startmonth
	 *
	 * @return integer $startmonth
	 */
	public function getStartmonth() {
		return $this->startmonth;
	}

	/**
	 * Sets the startmonth
	 *
	 * @param integer $startmonth
	 * @return void
	 */
	public function setStartmonth($startmonth) {
		$this->startmonth = $startmonth;
	}

	/**
	 * Returns the endmonth
	 *
	 * @return integer $endmonth
	 */
	public function getEndmonth() {
		return $this->endmonth;
	}

	/**
	 * Sets the endmonth
	 *
	 * @param integer $endmonth
	 * @return void
	 */
	public function setEndmonth($endmonth) {
		$this->endmonth = $endmonth;
	}

	/**
	 * Returns the styleclass
	 *
	 * @return string $styleclass
	 */
	public function getStyleclass() {
		return $this->styleclass;
	}

	/**
	 * Sets the styleclass
	 *
	 * @param string $styleclass
	 * @return void
	 */
	public function setStyleclass($styleclass) {
		$this->styleclass = $styleclass;
	}

}