<?php
namespace Brandnamic\BnSeasons\Domain\Model;

class PageLanguageOverlay extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * Saisonale Medien
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Brandnamic\BnSeasons\Domain\Model\Seasonmedia>
     * @cascade remove
     */
    protected $seasonmedias = NULL;

    /**
     * Adds a Seasonmedia
     *
     * @param \Brandnamic\BnSeasons\Domain\Model\Seasonmedia $seasonmedia
     * @return void
     */
    public function addSeasonmedia(\Brandnamic\BnSeasons\Domain\Model\Seasonmedia $seasonmedia) {
        $this->seasonmedias->attach($seasonmedia);
    }

    /**
     * Removes a Seasonmedia
     *
     * @param \Brandnamic\BnSeasons\Domain\Model\Seasonmedia $seasonmediaToRemove The Seasonmedia to be removed
     * @return void
     */
    public function removeSeasonmedia(\Brandnamic\BnSeasons\Domain\Model\Seasonmedia $seasonmediaToRemove) {
        $this->seasonmedias->detach($seasonmediaToRemove);
    }

    /**
     * Returns the seasonmedias
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Brandnamic\BnSeasons\Domain\Model\Seasonmedia> $seasonmedias
     */
    public function getSeasonmedias() {
        return $this->seasonmedias;
    }

    /**
     * Sets the seasonmedias
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Brandnamic\BnSeasons\Domain\Model\Seasonmedia> $seasonmedias
     * @return void
     */
    public function setSeasonmedias(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $seasonmedias) {
        $this->seasonmedias = $seasonmedias;
    }

}


?>