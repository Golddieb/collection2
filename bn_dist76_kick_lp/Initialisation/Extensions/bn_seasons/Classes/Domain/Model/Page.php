<?php
namespace Brandnamic\BnSeasons\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *           Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Page
 */
class Page extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * Saisonale Medien
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Brandnamic\BnSeasons\Domain\Model\Seasonmedia>
	 * @cascade remove
	 */
	protected $seasonmedias = NULL;

    /**
     * @var string
     */
    protected $title = NULL;

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->seasonmedias = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Adds a Seasonmedia
	 *
	 * @param \Brandnamic\BnSeasons\Domain\Model\Seasonmedia $seasonmedia
	 * @return void
	 */
	public function addSeasonmedia(\Brandnamic\BnSeasons\Domain\Model\Seasonmedia $seasonmedia) {
		$this->seasonmedias->attach($seasonmedia);
	}

	/**
	 * Removes a Seasonmedia
	 *
	 * @param \Brandnamic\BnSeasons\Domain\Model\Seasonmedia $seasonmediaToRemove The Seasonmedia to be removed
	 * @return void
	 */
	public function removeSeasonmedia(\Brandnamic\BnSeasons\Domain\Model\Seasonmedia $seasonmediaToRemove) {
		$this->seasonmedias->detach($seasonmediaToRemove);
	}

	/**
	 * Returns the seasonmedias
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Brandnamic\BnSeasons\Domain\Model\Seasonmedia> $seasonmedias
	 */
	public function getSeasonmedias() {
		return $this->seasonmedias;
	}

	/**
	 * Sets the seasonmedias
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Brandnamic\BnSeasons\Domain\Model\Seasonmedia> $seasonmedias
	 * @return void
	 */
	public function setSeasonmedias(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $seasonmedias) {
		$this->seasonmedias = $seasonmedias;
	}

}