<?php
namespace Brandnamic\BnSeasons\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *           Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Seasonmedia
 */
class Seasonmedia extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * Headerbild
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
	 * @cascade remove
	 */
	protected $headerimage = NULL;

	/**
	 * Headertext
	 *
	 * @var string
	 */
	protected $headertext = '';

	/**
	 * Saison
	 *
	 * @var \Brandnamic\BnSeasons\Domain\Model\Season
	 */
	protected $season = NULL;

	/**
	 * __construct
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		$this->headerimage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Adds a FileReference
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $headerimage
	 * @return void
	 */
	public function addHeaderimage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $headerimage) {
		$this->headerimage->attach($headerimage);
	}

	/**
	 * Removes a FileReference
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $headerimageToRemove The FileReference to be removed
	 * @return void
	 */
	public function removeHeaderimage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $headerimageToRemove) {
		$this->headerimage->detach($headerimageToRemove);
	}

	/**
	 * Returns the headerimage
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $headerimage
	 */
	public function getHeaderimage() {
		return $this->headerimage;
	}

    public function getHeaderimages() {
        return $this->headerimage;
    }

	/**
	 * Sets the headerimage
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $headerimage
	 * @return void
	 */
	public function setHeaderimage(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $headerimage) {
		$this->headerimage = $headerimage;
	}

	/**
	 * Returns the headertext
	 *
	 * @return string $headertext
	 */
	public function getHeadertext() {
		return $this->headertext;
	}

	/**
	 * Sets the headertext
	 *
	 * @param string $headertext
	 * @return void
	 */
	public function setHeadertext($headertext) {
		$this->headertext = $headertext;
	}

	/**
	 * Returns the season
	 *
	 * @return \Brandnamic\BnSeasons\Domain\Model\Season $season
	 */
	public function getSeason() {
		return $this->season;
	}

	/**
	 * Sets the season
	 *
	 * @param \Brandnamic\BnSeasons\Domain\Model\Season $season
	 * @return void
	 */
	public function setSeason(\Brandnamic\BnSeasons\Domain\Model\Season $season) {
		$this->season = $season;
	}

}