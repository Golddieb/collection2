<?php
namespace Brandnamic\BnSeasons\Controller;


use TYPO3\CMS\Core\Resource\ResourceFactory;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *           Alex Complojer <alex.complojer@brandnamic.com>, Brandnamic
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * PageController
 */
class PageController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * pageRepository
	 *
	 * @var \Brandnamic\BnSeasons\Domain\Repository\PageRepository
	 * @inject
	 */
	protected $pageRepository = NULL;

    /**
     * pageLanguageOverlayRepository
     *
     * @var \Brandnamic\BnSeasons\Domain\Repository\PageLanguageOverlayRepository
     * @inject
     */
    protected $pageLanguageOverlayRepository = NULL;

    /**
     * seasonRepository
     *
     * @var \Brandnamic\BnSeasons\Domain\Repository\SeasonRepository
     * @inject
     */
    protected $seasonRepository = NULL;

    /**
     * seasonmediaRepository
     *
     * @var \Brandnamic\BnSeasons\Domain\Repository\SeasonmediaRepository
     * @inject
     */
    protected $seasonmediaRepository = NULL;

    protected $debug = NULL;

    public function initializeAction()
    {
        if ($this->request->hasArgument("debug")) $this->debug = TRUE;
    }

	/**
	 * action pics
	 *
	 * @return void
	 */
	public function picsAction() {
        $this->checkCustomTemplate();
        $this->addMessage("DEBUG Active");

        $seasons = explode(",",$this->settings["flexform"]["bn_seasons"]);
        $myseasons = $this->seasonRepository->findByUids($seasons);

        if ($this->request->hasArgument("startpid")) {
            $startpid = $this->request->getArgument("startpid");
            $this->addMessage("Suche Bilder mit alternativem Startpunkt: " . $startpid);
        }

        if($startpid > 0) $startid = $startpid; else $startid = $GLOBALS['TSFE']->id;

           $mypage = $this->pageRepository->findOneByUid($startid);

           $res = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
           // wenn aktuelle Seite vorhanden
           if(isset($mypage)) {


               $seasonmedia = $mypage->getSeasonmedias();

               // falls aktuelle Seite keine Seasonmedias und nicht Rootseite Fallback
               if(count($seasonmedia) == 0 && $mypage->getPid() != 0) {
                  $this->addMessage("Keine Medien auf aktueller Seite vorhanden, Starte slide -1");
                  $mypage = $this->pageSlide($mypage->getUid());

                  if(isset($mypage) && !is_null($mypage) && $mypage->getUid() != 0) {
                      $seasonmedia = $mypage->getSeasonmedias();
                  }
               }

               foreach($seasonmedia as $key => $media) {
                   $curid = $media->getSeason()->getUid();

                   if(in_array($curid, $seasons)) {
                       $this->addMessage("Passende Bilder gefunden");
                       $res->attach($media);
                   }
               }
           }

           // TODO: Relationen aktuell nicht übersetzbar: https://forge.typo3.org/issues/57272

        if(isset($res) && !is_null($res) && count($res) > 0) $this->view->assign('seasonmedia', $res);
        if(isset($res) && !is_null($res) && count($res) > 0) $this->view->assign('seasonmedias', $res);
        if(isset($mypage) && !is_null($mypage)) $this->view->assign('page', $mypage);
        if(isset($myseasons) && !is_null($myseasons)) $this->view->assign('currentseasons', $myseasons);
	}

    function pageSlide($currentId) {
        $this->addMessage("Suche Bilder auf Seite mit ID: ".$currentId);
        $seasons = explode(",",$this->settings["flexform"]["bn_seasons"]);
        $this->addMessage("Suche nach folgenden Kategorien: ".$this->settings["flexform"]["bn_seasons"]);

        $page = $this->pageRepository->findOneByUid($currentId);

        if($page) {
        $seasonmedias = $page->getSeasonmedias();
        if(count($seasonmedias) == 0) {
            $this->addMessage("Keine Bilder vorhanden");
            // komplett keine Bilder vorhanden
           return $this->pageSlide($page->getPid());
        } else {
            foreach($seasonmedias as $key => $media) {
                $curid = $media->getSeason()->getUid();
                $this->addMessage("Aktuelle Kategorie: ".$curid);
                if(in_array($curid, $seasons)) {
                    $this->addMessage("Passende Bilder gefunden");
                    //min 1 Medium fuer aktuelle Seite gefunden
                    return $page;
                }
            }
        }

        $this->addMessage("Keine passenden Bilder gefunden");

        if($page->getPid() == 0) {
        $this->addMessage("Komplett keine passenden Bilder gefunden");
        return null;
        }
        else return $this->pageSlide($page->getPid());
        } else return null;
    }

	/**
	 * action autopics
	 *
	 * @return void
	 */
	public function autopicsAction() {
		
	}

	/**
	 * action css
	 *
	 * @return void
	 */
	public function cssAction() {
		
	}

    private function checkCustomTemplate()
    {
        $templatefile = $this->settings["flexform"]["template"];

        if (preg_match('/file:(\d+)/i', $templatefile, $matches)) {
            $file = ResourceFactory::getInstance()->getFileObject($matches[1]);
            $file = $file->toArray();
            $templatefile = $file["url"];
        }

        $partialpath = pathinfo($templatefile, PATHINFO_DIRNAME) . "/partials/";

        if (is_file($templatefile)) {
            $this->view->setTemplatePathAndFilename($templatefile);
            $this->view->setPartialRootPath($partialpath);
            $this->addMessage("Using Custom-Template from " . $templatefile);
        }
    }

    public function addMessage($message)
    {
        if($this->debug) $this->addFlashMessage($message);
    }



}