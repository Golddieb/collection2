# Bilderordner

Hier werden alle statischen Bilder Assets (Sprites, Icons, etc.) abgespeichert.

## Bilderkompression richtig nutzen

*PNG8* für Bilder mit weniger als 256 Farben (Skizzen, Grafiken, Logos)
*JPEG* für Bilder mit mehr als 256 Farben (Fotos)
*PNG24* für transparente Bilder
*GIF* nie benutzen, außer für Animationen

PNG Bilder lassen sich mit dem Onlinedienst [tinypng](https://tinypng.com) um bis zu 50% besser komprimieren.
*Achtung:* Bilder mit Transparenz werden nach der Optimierung in Photoshop unter Version CS6 falsch dargestellt.
Deshalb eignet sich der Dienst nicht für Bilder, die nachträglich noch bearbeitet werden könnten (Sprites).