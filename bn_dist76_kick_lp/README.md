# TYPO3 Distribution for Brandnamic’s Standard Landingpage

## Development conventions

* Avoid generating markup with TypoScript. Use Fluid
* Split JavaScript and CSS into separate files for a more modular approach. Use the same structure as the existing JS/CSS files


Technologies in use:

* TYPO3 7.6
* notable TYPO3 extensions:
    - mask
    - min
    - vhs
    - realurl
    - dd_googlesitemap
    - faltranslation (see https://bitbucket.org/t3easy_de/faltranslation/downloads)
    - bn_cookimp
    - bn_seasons
    - bn_tagmanager
    - bn_ts2js
* plain CSS and plain ES5 for JavaScript
* no client-side build systems

## Installation steps

* Install this extension (the extension key is *bn_typo_dist76*).
* Pfad für Mask-Templates in Extensionkonfiguration anpassen
* Pfad für Realurl in Extensionkonfiguration anpassen
* Go to the settings of the administrative backend user and copy the following lines into _TSconfig_ in the _Options_ tab:
```
options.pageTree.showPageIdWithTitle = 1
options.clearCache.system = 1
```
* Perform further steps required when copying a TYPO3 website.

### Tracking

The tracking on the landing page is prepared to be set up with Google Tag Manager (GTM). In addition to the regular tracking make sure the preset for the general Analytics property for all landing pages is imported. Steps:

1. Import the preset `./Resources/Private/tracking/general-property-analytics-lp.json` into the GTM container, choose “Merge” instead of “Overwrite”.
2. Duplicate the existing GTM tag for Analytics conversions and fill in `{{Konstante – allgemeine Analytics Property für alle LPs}}` as Tracking ID. Make sure the conversion page is `/general-request/de`, `/general-request/it` or `/general-request/en`.
3. Duplicate the existing GTM tag for Analytics events and fill in `{{Konstante – allgemeine Analytics Property für alle LPs}}` as Tracking ID.
4. Test the Analytics goals (for the requests) and the events (e.g. click on scroll to request button, add an offer to the request etc.) in the UA-93932824-1 account.

### Mask Paths

* General
    - File with project-specific mask configuration: typo3conf/ext/bn_typo_dist76/Files/extensions/mask/mask.json
* Frontend
    - Folder for Content Fluid Templates (with trailing slash): typo3conf/ext/bn_typo_dist76/Files/extensions/mask/templates/content/
    - Folder for Content Fluid Layouts (with trailing slash): typo3conf/ext/bn_typo_dist76/Files/extensions/mask/templates/content/Layouts/
    - Folder for Content Fluid Partials (with trailing slash): typo3conf/ext/bn_typo_dist76/Files/extensions/mask/templates/content/Partials/
* Backend
    - Folder for Backend Preview Templates (with trailing slash): typo3conf/ext/bn_typo_dist76/Files/extensions/mask/templates/backend/
    - Folder for Backend Preview Layouts (with trailing slash): typo3conf/ext/bn_typo_dist76/Files/extensions/mask/templates/backend/Layouts/
    - Folder for Backend Preview Partials (with trailing slash): typo3conf/ext/bn_typo_dist76/Files/extensions/mask/templates/backend/Partials/
    - Folder for preview-images (with trailing slash): typo3conf/ext/bn_typo_dist76/Files/extensions/mask/templates/preview/

### Realurl-Path

typo3conf/ext/bn_typo_dist76/Files/extensions/realurl/realurl_conf.php

## Page type settings

The page type (e.g. the “thank you” page, “newsletter confirm” or the home page with CTA buttons and header sliders) is determined by the frontend layout setting. Don’t use any hardcoded page ID (uid) in the code if possible, which makes it more portable.

## Favicons

Use https://realfavicongenerator.net/ to generate the favicons, set /typo3conf/ext/bn_typo_dist76/Resources/Public/favicons/ as folder for the favicons, download the files and place them in the corresponding folder.

## Disable minification for HTML/CSS/JS during development

To disable minification for HTML/CSS/JS and concatenation for CSS/JS (handled by the _min_ extension), log into the TYPO3 backend and pass `?debug=1` as URL query string.

## Helper snippets for editors

If the keyboard shortcuts `Ctrl + i` (on Windows/Linux) or `Cmd + i` (on macOS) are pressed, some buttons with helpful functionality will appear on the top of the page.

## Distro Todo

* automatically set Mask’s data paths in the extension settings
* data.t3d instead of data.sql
* automatically set TSconfig for the admin user (`showPageIdWithTitle = 1` etc.)
