# CONFIG-TS-FILES (immer vor "EXTENSIONS-TS-FILES" und "LIBS-TS-FILES")
# Globale Anpassungen (stdheader, tt_content rendering)
<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/config/global.ts">

<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/iframe-content.ts">

# EXTENSIONS-TS-FILES (immer vor "LIBS-TS-FILES")
<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/extensions/min/config.ts">

# LIBS-TS-FILES
# allgemeine Seitenbestandteile
<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/libs/elements.ts">
<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/libs/styling-settings.ts">
<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/libs/typo3-constants-from-dom-json.ts">
# Navigationen
<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/libs/navi.ts">

page.10 = FLUIDTEMPLATE
page.10 {
    file = EXT:bn_typo_dist76/Files/template.html

    partialRootPaths {
        10 = EXT:bn_typo_dist76/Files/ts/partials
    }

    variables {
        #HEADER
        LOGO < temp.LOGO
        REQUEST < temp.REQUEST

        # MA_SLIDER
        VISTA < temp.SEASONS
        SLOGAN < temp.SLOGAN
        ZITAT < temp.ZITAT

        # MAIN
        CONTENT < temp.CONTENT
        REQUEST_MAIN < temp.REQUEST_MAIN
        WIDGET_CONTAINER < temp.WIDGET_CONTAINER
        PARTNER_LOGOS < temp.PARTNER_LOGOS
        LEGAL_MENU < temp.LEGAL_MENU
        POWERED_BY < temp.BRANDNAMIC_LINK

        phoneNumberForLink = TEXT
        phoneNumberForLink.value = {$phoneNumberForLink}

        phoneCallCaption = TEXT
        phoneCallCaption.value = {$phoneCallCaption}
    }
}
