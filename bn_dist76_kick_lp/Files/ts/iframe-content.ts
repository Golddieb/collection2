iframecontent = PAGE
iframecontent {
    typeNum = 1

    config {
        sys_language_overlay = 1
        disablePrefixComment = 1
        locale_all = {$local_all}
        linkVars = L
        uniqueLinkVars = 1
        language = {$language}
        sys_language_uid = {$sys_language_uid}
        absRefPrefix = /
        tx_realurl_enable = 1
    }

    10 = FLUIDTEMPLATE
    10 {
        file = EXT:bn_typo_dist76/Files/ts/templates/iframe-page.html
        variables {
            content < styles.content.get
            content.select {
                where >
                andWhere {
                    cObject = COA
                    cObject {
                        10 = TEXT
                        10 {
                            wrap = colPos=|
                            data = GP:colpos
                            ifEmpty = 0
                        }

                        20 = TEXT
                        20 {
                            noTrimWrap = | AND uid=||
                            data = GP:uid
                            required = 1
                        }
                    }
                }
            }
        }
    }

    includeCSS {
        # Library CSS
        normalize = EXT:bn_typo_dist76/Files/css/vendor/normalize.css
        normalize.media = screen
    }
}
