# Lang
sys_language_uid = 2
language = en
locale_all = en_GB

meta_author = {$en_author}
meta_description = {$en_description}
meta_keywords = {$en_keywords}

ab = from
available = This offer is available in the following periods
comment_placeholder = Message
error_while_loading = An error occurred when loading the data.
euro = Euro
footer_legal_notice = {$en_footer_legal_notice}
nights = nights
single_night = night
night = night
no_offer = Currently there are no offers available
offer = Enquire now about the offer
offer_request =
offer_selected = Chosen offer
period_of_time = period
person = person
price = price
request = now
request_a = Enquire
request_button_caption_part_1 = Enquire
request_button_caption_part_2 = now
request_button_caption_part_2_1 = now
request_button_caption_part_2_2 =
read_more = read more
show_less_text = show less text

overnight_stay = Overnight stay
overnight_stays = Overnight stays
overnight_stays_abbreviation = OS

calc_route = Calculate Route
error_address_notfound = The address provided could not be found.
error_insert_address = Please insert an address.

brandnamic = https://www.brandnamic.com/en
