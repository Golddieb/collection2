# Lang
sys_language_uid = 0
language = de
locale_all = de_DE

# defaults for some meta tags
meta_author = {$de_author}
meta_description = {$de_description}
meta_keywords = {$de_keywords}

ab = ab
available = Dieses Angebot ist in folgenden Zeiträumen verfügbar
comment_placeholder = Teilen Sie uns Ihren Wunsch mit
error_while_loading = Es ist ein Fehler beim Laden der Daten aufgetreten.
euro = Euro
footer_legal_notice = {$de_footer_legal_notice}
nights = Nächte
single_night = Nacht
night = Nacht
no_offer = Momentan ist kein Angebot verfügbar
offer = Jetzt Angebot
offer_request = anfragen
offer_selected = Gewähltes Angebot
period_of_time = Zeitraum
person = Person
price = Preis
request = Unverbindlich anfragen
request_a = Jetzt
request_button_caption_part_1 = Jetzt
request_button_caption_part_2 = unverbindlich anfragen
request_button_caption_part_2_1 = unverbindlich
request_button_caption_part_2_2 = anfragen
read_more = mehr lesen
show_less_text = weniger Text anzeigen

overnight_stay = Übernachtung
overnight_stays = Übernachtungen
overnight_stays_abbreviation = ÜN

calc_route = Route berechnen
error_address_notfound = Die eingegebene Adresse wurde nicht gefunden.
error_insert_address = Bitte geben Sie eine Adresse an.

phoneCallCaption = [Jetzt anrufen]

brandnamic = https://www.brandnamic.com
