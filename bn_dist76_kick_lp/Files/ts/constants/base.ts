# General constants, see
# http://docs.typo3.org/typo3cms/TyposcriptSyntaxReference/TypoScriptTemplates/TheConstantEditor/Index.html

# customsubcategory=global=Globale Variablen

# cat= website/global/0007 type=int+; label=Telefonnummer für den „tel:“-Link (z. B. 00390474123456) des Anrufen-Buttons
phoneNumberForLink =

# cat= website/global/0008 type=string; label=Hotelname
hotel_name = Hotel Lorem Ipsum

# Konstanteneditor funktioniert für die Sprachversionierung nicht da die Konstanten als Schlüssel verwendet werden
# Dementsprechend können im KE max ein Satz von Sprachvariablen angezeigt werden

<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/constants/de-constant-editor.ts">
<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/constants/it-constant-editor.ts">
<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/constants/en-constant-editor.ts">

<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/constants/de.ts">

<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/constants/styling-settings.ts">

[globalVar = GP:L = 1]
    <INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/constants/it.ts">
[global]

[globalVar = GP:L = 2]
    <INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/constants/en.ts">
[global]
