# cat=translations it/0001; type=string; label=Text beim Footermenü
it_footer_legal_notice = Hotel Lorem Ipsum – Part. IVA IT 0123456789

# cat=translations it/0002; type=string; label=Standard für meta[name=author], soll "Hotelname | Ort | Familienname" sein
it_author =

# cat=translations it/0003; type=string; label=Standard für meta[name=description], Beschreibung der Webseite
it_description =

# cat=translations it/0004; type=string; label=Standard für meta[name=keywords], Schlagworte der Webseite
it_keywords =
