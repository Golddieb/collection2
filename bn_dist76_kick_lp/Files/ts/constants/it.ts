# Lang
sys_language_uid = 1
language = it
locale_all = it_IT

meta_author = {$it_author}
meta_description = {$it_description}
meta_keywords = {$it_keywords}

ab = da
available = Questa offerta è disponibile nei seguenti periodi
comment_placeholder = Comunicateci i vostri desideri
error_while_loading = Si è verificato un errore durante il caricamento dei dati.
euro = Euro
footer_legal_notice = {$it_footer_legal_notice}
nights = notti
single_night = notte
night = notte
no_offer = Purtroppo al momento non ci sono offerte disponibili
offer = Richiedi ora senza impegno l’offerta

# must be empty
offer_request =

offer_selected = Offerte selezionate
period_of_time = periodo
person = persona
price = prezzo
request = non vincolante
request_a = richiesta
request_button_caption_part_1 = Richiesta
request_button_caption_part_2 = non vincolante
request_button_caption_part_2_1 = non
request_button_caption_part_2_2 = vincolante
read_more = leggi di più
show_less_text = visualizza versione ridotta

overnight_stay = Pernottamento
overnight_stays = Pernottamenti
overnight_stays_abbreviation = PN

calc_route = Calcola percorso
error_address_notfound = L’indirizzo inserito non è stato trovato.
error_insert_address = Prego inserire un indirizzo.

brandnamic = https://www.brandnamic.com/it
