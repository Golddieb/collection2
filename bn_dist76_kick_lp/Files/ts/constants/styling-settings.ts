# customsubcategory=color=Farben

# cat=styling settings/color/0001; type=color; label=Standard-Text
styling_settings.defaultTextColor =

# cat=styling settings/color/0002; type=color; label=Links
styling_settings.defaultLinkColor =

# cat=styling settings/color/0003; type=color; label=Links beim Hovern
styling_settings.defaultLinkHoverColor =

# cat=styling settings/color/0004; type=color; label=Hintergrund der Seite
styling_settings.pageBackgroundColor =

# cat=styling settings/color/0006; type=color; label=Überschriften
styling_settings.headingColor =

# cat=styling settings/color/0006; type=options[light=300, normal=400, halb-fett=600, fett=700]; label=Schriftstärke der Überschriften
styling_settings.headingFontWeight = 700

# cat=styling settings/color/0006; type=options[light=300, normal=400, halb-fett=600, fett=700]; label=Schriftstärke der Titel bei den Tab-Slider-Boxen und bei den Angebots-Details
styling_settings.headingFontWeight2 = 700

# cat=styling settings/color/0007; type=color; label=Unterüberschriften
styling_settings.subheadingColor =

# cat=styling settings/color/0006; type=options[light=300, normal=400, halb-fett=600, fett=700]; label=Schriftstärke der Unterüberschriften
styling_settings.subheadingFontWeight = 400

# cat=styling settings/color/0006; type=options[light=300, normal=400, halb-fett=600, fett=700]; label=Schriftstärke der Kurzbeschreibung der Angebote, des Slogan-Untertitels
styling_settings.subheadingFontWeight2 = 400

# cat=styling settings/color/0006; type=options[light=300, normal=400, halb-fett=600, fett=700]; label=Schriftstärke des Slogan-Titels
styling_settings.sloganTitleFontWeight = 400

# cat=styling settings/color/0008; type=color; label=Hintergrund der CTA-Buttons
styling_settings.ctaButtonBackgroundColor =

# cat=styling settings/color/0009; type=color; label=Text der CTA-Buttons
styling_settings.ctaButtonTextColor =

# cat=styling settings/color/0010; type=color; label=Hintergrund der CTA-Buttons bei Hover
styling_settings.ctaButtonBackgroundColorHover =

# cat=styling settings/color/0011; type=color; label=Text der CTA-Buttons bei Hover
styling_settings.ctaButtonTextColorHover =

# cat=styling settings/color/0012; type=color; label=Farbe der Icons (z. B. €-Zeichen und Kalender bei Angeboten)
styling_settings.iconBackgroundColor =

# cat=styling settings/color/0013; type=color; label=Hintergrund Cookie-Banner
styling_settings.cookieBannerBackground =

# cat=styling settings/color/0014; type=color; label=Text Cookie-Banner
styling_settings.cookieBannerTextColor =

# cat=styling settings/color/0015; type=color; label=Hintergrund Cookie-Banner Button
styling_settings.cookieBannerButtonBackground =

# cat=styling settings/color/0016; type=string; label=Hintergrund Slogan im Header
styling_settings.headerSloganBackground =

# cat=styling settings/color/0016; type=color; label=Text Slogan im Header
styling_settings.headerSloganTextColor =

# cat=styling settings/color/0016; type=options[Normal=none, Großbuchstaben=uppercase]; label=Text Slogan im Header (Normal oder Großbuchstaben)
styling_settings.headerSloganTextTransform = uppercase

# cat=styling settings/color/0017; type=string; label=Hintergrund Zitate im Header
styling_settings.headerQuoteSliderBackground =

# cat=styling settings/color/0017; type=color; label=Text Zitate im Header
styling_settings.headerQuoteSliderTextColor =

# cat=styling settings/color/0018; type=color; label=Hintergrund Highlights-Box und der Überschriften-Balken beim Formular
styling_settings.highlightsAndFormCaptionBackgroundColor =

# cat=styling settings/color/0018; type=color; label=Titel Highlights-Box und der Überschriften-Balken beim Formular
styling_settings.highlightsAndFormCaptionHeaderColor =

# cat=styling settings/color/0019; type=color; label=Hintergrund Zitat-Boxen, Anfrageformular und Bewertungsportal-Widget im Footer
styling_settings.quoteBoxRequestFormAndWidgetBackgroundColor =

# cat=styling settings/color/0019; type=color; label=Text Zitat-Boxen, Anfrageformular und Bewertungsportal-Widget im Footer
styling_settings.quoteBoxRequestFormAndWidgetTextColor =

# cat=styling settings/color/0021; type=color; label=Hintergrund Angebots-Boxen und Tab-Slider-Boxen
styling_settings.offersAndTabSliderBoxesBackgroundColor =

# cat=styling settings/color/0022; type=color; label=Text Angebots-Boxen und Tab-Slider-Boxen
styling_settings.offersAndTabSliderBoxesTextColor =

# cat=styling settings/color/0023; type=color; label=Hintergrund inaktive Elemente bei den Angebots- und Tab-Slider-Boxen
styling_settings.offersAndTabSliderBoxesInactiveBackgroundColor =

# cat=styling settings/color/0023; type=color; label=Text inaktive Elemente bei den Angebots- und Tab-Slider-Boxen
styling_settings.offersAndTabSliderBoxesInactiveTextColor =

# cat=styling settings/color/0024; type=color; label=Hintergrund der Plakette im Formular, der Action-Buttons und der Rands des Formularfelds bei der Karte
styling_settings.formBadgeAndActionButtonsBackgroundColorAndMapInputFieldBorderColor =

# cat=styling settings/color/0025; type=color; label=Farbe der Logos in den Zitat-Boxen
styling_settings.quoteBoxLogoColor =

# cat=styling settings/color/0026; type=color; label=Farbe der Anführungszeichen-Icons
styling_settings.quoteIconColor =

# cat=styling settings/color/0027; type=color; label=Farbe der Icons beim Formular
styling_settings.formIconColor =

# cat=styling settings/color/0028; type=color; label=Hintergrund Action-Buttons (z. B. „mehr lesen“-Button)
styling_settings.actionButtonBackgroundColor =

# cat=styling settings/color/0029; type=color; label=Text Action-Buttons (z. B. „mehr lesen“-Button)
styling_settings.actionButtonTextColor =

# cat=styling settings/color/0029; type=string; label=Hintergrundfarbe der Slider-Navigations-Pfeile
styling_settings.sliderArrowBackgroundColor =

# cat=styling settings/0030; type=options[Normal=default, Volle Viewport-Breite=fullViewportWidth]; label=Variante des Seiten-Headers (bitte auch das entsprechende Template im Header-Slider auswählen)
styling_settings.pageHeaderVariant = default

# cat=styling settings/0031; type=string; label=Zu ladende Google Webfonts (kommagetrennt)
styling_settings.webFontFamilies = Source Sans Pro, Libre Baskerville

# cat=styling settings/0032; type=string; label=Standard-Schriftart
styling_settings.fontFamilyDefault = Source Sans Pro

# cat=styling settings/0033; type=options[light=300, normal=400, halb-fett=600, fett=700]; label=Schriftstärke der Standard-Schriften
styling_settings.fontWeightDefault = 400

# cat=styling settings/0033; type=string; label=Standard-Schriftart für Überschriften
styling_settings.fontFamilyHeadings = Libre Baskerville

# cat=styling settings/0033; type=options[light=300, normal=400, halb-fett=600, fett=700]; label=Schriftstärke der vom hervorgehobenen Text
styling_settings.fontWeightStrong = 700

# cat=styling settings/0034; type=string; label=Schriftart für Untertitel
styling_settings.fontFamilySubheadings = Libre Baskerville
