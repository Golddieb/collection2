# cat=translations en/0001; type=string; label=Text beim Footermenü
en_footer_legal_notice = Hotel Lorem Ipsum – VAT No. IT 0123456789

# cat=translations en/0002; type=string; label=Standard für meta[name=author], soll "Hotelname | Ort | Familienname" sein
en_author =

# cat=translations en/0003; type=string; label=Standard für meta[name=description], Beschreibung der Webseite
en_description =

# cat=translations en/0004; type=string; label=Standard für meta[name=keywords], Schlagworte der Webseite
en_keywords =
