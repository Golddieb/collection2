# Brandnamic Kickstarter v2.0.0-beta1

# Extension settings
config.tx_extbase.tx_tsjsconverter.settings {
    input_folder = EXT:bn_typo_dist76/Files/ts/constants/
}


# Konfiguration des "page"-Objekts
# für Details siehe http://docs.typo3.org/typo3cms/TyposcriptReference/Setup/Config/Index.html
page = PAGE
page {
    typeNum = 0
    config {
        admPanel = 0
        absRefPrefix = /
        doctype = html5
        xmlprologue = none
        metaCharset = UTF-8
        locale_all = {$locale_all}
        # Must be changed if there are more than 3 languages
        linkVars = L(0-2)
        uniqueLinkVars = 1
        sys_language_uid = {$sys_language_uid}
        language = {$language}
        sys_language_mode = ignore
        sys_language_overlay = hideNonTranslated
        noPageTitle = 1
        # Removes the mail encryption JS from <head>
        removeDefaultJS = 1
        inlineStyle2TempFile = 1

        # Note: SPAM protection for mail addresses is not enabled
        # because it’s not needed on the landing pages and would
        # cause a blocking script tag in <head>

        simulateStaticDocuments = 0
        prefixLocalAnchors = all
        tx_realurl_enable = 1
        intTarget = _top
        extTarget = _blank
        no_cache = 0
        sendCacheHeaders = 1
        # Set cache validity to 2 weeks
        cache_period = 1209600
        htmlTag_setParams = lang="{$language}" xml:lang="{$language}"

        compressCss = 1
        concatenateCss = 1
        compressJs = 1
        concatenateJs = 1

    }

    # meta-Tag Konfiguration
    meta {
        robots = index, follow, noodp
        viewport = width=device-width, initial-scale=1.0, user-scalable=yes

        # Standardwerte (Fallback)
        author = {$meta_author}
        description = {$meta_description}
        keywords = {$meta_keywords}

        # Overrides aus Backend
        author.override.field = author
        description.override.field = description
        keywords.override.field = keywords

        X-UA-Compatible = IE=edge
    }

    # header-Tag Konfiguration der Inhalte
    headerData {
        # Manually include <title>. Using titleTagFunction would be an alternative,
        # but that would require `includeLibs` which is unsupported in TYPO3 Composer mode
        10 = TEXT
        10 {
            wrap = <title>|</title>
            field = subtitle // title
        }

        # Copyright Info
        20 = FLUIDTEMPLATE
        20 {
            file = EXT:bn_typo_dist76/Files/ts/templates/copyright-notice.html
        }

        # Alternate language links
        30 = FLUIDTEMPLATE
        30 {
            file = EXT:bn_typo_dist76/Files/ts/templates/hreflang.html
        }

        # Favicons, tile icons & more
        40 = FLUIDTEMPLATE
        40 {
            file = EXT:bn_typo_dist76/Files/ts/templates/favicons.html
        }

        # Facebook Open Graph
        50 = FLUIDTEMPLATE
        50 {
            file = EXT:bn_typo_dist76/Files/ts/templates/facebook-open-grap.html
            variables {
                page_title = TEXT
                page_title.field = title

                meta_description_field = TEXT
                meta_description_field.field = description

                meta_description = TEXT
                meta_description.value = {$meta_description}

                hotel_name = TEXT
                hotel_name.value = {$hotel_name}
            }
        }
    }

    # CSS Includes
    includeCSS {
        # Library CSS
        normalize = EXT:bn_typo_dist76/Files/css/vendor/normalize.css
        normalize.media = screen
        magnific-popup = EXT:bn_typo_dist76/Files/libs/magnific-popup/magnific-popup.css
        magnific-popup.media = screen
        slick = EXT:bn_typo_dist76/Files/libs/slick/slick.css
        slick.media = all
        owl-carousel = EXT:bn_typo_dist76/Files/libs/owl-carousel/assets/owl.carousel.css
        owl-carousel.media = all

        bootstrap-tabs = EXT:bn_typo_dist76/Files/libs/bootstrap/bootstrap-tabs.css
        bootstrap-tabs.media = all

        # Icon fonts
        standard-lp-icons = EXT:bn_typo_dist76/Files/fonts/standard-lp-icons/style.css
        standard-lp-icons.media = screen

        # Custom CSS
        base = EXT:bn_typo_dist76/Files/css/base.css
        base.media = screen

        helper = EXT:bn_typo_dist76/Files/css/helper.css
        helper.media = screen

        style = EXT:bn_typo_dist76/Files/css/style.css
        style.media = screen

        header-slogan = EXT:bn_typo_dist76/Files/css/components/header-slogan.css
        header-slogan.media = screen

        sliders = EXT:bn_typo_dist76/Files/css/components/sliders.css
        sliders.media = screen

        request-form = EXT:bn_typo_dist76/Files/css/components/request-form.css
        request-form.media = screen

        page-footer = EXT:bn_typo_dist76/Files/css/components/page-footer.css
        page-footer.media = screen

        gallery_slider = EXT:bn_typo_dist76/Files/css/components/gallery_slider.css
        gallery_slider.media = screen

        google-maps-element = EXT:bn_typo_dist76/Files/css/components/google-maps-element.css
        google-maps-element.media = screen

        offer = EXT:bn_typo_dist76/Files/css/components/offer.css
        offer.media = screen

        selected-item-box = EXT:bn_typo_dist76/Files/css/components/selected-item-box.css
        selected-item-box.media = screen

        highlights-container = EXT:bn_typo_dist76/Files/css/components/highlights-container.css
        highlights-container.media = screen

        tab-slider-box = EXT:bn_typo_dist76/Files/css/components/tab-slider-box.css
        tab-slider-box.media = screen

        loading-indicators = EXT:bn_typo_dist76/Files/css/components/loading-indicators.css
        loading-indicators.media = screen

        widgets-container = EXT:bn_typo_dist76/Files/css/components/widgets-container.css
        widgets-container.media = screen

        partner-logos = EXT:bn_typo_dist76/Files/css/components/partner-logos.css
        partner-logos.media = screen

        lang-nav = EXT:bn_typo_dist76/Files/css/components/lang-nav.css
        lang-nav.media = screen

        page-header = EXT:bn_typo_dist76/Files/css/components/page-header.css
        page-header.media = screen

        quote-boxes = EXT:bn_typo_dist76/Files/css/components/quote-boxes.css
        quote-boxes.media = screen

        image-text-box = EXT:bn_typo_dist76/Files/css/components/image-text-box.css
        image-text-box.media = screen

        multi-slider = EXT:bn_typo_dist76/Files/css/components/multi-slider.css
        multi-slider.media = screen

        video-teaser = EXT:bn_typo_dist76/Files/css/components/video-teaser.css
        video-teaser.media = screen

        phone-call-button = EXT:bn_typo_dist76/Files/css/components/phone-call-button.css
        phone-call-button.media = screen

        cookiebanner-custom = EXT:bn_typo_dist76/Files/css/components/cookiebanner-custom.css
        cookiebanner-custom.media = screen

        trust-badges = EXT:bn_typo_dist76/Files/css/components/trust-badges.css
        trust-badges.media = screen

        general-components = EXT:bn_typo_dist76/Files/css/components/general-components.css
        general-components.media = screen

        print = EXT:bn_typo_dist76/Files/css/print.css
        print.media = print
    }

    # JS Includes kurz vor dem schließenden body-Tag
    includeJSFooter {
        # Polyfills
        lib-array-find-polyfill = EXT:bn_typo_dist76/Files/js/lib/array-find-polyfill.js

        # JS libraries
        jquery = EXT:bn_typo_dist76/Files/js/vendor/jquery-3.1.1.min.js
        # Better include jQuery as first library
        jquery.forceOnTop = 1

        bootstrap-tabs = EXT:bn_typo_dist76/Files/libs/bootstrap/bootstrap-tabs.js

        lodash-fp = EXT:bn_typo_dist76/Files/js/vendor/lodash-fp.min.js

        stickyfill = EXT:bn_typo_dist76/Files/js/vendor/stickyfill.js
        # Bibliotheken
        lazysizes = EXT:bn_typo_dist76/Files/libs/lazysizes/lazysizes.js
        magnific-popup = EXT:bn_typo_dist76/Files/libs/magnific-popup/jquery.magnific-popup.js
        jquery-dotdotdot = EXT:bn_typo_dist76/Files/js/vendor/jquery.dotdotdot.min.js
        object-fit-images = EXT:bn_typo_dist76/Files/js/vendor/object-fit-images.js
        picturefill = EXT:bn_typo_dist76/Files/libs/picturefill/picturefill.js
        waypoint = EXT:bn_typo_dist76/Files/libs/waypoints/waypoints.js
        handlebars = EXT:bn_typo_dist76/Files/libs/handlebars/handlebars.js
        handlebars_helper = EXT:bn_typo_dist76/Files/libs/handlebars/handlebar_helper.js
         # eHotelier
        confirm-mail = EXT:bn_typo_dist76/Files/libs/brandnamic/confirm-mail.js


        lib-lib-bootstrap = EXT:bn_typo_dist76/Files/js/lib/lib-bootstrap.js
        lib-web-analytics-tracking = EXT:bn_typo_dist76/Files/js/lib/web-analytics-tracking.js
        lib-split-to-array = EXT:bn_typo_dist76/Files/js/lib/split-to-array.js
        dynforms-config = EXT:bn_typo_dist76/Files/js/partials/dynforms-config.js
        dynforms-init = EXT:bn_typo_dist76/Files/js/partials/dynforms-init.js
        cookiebanner = EXT:bn_cookimp/Resources/Public/JS/cookieBanner.js

        slick = EXT:bn_typo_dist76/Files/libs/slick/slick.js
        owl-carousel = EXT:bn_typo_dist76/Files/libs/owl-carousel/owl.carousel.js
        # eigene JS sollten immer zum Schluss eingebunden werden

        lib-logger = EXT:bn_typo_dist76/Files/js/lib/logger.js
        lib-handle-errors = EXT:bn_typo_dist76/Files/js/lib/handle-errors.js
        lib-cache-busting = EXT:bn_typo_dist76/Files/js/lib/cache-busting.js
        lib-load-hbs = EXT:bn_typo_dist76/Files/js/lib/load-hbs.js
        lib-scroll-to-element-by-id = EXT:bn_typo_dist76/Files/js/lib/scroll-to-element-by-id.js
        lib-fetch-from-ehotelier = EXT:bn_typo_dist76/Files/js/lib/fetch-from-ehotelier.js
        lib-fetch-offers = EXT:bn_typo_dist76/Files/js/lib/fetch-offers.js
        lib-fetch-ehotelier-textplugin = EXT:bn_typo_dist76/Files/js/lib/fetch-ehotelier-textplugin.js
        lib-render-ehotelier-textplugin = EXT:bn_typo_dist76/Files/js/lib/render-ehotelier-textplugin.js
        lib-fetch-rooms = EXT:bn_typo_dist76/Files/js/lib/fetch-rooms.js
        lib-render-room-list = EXT:bn_typo_dist76/Files/js/lib/render-room-list.js
        lib-scroll-to-request = EXT:bn_typo_dist76/Files/js/lib/scroll-to-request.js
        lib-custom-arrows-config = EXT:bn_typo_dist76/Files/js/data/custom-arrows-config.js
        lib-typo3-constants-from-dom-json = EXT:bn_typo_dist76/Files/js/data/typo3-constants-from-dom-json.js

        # Google Webfonts
        webfonts = EXT:bn_typo_dist76/Files/js/webfonts.js
        lib-read-more-box = EXT:bn_typo_dist76/Files/js/lib/read-more-box.js
        lib-init-room-slider = EXT:bn_typo_dist76/Files/js/lib/init-room-slider.js
        lib-handle-cookiebanner = EXT:bn_typo_dist76/Files/js/lib/handle-cookiebanner.js
        lib-google-maps-element = EXT:bn_typo_dist76/Files/js/lib/google-maps-element.js
        lib-init-popup-gallery = EXT:bn_typo_dist76/Files/js/lib/init-popup-gallery.js
        lib-init-showcase-slider = EXT:bn_typo_dist76/Files/js/lib/init-showcase-slider.js
        lib-offer = EXT:bn_typo_dist76/Files/js/lib/offer.js
        lib-insert-portal-widgets = EXT:bn_typo_dist76/Files/js/lib/insert-portal-widgets.js
        lib-multi-slider = EXT:bn_typo_dist76/Files/js/lib/multi-slider.js
        main = EXT:bn_typo_dist76/Files/js/main.js
        lib-editor-snippets = EXT:bn_typo_dist76/Files/js/lib/editor-snippets.js
    }

    bodyTagCObject = TEXT
    bodyTagCObject {
        dataWrap = <body |>

        # erweiterbare Klassen
        cObject = COA
        cObject {
            10 = TEXT
            10 {
                noTrimWrap = | data-default-lang-page-title="|" |
                data = DB:pages:{field:uid}:title
                data.insertData = 1
                insertData = 1
            }
        }
    }

}

# no Cache wenn Backend User eingeloggt ist
[globalVar = TSFE:beUserLogin = 1]
    page.config.no_cache = 1
[global]


# noindex Einstellungen für dev-Domains
[globalString = IENV:HTTP_HOST = *bnamic.com]
    page.meta.robots = noindex, nofollow
[global]

[globalString = IENV:HTTP_HOST = *mittwaldserver.info]
    page.meta.robots = noindex, nofollow
[global]

[globalString = IENV:HTTP_HOST = *plusserver.de]
    page.meta.robots = noindex, nofollow
[global]

[globalString = IENV:HTTP_HOST = *brandnamic.com]
    page.meta.robots = noindex, nofollow
[global]


# noindex Einstellungen für Dankesseiten, 404 & Suchergebnisse
[PIDinRootline = 36,54,8]
    page.meta.robots = noindex, nofollow
[global]

# TEMPLATES
<INCLUDE_TYPOSCRIPT:source="file:EXT:bn_typo_dist76/Files/ts/templates.ts">

# Disable minification and concatenation when an user
# is logged into the backend and the ?debug=1 was passed
# to the URL. This code snippet must be included after
# the default tinysource configuration
[globalVar = TSFE : beUserLogin > 0] && [globalVar = GP:debug = 1]
    plugin.tx_min.tinysource.enable = 0
    page.config {
        linkVars := addToList(debug(1))
        compressCss = 0
        concatenateCss = 0
        compressJs = 0
        concatenateJs = 0

        # Turn on more specific error messages for content objects
        contentObjectExceptionHandler = 0
    }
[global]
