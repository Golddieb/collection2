# Dateiinclude unter Seiteneigenschaften Template

# Standardeinstellungen des "section_frame"-Felds löschen
# Vorschlag: "section_frame" als Konfigurationsfeld für Frontend-Varianten
TCEFORM.tt_content {
    section_frame.removeItems = 1,5,6,10,11,12,20,21
    section_frame.altLabels.0 = Standard
}

# Standardeinstellungen des "layout"-Felds löschen
# "layout"-Feld als key für Frontend-Varianten
TCEFORM.tt_content {
    layout.removeItems = 1,2,3,4,5
    layout.altLabels.0 = Standard
}

# Standardeinstellungen des "Seitenlayout"-Felds löschen
# "Seitenlayout"-Feld als key für Frontend-Varianten
TCEFORM.pages.layout {
    removeItems = 1,2,3
    altLabels.0 = Seite ohne Seiten-Header-Slider und Anfrage-Buttons
    addItems.110 = Seite mit Seiten-Header-Slider und Anfrage-Buttons
    addItems.120 = Seite für die E-Mail (Newsletter) Bestätigung
    addItems.130 = 404-Fehlerseite
}

# Setzen der Seitenbaumrechte per TypoScript
TCEMAIN {
    # Besitzer
    permissions.userid = 3

    # Besitzergruppe ID
    permissions.groupid = 1

    # Rechte Besitzer:
    permissions.user = show, editcontent, edit, delete, new

    # Rechte Besitzergruppe:
    permissions.group = show, editcontent, edit, delete, new

    # Rechte "everybody" (kann ungesetzt bleiben):
    permissions.everybody =

    # "Kopie 1" im header Feld beim Kopieren von Inhaltselementen entfernen
    table {
        pages.disablePrependAtCopy = 1
        tt_content.disablePrependAtCopy = 1
    }
}

# Aktiviere bei neu angelegten Seiten die Checkbox "Seite verbergen, wenn keine Übersetzung für die aktuelle Sprache vorhanden ist"
TCAdefaults.pages.l18n_cfg = 2

# Keine Klassen zur Auswahl verfügbar machen
RTE.classes >
#### Try this out see what it does ####
RTE.classesAnchor >

#### Standardkonfiguration des RTE neu definieren
RTE.default >
RTE.default {
    # Statuszeile anzeigen
    showStatusBar =  1

    defaultContentLanguage = en
    buttons.spellcheck {
        enablePersonalDictionaries = 1
        dictionaries.de.defaultValue = Deutsch
        dictionaries.items = en,de,fr,it,nl
        dictionaries.restrictToItems = en,de,fr,it,nl
    }

    showTagFreeClasses = 1
    # CSS-Datei für Darstellung im BE einbinden
    contentCSS = EXT:bn_typo_dist76/Files/css/rte.css
    # showButtons = findreplace, spellcheck, bold, italic, chMode, formatblock, blockstyle, blockstylelabel, textstyle, textstylelabel, link, image, bold, italic, emphasis, underline, strikethrough, orderedlist, unorderedlist, link, image, table, copy, cut, paste, undo, redo, tableproperties, rowproperties, rowinsertabove, rowinsertunder, rowdelete, rowsplit, columninsertbefore, columninsertafter, columndelete, columnsplit, cellproperties, cellinsertbefore, cellinsertafter, celldelete, cellsplit, cellmerge
    showButtons = formatblock, spellcheck, bold, italic, chMode, link, image, bold, italic, emphasis, underline, strikethrough, orderedlist, unorderedlist, link, image, table, copy, cut, paste, undo, redo, tableproperties, rowproperties, rowinsertabove, rowinsertunder, rowdelete, rowsplit, columninsertbefore, columninsertafter, columndelete, columnsplit, cellproperties, cellinsertbefore, cellinsertafter, celldelete, cellsplit, cellmerge
    proc {
        dontConvBRtoParagraph = 1
        overrideMode = css_transform
        denyTags = u, font, hr, sub, sup
        entryHTMLparser_db = 1
        entryHTMLparser_db {
            allowTags = a, p, b, i, blockquote, pre, h1, h2, h3, h4, h5, h6, span, div, address, strong, em, li, ul, ol, strike, img, br, center, table, thead, tbody, th, td, tr
        }
    }

    buttons.formatblock.removeItems = h1, h2, h5, h6, pre, address, blockquote, div
    # p, h1, h2, h3, h4, h5, h6, pre, address, blockquote, div
    buttons.formatblock.items.h3.label = Überschrift
    buttons.formatblock.items.h4.label = Unterüberschrift

}

# Mögliche Buttons:
# blockstylelabel, blockstyle, textstylelabel, textstyle, fontstyle, fontsize, formatblock, deletedtext, big, small,  subscript, superscript, lefttoright, righttoleft, left, center, right, justifyfull, orderedlist, unorderedlist, outdent, indent, textcolor, bgcolor, textindicator, emoticon, insertcharacter, line, link, image, table, user, acronym, findreplace, spellcheck, chMode, inserttag, removeformat, copy, cut, paste, undo, redo, showhelp, about, toggleborders

FeEdit {
    editWindow.height = 800
    editWindow.width = 1000
    reloadPageOnContentUpdate = 1
}
