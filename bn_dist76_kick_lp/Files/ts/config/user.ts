# Datei-Include unter Benutzergruppe "Redakteure"
# Benutzereinstellungen
options.clearCache.all = 1
options.clearCache.pages = 1
options.clearCache.system = 1
options.pageTree.showPageIdWithTitle = 1

# Lassen sich nicht in der GUI ausschalten
TCEFORM.pages.alias.disabled = 1
TCEFORM.pages.content_from_pid.disabled = 1
