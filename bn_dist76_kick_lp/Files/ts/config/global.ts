# TODO: OPTIMIEREN, AUFRAEUMEN, ERSETZEN!

lib.parseFunc_RTE {
    # Ummantelungen
    nonTypoTagStdWrap.encapsLines {
        # class="bodytext" bei RTE abstellen
        addAttributes.P >

        # p-Tag Ummantelung bei folgenden Tags verhindern
        encapsTagList = cite, div, p, pre, hr, h1, h2, h3, h4, h5, h6, table, tr, th, td, ul, ol, li
    }

    # Tabellen (Einstellungen für TextPic-Element)
    externalBlocks.table.stdWrap {
        # HTML-Nachbearbeitung
        HTMLparser {
            # p bei Tabellenzellen entfernen
            removeTags = p

            # Tabellenklassen
            tags.table.fixAttrib.class {
                # Klassen in Tabellen zulassen
                list >

                # Klassen für responsive Tabellenzellen
                default = content-table
            }
        }
        # Wrapper für responsive Tabellen
        wrap = <div class="table-responsive">|</div>
    }

    # Lists
    tags.typolist {
        # ohne Zusätze
        default {
            wrap = <ul>|</ul>
            split.1.wrap = <li>|</li>
        }

        # ohne Zusätze
        1 {
            fontTag = <ol>|</ol>
            split.1.wrap = <li>|</li>
        }
    }

}


# Tabellen (Einstellungen für Table-Element)
tt_content {
    table {
        20 {
            stdWrap {
                wrap = <div class="table-responsive">|</div>
            }
        }
    }
}


# Anpassungen im Standard-Renderobjekt
tt_content {
    # Zusätze entfernen und ersetzen
    stdWrap {
        # sprachversionierte Anchors ausblenden
        prepend >
        # innerWrap durch Wrapper mit sprachversionierter ID ersetzen
        innerWrap >
        innerWrap {
            cObject = COA
            cObject {
                # Öffnender Tag für Wrapper
                1 = LOAD_REGISTER
                1 {
                    overrideSwitch {
                        cObject = CASE
                        cObject {
                            key.field = CType

                            default = TEXT
                            default.value = 1
                        }
                    }
                }

                10 = TEXT
                10 {
                    wrap {
                        cObject = TEXT
                        cObject {
                            # wenn kein "header" und kein "subheader" definiert wurde oder es sich um den ersten Record handelt wird mit div gewrappt (HTML5 Outline)
                            value = <div class="stdcontent clearfix" id="c|"><div class="central">
                            # ansonsten mit section (HTML5 Outline)
                            override {
                                cObject = TEXT
                                cObject {
                                    value = <section class="stdcontent clearfix" id="c|"><div class="central">

                                    if {
                                        isTrue.data = register:overrideSwitch
                                        isFalse {
                                            cObject = TEXT
                                            cObject {
                                                value = 1

                                                if {
                                                    isTrue.field = header // subheader
                                                    isFalse {
                                                        cObject = TEXT
                                                        cObject {
                                                            value = 1

                                                            if {
                                                                value = 1
                                                                equals.data = cObj:parentRecordNumber
                                                            }
                                                        }
                                                    }

                                                    negate = 1
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    # die Sprachversionierte id wird abgefragt um den Anchor zu generieren
                    cObject = CONTENT
                    cObject {
                        table = tt_content
                        select {
                            andWhere {
                                cObject = COA
                                cObject {
                                    10 = TEXT
                                    10 {
                                        wrap = l18n_parent=|
                                        field = uid
                                    }

                                    20 = TEXT
                                    20 {
                                        noTrimWrap = | AND sys_language_uid=||
                                        value = {$sys_language_uid}
                                        insertData = 1
                                    }
                                }
                            }
                            selectFields = uid
                        }

                        renderObj = TEXT
                        renderObj.field = uid
                    }

                    ifEmpty.field = uid
                }

                # Schliessender Tag
                20 < .10.wrap.cObject
                20 {
                    value = |</div></div>
                    override.cObject.value = |</div></section>
                }

                if {
                    isTrue{
                        cObject = CASE
                        cObject {
                            key {
                                field = CType
                                split {
                                    token = _
                                    cObjNum = 1 || n

                                    1 {
                                        current = 1
                                    }
                                }
                            }

                            list = TEXT
                            list.value = 0

                            # Extensions welche mit tx_ eingetragen werden standardmäßig nicht gewrappt
                            tx = TEXT
                            tx.value = 0

                            mask = TEXT
                            mask.value = 0

                            default = TEXT
                            default.value = 1
                        }
                    }
                }
            }
        }

        required = 1
    }
}
