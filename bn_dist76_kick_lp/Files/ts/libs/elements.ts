# Elemente auf der Webseite
# Verwendung von temp. anstatt lib. um unnötiges Cacheing zu vermeiden
# lib. nur verwenden wenn ein Element referenziert (=<) werden muss
#################################################

# Logo
# Telefonnummer
# Headerbilder
# etc.

temp.CONTENT < styles.content.get

temp.LOGO < styles.content.get
temp.LOGO {
    select.where = colPos=1
    slide = -1
}

temp.SLOGAN < styles.content.get
temp.SLOGAN {
	select.where = colPos=2
	slide = -1
}

temp.ZITAT < styles.content.get
temp.ZITAT {
    select.where = colPos=3
    slide = -1
}

temp.REQUEST = TEXT
temp.REQUEST.wrap = <strong>{$request_button_caption_part_1} </strong>
temp.REQUEST.value = {$request_button_caption_part_2}

temp.REQUEST_MAIN = TEXT
temp.REQUEST_MAIN.wrap (
    <strong>{$request_button_caption_part_1}</strong>
    <span>{$request_button_caption_part_2_1}</span>
    <span>{$request_button_caption_part_2_2}</span>
    <span class="icon-lp-arrow-right-2 request_button_icon"></span>
)

#--------------------SEASONS:

temp.SEASONS < styles.content.get
temp.SEASONS.select.pidInList=17

temp.WIDGET_CONTAINER < styles.content.get
temp.WIDGET_CONTAINER.select.where = colPos = 8

temp.PARTNER_LOGOS < styles.content.get
temp.PARTNER_LOGOS.select.where = colPos = 7

temp.BRANDNAMIC_LINK = TEXT
temp.BRANDNAMIC_LINK {
    wrap (
        <a class="powered-by-link no-custom-texting" href="{$brandnamic}" title="Brandnamic GmbH &#124; Hotel & Destination Marketing" target="_blank">
            <span class="powered-by-link-text">powered by</span>
        </a>
    )
}

# Get a get parameter from the URL, see
# http://blog.teamgeist-medien.de/2016/06/typo3-fluid-get-post-parameter-auslesen.html
lib.urlGetParameter = COA
lib.urlGetParameter {
    # XSS protection
    stdWrap.htmlSpecialChars = 1

    10 = TEXT
    10 {
        dataWrap = GP:{current}
        insertData = 1
        wrap3 = {|}
    }
}
