page.headerData.577 = FLUIDTEMPLATE
page.headerData.577 {
    file = EXT:bn_typo_dist76/Files/ts/templates/styling-settings.html

    variables {
        defaultTextColor = TEXT
        defaultTextColor.value = {$styling_settings.defaultTextColor}

        defaultLinkColor = TEXT
        defaultLinkColor.value = {$styling_settings.defaultLinkColor}

        defaultLinkHoverColor = TEXT
        defaultLinkHoverColor.value = {$styling_settings.defaultLinkHoverColor}

        pageBackgroundColor = TEXT
        pageBackgroundColor.value = {$styling_settings.pageBackgroundColor}

        headingColor = TEXT
        headingColor.value = {$styling_settings.headingColor}

        headingFontWeight = TEXT
        headingFontWeight.value = {$styling_settings.headingFontWeight}

        headingFontWeight2 = TEXT
        headingFontWeight2.value = {$styling_settings.headingFontWeight2}

        subheadingColor = TEXT
        subheadingColor.value = {$styling_settings.subheadingColor}

        subheadingFontWeight = TEXT
        subheadingFontWeight.value = {$styling_settings.subheadingFontWeight}

        subheadingFontWeight2 = TEXT
        subheadingFontWeight2.value = {$styling_settings.subheadingFontWeight2}

        sloganTitleFontWeight = TEXT
        sloganTitleFontWeight.value = {$styling_settings.sloganTitleFontWeight}

        ctaButtonBackgroundColor = TEXT
        ctaButtonBackgroundColor.value = {$styling_settings.ctaButtonBackgroundColor}

        ctaButtonTextColor = TEXT
        ctaButtonTextColor.value = {$styling_settings.ctaButtonTextColor}

        ctaButtonBackgroundColorHover = TEXT
        ctaButtonBackgroundColorHover.value = {$styling_settings.ctaButtonBackgroundColorHover}

        ctaButtonTextColorHover = TEXT
        ctaButtonTextColorHover.value = {$styling_settings.ctaButtonTextColorHover}

        iconBackgroundColor = TEXT
        iconBackgroundColor.value = {$styling_settings.iconBackgroundColor}

        cookieBannerBackground = TEXT
        cookieBannerBackground.value = {$styling_settings.cookieBannerBackground}

        cookieBannerTextColor = TEXT
        cookieBannerTextColor.value = {$styling_settings.cookieBannerTextColor}

        cookieBannerButtonBackground = TEXT
        cookieBannerButtonBackground.value = {$styling_settings.cookieBannerButtonBackground}

        pageHeaderVariant = TEXT
        pageHeaderVariant.value = {$styling_settings.pageHeaderVariant}

        headerSloganBackground = TEXT
        headerSloganBackground.value = {$styling_settings.headerSloganBackground}

        headerSloganTextColor = TEXT
        headerSloganTextColor.value = {$styling_settings.headerSloganTextColor}

        headerSloganTextTransform = TEXT
        headerSloganTextTransform.value = {$styling_settings.headerSloganTextTransform}

        headerQuoteSliderBackground = TEXT
        headerQuoteSliderBackground.value = {$styling_settings.headerQuoteSliderBackground}

        headerQuoteSliderTextColor = TEXT
        headerQuoteSliderTextColor.value = {$styling_settings.headerQuoteSliderTextColor}

        highlightsAndFormCaptionBackgroundColor = TEXT
        highlightsAndFormCaptionBackgroundColor.value = {$styling_settings.highlightsAndFormCaptionBackgroundColor}

        highlightsAndFormCaptionHeaderColor = TEXT
        highlightsAndFormCaptionHeaderColor.value = {$styling_settings.highlightsAndFormCaptionHeaderColor}

        quoteBoxRequestFormAndWidgetBackgroundColor = TEXT
        quoteBoxRequestFormAndWidgetBackgroundColor.value = {$styling_settings.quoteBoxRequestFormAndWidgetBackgroundColor}

        quoteBoxRequestFormAndWidgetTextColor = TEXT
        quoteBoxRequestFormAndWidgetTextColor.value = {$styling_settings.quoteBoxRequestFormAndWidgetTextColor}

        offersAndTabSliderBoxesBackgroundColor = TEXT
        offersAndTabSliderBoxesBackgroundColor.value = {$styling_settings.offersAndTabSliderBoxesBackgroundColor}

        offersAndTabSliderBoxesTextColor = TEXT
        offersAndTabSliderBoxesTextColor.value = {$styling_settings.offersAndTabSliderBoxesTextColor}

        offersAndTabSliderBoxesInactiveBackgroundColor = TEXT
        offersAndTabSliderBoxesInactiveBackgroundColor.value = {$styling_settings.offersAndTabSliderBoxesInactiveBackgroundColor}

        offersAndTabSliderBoxesInactiveTextColor = TEXT
        offersAndTabSliderBoxesInactiveTextColor.value = {$styling_settings.offersAndTabSliderBoxesInactiveTextColor}

        formBadgeAndActionButtonsBackgroundColorAndMapInputFieldBorderColor = TEXT
        formBadgeAndActionButtonsBackgroundColorAndMapInputFieldBorderColor.value = {$styling_settings.formBadgeAndActionButtonsBackgroundColorAndMapInputFieldBorderColor}

        quoteBoxLogoColor = TEXT
        quoteBoxLogoColor.value = {$styling_settings.quoteBoxLogoColor}

        quoteIconColor = TEXT
        quoteIconColor.value = {$styling_settings.quoteIconColor}

        formIconColor = TEXT
        formIconColor.value = {$styling_settings.formIconColor}

        actionButtonBackgroundColor = TEXT
        actionButtonBackgroundColor.value = {$styling_settings.actionButtonBackgroundColor}

        actionButtonTextColor = TEXT
        actionButtonTextColor.value = {$styling_settings.actionButtonTextColor}

        sliderArrowBackgroundColor = TEXT
        sliderArrowBackgroundColor.value = {$styling_settings.sliderArrowBackgroundColor}

        fontFamilyDefault = TEXT
        fontFamilyDefault.value = {$styling_settings.fontFamilyDefault}

        fontWeightDefault = TEXT
        fontWeightDefault.value = {$styling_settings.fontWeightDefault}

        fontFamilyHeadings = TEXT
        fontFamilyHeadings.value = {$styling_settings.fontFamilyHeadings}

        fontWeightStrong = TEXT
        fontWeightStrong.value = {$styling_settings.fontWeightStrong}

        fontFamilySubheadings = TEXT
        fontFamilySubheadings.value = {$styling_settings.fontFamilySubheadings}

    }
}
