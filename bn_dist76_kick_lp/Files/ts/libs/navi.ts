# Menüs & Navigationen
# Verwendung von temp. anstatt lib. um unnötiges Cacheing zu vermeiden
# lib. nur verwenden wenn ein Element referenziert (=<) werden muss
#################################################

temp.LEGAL_MENU = COA
temp.LEGAL_MENU {
    10 = TEXT
    10 {
        noTrimWrap = |© | {$footer_legal_notice}|
        data = date:U
        strftime =%Y
    }

    20 = HMENU
    20 {
        wrap = <span class="no-custom-texting">|</span>
        special = list
        special.value = 45, 48

        1 = TMENU
        1 {
            noBlur = 1

            NO.ATagTitle.field = title
        }
    }
}
