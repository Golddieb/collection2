<?php
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = array(
    '_DEFAULT' => array(
        'cache' => array(
            // Exclude all URLs from cache that contain an additional query parameter
            // after id (e.g. Google campaign parameters gclid etc.)
            'banUrlsRegExp' => '/^(?!(?:L=|id=))|(?:id|cHash)=[0-9a-f]+(?:&|\?)/',
        ),
        'preVars' => array(
            array(
                'GETvar' => 'no_cache',
                    'valueMap' => array(
                        'nc' => 1,
                    ),
                    'noMatch' => 'bypass',
                ),
                array(
                    'GETvar' => 'L',
                    'valueMap' => array(
                        'it' => '1',
                        'en' => '2',
                    ),
                    'noMatch' => 'bypass',
                ),
            ),
    ),
);
