# Extension Ordner

Extensions dürfen NICHT im typo3conf Ordner verändert werden.
Müssen Änderungen an einer Extension vorgenommen werden, in diesem Verzeichnis einen eigenen Ordner anlegen und hier konfigurieren.
Laut Konvention besitzt jedes Verzeichnis eine *config.ts*, welche manuell in die *templates.ts* Datei eingebunden werden muss.
