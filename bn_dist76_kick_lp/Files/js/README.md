# Javascript Ordner

Alle eigenen Javascript Dateien finden hier Platz. Third Party Libs werden im Unterordner "vendor" abgelegt.
Immer unkomprimierte libs herunterladen, damit das Debuggen leichter fällt und bei erneuter Kompression keine Fehler auftreten.

*Achtung:* Libs mit eigenen CSS Dateien und Bildern (jquery-ui, magnific popup etc.) nicht hier abspeichern, sondern im eigens dafür vorgesehen /libs/ Ordner.


## Modernizr

Sollte es modernizr brauchen, die Bibliothek immer custom aus den tatsächlich benutzen Funktionalitäten zusammenstellen.


## Responsive Bibliotheken

Kurze Anleitung: [github](https://gist.github.com/davidspiess/8007596)