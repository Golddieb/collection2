(function () {
    'use strict';

    function initDynform () {
        /* eslint-disable */
        (function (b, n, f, o, r, m, s) {
            if(r&&!(new RegExp("^"+n+"\\/\\/"+f,"i")).test(r))m.cookie='bn_ref='+r+(o?(/\?/.test(r)?'&':'?')+o.substr(1):'')+'; path=/';
            if (m.querySelectorAll('bn-dynform-init')) {
                var i=0,d=[
                    n+'//admin.ehotelier.it/js/bn_dynform_v1.js',
                    n+'//slave.ehotelier.it/js/bn_dynform_v1.js'
                ],c = function c(e){var a=document.createElement("script");a.type="text/javascript",a.readyState?a.onreadystatechange=function(){"error"==a.readyState&&(a.onreadystatechange=null,e.length&&e.shift()&&(s.proxyUrl=e[0])&&c(e))}:a.onerror=function(){e.length&&e.shift()&&(s.proxyUrl=e[0])&&c(e)},a.src=e[0],document.getElementsByTagName("head")[0].appendChild(a)};
                (s.proxyUrl=d[0])&&c(d);
            }
        })(window, location.protocol, location.host, location.search, document.referrer, document, window.brandnamic);
        /* eslint-enable */
    }


    var formContainerElement = document.querySelector('.js-request-form-outer');

    // Lazy load the request form if the form container element exists
    formContainerElement && formContainerElement.addEventListener('lazybeforeunveil', function () {
        $.ajax({
            url: 'https://referrer.bnamic.com/referrer/referrer.js',
            dataType: 'script',
            cache: true,
        })
        .then(initDynform)
        .catch(function () {
            window.console && console.warn(
                'Loading of referrer.js failed. Could be because of an Adblocker.',
                arguments
            );
            initDynform();
        });
    });

}());

