/*global BN*/
(function () {
    'use strict';

    var webAnalyticsTracking = window.BN.functions.webAnalyticsTracking;
    var splitToArray = window.BN.functions.splitToArray;

    window.BN = window.brandnamic = (window.brandnamic || window.BN || {});

    var formContainerId = 'collective';
    var $formContainer = $('#' + formContainerId);

    // If the form container is not present on the page,
    // skip the rest of the execution
    if (!$formContainer.length) {
        return;
    }

    var $dynformsReplacementMarkup = $('.dynforms-replacement-markup');

    function onFormComplete () {
        var instance = this;
        var $cont = instance.$container;
        var formContainerId = instance.options.formcontainer_id;

        $cont.find('.bn__form-grid__js--submit')
            // Replace HTML content
            .html($dynformsReplacementMarkup
            .find('.js-replace-submit-button').html());

            // Insert the badge
        $cont.find('.bn__form__submit-button-container')
                .before($dynformsReplacementMarkup.find('.js-replace-badge').html())
                .removeClass('bn__form-grid__column__offset-left-3')
                .removeClass('bn__form-grid');

        // Remove the label of the comment field and add a placeholder
        $cont.find('[for="' + formContainerId + '_comment"]').remove();
        $cont.find('[id="' + formContainerId + '_comment"]')
            .attr('placeholder', BN.lang.i18n('comment_placeholder'));

        // Set the source of business
        instance.setField('sourceofbusiness', 'Landingpage');

        $cont.addClass('quickrequest_container_loaded');

        // If an offer was selected before the form was loaded, add it to the
        // form as soon as it’s loaded
        if (typeof BN.data.selectedOfferId !== 'undefined') {
            instance.setField('offer_id', BN.data.selectedOfferId);
        }

        // Hide the animation and show the form after it’s loaded
        // to prevent FOUC
        $('.js-request-form-outer')
            .removeClass('loading-animation')
            .removeClass('request-form-outer-loading');
    }

    function onFormSubmitCustom () {
        webAnalyticsTracking.trackEvent({
            eventCategory: 'Anfrage',
            eventAction: 'Formular absenden',
            eventLabel: $('.js-selected-offer-box .title').html(),
        });

        return true;
    }

    window.brandnamic.formDefaults = {
        all: {
            language: BN.lang.currLang,
            hotel_id: splitToArray($formContainer.data('ehotelier_id')).join(','),
            validation: 'live',
            // Uncomment clearcache: 1 during dynforms development
            // clearcache: 1,
        },
        lp_quickrequest_v1: {
            onFormComplete: onFormComplete,
            onFormSubmitCustom: onFormSubmitCustom,
        },
        lp_quickrequest_portal_v1: {
            portal_id: $formContainer.data('ehotelier_portal_id'),
            hotel_ids: splitToArray($formContainer.data('ehotelier_id')).join(','),
            onFormComplete: onFormComplete,
            onFormSubmitCustom: onFormSubmitCustom,
        },
    };

}());
