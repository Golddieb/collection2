(function () {
    'use strict';

    // Lazily load the widgets in the footer when scrolling into viewport

    function renderWidgetTemplate (event) {
        var $target = $(event.target);
        var widgetHTML = $target.find('.js-widget-template').html()
            .replace(/<\\\/script>/g, '</script>');
        $target.append(widgetHTML);
    }

    function insertPortalWidgets () {
        $('.js-widget-lazyload-trigger').each(function (index, element) {
            var $element = $(element);
            var $widgetTemplate = $element.find('.js-widget-template');

            $element.on('lazybeforeunveil', renderWidgetTemplate);
        });
    }

    // Export function to object
    window.BN.functions.insertPortalWidgets = insertPortalWidgets;

}());
