(function () {
    'use strict';

    // Load functions from global BN object
    var fetchFromEhotelier = window.BN.functions.fetchFromEhotelier;
    var handleErrors = window.BN.functions.handleErrors;

    var currLang = window.BN.lang.currLang;

    function fetchRooms (options) {
        var hotel_id = options.hotel_id;
        var roomtype_ids = options.roomtype_ids;

        return fetchFromEhotelier({
            servicePath: 'services/hoteldata/' + currLang,
            additionalParameters: {
                'portal_id': 0,
                'hotel_ids': hotel_id,
                'roomtype_ids': roomtype_ids,
            },
            serviceFields: {
                roomtypes: {
                    id: null,
                    name: null,
                    sizeinqm: null,
                    category: null,
                    defaultpersons: null,
                    description: null,
                    information: null,
                    image1: null,
                    image2: null,
                    image3: null,
                    image4: null,
                    asacode: null,
                    externalid: null,
                    roomprices: {
                        name: null,
                        description: null,
                        roompriceguests: {
                            '*': null
                        },
                        minstay: null,
                        maxstay: null,
                        board: {
                            '*': null
                        },
                        seasondatas: {
                            season: {
                                '*': null
                            },
                            fromdate: null,
                            todate: null
                        }
                    }
                }
            },
        }).catch(function (error) {
            handleErrors(error, 'Error while fetching eHotelier rooms');
        });
    }

    // Export function to object
    window.BN.functions.fetchRooms = fetchRooms;

}());
