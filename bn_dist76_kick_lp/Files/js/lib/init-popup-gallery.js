/*global $*/

(function () {
    'use strict';

    /**
     * Initializes a popup gallery using Magnific popup
     * @param  {object} $containerElement The jQuery element of one of
     * the parents of the popup gallery
     * @return {undefined}
     */
    function initPopupGallery ($containerElement) {
        $containerElement.find('.js-popup-gallery').each(function (index, element) {
            $(element).magnificPopup({
                delegate: '.js-popup-gallery-item',
                gallery: {
                    enabled: true,
                },
                type: 'image',
            });
        });
    }


    // Export function to object
    window.BN.functions.initPopupGallery = initPopupGallery;

}());
