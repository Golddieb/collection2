(function () {
    'use strict';

    /**
     * Log a message. Currently uses console.* to print
     * out messages
     * @param {mixed} message Either a string, an arry, object or something else
     * @param {string} loggerType One of the console methods
     * @return {undefined}
     */
    function logger (message, loggerType) {
        // console.log() by default
        var consoleMethod = console[loggerType || 'log'];

        if (window.console && consoleMethod) {
            consoleMethod.call(console, message);
        }
    }

    // Export function to object
    window.BN.functions.logger = logger;

}());
