/*global jQuery, Handlebars, _*/
(function ($) {
    'use strict';

    // Load functions from global BN object
    var fetchRooms = window.BN.functions.fetchRooms;
    var loadHbs = window.BN.functions.loadHbs;
    var handleErrors = window.BN.functions.handleErrors;

    // TODO: don’t duplicate retrival of constants
    // current translation and base constants
    var constants = $.extend(
        window.BN.lang.locales[window.BN.lang.currLang],
        window.BN.lang.locales.base
    );

    /**
     * Put alle roomtype images into an arry and optionally
     * sort the roomtypes according to an array of roomtype IDs
     * @param  {array} roomtypes
     * @param  {array} sortedRoomtypeIDsArray Is optional
     * @return {array} The processed and modified roomtypes array
     */
    function processRoomtypes (roomtypes, sortedRoomtypeIDsArray) {
        return _.flow(
            // Sort by the passed array of roomtype IDs if it’s present,
            // otherwise just pass the roomtypes array on without modification
            sortedRoomtypeIDsArray && sortedRoomtypeIDsArray.length
                ? _.sortBy(function (roomtype) {
                    return sortedRoomtypeIDsArray.indexOf(roomtype.id);
                })
                : _.identity,
            _.map(function (roomtype) {
                roomtype._images = ['image1', 'image2', 'image3', 'image4']
                    .map(function (imageNumber) {
                        return roomtype[imageNumber];
                    })
                    .filter(function (image) {
                        return image;
                    });

                return roomtype;
            })
        )(roomtypes);
    }

    /**
     * Render a room list by fetching the room data from eHotelier
     * @param  {object} options
     * @return {jQuery deferred object} A jQuery deferred that resolves
     * to the rendered template as value, ready to be attached to the DOM
     */
    function renderRoomList (options) {

        var hotel_id = options.hotel_id;
        var templatePath = options.templatePath;
        var roomtype_ids = options.roomtype_ids;

        return $.when(
            fetchRooms({
                hotel_id: hotel_id,
                roomtype_ids: roomtype_ids ? roomtype_ids.join(',') : null,
            }),
            loadHbs(templatePath)
        ).then(function (response, loadTemplateResult) {
            var templateContent = loadTemplateResult[0];
            var compiledTemplate = Handlebars.compile(templateContent);

            var responseObject = response.records[0];

            var transformedRoomtypes = processRoomtypes(responseObject.roomtypes, roomtype_ids);

            var templateContext = {
                roomtypes: transformedRoomtypes,
                trans: constants,
            };

            var renderedTemplate = compiledTemplate(templateContext);

            return renderedTemplate;
        }).catch(function (error) {
            handleErrors(error, 'Error while processing eHotelier rooms');
        });
    }

    // Export function to object
    window.BN.functions.renderRoomList = renderRoomList;

}(jQuery));
