(function () {
    'use strict';

    // Init global BN object with functions if it doesn’t already exist
    window.BN = window.BN || {};
    window.BN.functions = window.BN.functions || {};
    window.BN.data = window.BN.data || {};

}());
