/*global BN*/
(function ($) {
    'use strict';

    // Load data from global BN object
    var customArrowsConfig = window.BN.data.customArrowsConfig;
    var readMoreBox = window.BN.functions.readMoreBox;

    function initRoomSlider ($parentElement) {
        // Initialize sliders
        var $elementSlider = $parentElement
            .find('.js-tab-slider-box-slider:not(.slick-initialized)');
        var $tabSelectionSlider = $parentElement
            .find('.js-tab-slider-box-switcher:not(.slick-initialized)');

        // Initialize the image slider inside the tabs
        $parentElement
            .find('.js-tab-slider-box-image-slider')
            .each(function () {
                var $this = $(this);

                $this.owlCarousel({
                    // Only show dots if there’s more than one image
                    dots: $this.children().length > 1,
                    loop: true,
                    mouseDrag: false,
                    touchDrag: false,
                    freeDrag: false,
                    pullDrag: false,
                    items: 1,
                    dotsContainer: $this
                        .closest('.js-tab-slider-box-image-slider-outer')
                        .find('.js-tab-slider-box-image-slider-dots'),
                });

                $this.on('resize.owl.carousel', function () {
                    // Refresh the parent slider, otherwise some content could be cropped
                    // at the bottom on resize. This happens if the resizing of the owlcarousel
                    // inside the slick carousel happens before the slick resize is triggered
                    $elementSlider.slick('setPosition');
                });
            });

        $elementSlider.addClass('loaded');
        $elementSlider.slick($.extend({}, {
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: false,
            adaptiveHeight: true,
            asNavFor: $tabSelectionSlider,
            appendArrows: $parentElement.find('.js-slider-arrows-sticky-inner'),
            responsive: [
                {
                    breakpoint: 777,
                    fade: false,
                    settings: customArrowsConfig,
                }
            ]
        }, customArrowsConfig));

        var switchItemsCount = $tabSelectionSlider.find('.js-tab-slider-box-switcher-item').length;

        $tabSelectionSlider.slick({
            // If there are less than 6 items, set the icons to the actual
            // number of items to cover 100 % width.
            slidesToShow: switchItemsCount <= 6 ? switchItemsCount : 6,
            adaptiveHeight: true,
            asNavFor: $elementSlider,
            arrows: false,
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 380,
                    settings: {
                        slidesToShow: 1,
                        draggable: false,
                    },
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 2,
                    },
                },
                {
                    breakpoint: 650,
                    settings: {
                        slidesToShow: switchItemsCount <= 3 ? switchItemsCount : 3,
                    },
                },
                {
                    breakpoint: 850,
                    settings: {
                        slidesToShow: switchItemsCount <= 4 ? switchItemsCount : 4,
                    },
                },
            ],
        });

        // Truncate the text and add the more button
        readMoreBox({
            $selectorForBox: $elementSlider.find('.js-read-more-wrap'),
            height: 260,
            tolerance: 60,
            callback: function () {
                // Refresh slider, otherwise the height is calculated for
                // the expanded text, not the collapsed one
                $elementSlider.slick('setPosition');
            },
        });

        $parentElement.find('.js-slider-arrows-sticky-inner').Stickyfill();
    }

    // Export function to object
    window.BN.functions.initRoomSlider = initRoomSlider;

}(jQuery));
