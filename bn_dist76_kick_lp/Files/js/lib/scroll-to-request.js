/*global BN, jQuery*/
(function ($) {
    'use strict';

    // Load functions from global BN object
    var webAnalyticsTracking = window.BN.functions.webAnalyticsTracking;

    // Request Function
    function scrollToRequest (scrollTo) {
        if (scrollTo.length) {
            scrollTo.on('click', function () {
                var $this = $(this);
                // Better usability: This interrupts scrolling if
                // the user scrolls manually, presses a key etc.
                var eventNames = 'scroll mousedown DOMMouseScroll mousewheel keyup';
                $('html, body').bind(eventNames, function () {
                    $('html, body').stop();
                });
                // Scroll to body
                $('html, body').animate({
                    scrollTop: $('.quickrequest.container').offset().top
                }, 1000);

                if ($this.attr('data-offername')) {

                    var offerid = $this.data('offerid');

                    // TODO: Don’t construct the selected offer using DOM.
                    // Find closest item of the elment with the class item
                    var item = $this.closest('.item');
                    var offer_transfer = $('.js-offer-transfer');
                    offer_transfer.find('.js-selected-item-box-image').html(item.find('.image.hidden').html());
                    offer_transfer.find('.js-selected-item-box-text').html(' \
                        <div class="selected-item-box-title">' + item.find('.title').html() + '</div> \
                        <div>' + item.find('.season.hidden').html() + '</div> \
                        <div>' +
                        (item.find('.price.hidden').html() || '') + '</div>');
                    $('.js-selected-offer-box').removeClass('hidden');
                    $('.js-selected-offer-box').slideDown();

                    webAnalyticsTracking.trackEvent({
                        eventCategory: 'Hinzufügen eines Angebots zur Anfrage',
                        eventAction: 'Click',
                        eventLabel: item.find('.title').html(),
                    });


                    // If dynforms is not yet loaded, add the offer ID as variable in the
                    if (!BN.forms) {
                        BN.data.selectedOfferId = offerid;
                    } else {
                        // Otherwise add the offer directly to request form
                        BN.forms.collective.setField('offer_id', offerid);
                    }

                }
            });
        }
    }

    // Export function to object
    window.BN.functions.scrollToRequest = scrollToRequest;

}(jQuery));
