/*global jQuery*/
(function ($) {
    'use strict';

    // Load functions from global BN object
    var cacheBusting = window.BN.functions.cacheBusting;

    /**
     * Load a Handlebars template and append the
     * timestamp for cache expiration
     * @param  {string} url URL to the Handlebars template file
     * @return {jQuery deferred object}
     */
    function loadHbs (url) {
        return $.ajax({
            url: url,
            data: {
                cacheBusting: cacheBusting(5400),
            },
            dataType: 'html',
        });
    }

    // Export function to object
    window.BN.functions.loadHbs = loadHbs;

}(jQuery));
