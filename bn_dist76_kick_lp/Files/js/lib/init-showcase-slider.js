/*global picturefill*/

(function () {
    'use strict';

    // Load functions from global BN object
    var readMoreBox = window.BN.functions.readMoreBox;
    var scrollToElementByID = window.BN.functions.scrollToElementByID;
    var customArrowsConfig = window.BN.data.customArrowsConfig;
    var scrollToRequest = window.BN.functions.scrollToRequest;
    var initPopupGallery = window.BN.functions.initPopupGallery;

    function initShowcaseSlider (numberOfOffers, $containerElement) {
        // Compatibility for browsers that don’t support picture/srcset.
        // Needs to be called with dynamically loaded content
        // see https://github.com/scottjehl/picturefill/issues/633
        picturefill();

        $containerElement.removeClass('showcase-slider-container-loading');

        var sync1 = $containerElement.find('.js_offer_detail_items');
        var sync2 = $containerElement.find('.js_offerbox_switch_items');
        var $offer_from_category_slider = $containerElement
            .find('.js-offer_from_category_slider');

        // Truncate the text and add the more button
        readMoreBox({
            $selectorForBox: $containerElement.find('.js-read-more-wrap'),
            tolerance: 60,
            height: 800,
            callback: function () {
                // Refresh slider, otherwise the height is calculated for
                // the expanded text, not the collapsed one
                sync1.hasClass('slick-initialized') && sync1.slick('setPosition');
                // $offer_from_category_slider.slick('setPosition');
            },
        });


        // Load Slider
        // Bildergalerie Accordion


        sync1.slick({
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: sync2,
            adaptiveHeight: true,
        });

        sync2.slick({
            infinite: false,
            // If there are two offers, show 2 slides, otherwise 3
            slidesToShow: numberOfOffers === 2 ? 2 : 3,
            asNavFor: sync1,
            arrows: false,
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            appendArrows: $containerElement.find('.js-slider-arrows-sticky-inner'),
            responsive: [
                {
                    breakpoint: 700,
                    settings: $.extend({}, {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }, customArrowsConfig)
                }
            ]
        });

        scrollToRequest($containerElement.find('.request.scroll'));


        $offer_from_category_slider.slick(
            $.extend({}, {
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                adaptiveHeight: true,
                appendArrows: $containerElement
                    .find('.js-slider-arrows-sticky-inner'),
            }, customArrowsConfig)
        );

        $containerElement.find('.js-slider-arrows-sticky-inner').Stickyfill();

        scrollToElementByID(location.hash);

        initPopupGallery($containerElement);
    }

    // Export function to object
    window.BN.functions.initShowcaseSlider = initShowcaseSlider;

}());
