(function () {
    'use strict';

    // These are code snippets that do certain things to help the editor.
    // The buttons are only shown if the user is authenticated in the backend.

    /**
     * Highlights all elements which have a `no-custom-texting` class
     */
    function highlightNoCustomText () {
        var elements = document.querySelectorAll('.no-custom-texting');
        [].forEach.call(elements, function (DOMElement) {
            DOMElement.classList.add('highlight-for-editor');
        });
    }

    /**
     * Shows the full links with anchor below each heading
     */
    function showHeadingAnchors () {
        var elements = document.querySelectorAll('h1, h2, h3, h4, h5, h6');

        for (var i = 0; i < elements.length; i++) {
            var link =location.protocol+'//'+location.host+location.pathname +
                '#' + elements[i].id;
            elements[i].innerHTML += '<a style="display: block; ' +
                'font-size: 18px; text-transform: initial;" href=' +
                link + '>' + link + '</a>';
        }
    }

    /**
     * Makes the entire page editable in the browser
     */
    function makeDocumentEditable () {
        document.body.setAttribute('contenteditable', true);
    }

    function onclick (elementSelector, execFunction) {
        var DOMElement = document.querySelector(elementSelector);

        if (!DOMElement) {
            return;
        }

        DOMElement.addEventListener('click', execFunction);
    }

    onclick('.editor-snippet-no-custom-texting', highlightNoCustomText);
    onclick('.editor-snippet-document-editable', makeDocumentEditable);
    onclick('.editor-snippet-show-heading-anchor-links', showHeadingAnchors);

    var editorSnippetButtonsElement = document.querySelector('.js-editor-snippet-buttons');

    function showButtonsOnKeyboardShortcut (event) {
        // If the user has pressed `Ctrl + i` or `Cmd + i`
        if ((event.ctrlKey || event.metaKey) && event.which === 73) {
            editorSnippetButtonsElement.removeAttribute('hidden');
        }
    }

    if (editorSnippetButtonsElement) {
        document.addEventListener('keydown', showButtonsOnKeyboardShortcut);
    }

}());
