(function ($) {
    'use strict';

    var webAnalyticsTracking = window.BN.functions.webAnalyticsTracking;

    function handleCookiebanner () {
        var $body = $('body');

        new window.BN.CookieBanner({
            excludes: '.cb-infotext .cb-text, .cb-infolink, ' +
                '.cb-infotext .infolink, .legal-menu a',
            activationSnippets: {
                triggerCookiebannerConfirmed: function () {
                    webAnalyticsTracking.trackEvent({
                        event: 'CookiebannerConfirmed',
                    });

                    // Remove margin for cookie banner
                    $body.css('margin-top', '');

                }
            },
            techcookies: [
                '_ga',
                '_gat',
                'be_typo_user',
                'PHPSESSID',
                't3psob',
                't3pref',
                't3pfilter',
                't3pentry',
            ],
        });

        if ($('#cookiebanner').is(':visible')) {
            $body.css('margin-top', $('#cookiebanner').height());
        }
    }

    // Export function to object
    window.BN.functions.handleCookiebanner = handleCookiebanner;

}(jQuery));
