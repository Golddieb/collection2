/*global jQuery*/
(function ($) {
    'use strict';

    // Load functions from global BN object
    var fetchFromEhotelier = window.BN.functions.fetchFromEhotelier;
    var handleErrors = window.BN.functions.handleErrors;

    /**
     * Fetch textplugin data from eHotelier
     * @param  {object} options Input object, consisting of the following properties
     * * language
     * * textplugin_id
     * * hotel_id
     * * cookie_group_id
     * * selected_textplugins Should be an array of IDs
     * @return {jQuery deferred} The jQuery deferred object of the request. Chain it
     * with a .then() method. Don’t forget to .catch() errors.
     */
    function fetchEhotelierTextplugin (options) {

        var language = options.language;
        var textplugin_id = options.textplugin_id;

        return fetchFromEhotelier({
            servicePath: 'services/textplugin/' + language + '/' + textplugin_id,
            dataType: 'html',
            additionalParameters: $.extend({}, options, {
                // It’s not a portal
                portal_id: 0,

                // Set options which shouldn’t appear in the query string to null
                language: null,
                textplugin_id: null,
            })
        }).catch(function (error) {
            handleErrors(error, 'Error while fetching textplugin data form eHotelier');
        });
    }

    // Export function to object
    window.BN.functions.fetchEhotelierTextplugin = fetchEhotelierTextplugin;

}(jQuery));
