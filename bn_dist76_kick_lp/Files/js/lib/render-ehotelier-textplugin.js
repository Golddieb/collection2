/*global jQuery*/
(function ($) {
    'use strict';

    // Load functions from global BN object
    var fetchEhotelierTextplugin = window.BN.functions.fetchEhotelierTextplugin;
    var handleErrors = window.BN.functions.handleErrors;
    var splitToArray = window.BN.functions.splitToArray;


    var language = window.BN.lang.currLang;
    var bnI18n = window.BN.lang.i18n;


    /**
     * Converts an object path represented as string in dot notation in bracket notation
     * @param  {string} inputKey The input key as string, e.g. 'myProp' or 'myProp.mySubprop'
     * @return {string} The input object path, but in brackets notation, e.g. 'myProp[mySubprop]'
     */
    function replaceKeyDotWithBracketNotation (inputKey) {

        // If it doesn’t contain a dot, return the key as it is
        if (inputKey.indexOf('.') === -1) {
            return inputKey;
        }

        var keyPathArray = inputKey.split('.');

        var secondKeyPath = keyPathArray
            .slice(1)
            .map(function (keyPathPartial) {
                return '[' + keyPathPartial + ']';
            })
            .join('');

        return keyPathArray[0] + secondKeyPath;
    }

    /**
     * Transforms the keys of an object (only those at the first level).
     * string to bracket notation.
     * Example: input: {'myProp': true}, output: {'myTransformedProp': true}
     *
     * @param  {[type]} inputObject [description]
     * @param {function} transformFunction A transform function that accepts one argument:
     * the object’s key
     * @return {object} The transformed object
     */
    function stringTransformObjectKeys (inputObject, transformFunction) {
        return Object.keys(inputObject).reduce(function (objectToBuild, key) {
            var transformedKey = transformFunction(key);

            objectToBuild[transformedKey] = inputObject[key];

            return objectToBuild;
        }, {});
    }

    /**
     * Transforms object properties (only at first level)
     * @param  {object} inputObject
     * @param  {function} transformFunction
     * @return {object} The transformed object
     */
    function transformObjectProps (inputObject, transformFunction) {
        return Object.keys(inputObject).reduce(function (objectToBuild, key) {
            objectToBuild[key] = transformFunction(inputObject[key]);

            return objectToBuild;
        }, {});
    }

    /**
     * Trim whitespace after commas
     * @param  {mixed} input
     * @return {string} string output
     */
    function trimSpacesAfterComma (input) {

        if (typeof input !== 'string') {
            return input;
        }

        return input.replace(/,\s*/g, ',');
    }



    $('.js-ehotelier-text-plugin-container').each(function () {
        var $targetElement = $(this);
        var elementData = $targetElement.data();

        var elementDataTransformedKeys = stringTransformObjectKeys(
            elementData,
            replaceKeyDotWithBracketNotation
        );

        var elementDataTransformedProps = transformObjectProps(
            elementDataTransformedKeys,
            trimSpacesAfterComma
        );

        fetchEhotelierTextplugin($.extend({}, elementDataTransformedProps, {
            language: language,
            // TODO: this approach of applying the correct data afterwards is
            // a bit ugly
            'custom_params[textplugins]': splitToArray(
                elementDataTransformedProps['custom_params[textplugins]']
            ).join(','),
        }))
        .then(function (responseText) {

            if (!responseText || /^\s*$/.test(responseText)) {
                throw new Error('Response is empty. Check if the request is correct.');
            }

            // Render response in DOM
            $targetElement.html(responseText);
        })
        .catch(function (error) {

            // Render the error text in case of error
            $targetElement.html('<p>' + bnI18n('error_while_loading') + '</p>');

            handleErrors(error, 'Error while fetching/rendering the eHotelier textplugin text');
        });
    });


}(jQuery));
