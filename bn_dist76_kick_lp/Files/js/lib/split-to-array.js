(function () {
    'use strict';

    /**
     * Splits a string to an arry using a separator. Casts to JS numbers if possible.
     * Returns null if an empty string (or one with only whitespace) is passed
     * Trims whitespace and removes falsy values from the result
     * @param  {string} inputString
     * @param  {string} separator The separator where to split. Comma by default
     * @return {array|null}
     */
    function splitToArray(inputString, separator) {
        separator = separator || ',';

        if (typeof inputString !== 'string') {
            throw TypeError('The input must be string.');
        }

        // Return null if it’s an empty string
        if (!inputString.trim()) {
            return null;
        }

        return inputString
            .split(separator)
            .map(function (item) {
                var trimmedItem = item.trim();

                // Cast to number if it’s a number represented as string
                if (isNaN(+trimmedItem)) {
                    return trimmedItem;
                }

                return +trimmedItem;
            })
            .filter(function (item) {
                // Filter out falsy values
                return !!item;
            });
    }

    // Export function to object
    window.BN.functions.splitToArray = splitToArray;

}());
