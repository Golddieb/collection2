/*global google*/

// TODO: refactor code, remove global functions, cleanup etc.
(function () {
    'use strict';

    // Load functions from global BN object
    var handleErrors = window.BN.functions.handleErrors;

    /**
     * Cast a number with either a comma or a dot as decimal separator to a string
     * @param  {string || number} inputNumber
     * @return {null || number}
     */
    function toNumber (inputNumber) {
        if (typeof inputNumber === 'undefined' || inputNumber === null) {
            window.console && console.warn('No number passed to function');
            return null;
        }

        var castedNumber = +inputNumber.toString().replace(',', '.');

        if (isNaN(castedNumber)) {
            window.console && console.warn('An invalid number was passed');
            return null;
        }

        return castedNumber;
    }

    var currLang = window.BN.lang.currLang;

    var directionsDisplay;
    var directionsService;
    var latlng;
    var map;
    var marker;
    var info;

    var mapConfigData = $('.js-google-maps-element').data();

    // If the element is on the page
    if (mapConfigData) {
        // Casts numbers as strings to number
        var centerLatitude = toNumber(mapConfigData.centerLatitude);
        var centerLongitude = toNumber(mapConfigData.centerLongitude);
        var destinationAddress = mapConfigData.destinationAddress;
        var zoomFactor = toNumber(mapConfigData.zoomFactor);
        var markerTitle = mapConfigData.markerTitle;
        var markerIconPath = mapConfigData.markerIconPath;

        var $mapCanvasRoute = $('#map_canvas_route');

        // Load Google Maps lazily when it’s in the viewport
        $mapCanvasRoute.on('lazybeforeunveil', function () {
            $.ajax({
                url: 'https://maps.google.com/maps/api/js',
                dataType: 'script',
                cache: true,
                data: {
                    key: 'AIzaSyBzEBRcHWtJdr-ijLxsaRR7eDUSqT1535w',
                    sensor: false,
                    libraries: 'places',
                    language: currLang,
                },
            }).then(function () {
                directionsService = new google.maps.DirectionsService();
                latlng = new google.maps.LatLng(centerLatitude, centerLongitude);
                initialize_route();
            }).catch(handleErrors);
        });


        $('body').on('click', '#map-error .button', function () {
            $('#map-error').find('.error-message').html();
            $('#map-error').hide();
        });

        $('.google-maps-search-form').on('submit', function (event) {
            event.preventDefault();
            calc_route();
        });
    }

    function initialize_route () {

        directionsDisplay = new google.maps.DirectionsRenderer();
        var styles = [{
            featureType: 'water',
            elementType: 'all',
            stylers: []
        }, {
            featureType: 'landscape',
            elementType: 'all',
            stylers: []
        }, {
            featureType: 'road',
            elementType: 'all',
            stylers: []
        }, {
            featureType: 'poi',
            elementType: 'labels',
            stylers: []
        }, {
            featureType: 'administrative',
            elementType: 'all',
            stylers: []
        }];

        var myOptions = {
            center: latlng,
            disableDefaultUI: true,
            zoomControl: true,
            zoom: zoomFactor,
            mapTypeId: 'Styled',
            scrollwheel: false
        };

        map = new google.maps.Map(document.getElementById('map_canvas_route'), myOptions);
        marker = new google.maps.Marker({
            map: map,
            position: latlng,
            title: markerTitle,
            icon: markerIconPath,
        });

        var styledMapType = new google.maps.StyledMapType(styles, {
            name: 'Styled'
        });
        map.mapTypes.set('Styled', styledMapType);


        directionsDisplay.setMap(map);

        $mapCanvasRoute.on('click', function () {
            $mapCanvasRoute.addClass('zoomable');
            map.setOptions({
                scrollwheel: true
            });
        });

        $(window).on('scroll', function () {
            if ($mapCanvasRoute.hasClass('zoomable')) {
                if ($(window).scrollTop() > 1) {
                    $mapCanvasRoute.removeClass('zoomable');
                    map.setOptions({
                        scrollwheel: false
                    });
                }
            }
        });

        var input = /** @type {!HTMLInputElement} */ (document.getElementById('gm_start'));
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        info = new google.maps.InfoWindow({
            size: google.maps.Size(50, 50)
        });
        info.set('isdomready', false);
    }


    function calc_route () {

        var start = $('#gm_start').val();
        var destination = destinationAddress;

        if (start !== '') {
            var request = {
                origin: start,
                destination: destination,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };

            directionsService.route(request, function (response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    marker.setMap(null);
                    directionsDisplay.setDirections(response);


                } else {
                    var errormessage = $('#error_address_notfound').html();

                    $('#map-error').find('.error-message').html(errormessage);
                    $('#map-error').show();
                }
            });
        } else {
            var errormessage = $('#error_insert_address').html();

            $('#map-error').find('.error-message').html(errormessage);
            $('#map-error').show();
        }
    }

}());
