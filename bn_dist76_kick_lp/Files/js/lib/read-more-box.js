(function () {
    'use strict';

    function readMoreBox (options) {
        var $selectorForBox = options.$selectorForBox;
        var callback = options.callback;
        var height = options.height;
        var tolerance = options.tolerance;

        function eachSelectedBox ($selectorForBox, iteratorFunction) {
            $selectorForBox.each(function (index, selectedWrapperElement) {
                var $selectedWrapperElement = $(selectedWrapperElement);

                var $textToTruncate = $selectedWrapperElement.find('.js-read-more-text');
                var $moreButton = $selectedWrapperElement.find('.js-read-more-button');
                var $fullTextContainer = $selectedWrapperElement.find('.js-full-text-container');

                iteratorFunction({
                    $textToTruncate: $textToTruncate,
                    $moreButton: $moreButton,
                    $fullTextContainer: $fullTextContainer,
                });
            });
        }

        function toggleReadMoreStatus (event) {

            eachSelectedBox($selectorForBox, function (data) {
                var $textToTruncate = data.$textToTruncate;
                var $moreButton = data.$moreButton;
                var $fullTextContainer = data.$fullTextContainer;

                $textToTruncate.trigger('originalContent', function (originalContent) {
                    $fullTextContainer.html(originalContent);
                    $textToTruncate.toggleClass('hidden');
                    $fullTextContainer.toggleClass('hidden');
                    $moreButton.toggleClass('read-more-button-for-expanded');
                });
            });

            if (typeof callback === 'function') {
                callback();
            }
        }

        eachSelectedBox($selectorForBox, function (data) {
            var $textToTruncate = data.$textToTruncate;
            var $moreButton = data.$moreButton;
            var $fullTextContainer = data.$fullTextContainer;

            $textToTruncate.dotdotdot({
                watch: 'window',
                tolerance: tolerance,
                height: height,
            });

            // var isTruncated = $textToTruncate.triggerHandler('isTruncated');

            // if (isTruncated) {
            //     $moreButton.removeClass('hidden');
            // }
        });

        if (typeof callback === 'function') {
            callback();
        }

        $selectorForBox.on('click', '.js-read-more-button', toggleReadMoreStatus);
    }

    // Export function to object
    window.BN.functions.readMoreBox = readMoreBox;

}());
