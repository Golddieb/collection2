(function () {
    'use strict';

    // Load functions from global BN object
    var fetchFromEhotelier = window.BN.functions.fetchFromEhotelier;
    var handleErrors = window.BN.functions.handleErrors;
    var cacheBusting = window.BN.functions.cacheBusting;

    var currLang = window.BN.lang.currLang;

    /**
     * Fetch offers from eHotelier
     * @param  {object} options
     * @return {jQuery deferred object}
     */
    function fetchOffers (options) {

        var hotel_id = options.hotel_id;
        var offercategories = options.offercategories;
        var portal_id = options.portal_id || 0;
        var limit = options.limit || 100;

        return fetchFromEhotelier({
            servicePath: 'services/offer/' + currLang,
            additionalParameters: {
                'portal_id': portal_id,
                'hotel_ids': hotel_id,
                'offercategories': offercategories,
                'getexpired': 'false',
                'sort': 'fromdate:asc',
                'limit': limit,
                // If there are prices, bust the cache every hour
                'cacheBusting': cacheBusting(3600),
            },
            serviceFields: {
                '*': null,
                'offercategories': {
                    'id': null,
                    'title': null
                },
                'offerhotels': {
                    '*': null,
                    'roomprices': {
                        '*': null,
                        'roomtype': {
                            '*': null
                        },
                        'roompriceguests': {
                            '*': null
                        },
                        'board': {
                            '*': null
                        }
                    }
                },
                'seasondatas': {
                    '*': null
                }
            },
        }).catch(function (error) {
            handleErrors(error, 'Error while fetching offers form eHotelier');
        });
    }

    // Export function to object
    window.BN.functions.fetchOffers = fetchOffers;

}());
