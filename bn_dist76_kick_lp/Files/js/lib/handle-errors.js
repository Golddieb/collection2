(function () {
    'use strict';

    function handleErrors (error, additionalMessage) {
        window.console && console.error(
            additionalMessage || 'An error occurred:', error
        );
    }

    // Export function to object
    window.BN.functions.handleErrors = handleErrors;

}());
