/*global $*/
(function () {
    'use strict';

    // Load data from global BN object
    var customArrowsConfig = window.BN.data.customArrowsConfig;

    $('.js-multi-slider').each(function (index, multiSlider) {
        var $multiSlider = $(multiSlider);

        // If the user navigates to another tab, refresh the slider,
        // otherwise it doesn’t get the correct dimensions, see
        // https://github.com/kenwheeler/slick/issues/187#issuecomment-41548888
        $multiSlider.on('shown.bs.tab', '[data-toggle="tab"]', function (event) {
            var $targetTab = $(event.target.getAttribute('href'));
            $targetTab.find('.js-multi-slider-inner-slider').slick('setPosition');
        });

        $multiSlider.find('.js-multi-slider-inner').each(function (index, element) {
            var $element = $(element);
            $element.find('.js-multi-slider-inner-slider').slick($.extend({}, {
                infinite: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                mobileFirst: true,
                responsive: [{
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                }, {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                }],
                appendArrows: $element.find('.js-slider-arrows-sticky-inner'),
            }, customArrowsConfig));

        });
    });

}());
