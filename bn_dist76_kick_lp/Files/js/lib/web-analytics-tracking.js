/*global dataLayer*/
(function () {
    'use strict';

    function trackEvent (options) {
        // Default value of 'AnalyticsEvent'
        var event = options.event || 'AnalyticsEvent';
        var eventCategory = options.eventCategory;
        var eventAction = options.eventAction;
        var eventLabel = options.eventLabel;

        dataLayer.push({
            event: event,
            eventCategory: eventCategory,
            eventAction: eventAction,
            eventLabel: eventLabel,
        });
    }

    function trackLinkClickEvent (eventCategory, thisElement) {
        trackEvent({
            eventCategory: eventCategory,
            eventAction: 'Click',
            eventLabel: $(thisElement).attr('href'),
        });
    }

    // Export to global namespace
    window.BN.functions.webAnalyticsTracking = {
        trackEvent: trackEvent,
        trackLinkClickEvent: trackLinkClickEvent,
    };

}());
