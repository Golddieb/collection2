/*global jQuery*/
(function ($) {
    'use strict';

    // Load functions from global BN object
    var cacheBusting = window.BN.functions.cacheBusting;
    var handleErrors = window.BN.functions.handleErrors;

    /**
     * Fetch data from eHotelier
     * @param  {object} options Input object, consisting of the following properties
     * * servicePath
     * * serviceFields: an object for the fields
     * * additionalParameters: an object with additional parameters to add to the URL
     * @return {jQuery deferred} The jQuery deferred object of the request. Chain it
     * with a .then() method. Don’t forget to .catch() errors.
     */
    function fetchFromEhotelier (options) {
        var servicePath = options.servicePath;
        var serviceFields = options.serviceFields;
        var additionalParameters = options.additionalParameters;
        // JSON data by default
        var dataType = options.dataType || 'json';

        var urlData = $.extend(
            {}, {
                path: servicePath,
                fields: JSON.stringify(serviceFields),
                // Let the cache expire exery day
                cacheBusting: cacheBusting(3600 * 24),
            }, additionalParameters
        );

        // Make the request
        return $.ajax({
            url: 'https://admin.ehotelier.it/bnlocalproxy.php',
            dataType: dataType,
            data: urlData,
        })
            .then(function (responseData) {
                // If the eHotelier API returns an error message, throw it
                if (responseData.error) {
                    throw Error(responseData.error);
                }

                return responseData;
            })
            .catch(function (error) {
                handleErrors(error, 'Error while fetching from the eHotelier service');
            });
    }

    // Export function to object
    window.BN.functions.fetchFromEhotelier = fetchFromEhotelier;

}(jQuery));
