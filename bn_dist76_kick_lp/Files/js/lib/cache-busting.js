(function () {
    'use strict';

    /**
     * Returns a timestamp that
     * invalidates the cache. If no parameter is passed,
     * the default timestamp is 1 hour.
     * @param  {number} expireSeconds Seconds after the cache should expire
     * @return {number} The query string parameter without ? or &.
     */
    function cacheBusting (expireSeconds) {
        var expireTime = expireSeconds || 3600;

        if (typeof expireTime !== 'number') {
            throw new TypeError('Input seconds are not a number.');
        }

        return Math.floor(Date.now() / 1000 / expireTime);
    }

    // Export function to object
    window.BN.functions.cacheBusting = cacheBusting;

}());
