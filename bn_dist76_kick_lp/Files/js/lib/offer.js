/*global Handlebars, _, picturefill*/

(function () {
    'use strict';

    // Load functions from global BN object
    var fetchOffers = window.BN.functions.fetchOffers;
    var loadHbs = window.BN.functions.loadHbs;
    var handleErrors = window.BN.functions.handleErrors;
    var splitToArray = window.BN.functions.splitToArray;
    var initShowcaseSlider = window.BN.functions.initShowcaseSlider;

    // current translation and base constants
    var constants = $.extend(
        window.BN.lang.locales[window.BN.lang.currLang],
        window.BN.lang.locales.base
    );

    /**
     * Get the default roomprice (the one corresponding to the default persons)
     * for each roomprices object in the roomprices array
     * @param  {array} input roomprices array from eHotelier API
     * @return {array}
     */
    var getDefaultRoomprices = _.flow(
        // Construct an object that contains the calculated price per Person and the
        // amount of nights for each roomprice
        _.map(function (roompricesItem) {
            var roomDefaultguestcount = roompricesItem.roomtype[0].defaultpersons;
            // Find the roompriceguests object with the default guest count and divide
            // the amount by the default guest count
            var roompriceObjectFound = roompricesItem.roompriceguests
                .find(function (roompriceguestsItem) {
                    return roompriceguestsItem.guestcount === roomDefaultguestcount;
                });

            // If no price is found, return
            // an empty object to avoid future errors
            if (!roompriceObjectFound) {
                return {};
            }

            var roompricePerPerson = roompriceObjectFound.amount / roomDefaultguestcount;

            return {
                nights: roompricesItem.nights,
                roompricePerPerson: roompricePerPerson,
            };
        })
    );

    /**
     * Group a roomprices array by the amount of nights
     * @param {array} the input array of roomprices coming from the eHotelier API
     * @return {object} grouped room prices with the amount of nights as keys
     */
    var groupRoompricesByNights = _.flow(
        getDefaultRoomprices,
        // Group to an object with the amount of nights as keys
        _.groupBy('nights')
    );

    /**
     * Calculate the cheapest room price for each amount of nights
     * @param {array} the input array of roomprices coming from the eHotelier API
     * @return {array} the ascendingly sorted array of nights with the
     * corresponding minimum price
     */
    var getCheapestRoompricesPerNights = _.flow(
        groupRoompricesByNights,
        // Map to an array of an array of objects containing the
        // cheapest roomprice for the corresponding amonut of nights
        _.map(function (roompricesGroupByNightsAmount) {
            var prices = _.map('roompricePerPerson', roompricesGroupByNightsAmount);
            var minPrice = Math.min.apply(Math, prices);

            return {
                nights: roompricesGroupByNightsAmount[0].nights,
                minPrice: minPrice,
            };
        }),
        // Sort by nights in ascending order
        _.sortBy('nights')
    );

    /**
     * Get the cheapest roomprice from eHotelier’s roomprices array
     * @param  {array} roompricesArray The `roomprices` array from an eHotelier response
     * @return {object} An object with the cheapest roomprice found and the amount of nights
     */
    var getCheapestRoomprice = _.flow(
        getDefaultRoomprices,
        // Reduce the array to the cheapest constructed room price object
        _.reduce(function (savedCheapestRoompriceObject, currentRoompriceObject) {
            var savedCheapestPrice = savedCheapestRoompriceObject.roompricePerPerson;
            var currentPrice = currentRoompriceObject.roompricePerPerson;

            if (savedCheapestPrice < currentPrice) {
                return savedCheapestRoompriceObject;
            }

            return currentRoompriceObject;
        }, {})
    );

    /**
     * Convert a date or an Unix timestamp to ISO 8601 (without time)
     * @param  {number/dateObject} inputDate
     * @return {string} The ISO 8601 date string
     */
    function dateToISO8601 (inputDate) {
        return new Date(inputDate).toISOString().match(/^[\d-]+/)[0];
    }

    /**
     * Takes an array of objects with from/todates and calculates
     * the minimum and the maximum date
     * @param  {array} seasondatas Array of objects with `fromdate` and `todate` properties
     * @return {object} Object with `minDate` and `maxDate` properties.
     */
    function getFarthestDates (seasondatas) {
        var allDates = seasondatas.reduce(function (tempDatesArray, currentSeasonObj) {
            var fromdate = Date.parse(currentSeasonObj.fromdate);
            var todate = Date.parse(currentSeasonObj.todate);

            return tempDatesArray.concat(fromdate, todate);
        }, []);

        return {
            minDate: dateToISO8601(Math.min.apply(Math, allDates)),
            maxDate: dateToISO8601(Math.max.apply(Math, allDates)),
        };
    }


    // offer
    // TODO: a part of the error stack seems to be swallowed
    // in this function (maybe it’s jQuery’s promise implementation)?
    function offer (options) {

        var hotel_id = options.hotel_id;
        var $containerElement = options.$containerElement;
        var containerElementData = $containerElement.data();
        var categories = splitToArray(containerElementData.offercategories);
        // Cast to number and then to boolean
        var forceOfferFromPriceUsage = Boolean(+containerElementData.forceOfferFromPriceUsage);

        var elementSettings = {
            showOfferTitleInRequestButton: containerElementData.showOfferTitleInRequestButton,
        };

        fetchOffers({
            limit: 20,
            hotel_id: hotel_id,
            offercategories: categories ? categories.join(',') : null,

        }).then(function (jsondata) {
            jsondata.trans = constants;
            jsondata.settings = elementSettings;

            // Calculate the cheapest room price and the minimum/maximum
            // season dates. Append the results to the future
            // context
            jsondata.records = jsondata.records.map(function (record) {
                var roomprices = record.offerhotels[0].roomprices;
                var useMinamount = !roomprices.length || forceOfferFromPriceUsage;

                var cheapestRoomprice = getCheapestRoomprice(roomprices);
                var cheapestRoompricesPerNights = getCheapestRoompricesPerNights(roomprices);
                var farthestDates = getFarthestDates(record.seasondatas);

                var pricesFromMinamount = [{
                    minPrice: record.minamount,
                    roompricePerPerson: record.minamount,
                    nights: record.nights,
                }];

                // Return the object with the mixins
                // TODO: improve and simplify data structure for rendering
                return $.extend({}, record, {
                    _farthestDates: farthestDates,
                    // Choose the source of prices: minamount or roomprices array
                    _cheapestRoomprice: useMinamount
                        ? pricesFromMinamount[0]
                        : cheapestRoomprice,
                    _cheapestRoompricesPerNights: useMinamount
                        ? pricesFromMinamount
                        : cheapestRoompricesPerNights,
                    offerhotels: [
                        $.extend({},
                            record.offerhotels[0], {
                                _farthestDates: farthestDates
                            }
                        ),
                    ],
                });
            });

            var numberOfOffers = jsondata.records.length;

            // Switch the template according to the number of offers

            // If there is only one offer or more than three, use the template
            // with the single, full width offer image and a slider for > 3 offers
            if (numberOfOffers === 1 || numberOfOffers > 3) {
                $.when(
                    loadHbs('/typo3conf/ext/bn_typo_dist76/Files/js/templates/offers-full-with-image.hbs'),
                    loadHbs('/typo3conf/ext/bn_typo_dist76/Files/js/templates/offer-detail-partial.hbs')
                ).then(function (offerCategoryTemplateArray, offerDetailPartialArray) {
                    var offerCategoryTemplate = offerCategoryTemplateArray[0];
                    var offerDetailPartial = offerDetailPartialArray[0];

                    Handlebars.registerPartial('offerDetail', offerDetailPartial);

                    var compiledOfferCategoryTemplate = Handlebars.compile(offerCategoryTemplate);

                    $containerElement.html(compiledOfferCategoryTemplate(jsondata));

                    // Only run if there are offers in the response
                    numberOfOffers && initShowcaseSlider(numberOfOffers, $containerElement);

                }).catch(handleErrors);

            } else {
                $.when(
                    loadHbs('/typo3conf/ext/bn_typo_dist76/Files/js/templates/offers-with-switch-slider.hbs'),
                    loadHbs('/typo3conf/ext/bn_typo_dist76/Files/js/templates/offer-detail-partial.hbs'),
                    loadHbs('/typo3conf/ext/bn_typo_dist76/Files/js/templates/offer-switch-slider-partial.hbs')

                // If all files are loaded proceed with rendering
                ).then(function (
                    offerTemplateArray,
                    offerDetailPartialArray,
                    offerSwitchSliderPartialArray
                ) {
                    var offerTemplate = offerTemplateArray[0];
                    var offerDetailPartial = offerDetailPartialArray[0];
                    var offerSwitchSliderPartial = offerSwitchSliderPartialArray[0];

                    Handlebars.registerPartial('offerDetail', offerDetailPartial);
                    Handlebars.registerPartial('offerSwitchSlider', offerSwitchSliderPartial);

                    var compiledOfferTemplate = Handlebars.compile(offerTemplate);

                    $containerElement.html(compiledOfferTemplate(jsondata));

                    // Only run if there are offers in the response
                    numberOfOffers && initShowcaseSlider(numberOfOffers, $containerElement);

                }).catch(handleErrors);
            }
        }).catch(handleErrors);
    }

    // Export function to object
    window.BN.functions.offer = offer;

}());
