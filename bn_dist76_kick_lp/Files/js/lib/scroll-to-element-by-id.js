(function () {
    'use strict';

    /**
     * Scroll to a given id of an element. This is necessary because
     * the browser functionality for jumping to hash doesn’t
     * take elements into account that are created with JS.
     * Thus the scrolling position could mismatch after another
     * content is loaded. This functions needs to be manually
     * triggered after each change ()
     * @param  {string} elementID The element ID to scroll to, including the hash
     * @return {undefined}
     */
    function scrollToElementByID (elementID) {
        if (!elementID) return;

        var element = document.querySelector(elementID);

        if (!element) return;

        element.scrollIntoView();
    }

    // Export to global namespace
    window.BN.functions.scrollToElementByID = scrollToElementByID;

}());




