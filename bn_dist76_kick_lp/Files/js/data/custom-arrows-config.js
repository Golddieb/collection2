(function () {
    'use strict';

    var customArrowsConfig = {
        arrows: true,
        prevArrow: '\
            <button class="prev slider-arrow js-slider-arrow \
            slider-arrow-prev icon-lp-arrow-left"></button>',
        nextArrow: '\
            <button class="next slider-arrow js-slider-arrow \
            slider-arrow-next icon-lp-arrow-right"></button>',
    };

    // Export function to object
    window.BN.data.customArrowsConfig = customArrowsConfig;
}());
