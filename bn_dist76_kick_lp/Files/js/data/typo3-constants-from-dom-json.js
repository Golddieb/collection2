(function () {
    'use strict';

    var typo3ConstantsFromDomJson = JSON.parse(
        $('.js-typo3-constants-from-dom-json').text()
    );

    // Export object
    window.BN.data.typo3ConstantsFromDomJson = typo3ConstantsFromDomJson;

}());
