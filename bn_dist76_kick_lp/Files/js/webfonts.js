/**
 * Get the font settings from the Typo3 constants and load them
 */

(function ($, window, document) {

    'use strict';


    function splitByComma (value) {
        if (!value) {
            return null;
        }

        return value
            .trim()
            .replace(/ *, +/g, ',')
            .replace(/ +/g, '+')
            .split(',');
    }

    var typo3ConstantsFromDomJson = window.BN.data.typo3ConstantsFromDomJson;

    var fontFamilies = splitByComma(
        typo3ConstantsFromDomJson.styling_settings.webFontFamilies
    );

    // If there are Webfonts specified, load them
    if (fontFamilies) {

        var fontVariants = '400, 400italic, 300italic, 300, 600italic, 600, 700'
            .replace(/ *, +/g, ',');

        var WebFontConfig = {
            google: {
                // Example:
                // families: [ 'Source+Sans+Pro:400,400italic,300italic,300,600italic,600,700']
                families: fontFamilies.map(function (fontFamilyName) {
                    return fontFamilyName + ':' + fontVariants;
                }),
            }
        };

        // load webfonts
        window.WebFontConfig = WebFontConfig;

        var wf = document.createElement('script');
        wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    }


})(jQuery, window, document);
