/**
 * * Use ESLint to check your code, see https://brandnamic.plan.io/projects/knowledge-base/wiki/ESLint
 * * Avoid global variables
 */

/*global jQuery, Waypoint, objectFitImages, brandnamic_newsletterconfirm, BN, picturefill*/

(function ($, window, document) {
    'use strict';

    // Load functions from global BN object
    var cacheBusting = window.BN.functions.cacheBusting;
    var renderRoomList = window.BN.functions.renderRoomList;
    var customArrowsConfig = window.BN.data.customArrowsConfig;
    var initRoomSlider = window.BN.functions.initRoomSlider;
    var handleCookiebanner = window.BN.functions.handleCookiebanner;
    var handleErrors = window.BN.functions.handleErrors;
    var logger = window.BN.functions.logger;
    var webAnalyticsTracking = window.BN.functions.webAnalyticsTracking;
    var scrollToRequest = window.BN.functions.scrollToRequest;
    var offer = window.BN.functions.offer;
    var scrollToElementByID = window.BN.functions.scrollToElementByID;
    var splitToArray = window.BN.functions.splitToArray;
    var insertPortalWidgets = window.BN.functions.insertPortalWidgets;
    var initShowcaseSlider = window.BN.functions.initShowcaseSlider;
    var initPopupGallery = window.BN.functions.initPopupGallery;


    // Object-fit polyfill for IE, Edge, etc.
    (function () {
        var checkElement = document.createElement('img');

        var supportsObjectFit = typeof checkElement.style.objectFit !== 'undefined';
        var supportsSrcset = ('sizes' in checkElement);
        var supportsPicture = !!window.HTMLPictureElement;

        // If everything is supported, skip the polyfill
        if (supportsObjectFit && supportsSrcset && supportsPicture) {
            return;
        }

        // If object-fit is not supported, but picture and srcset is,
        // only run objectFitImages(). This is true for MS Edge
        if (!supportsObjectFit && supportsSrcset && supportsPicture) {
            objectFitImages();
            return;
        }

        // Run object-fit polyfill for IE
        // (https://github.com/bfred-it/object-fit-images/tree/v2.5.3)
        $('img:not(.lazyload):not(.lazyloaded)').each(function (index, element) {
            objectFitImages(element, {watchMQ: true});
        });


        $(document).on('lazybeforeunveil', function (event) {
            var $target = $(event.target);

            if ($target.css('font-family').indexOf('object-fit:') === -1) return;

            var timeReference = setTimeout(function () {
                if ($target.hasClass('lazyloaded')) {
                    objectFitImages($target[0], {watchMQ: true});
                    clearTimeout(timeReference);
                }
            }, 500);
        });
    }());

    $('.header_slider').addClass('loaded');
    $('.header_slider').slick(
        $.extend({}, {
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            speed: 1000,
            pauseonHover: false,
            slidesToScroll: 1,
            fade: true,
        }, customArrowsConfig)
    );
    if ($('.js-quote-slider .js-quote-box').length > 1) {
        var zitat = $('.js-quote-slider');
        zitat.addClass('loaded');
        zitat.slick({
            autoplay: true,
            autoplaySpeed: 4000,
            speed: 1000,
            pauseonHover: false,
            arrows: false,
            fade: true,
            // adaptiveHeight: true,
        });
    }

    // Init room slider for CMS-based room slider
    $('.js-tab-slider-box').each(function (index, containerElement) {
        var $containerElement = $(containerElement);
        initRoomSlider($containerElement);
    });

    // Waypoints
    (function () {
        var $mainRequestButton = $('.main .request.scroll');
        var requestButtonHiddenClass = 'request_button_hidden';
        var phoneCallButtonHiddenClass = 'phone-call-button-hidden';
        var $phoneCallButton = $('.js-phone-call-button');

        var headerSliderElement = $('.header_slider')[0];

        if (headerSliderElement) {
            new Waypoint({
                element: headerSliderElement,
                handler: function (direction) {
                    if (direction === 'down') {
                        $mainRequestButton.removeClass(requestButtonHiddenClass);
                        $phoneCallButton.removeClass(phoneCallButtonHiddenClass);
                    } else {
                        $mainRequestButton.addClass(requestButtonHiddenClass);
                        $phoneCallButton.addClass(phoneCallButtonHiddenClass);
                    }
                },
            });
        }

        var quickrequestElement = $('.quickrequest.container')[0];

        if (quickrequestElement) {
            new Waypoint({
                element: quickrequestElement,
                handler: function (direction) {
                    if (direction === 'down') {
                        $mainRequestButton.addClass(requestButtonHiddenClass);
                        $phoneCallButton.addClass(phoneCallButtonHiddenClass);
                    } else {
                        $mainRequestButton.removeClass(requestButtonHiddenClass);
                        $phoneCallButton.removeClass(phoneCallButtonHiddenClass);
                    }
                },
            });
        }


        // Ugly hack: update the waypoints every two seconds infinitely
        // This is done because lods of DOM changes with JS even
        // after the page load event.
        // TODO: Make this more efficient
        setInterval(function () {
            Waypoint.refreshAll();
        }, 2000);

    }());

    // Initialize the sliders and other things for all
    // showcase slider elements
    (function () {
        var $jsShowcaseSlider = $('.js-showcase-slider');
        $jsShowcaseSlider.each(function (index, containerElement) {
            var $containerElement = $(containerElement);
            var numberOfSliderItems = $containerElement
                .find('.js-showcase-slider-switcher-item').length;
            initShowcaseSlider(numberOfSliderItems, $(containerElement));
        });
    }());

    var $js_offer = $('.js_offer');

    function initOffers () {
        // Iterate over offer container elements and load
        // the corresponding offers for each element
        $js_offer.each(function (index, containerElement) {
            var $containerElement = $(containerElement);
            var hotel_id = $containerElement.data('id');

            offer({
                hotel_id: hotel_id,
                $containerElement: $containerElement,
            });
        });

        // Unregister the event to prevent infinite loops
        $js_offer.off('lazybeforeunveil', initOffers);
    }

    // Lazy load eHotelier offers
    $js_offer.on('lazybeforeunveil', initOffers);

    scrollToRequest($('.request.scroll'));

    /* Delete Offer Transfer */
    $('.js-selected-item-box-close-button').on('click', function () {
        $('.js-selected-offer-box').slideUp();
        var offer_transfer = $('.js-offer-transfer');

        webAnalyticsTracking.trackEvent({
            eventCategory: 'Entfernen des Angebots von der Anfrage',
            eventAction: 'Click',
            eventLabel: offer_transfer.find('.title').html(),
        });

        offer_transfer.find('.image').html('');
        offer_transfer.find('.title').html('');
        offer_transfer.find('.season').html('');
        offer_transfer.find('.prices').html('');

        // Clear the offer to request form
        BN.forms.collective.setField('offer_id', '');

    });

    handleCookiebanner();

    // Newsletterconfirm - DoubleOptUrl
    if ($('[data-is-mail-confirm-page]').length) {
        // The `brandnamic_newsletterconfirm` constructor
        // accepts a configuration object with an `onerror` function
        // (which returns a `message` argument) an an `onsuccess` function.
        new brandnamic_newsletterconfirm({
            onerror: function () {},
            onsuccess: function () {},
        });
    }


    // Lazy load eHotelier rooms slider
    var $roomDataContainer = $('.js-room-data-container');

    function initRooms () {
        // Unregister the event to prevent infinite loops
        $roomDataContainer.off('lazybeforeunveil', initRooms);

        // eHotelier rooms
        $('.js-room-data-container').each(function (index, containerElement) {
            var $containerElement = $(containerElement);

            var eHotelierID = $containerElement.data('ehotelier-id');
            var roomsToShowArray = splitToArray($containerElement.data('rooms-to-show'));

            // If eHotelier ID is not set
            if (!eHotelierID) {
                logger('No eHotelier ID passed', 'trace');
            }

            renderRoomList({
                hotel_id: eHotelierID,
                templatePath: '/typo3conf/ext/bn_typo_dist76/Files/js/templates/room_list.hbs',
                roomtype_ids: roomsToShowArray,
            }).then(function (renderedTemplate) {
                $containerElement.html(renderedTemplate);

                $('.js-room-data-container').removeClass('tab-slider-box-loading');

                initRoomSlider($containerElement);

                picturefill();

                scrollToElementByID(location.hash);
            }).catch(handleErrors);

        });
    }

    $roomDataContainer.on('lazybeforeunveil', initRooms);


    // Handle the portal widgets in the footer (template insertion, iframe and so on)
    insertPortalWidgets();


    // Initialize slider-with-dots

    var navDotsTemplate = '' +
        '<button class="slider-nav-dot" data-role="none" role="button" ' +
        'aria-required="false" tabindex="0"></button>';

    $('.js-slider-with-dots').slick({
        dots: true,
        arrows: false,
        customPaging: function customPaging () {
            return navDotsTemplate;
        },
    });

    // GTM events for clicks on external links
    $('body').on('click', 'a[href^=http\\:], a[href^=https\\:], a[href^=\\/\\/]', function () {
        webAnalyticsTracking.trackLinkClickEvent('Externe Links', this);
    });

    $('.js-legal-menu').on('click', 'a', function () {
        webAnalyticsTracking.trackLinkClickEvent('Footer-Menü-Link', this);
    });

    $('.js-request-scroll-button').on('click', function () {
        webAnalyticsTracking.trackEvent({
            eventCategory: 'Klick auf Scrollbutton zum Anfrageformular',
            eventAction: 'Click',
        });
    });

    // Send event for the 404 error page
    if ($('[data-is-404-error-page]').length) {
        webAnalyticsTracking.trackEvent({
            eventCategory: 'Error',
            eventAction: '404',
            eventLabel: 'ref: ' + document.referrer,
        });
    }

    $('.gallery_slider').each(function () {
        $(this).magnificPopup({
            delegate: 'a.mfp-item',
            gallery: {
                enabled: true
            },
            type: 'image'
        });
    });
    $('.mfp-item-video').each(function () {
        var video_link = $(this).attr('href');
        $(this).magnificPopup({
            items: {
                src: video_link
            },
            type: 'iframe',
            iframe: {
                markup: '<div class="mfp-iframe-scaler">'+
                        '<div class="mfp-close"></div>'+
                        '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+
                        '</div>',
                patterns: {
                    youtube: {
                        index: 'youtube.com/',
                        id: 'v=',
                        src: '//www.youtube.com/embed/%id%?autoplay=1'
                    },
                    vimeo: {
                        index: 'vimeo.com/',
                        id: '/',
                        src: '//player.vimeo.com/video/%id%?autoplay=1'
                    }
                },
                srcAction: 'iframe_src'
            }
        });
    });

    // Initialize the popup gallery

    initPopupGallery($('body'));

})(jQuery, window, document);
