# Libs Ordner

Libs mit eigenen CSS Dateien und Bildern werden hier gebündelt abgelegt und dürfen nicht verändert werden.
Dadurch erleichtert sich das Updaten der Bibliotheken.
Sollte CSS überschrieben werden müssen, diese Änderungen in der layout.css vornehmen.
Eigene Bilder kommen in den img Ordner.