/*global Handlebars*/

(function() {

    // strict mode
    "use strict";

    /**
     * Parse a Date string to a JS date object
     * @param  {string} inputDateString
     * @return {date}
     */
    function parseDate(inputDateString) {
        return new Date(Date.parse(inputDateString));
    }

    function padToTwoDigits(number) {
        if (number < 10) {
            return '0' + number;
        }

        return number;
    }

    function toGermanDayMonth(inputDate) {
        return '' +
            padToTwoDigits(inputDate.getUTCDate()) + '.' +
            padToTwoDigits(inputDate.getUTCMonth() + 1) + '.';
    }

    function toGermanDateFormat(inputDate) {
        return '' +
            toGermanDayMonth(inputDate) +
            padToTwoDigits(inputDate.getUTCFullYear());
    }

    // helper to convert the date v1
    Handlebars.registerHelper('convertISODate', function(date) {
        if (!date) {
            return '';
        }

        date = parseDate(date);
        return toGermanDateFormat(date);
    });
    // helper to convert the date v1
    Handlebars.registerHelper('shortdate', function(date) {
        if (!date) {
            return '';
        }

        date = parseDate(date);
        return toGermanDayMonth(date);
    });

    /**
     * Formats a number according to the common German formatting style,
     * e.g. 123.456,55
     * @param  {number} number Input number
     * @return {string} The formatted number as string
     */
    function formatNumberGermanStyle(number) {
        return number.toFixed(2)
            .replace(/\./, ',')
            .replace(/(\d)(?=(\d{3})+,)/g, '$1.');
    }

    // helper to format price
    Handlebars.registerHelper('currency', function(amount) {
        // Plus sign to cast to JS number
        return formatNumberGermanStyle(+ amount);
    });

    Handlebars.registerHelper('pluralization', function (count, options) {
        // Cast to number
        var countCasted = + count;
        if (countCasted === 1 || countCasted === -1) {
            return options.hash.singular;
        }

        return options.hash.plural;
    });

    // Takes two dates (from date and to date) and formats it
    // in the German date format. If the year of the from date is
    // different than the year of the to date, also print the year
    // in the from date
    Handlebars.registerHelper('date-from-to', function (options) {
        var fromDate = parseDate(options.hash.from);
        var toDate = parseDate(options.hash.to);

        var isSameYear = fromDate.getFullYear() === toDate.getFullYear();

        var formattedFromDate = isSameYear
            ? toGermanDayMonth(fromDate)
            : toGermanDateFormat(fromDate);
        var formattedToDate = toGermanDateFormat(toDate);

        return formattedFromDate + '–' + formattedToDate;
    });

})();
