/**
 * Warning: this library was modified to use `bnlocalproxy`
 * and wrapped in an IIFE
 */

(function () {
    var serverurl='https://admin.ehotelier.it/bnlocalproxy.php';


    function getQueryParams(qs) {
        qs = qs.split("+").join(" ");
        var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = re.exec(qs)) {
            params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
        }

        return params;
    }
    window.brandnamic_newsletterconfirm=function(config){
        var getvars=getQueryParams(document.location.search);
        this.config=config;
        if (typeof this.config.serverurl != 'undefined') serverurl=this.config.serverurl;
        this.init=function(){
            myUrl=serverurl+'?'+(typeof brandnamicnlconfirm.config.test =='undefined' ? '' : 'test='+brandnamicnlconfirm.config.test+'&')+'path=newsletterconfirm&email='+getvars.email+'&hash='+getvars.hash+'&callback=?'
        jQuery.ajax({
                url: myUrl,
                dataType: 'json',
                context: this,
                success: function(data) {
                    brandnamicnlconfirm=this;
                    if (typeof(data.status)!='undefined' && data.status=='OK'){
                        if (typeof brandnamicnlconfirm.config.onsuccess != 'undefined'){
                            brandnamicnlconfirm.config.onsuccess();
                        }else{
                            jQuery(brandnamicnlconfirm.config.errorlayer).hide();
                            jQuery(brandnamicnlconfirm.config.successlayer).show();
                        }

                    }else{
                        if (typeof brandnamicnlconfirm.config.onerror != 'undefined'){
                            brandnamicnlconfirm.config.onerror(data.message);
                        }else{
                            jQuery(brandnamicnlconfirm.config.errorlayer).show();
                            jQuery(brandnamicnlconfirm.config.successlayer).hide();
                        }

                    }
                }
            });
        }
        brandnamicnlconfirm=this;
        brandnamicnlconfirm.init();
    }




}());

