<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 21.08.2017
 * Time: 09:55
 */

namespace BN\BnTypoDist\Conditions;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Configuration\TypoScript\ConditionMatching\AbstractCondition;
use WhichBrowser\Parser;

class BrowserDetection extends AbstractCondition {

    /**
     * @var Parser
     */
    private $parser = null;

    /**
     * check the current requesting browser from the given HTTP_USER_AGENT string
     * and return either true or false if a specific browser (and version) is matched.
     *
     * @return bool
     */
    protected function checkBrowser() {
        $this->parser = new Parser(GeneralUtility::getIndpEnv('HTTP_USER_AGENT') ?? "");
        $browser = $this->parser->browser;

        if  (
                (
                    // ALL IE BROWSERS LOWER THAN VERSION 11
                    "Internet Explorer" === $browser->getName() &&
                    $browser->version->is('<', '11')
                ) ||
                (
                    // ALL SAFARI BROWSERS LOWER THAN VERSION 8
                    "Safari" === $browser->getName() &&
                    $browser->version->is('<', '8')
                )
            )
        {
            return true;
        }

        return false;
    }


    /**
     * required function from parent class
     *
     * @param array $conditionParameters not used at the moment
     *
     * @return bool
     */
    public function matchCondition(array $conditionParameters) {
        return $this->checkBrowser();
    }
}