<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 26.07.2017
 * Time: 11:56
 */

namespace BN\BnTypoDist\Services;


use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ConstantService {

    /**
     * @var array
     */
    protected $constants;

    /**
     * @var \TYPO3\CMS\Core\TypoScript\TypoScriptService    Service for the typoscript functions
     */
    protected $typoscriptService = NULL;

    /**
     * Helper function for item removal from arrays
     *
     * @param array $array  The given array
     * @param array $keys   The keys which shoulb be removed
     *
     * @return array
     */
    private function array_except($array, $keys) {
        return array_diff_key($array, array_flip((array) $keys));
    }

    public function __construct() {
        // initialize a ts service instance
        $this->typoscriptService = GeneralUtility::makeInstance(TypoScriptService::class);
        // get constants from $GLOBALS['TSFE'] array
        $this->constants = $GLOBALS['TSFE']->tmpl->setup_constants;
        // convert the constants to a proper structure
        $this->constants = $this->typoscriptService->convertTypoScriptArrayToPlainArray($this->constants);
        // remove unimportant constants @todo: make this configurable via TS
        $this->constants = $this->array_except($this->constants, ["styles", "module"]);
    }


    /**
     * Iterate over an array of constants (recursive) and
     * look for nested constants + resolve them.
     *
     * @param array $constantsArray  Array of constants
     *
     * @return array Complete processed array of constants
     */
    protected function iterateArrayRecursive($constantsArray) {
        foreach ($constantsArray as $item => $value) {
            if (is_array($value))
                $constantsArray[$item] = $this->iterateArrayRecursive($value);
            else {
                $constantsArray[$item] = $this->lookForNestedConstants($value);
            }
        }
        return $constantsArray;
    }


    /**
     * Find the requested constant in the constant array.
     * Take in account that some constants may be nested.
     *
     * @param string $constantName Name of the constant (perhaps nested)
     *
     * @return string Value of the constant
     */
    public function findConstantInArray($constantName): string {
        $arrayPathSegments = explode(".", $constantName);

        if (count($arrayPathSegments) !== 1) {

            $arrayCurrentLayer = $this->constants[$arrayPathSegments[0]];
            array_shift($arrayPathSegments);

            return $this->findConstantInArrayRecursive($arrayCurrentLayer, $arrayPathSegments);
        } else if (isset($this->constants[$constantName])) {
            return $this->constants[$constantName];
        }

        return '';
    }


    /**
     * Go through each Layer of the array to find and return the
     * requested constant.
     *
     * @param array $arrayCurrentLayer  Current layer of the constant array
     * @param array $arrayPathSegments  Requested constant, seperated by array level.
     *                                  Each array level as single array item.
     *
     * @return string
     */
    public function findConstantInArrayRecursive($arrayCurrentLayer, $arrayPathSegments): string {
        $arrayCurrentLayer = $arrayCurrentLayer[$arrayPathSegments[0]];

        if ( !empty($arrayCurrentLayer) ) {
            if ( is_array($arrayCurrentLayer) ) {
                array_shift($arrayPathSegments);

                return $this->findConstantInArrayRecursive($arrayCurrentLayer, $arrayPathSegments);
            } else {
                return (string) $arrayCurrentLayer;
            }
        }

        return '';
    }


    /**
     * Find all possible nested constants occurrences inside a
     * constant value and resolve them.
     *
     * @param string $constantValue  Value of a constant
     *
     * @return string Value of a constant with all occurrences resolved
     */
    public function lookForNestedConstants($constantValue) {

        /*
         * if something like {$blabla} occurs inside the $constantValue, than
         * the $matches array will contain an array of matches of the whole
         * string ($matches[0]) like "{$blabla}" and an array of matches of
         * the just the name of the variable string ($matches[1]) like
         * "blabla" (without the {$} wrap).
         */
        preg_match_all('/\{\$([^\}]+)\}/', $constantValue, $matches);

        if (!empty($matches[1])) {
            $references = [];

            foreach ($matches[1] as $value) {
                $references[] = $this->findConstantInArray($value);
            }
            // replace the match of the nested constant with the resolved value
            return str_replace($matches[0], $references, $constantValue);
        }
        return $constantValue;
    }


    /**
     * @return array Array of all found constants
     */
    public function getConstants() {
        return $this->iterateArrayRecursive($this->constants);
    }
}