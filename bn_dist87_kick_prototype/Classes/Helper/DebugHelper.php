<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 09.10.2017
 * Time: 10:39
 */

namespace BN\BnTypoDist\Helper;


use TYPO3\CMS\Core\Utility\DebugUtility;

/**
 * Class DebugHelper
 *
 * debug whatever you want
 *
 * @package BN\BnTypoDist\Helper
 */
class DebugHelper
{
    /**
     * this function will be called from outside
     */
    public function debug() {
        DebugUtility::debug("debug something useful...");
    }
}