<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 02.08.2017
 * Time: 09:04
 */

namespace BN\BnTypoDist\Helper;


use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

class FileLookUpHelper {

    /**
     * @var string absolute path to ext directory
     */
    public static $absoluteExtPath = PATH_site . "typo3conf/ext/";

    /**
     * @var string relative path to ext directory
     */
    public static $relativeExtPath = "/typo3conf/ext/";

    /**
     * Look inside the ext directory for installed extensions
     * and wrap them with the $relativeExtPath string
     *
     * @return array wrapped, installed extensions
     */
    public static function lookForExtensions() {
        $foundExtensions = [];
        $extDir = @dir(self::$absoluteExtPath);
        while ($extDirEntry = $extDir->read()) {
            if ($extDirEntry != "." && $extDirEntry != "..") {
                // Only add loaded extensions to array
                if (ExtensionManagementUtility::isLoaded($extDirEntry))
                    $foundExtensions[] = self::$relativeExtPath . $extDirEntry;
            }
        }
        $extDir->close();
        return $foundExtensions;
    }

    /**
     * Wrap the found extension with proper robots keywords
     *
     * @return string
     */
    public static function formatRobotsOutput() {
        $foundExtensions = self::lookForExtensions();
        $extensionPaths = "";

        if (!empty($foundExtensions)) {
            foreach ($foundExtensions as $extension) {
                $extensionPaths .= "Allow: " . (string) $extension . "/Resources/Public/\n";
            }
        }
        return $extensionPaths;
    }

    /**
     * @return string
     */
    public static function generateRobotsAllowedExtensions() {
        return self::formatRobotsOutput();
    }
}