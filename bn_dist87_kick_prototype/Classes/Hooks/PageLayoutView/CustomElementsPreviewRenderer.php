<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 29.09.2017
 * Time: 12:04
 */

namespace BN\BnTypoDist\Hooks\PageLayoutView;

use \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use \TYPO3\CMS\Backend\View\PageLayoutView;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use \TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Resource\FileCollector;

/**
 * Contains a preview rendering for the page module of CType="*whatever*"
 */
class CustomElementsPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    /**
     * @var \TYPO3\CMS\Lang\LanguageService
     */
    protected $LangService = null;


    public function __construct() {
        $this->LangService = GeneralUtility::makeInstance(\TYPO3\CMS\Lang\LanguageService::class);
    }

    protected function getLanguageService() {
        return $this->LangService;
    }

    /**
     * Preprocesses the preview rendering of a content element.
     *
     * @param PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionalities
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    ) {

        if ('bn_seasonswitch' === $row['CType']) {
            $this->handleSeasonSwitch($parentObject,$drawItem,
                                        $headerContent,$itemContent,$row);
        } elseif ('bn_seasonelement' === $row['CType']) {
            $this->handleSeasonElement($parentObject,$drawItem,
                                        $headerContent,$itemContent,$row);
        }
    }

    /**
     * Handle the SeasonSwitch
     *
     * @param PageLayoutView $parentObject
     * @param $drawItem
     * @param $headerContent
     * @param $itemContent
     * @param array $row
     */
    protected function handleSeasonSwitch(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    ) {
        /** @var Connection $db_con */
        $db_con = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tt_content');

        $config["selectedCategories"] = explode(",", $row['selected_categories'] ?? null);

        /** @var QueryBuilder $qbCategories */
        $qbCategories = $db_con->createQueryBuilder();
        $qbCategories
            ->select(
                'cat.title',
                'cat.uid',
                'cat.pid'
            )
            ->from('sys_category', 'cat')
            ->where(
                $qbCategories->expr()->eq(
                    'cat.pid',
                    $qbCategories->createNamedParameter($row['pid'], \PDO::PARAM_INT)
                )
            )
            ->getRestrictions()
            ->removeAll();

        // execute sql statement and fetch result
        $records = $qbCategories->execute()->fetchAll();

        // mark and set the selected categories for the fluid data
        if (!empty($records)) {
            foreach ($records as $key => $value) {
                $records[$key]["selected"] = 0;
                if (in_array((int)$records[$key]["uid"], $config["selectedCategories"])) {
                    $records[$key]["selected"] = 1;
                }
            }
        }

        // build data array
        $data = [
            "categories" => $records ?? [0 => "no categories"],
            "ctype_label" => $parentObject->CType_labels[$row['CType']] ?? "no label",
        ];
        // set data
        $row["additionalData"] = $data;
    }

    /**
     * Handle the SeasonElement
     *
     * @param PageLayoutView $parentObject
     * @param $drawItem
     * @param $headerContent
     * @param $itemContent
     * @param array $row
     */
    protected function handleSeasonElement(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    ) {
        /** @var Connection $db_con */
        $db_con = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('sys_category');

        /** @var QueryBuilder $qbSeasonElem*/
        $qbSeasonElem = $db_con->createQueryBuilder();

        // build SQL query to get the desired values from the season switch
        $qbSeasonElem
            ->select('cat.title', 'cat.uid')
            ->from('sys_category', 'cat')
            ->join(
                'cat',
                'sys_category_record_mm',
                'cat_mm',
                $qbSeasonElem->expr()->eq(
                    'cat_mm.uid_local',
                    $qbSeasonElem->quoteIdentifier('cat.uid')
                )
            )
            ->where(
                $qbSeasonElem->expr()->eq(
                    'cat_mm.uid_foreign',
                    $qbSeasonElem->createNamedParameter($row['uid'], \PDO::PARAM_INT, ':pid'))
            )
            ->getRestrictions()
            ->removeAll();

        // execute sql statement and fetch result
        $records = $qbSeasonElem->execute()->fetchAll();

        // get the image/video references
        /** @var FileCollector $fileCollector */
        $fileCollector = GeneralUtility::makeInstance(FileCollector::class);
        $fileCollector->addFilesFromRelation('tt_content', 'assets', $row);

        // save them into the record
        $row['assets'] = $fileCollector->getFiles();

        // build data array
        $data = [
            "cat_label" => $records[0]["title"] ?? "no category",
            "cat_id" => $records[0]["uid"] ?? "no id",
            "ctype_label" => $parentObject->CType_labels[$row['CType']] ?? "no label",
        ];
        // set data
        $row["additionalData"] = $data;
    }
}