<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 14.09.2017
 * Time: 15:03
 */

namespace BN\BnTypoDist\DataProcessing\Elements;


use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentDataProcessor;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Frontend\Resource\FileCollector;

class SeasonsProcessor implements DataProcessorInterface
{
    /**
     * @var ContentDataProcessor
     */
    protected $contentDataProcessor;

    /**
     * @var Connection
     */
    protected $DBConnection = null;

    /**
     * @var int
     */
    protected $curPID;

    /**
     * @var TypoScriptFrontendController
     */
    protected $TSFEController;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * SeasonsProcessor constructor.
     */
    public function __construct() {
        $this->contentDataProcessor = GeneralUtility::makeInstance(ContentDataProcessor::class);
        $this->TSFEController = $GLOBALS['TSFE'];
    }

    /**
     * Set global values like the current pid
     */
    protected function setGlobalProperties() {
        // set the current Page ID
        $this->curPID = $GLOBALS["TSFE"]->id;
    }

    protected function getTSFEController() {
        return $this->TSFEController;
    }

    /**
     * Get configuration value from processorConfiguration
     *
     * @param string $key
     * @param ContentObjectRenderer $cObj
     * @param mixed $processorConfiguration
     *
     * @return string
     */
    protected function getConfigurationValue($key, &$cObj, &$processorConfiguration) {
        return $cObj->stdWrapValue($key, $processorConfiguration ?? []);
    }

    /**
     * @param ContentObjectRenderer $cObj
     * @param array $proConf
     */
    protected function checkProcessorConfig(ContentObjectRenderer &$cObj, array &$proConf) {
        // the switchPid to query
        $switchPid = (int)$this->getConfigurationValue('switchPid', $cObj, $proConf);
        $this->config["switchPid"] = $switchPid;

        // get the desired colpos of the season element. (default: 1)
        $colPos = (int)$this->getConfigurationValue('colPos', $cObj, $proConf);
        if (empty($colPos)) {
            $colPos = 1;
        }
        $this->config["colPos"] = $colPos;

        // look for slide (default: 0)
        $slide = (int)$this->getConfigurationValue('slide', $cObj, $proConf);
        if (!$slide) {
            $slide = 0;
        }
        $this->config["slide"] = $slide;

        // look for slide.collect (default: 0)
        $slideCollect = (int)$this->getConfigurationValue('collect', $cObj, $proConf['slide.']);
        if (!$slideCollect) {
            $slideCollect = 0;
        }
        $this->config["slideCollect"] = $slideCollect;

        // look for slide.collectReverse
        $slideCollectReverse = (bool)$this->getConfigurationValue('collectReverse', $cObj, $proConf['slide.']);
        $this->config["slideCollectReverse"] = $slideCollectReverse;
    }

    /**
     * Establish a connection for a given table
     * @param string $table
     * @return mixed
     */
    protected function makeConnectionForTable(string $table) {
        $tempDBConnection = null;
        if ($table !== "" && $table !== null) {
            $tempDBConnection = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable($table);
        }
        return $tempDBConnection;
    }

    /**
     * @param $resultSeasonSwitch
     */
    protected function setConfigFromSeasonSwitch($resultSeasonSwitch) {
        // prepare the 'selected_categories' result for the next query
        $this->config["selectedCategories"] = explode(",", $resultSeasonSwitch[0]['selected_categories'] ?? null);
    }

    /**
     * @param ContentObjectRenderer $cObj
     * @param array $contentObjectConfiguration
     * @param array $processorConfiguration
     * @param array $processedData
     * @return array
     */
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        // set properties like current pid
        $this->setGlobalProperties();

        // check for possible if statement first
        if (isset($processorConfiguration['if.']) && !$cObj->checkIf($processorConfiguration['if.'])) {
            return $processedData;
        }

        // get and save all the config from the dataProcessor configuration
        $this->checkProcessorConfig($cObj, $processorConfiguration);

        // exit if no switchPid is set
        if (empty($this->config["switchPid"])) {
            return $processedData;
        }

        // establish a connection for table 'tt_content'
        $this->DBConnection = $this->makeConnectionForTable('tt_content');

        /** @var QueryBuilder $qbSeasonSwitch*/
        $qbSeasonSwitch = $this->DBConnection->createQueryBuilder();

        // build SQL query to get the desired values from the season switch
        $qbSeasonSwitch
            ->select('selected_categories')
            ->from('tt_content')
            ->where(
                $qbSeasonSwitch->expr()->eq(
                    'pid',
                    $qbSeasonSwitch->createNamedParameter($this->config["switchPid"], \PDO::PARAM_INT)
                )
            )
            ->setMaxResults(1);

        // execute sql statement and fetch result
        $resultSeasonSwitch = $qbSeasonSwitch->execute()->fetchAll();

        // exit if no season switch was found
        if ( empty($resultSeasonSwitch[0]['selected_categories']) ) {
            return $processedData;
        }

        // set config from season switch result
        $this->setConfigFromSeasonSwitch($resultSeasonSwitch);

        /** @var QueryBuilder $qbSeasonElem*/
        $qbSeasonElem = $this->DBConnection->createQueryBuilder();

        // build SQL query to get the seasons
        $qbSeasonElem
            ->select(
                'tt_content.uid',
                'tt_content.pid',
                'cat.title as cat_title',
                'cat.uid as cat_uid',
                'tt_content.sorting'
            )
            ->from('tt_content')
            ->join(
                'tt_content',
                'sys_category_record_mm',
                'cat_mm',
                $qbSeasonElem->expr()->eq(
                    'tt_content.uid',
                    $qbSeasonElem->quoteIdentifier('cat_mm.uid_foreign')
                )
            )
            ->join(
                'cat_mm',
                'sys_category',
                'cat',
                $qbSeasonElem->expr()->eq(
                    'cat_mm.uid_local',
                    $qbSeasonElem->quoteIdentifier('cat.uid')
                )
            )
            ->where(
                $qbSeasonElem->expr()->eq(
                    'tt_content.pid',
                    $qbSeasonElem->createNamedParameter(
                        $this->curPID, \PDO::PARAM_INT, ':pid')
                ),
                $qbSeasonElem->expr()->eq(
                    'cat_mm.uid_local',
                    $qbSeasonElem->createNamedParameter(
                        '0',\PDO::PARAM_STR, ':cat')
                ),
                $qbSeasonElem->expr()->eq(
                    'tt_content.colPos',
                    $qbSeasonElem->createNamedParameter(
                        $this->config["colPos"], \PDO::PARAM_INT)
                ),
                $qbSeasonElem->expr()->eq(
                    'tt_content.CType',
                    $qbSeasonElem->createNamedParameter(
                        'bn_seasonelement', \PDO::PARAM_STR, ':ctype')
                ),
                $qbSeasonElem->expr()->eq(
                    'tt_content.sys_language_uid',
                    $qbSeasonElem->createNamedParameter(
                        $this->getTSFEController()->sys_language_uid,
                        \PDO::PARAM_INT,
                        ':sys_lang_uid')
                )
            );

        # predefine output array
        $recordsCollection = [];

        // loop for content gathering
        do {
            // predefined value
            $again = false;

            $rowsSeasonElements = [];

            // iterate through the selected_categories and make a query for each of them
            foreach ($this->config["selectedCategories"] as $category) {
                // reset query constraints
                $qbSeasonElem->setParameter('cat', $category);
                $qbSeasonElem->setParameter('sys_lang_uid', $this->getTSFEController()->sys_language_uid);

                // execute sql statement and fetch result
                $rowsPerCategory = $qbSeasonElem->execute()->fetchAll();

                // if no records for current language found, look for 'All' (-1)
                if (empty($rowsPerCategory)) {
                    $qbSeasonElem->setParameter('sys_lang_uid',-1);
                    $rowsPerCategory = $qbSeasonElem->execute()->fetchAll();
                }
                // if result not empty, save to array
                if (!empty($rowsPerCategory)) {
                    $rowsSeasonElements[] = $rowsPerCategory[0];
                }
            }

            // if result not empty, proceed with some further steps
            // like: add assets relations for tt_content or process nested processors
            if (!empty($rowsSeasonElements)) {

                // sort the records by 'sorting' field
                $sorting = [];
                foreach ($rowsSeasonElements as $sortKey => $sortRow) {
                    $sorting[$sortKey] = $sortRow["sorting"];
                }
                array_multisort($sorting, $rowsSeasonElements);

                // reset the processedRecordVariables
                $processedRecordVariables = [];

                foreach ($rowsSeasonElements as $key => $record) {
                    /** @var ContentObjectRenderer $recordContentObjectRenderer */
                    $recordContentObjectRenderer = GeneralUtility::makeInstance(ContentObjectRenderer::class);
                    $recordContentObjectRenderer->start($record, 'tt_content');

                    // get the image/video references
                    /** @var FileCollector $fileCollector */
                    $fileCollector = GeneralUtility::makeInstance(FileCollector::class);
                    $fileCollector->addFilesFromRelation('tt_content', 'assets', $recordContentObjectRenderer->data);

                    // save them into the record
                    $record['assets'] =  $fileCollector->getFiles();

                    // process nested processors
                    $processedRecordVariables[$key] = ['data' => $record];
                    $processedRecordVariables[$key] = $this->contentDataProcessor->process(
                        $recordContentObjectRenderer,
                        $processorConfiguration,
                        $processedRecordVariables[$key]
                    );
                }
                // save the processed records in an own array for later purpose
                $recordsCollection[] = $processedRecordVariables;
            }

            if (
                ( (empty($rowsSeasonElements)) && ( true === (bool)$this->config['slide'] ))
                || ( true === (bool)$this->config['slideCollect'])
            ) {
                // slide pid up till rootline
                $this->curPID = $cObj->getSlidePids($this->curPID, []);

                // check if we are not at the rootpage already
                if ($this->curPID !== '' && $this->curPID !== '0') {

                    if ($this->config['slide'] > 0) {
                        $this->config['slide']--;
                    }
                    if ($this->config['slideCollect'] > 0) {
                        $this->config['slideCollect']--;
                    }

                    // set new pid for the query
                    $qbSeasonElem->setParameter('pid',$this->curPID);

                    $again = true;
                }
            }

        } while ($again === true);

        // do further steps if some records are found
        if (!empty($recordsCollection)) {
            // reverse order of array if desired
            if ($this->config["slideCollectReverse"]) {
                $recordsCollection = array_reverse($recordsCollection);
            }
            // flatten the two-dimensional array to a simple array
            $recordsCollection = array_merge(...$recordsCollection);
        }

        // construct the result values
        $processedData['seasons'] = [
            "config"        => $this->config,
            "records"       => $recordsCollection,
        ];
        return $processedData;
    }
}