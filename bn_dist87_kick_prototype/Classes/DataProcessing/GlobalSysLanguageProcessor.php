<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 09.10.2017
 * Time: 11:40
 */

namespace BN\BnTypoDist\DataProcessing;


use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Class GlobalSysLanguageProcessor
 *
 * checks for each record if the given language is globally available.
 * The result will be set in a variable.
 *
 * @package BN\BnTypoDist\DataProcessing
 */
class GlobalSysLanguageProcessor implements DataProcessorInterface
{
    /**
     * @var Connection $DBConnection
     */
    protected $DBConnection = null;

    /**
     * Establish a connection for a given table
     * @param string $table
     * @return mixed
     */
    protected function makeConnectionForTable(string $table) {
        $tempDBConnection = null;
        if ($table !== "" && $table !== null) {
            $tempDBConnection = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable($table);
        }
        return $tempDBConnection;
    }

    /**
     * @param \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer $cObj
     * @param array $contentObjectConfiguration
     * @param array $processorConfiguration
     * @param array $processedData
     * @return array
     */
    public function process(
        \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        // exit immediately if no processed data or 'languageUid' is available
        if ( empty($processedData) || !isset($processedData["languageUid"])) {
            return $processedData;
        }

        // establish a connection for table 'sys_language'
        $this->DBConnection = $this->makeConnectionForTable('sys_language');

        /** @var QueryBuilder $qbSysLang */
        $qbSysLang = $this->DBConnection->createQueryBuilder();

        // build SQL query to get the desired values
        // + remove default typo3 query restrictions
        $qbSysLang
            ->select('uid', 'hidden')
            ->from('sys_language')
            ->getRestrictions()
            ->removeAll();

        // execute sql statement and fetch result
        $statementSysLang = $qbSysLang->execute();
        $resultSysLang = $statementSysLang->fetchAll();

        // exit if no result
        if (empty($resultSysLang)) {
            return $processedData;
        }

        // exit if languageUid is 0 (default language has no entry in sys_language table)
        if ($processedData['languageUid'] === '0') {
            $processedData['availableGlobally'] = 1;
            return $processedData;
        }

        // go through the fetched records and look for the global language settings
        // + set a variable
        foreach ($resultSysLang as $result) {
            if ((int)$processedData['languageUid'] === $result['uid']) {
                if ($result['hidden'] === 1) {
                    $processedData['availableGlobally'] = 0; break;
                } else {
                    $processedData['availableGlobally'] = 1; break;
                }
            }
        }

        return $processedData;
    }
}