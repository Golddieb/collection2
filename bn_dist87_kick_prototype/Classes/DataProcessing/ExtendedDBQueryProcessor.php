<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 11.09.2017
 * Time: 13:58
 */

namespace BN\BnTypoDist\DataProcessing;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentDataProcessor;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

class ExtendedDBQueryProcessor implements DataProcessorInterface
{
    /**
     * @var ContentDataProcessor
     */
    protected $contentDataProcessor;

    /**
     * ExtendedDBQueryProcessor constructor.
     */
    public function __construct() {
        $this->contentDataProcessor = GeneralUtility::makeInstance(ContentDataProcessor::class);
    }

    /**
     * Look throught the processorConfiguration and handle the slide configuration
     *
     * @param ContentObjectRenderer $cObj
     * @param $processorConfiguration
     * @return array Array of the slide config
     */
    public function getSlideConfig(ContentObjectRenderer $cObj, $processorConfiguration) {
        $tempConfig = [];
        // look for slide = ?
        $slide = isset($processorConfiguration['slide.'])
            ? (int)trim($cObj->stdWrap($processorConfiguration['slide'], $processorConfiguration['slide.']))
            : (int)trim($processorConfiguration['slide']);
        // if the value is false then set zero as a default value
        if (!$slide) {
            $slide = 0;
        }
        $tempConfig["slide"] = $slide;

        // look for slide.collect = ?
        $slideCollect = isset($processorConfiguration['slide.']['collect.'])
            ? (int)trim($cObj->stdWrap($processorConfiguration['slide.']['collect'], $processorConfiguration['slide.']['collect.']))
            : (int)trim($processorConfiguration['slide.']['collect']);
        // if the value is false then set zero as a default value
        if (!$slideCollect) {
            $slideCollect = 0;
        }
        $tempConfig["slideCollect"] = $slideCollect;

        // look for slide.collectReverse = ?
        $slideCollectReverse = isset($processorConfiguration['slide.']['collectReverse.'])
            ? (bool)trim($cObj->stdWrap($processorConfiguration['slide.']['collectReverse'], $processorConfiguration['slide.']['collectReverse.']))
            : (bool)trim($processorConfiguration['slide.']['collectReverse']);

        $tempConfig["slideCollectReverse"] = $slideCollectReverse;

        // look for slide.collectFuzzy = ?
        $slideCollectFuzzy = isset($processorConfiguration['slide.']['collectFuzzy.'])
            ? (bool)trim($cObj->stdWrap($processorConfiguration['slide.']['collectFuzzy'], $processorConfiguration['slide.']['collectFuzzy.']))
            : (bool)trim($processorConfiguration['slide.']['collectFuzzy']);
        if (!$slideCollect) {
            $slideCollectFuzzy = true;
        }
        $tempConfig["slideCollectFuzzy"] = $slideCollectFuzzy;

        return $tempConfig;
    }


    /**
     * @param ContentObjectRenderer $cObj
     * @param array $contentObjectConfiguration
     * @param array $processorConfiguration
     * @param array $processedData
     * @return array
     */
    public function process(ContentObjectRenderer $cObj,
                            array $contentObjectConfiguration,
                            array $processorConfiguration,
                            array $processedData )
    {
        // check for if statement
        if (isset($processorConfiguration['if.']) && !$cObj->checkIf($processorConfiguration['if.'])) {
            return $processedData;
        }

        // the table to query, if none given, exit
        $tableName = isset($processorConfiguration['table.'])
                        ? trim($cObj->stdWrap($processorConfiguration['table'], $processorConfiguration['table.']))
                        : trim($processorConfiguration['table']);
        if (empty($tableName)) {
            return $processedData;
        }

        // get the select config, if null, take empty array
        $selectOptions = $processorConfiguration['select.'] ?: [];

        // get the slide config
        $slideConfig = $this->getSlideConfig($cObj, $processorConfiguration);

        // define the variable to be used within the result
        $targetVariableName = $cObj->stdWrapValue('as', $processorConfiguration, 'records');

        $recordsCollection = [];
        $again = false;

        do {
            // Execute a SQL statement to fetch the records
            $records = $cObj->getRecords($tableName, $selectOptions);

            if ( !empty($records) ) {
                // reset the processedRecordVariables
                $processedRecordVariables = [];

                foreach ($records as $key => $record) {
                    /** @var ContentObjectRenderer $recordContentObjectRenderer */
                    $recordContentObjectRenderer = GeneralUtility::makeInstance(ContentObjectRenderer::class);
                    $recordContentObjectRenderer->start($record, $tableName);

                    $processedRecordVariables[$key] = ['data' => $record];
                    $processedRecordVariables[$key] = $this->contentDataProcessor->process(
                        $recordContentObjectRenderer,
                        $processorConfiguration,
                        $processedRecordVariables[$key]
                    );
                }
                // save the processed records in an own array for later purpose
                $recordsCollection[] = $processedRecordVariables;
            }

            if ($slideConfig["slideCollect"] > 0) {
                $slideConfig["slideCollect"]--;
            }
            if ($slideConfig["slide"]) {
                if ($slideConfig["slide"] > 0) {
                    $slideConfig["slide"]--;
                }

                // look for the parent id of the current page
                $selectOptions['pidInList'] = $cObj->getSlidePids(
                    $selectOptions['pidInList'],
                    $selectOptions['pidInList.']
                );
                unset($selectOptions['pidInList.']);

                // stop loop if slide is already on the top of the rootline
                $again = ( (string)$selectOptions['pidInList'] !==  '' );
            }
        } while(
            $again && $slideConfig["slide"]
            && ( empty($recordsCollection) && $slideConfig["slideCollectFuzzy"] || $slideConfig["slideCollect"] )
        );

        // reverse order of array if desired
        if ($slideConfig["slideCollectReverse"]
            && !empty($recordsCollection)) {
            $recordsCollection = array_reverse($recordsCollection);
        }

        // flatten the bidimensonal array to a simple array
        if (!empty($recordsCollection)) {
            $recordsCollection = array_merge(...$recordsCollection);
        }

        // save the processed Data with the desired targetVariableName
        $processedData[$targetVariableName] = $recordsCollection;

        return $processedData;
    }
}