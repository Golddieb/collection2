# ViewHelper Dokumentation

Hier werden alle Viewhelper abgelegt und werden - je nach Aufgabengebiet - in eigene Ordner abgelegt.

1. Allgemein
    1. Konventionen
2. Viewhelpers
    1. Constant
        1. Constant-Viewhelper
    2. Content
        1. Headings-Viewhelper
        2. Wrapper-Viewhelper

## Allgemein

Für den Aufruf der Viewhelper, muss bei jedem Template ein Namespace definiert werden, damit dieser den Viewhelper auch finden kann.

**Namespace für BN-Viewhelper:**
````
{namespace b=BN\BnTypoDist\ViewHelpers}
````

**Distribution-Settings:**
> Die Distribution-Settings sind unter `\Resources\Private\typoscript\system\global\distro.ts` zu finden.

### Konventionen

- Sie werden in ihren Aufgabengebiet gruppiert (Ordner).
- 'Do one thing and do it well'-Prinzip anwenden.
- Falls die Viewhelper eine Konfiguration brauchen, benutzt die Settings der Distribution (_eg.:_ `plugin.tx_bn_typo_dist`).


## Viewhelpers

### Constants

#### Constant-Viewhelper

Der **Constant**-Viewhelper liefert die gewünschte Konstante innerhalb eines Fluid-Templates.

##### Aufruf:
````html
<b:constants.get name="nameOfTheConstant"/>
````
oder inline
````html
{b:constants.get(name:'nameOfTheConstant')}
````

##### Attribute:
Als Attribut gibt es nur `name`, welches der Name der Konstante ist. 

Falls die Konstanten im Typoscript innerhalb eines Arrays definiert wurde, wie z.B.:
````
nested {
    ts {
        constant = value
    }
}
````
dann muss beim Aufruf, der Parameter in **Array-Punkt-Notation** angegeben werden:
````html
{b:constants.get(name:'nested.ts.constant')}
````

### Content

#### Headings-Viewhelper
Der **Headings**-Viewhelper wird verwendet um komformen HTML5-Ouline Code zu erhalten.

##### Aufruf:
````html
<b:content.heading headerType="header">
    {data.header}
</b:content.heading>
````
Für den Aufruf kann der **"headerType" Parameter** mitgegeben werden. Er definiert um welchen Header-Typ es sich handelt (_siehe Settings unten_), falls er nicht mit angegeben wurde, dann gilt der `default` value.

Standardmäßig wurde die **Hauptüberschrift** (`header`), die **Unterüberschrift** (`subheader`) und eine evtl. **dritte Überschrift** (hier `thirdheader`) definiert. Diese Typen mappen sich mit den vorhanden Tags (siehe Settings "`tags`")

##### Settings:
````typo3_typoscript
headings {
    tags {
         1 = h1
         2 = h2
         3 = h3
         4 = h4
         5 = h5
         last = h6
         default = span
    }
    types {
        header = 1
        subheader = 2
        thirdheader = 3
    }
    increment {
        by = 1
        begin = 2
    }
}
````

Beispiel:
> Der Typ `header` hat als Value eine `1`, d.h. es wird mit einem tag - der den _Key_ `1` hat - gemappt. Der Tag mit dem Key `1` hat den Value `h1`. Somit bekommt `header` einen `h1` tag.
>
> Der Grund für diese Komplexität, ist der Inkrementierung der Tags zuzuschulden.

Die **Tags** werden - falls `begin` von `increment` größer als die aktuelle _parentRecordNumber_ ist - um den Wert von `by` erhöht.

D.h.:
> Der Wert des Typs `header` wird um `1` erhöht. Er wird zu `2` und somit zu `h2` (wird gemappt mit dem tag key `2`).

Falls man bei der Inkrementierung zum Ende kommt und keine numerischen keys mehr übrig sind, dann wird automatisch der `last` value verwendet (auch wenn schon der `last` verwendet wird).

Der `default` value wird verwendet, falls ein (oder kein) **Headertyp** angegeben wird, der nicht in den **Settings** definiert wurde.

##### Attribute:
Der Viewhelper unterstützt schon einige Attribute nativ.
````text
id          uniqe identifier for this HTML element.
class       class(es) for this element
style       CSS styles for this element
data        data-attributes (as array)
````
Diese können beim Aufruf mitgegeben werden. Beispiel:
````html
<b:content.heading headerType="header" class="std-text" id="{data.uid}">
    {data.header}
</b:content.heading>
````
wird zu
````html
<h1 class="std-text" id="1">...</h1>
````

Die `data-*` Attribute werden mit dem `data`-Parameter übergegeben (in array Schreibweise):
````html
<b:content.heading headerType="header" data="{test:'1',test2:'2'}">
    {data.header}
</b:content.heading>
````
wird zu
````html
<h1 data-test="1" data-test2="2">...</h1>
````

Beliebig andere Attribute müssen mit dem `additionalAttributes`-Parameter übergegeben werden (in array Schreibweise):
````html
<b:content.heading headerType="header" additionalAttributes="{test:'1',test2:'2'}">
    {data.header}
</b:content.heading>
````
wird zu
````html
<h1 test="1" test2="2">...</h1>
````

#### Wrapper-Viewhelper
Der Wrapper-Viewhelper wird ebenfalls für komformen HTML5-Ouline-Code verwendet.


````html
<b:content.wrapper header="{0:'{data.header}',1:'{data.subheader}'}" id="c{data.uid}" class="std-element">
    {data}
</b:content.wrapper>
````

Neben den Standard-Attributen, die schon beim _heading-Viewhelper_ erklärt wurden, muss hier noch ein weiterer beachtet werden.

Der `header` Parameter verlangt ein Array aus den vorhandenen Überschriften des Templates.

Es muss min. eine Überschrift einen Wert haben, ansonsten wird das gesamte Element mit einem `<div>` gewrappt (siehe `ifNoHeaderSet`).

Ebenfalls mit einem `<div>` (siehe `first`), wird das erste Content-Element gewrappt. Die Reihenfolge bezieht sich immer auf die `parentRecordNumber` des jeweiligen Content-Objects.

Und alle anderen Elemente - bei denen die vorherigen Bedingungen nicht zutreffen - wird ein `<section>` tag verwendet.

##### Settings:
````typo3_typoscript
wrapper {
    tags {
        first = div
        ifNoHeaderSet = div
        default = section
    }
}
````