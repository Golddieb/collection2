<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 20.07.2017
 * Time: 13:39
 */

namespace BN\BnTypoDist\ViewHelpers\Constants;


use \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use \BN\BnTypoDist\Services\ConstantService;

class GetViewHelper extends AbstractViewHelper {
    
    /**
     * @var \BN\BnTypoDist\Services\ConstantService   service for the constants functions
     */
    protected $constantService = NULL;

    /**
     * Inject a Constant service
     *
     * @param \BN\BnTypoDist\Services\ConstantService $constantService
     */
    public function injectConstantService(ConstantService $constantService) {
        $this->constantService = $constantService;
    }

    /**
     * Handle constant rendering
     *
     * @param string $name  name of the requested constant
     *
     * @return string       final value of the constant
     */
    public function render($name) {
        if (!is_string($name))
            return '';

        $constantValue = $this->constantService->findConstantInArray($name);
        $constantValue = $this->constantService->lookForNestedConstants($constantValue);
        return $constantValue;
    }
}