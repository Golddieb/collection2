<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 18.07.2017
 * Time: 15:29
 */

namespace BN\BnTypoDist\ViewHelpers\Number;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

class IncrementViewHelper extends AbstractViewHelper {

    /**
     * Increase given number by 1
     * @param integer $number number to increase
     * @return integer|float
     */
    public function render($number) {
        if (is_int($number) || is_float($number)) {
            return ++$number;
        } else {
            return 0;
        }
    }
}