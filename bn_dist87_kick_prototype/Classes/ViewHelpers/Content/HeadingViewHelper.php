<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 18.03.2017
 */

namespace BN\BnTypoDist\ViewHelpers\Content;

use \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class HeadingViewHelper extends AbstractTagBasedViewHelper {
    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected $contentObject;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var array Setting of the Plugin (Extension|Distro) "bn_typo_dist"
     */
    protected $pluginSetting = [];

    /**
     * @var array Setting for the viewhelper (node array of plugin settings)
     */
    protected $viewHelperSettings = [];

    /**
     * @var boolean
     */
    protected $escapeOuput = false;

    /**
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
     * @return void
     */
    public function injectConfigurationManager(ConfigurationManagerInterface  $configurationManager) {
        $this->configurationManager = $configurationManager;
        $this->contentObject = $configurationManager->getContentObject();
    }

    /**
     * Set the Plugin Settings
     * @return void
     */
    public function setPluginSettings() {
        if (empty($this->pluginSetting)) {
            $this->pluginSetting = $this->configurationManager->getConfiguration(
                ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
                "bn_typo_dist");
        }
    }

    /**
     * Get the Plugin Settings
     * @return array
     */
    public function getPluginSettings() {
        return $this->pluginSetting;
    }

    /**
     * Set the Viewhelper setting array from plugin settings (typoscript)
     * @return void
     */
    public function setViewHelperSettings() {
        $pluginSettings = $this->getPluginSettings();
        if (is_array($pluginSettings["headings"])) {
            $this->viewHelperSettings = $pluginSettings["headings"];
        } else {
            $this->viewHelperSettings = [];
        }
    }

    /**
     * Get the Viewhelper setting array
     * @return array
     */
    public function getViewHelperSettings() {
        return $this->viewHelperSettings;
    }

    /**
     * Define the tag in consideration of the settings
     * @return string
     */
    public function defineTag() {
        // the initial tag is a div
        $tagType = "div";
        // get the headertype argument from the viewhelper call
        $viewHelperTagType = $this->arguments['headerType'];
        // get the tags from the settings
        $viewHelperTags = $this->viewHelperSettings['tags'];

        if (!empty($viewHelperTagType)) {
            $settingTagTypeValue = $this->viewHelperSettings['types'][$viewHelperTagType];
            if (!empty($settingTagTypeValue)) {
                $settingTagValue = $viewHelperTags[$settingTagTypeValue];
                if (!empty($settingTagValue)) {
                    $begin = $this->viewHelperSettings['increment']['begin'];
                    if ($this->contentObject->parentRecordNumber < $begin) {
                        $tagType = $settingTagValue;
                    } elseif (empty($begin) || !is_numeric($begin)) {
                        $tagType = $settingTagValue;
                    } else {
                        $incrementBy = $this->viewHelperSettings['increment']['by'];
                        if (!empty($incrementBy) && is_numeric($incrementBy)) {
                            $nextTag =$viewHelperTags[$settingTagTypeValue + $incrementBy];
                            if (isset($nextTag)) {
                                $tagType = $nextTag;
                            } elseif (!empty($viewHelperTags['last'])) {
                                $tagType = $viewHelperTags['last'];
                            } elseif (!empty($viewHelperTags['default'])) {
                                $tagType = $viewHelperTags['default'];
                            }
                        } else {
                            $tagType = $settingTagValue;
                        }
                    }
                }
            }
        } else {
            if (!empty($viewHelperTags["default"])) {
                // do something for default
                $tagType = $viewHelperTags["default"];
            }
        }
        return $tagType;
    }


    /**
     * Initialize Everything
     */
    public function initialize() {
        $this->tag->reset();

        if ($this->hasArgument('id')) {
            $this->tag->addAttribute('id', $this->arguments['id']);
        }
        if ($this->hasArgument('class')) {
            $this->tag->addAttribute('class', $this->arguments['class']);
        }
        if ($this->hasArgument('style')) {
            $this->tag->addAttribute('style', $this->arguments['style']);
        }
        if ($this->hasArgument('additionalAttributes') && is_array($this->arguments['additionalAttributes'])) {
            $this->tag->addAttributes($this->arguments['additionalAttributes']);
        }
        if ($this->hasArgument('data') && is_array($this->arguments['data'])) {
            foreach ($this->arguments['data'] as $dataAttributeKey => $dataAttributeValue) {
                $this->tag->addAttribute('data-' . $dataAttributeKey, $dataAttributeValue);
            }
        }

        $this->tag->forceClosingTag(true);
        $this->setPluginSettings();
        $this->setViewHelperSettings();
        $this->tag->setTagName($this->defineTag());
    }

    /**
     * Initialize Arguments
     */
    public function initializeArguments() {
        $this->registerTagAttribute("headerType","string","The Tag-Type of the Element",false,"");
        $this->registerTagAttribute('class', 'string', 'CSS class(es) for this element');
        $this->registerTagAttribute('id', 'string', 'Unique (in this file) identifier for this HTML element.');
        $this->registerTagAttribute('style', 'string', 'Individual CSS styles for this element');
    }

    /**
     * Render the Tag with content inside
     * @return string
     */
    public function render() {
        // render tag content
        $this->tag->setContent($this->renderChildren());
        // render the the final tag itself
        return $this->tag->render();
    }
}