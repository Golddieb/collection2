<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 17.04.2017
 */

namespace BN\BnTypoDist\ViewHelpers\Content;

use \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class WrapperViewHelper extends AbstractTagBasedViewHelper {
    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected $contentObject;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * @var array Setting of the Plugin (Extension|Distro) "bn_typo_dist"
     */
    protected $pluginSetting = [];

    /**
     * @var array Setting for the viewhelper (node array of plugin settings)
     */
    protected $viewHelperSettings = [];

    /**
     * @var boolean
     */
    protected $escapeOuput = false;

    /**
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
     * @return void
     */
    public function injectConfigurationManager(ConfigurationManagerInterface  $configurationManager)
    {
        $this->configurationManager = $configurationManager;
        $this->contentObject = $configurationManager->getContentObject();
    }

    /**
     * Set the Plugin Settings
     */
    public function setPluginSettings() {
        if (empty($this->pluginSetting)) {
            $this->pluginSetting = $this->configurationManager->getConfiguration(
                ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
                "bn_typo_dist");
        }
    }

    /**
     * Get the Plugin Settings
     * @return array
     */
    public function getPluginSettings() {
        return $this->pluginSetting;
    }

    /**
     * Set the Viewhelper setting array from plugin settings (typoscript)
     */
    public function setViewHelperSettings() {
        $pluginSettings = $this->getPluginSettings();
        if (is_array($pluginSettings["wrapper"])) {
            $this->viewHelperSettings = $pluginSettings["wrapper"];
        } else {
            $this->viewHelperSettings = [];
        }
    }

    /**
     * Get the Viewhelper setting array
     * @return array
     */
    public function getViewHelperSettings() {
        return $this->viewHelperSettings;
    }

    /**
     * Define the tag, which the renderer should finally render
     * @return string
     */
    public function defineTag() {
        // get all header strings, which are given in the viewhelper argument array
        $viewHelperHeaderArray = $this->arguments['header'];
        // get the tags from the settings
        $viewHelperTags = $this->viewHelperSettings['tags'];
        // the value for the initial tag comes from
        // the plugin settings array ("default" value)
        $defaultTag = $viewHelperTags["default"];
        if (!empty($defaultTag)) {
            $tagType = $defaultTag;
        } else {
            $tagType = "div";
        }

        // if not at least one header string is given or it's not the first record,
        // wrap everything with the default tag
        if ($this->contentObject->parentRecordNumber > 1) {
            if (is_array($viewHelperHeaderArray)) {
                foreach ($viewHelperHeaderArray as $key) {
                    if (!empty($key) && !empty($defaultTag)) {
                        $tagType = $defaultTag;
                        break;
                    } else {
                        $noHeaderSetTag = $viewHelperTags["ifNoHeaderSet"];
                        if ($noHeaderSetTag) {
                            $tagType = $noHeaderSetTag;
                        } else {
                            $tagType = "div";
                        }
                    }
                }
            }
        } else {
            if (!empty($viewHelperTags["first"])) {
                $tagType = $viewHelperTags["first"];
            }
        }
        return $tagType;
    }


    /**
     * Initialize Everything
     */
    public function initialize() {
        $this->tag->reset();

        if ($this->hasArgument('id')) {
            $this->tag->addAttribute('id', $this->arguments['id']);
        }
        if ($this->hasArgument('class')) {
            $this->tag->addAttribute('class', $this->arguments['class']);
        }
        if ($this->hasArgument('style')) {
            $this->tag->addAttribute('style', $this->arguments['style']);
        }
        if ($this->hasArgument('additionalAttributes') && is_array($this->arguments['additionalAttributes'])) {
            $this->tag->addAttributes($this->arguments['additionalAttributes']);
        }
        if ($this->hasArgument('data') && is_array($this->arguments['data'])) {
            foreach ($this->arguments['data'] as $dataAttributeKey => $dataAttributeValue) {
                $this->tag->addAttribute('data-' . $dataAttributeKey, $dataAttributeValue);
            }
        }

        $this->tag->forceClosingTag(true);
        $this->setPluginSettings();
        $this->setViewHelperSettings();
        $this->tag->setTagName($this->defineTag());
    }

    /**
     * Initialize Arguments
     */
    public function initializeArguments() {
        $this->registerTagAttribute("header","array","An array of header strings",false,"");
        $this->registerTagAttribute('class', 'string', 'CSS class(es) for this element');
        $this->registerTagAttribute('id', 'string', 'Unique (in this file) identifier for this HTML element.');
        $this->registerTagAttribute('style', 'string', 'Individual CSS styles for this element');
    }

    /**
     * Render the Tag with content inside
     * @return string
     */
    public function render() {
        // render tag content
        $this->tag->setContent($this->renderChildren());
        // render the the final tag itself
        return $this->tag->render();
    }
}