<?php
/**
 * Created by PhpStorm.
 * User: Julian Mair
 * Date: 27.07.2017
 * Time: 13:34
 */

namespace BN\BnTypoDist\Converter;

use BN\BnTypoDist\Services\ConstantService;
use BN\BnTypoDist\Services\TranslationService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Ts2JsConverter {

    /**
     * @var array The stored items and values which should
     *            get outputted
     */
    protected $outputArray = [];

    /**
     * @var \BN\BnTypoDist\Services\ConstantService   service for the constants functions
     */
    protected $constantService = NULL;


    public function __construct() {
        $this->constantService = GeneralUtility::makeInstance(ConstantService::class);
    }

    /**
     * @param array $typoArray
     *
     * @return string The data as a json object
     */
    private static function convertArrayToJson($typoArray) {
        return json_encode($typoArray, JSON_FORCE_OBJECT + JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param string $code The code which should wrapped
     *                     with the default js code block
     *
     * @return string code
     */
    private static function wrapWithDefaultJSCode($code) {
        $wrappedCode = <<<EOT
(function(window){
    'use strict';
    var BN = window.brandnamic = window.BN = window.brandnamic || window.BN || {};
    BN.lang = BN.lang || {};
    BN.lang = $code
})(window);
EOT;
        return $wrappedCode;
    }

    /**
     * @param string $code The code which should wrapped
     *                     with the html script tag
     *
     * @return string code
     */
    private static function wrapIntoScriptTag($code) {
        return "<script type=\"text/javascript\">" .$code ."</script>";
    }

    /**
     * Get the requested data and convert it into json format.
     *
     * @return string json object
     */
    public function getDataInJsonStructure() {
        $this->outputArray["constants"] = $this->constantService->getConstants();
        $jsonObject = self::convertArrayToJson($this->outputArray);

        return $jsonObject;
    }

    /**
     * Format everything in an standarized format.
     *
     * @return string the whole generated code
     */
    public function formattedOutput() {
        return self::wrapIntoScriptTag(
            self::wrapWithDefaultJSCode(
                $this->getDataInJsonStructure()
            )
        );
    }

    /**
     * Return the generated inline javascript code
     *
     * @return string   the generated code
     */
    public function getData() {
        return $this->formattedOutput();
    }

}