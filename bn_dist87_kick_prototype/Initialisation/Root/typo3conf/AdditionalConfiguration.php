<?php
defined('TYPO3_MODE') or die();
/**
 * File:    AdditionalConfiguration.php
 * Purpose: Set up your config in this file if you want
 *          that specific pieces should have a default
 *          value from the very beginning.
 * ATTENTION:   Everything what is written here will
 *              override the config from Install-Tool
 *              (i.e. LocalConfiguration.php)
 *
 * More Information:
 *  https://docs.typo3.org/typo3cms/InsideTypo3Reference/CoreArchitecture/Configuration/AdditionalConfiguration/Index.html
 */

/**
 * Current Extension Key
 */
$_EXTKEY = "bn_typo_dist";

/*
* ---------------
*      MASK
* ---------------
*
* NOTE(!): if someone find a similar solution directly in typoscript, please
*          add it into the kickstarter and delete the following code.
* */

// get unserialized value (array) of the configuration
$tmpMaskConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['mask']);
// adjust the paths
$tmpMaskConf['json']            = "typo3conf/ext/" . $_EXTKEY . "/Resources/Private/extensions/mask/mask.json";
$tmpMaskConf['content']         = "typo3conf/ext/" . $_EXTKEY . "/Resources/Private/extensions/mask/content/";
$tmpMaskConf['layouts']         = "typo3conf/ext/" . $_EXTKEY . "/Resources/Private/extensions/mask/content/layouts/";
$tmpMaskConf['partials']        = "typo3conf/ext/" . $_EXTKEY . "/Resources/Private/extensions/mask/content/partials/";
$tmpMaskConf['backend']         = "typo3conf/ext/" . $_EXTKEY . "/Resources/Private/extensions/mask/backend/";
$tmpMaskConf['layouts_backend'] = "typo3conf/ext/" . $_EXTKEY . "/Resources/Private/extensions/mask/backend/layouts/";
$tmpMaskConf['partials_backend']= "typo3conf/ext/" . $_EXTKEY . "/Resources/Private/extensions/mask/backend/partials/";
$tmpMaskConf['preview']         = "typo3conf/ext/" . $_EXTKEY . "/Resources/Private/extensions/mask/preview/";
// serialize and save the previous temporary values into the global config array
$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['mask'] = serialize($tmpMaskConf);


// ------ --------
// GLOBAL DEFAULTS
// ------ --------

// switch option in the backend
$GLOBALS['TYPO3_CONF_VARS']['FE']['hidePagesIfNotTranslatedByDefault'] = '1';

// GraphicsMagick yields better results on Google PageSpeed Insights
$GLOBALS['TYPO3_CONF_VARS']['GFX']['im_version_5'] = 'gm';
$GLOBALS['TYPO3_CONF_VARS']['GFX']['colorspace'] = 'RGB';
$GLOBALS['TYPO3_CONF_VARS']['GFX']['jpg_quality'] = '70';

//  RTE presets registration
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['bn_default'] = 'EXT:' . $_EXTKEY . '/Configuration/RTE/bn_default.yaml';