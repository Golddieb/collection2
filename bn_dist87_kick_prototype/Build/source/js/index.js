'use strict';

/** ********************************
 *         IMPORTS & EXPORTS
 ** ****************************** */

// LIBS -- third party
var $ = require('jquery');
var lazySizes = require('lazysizes');
var pictureFill = require('picturefill');
var moment = require('moment');
require('moment/locale/de');
require('moment/locale/it');
require('magnific-popup');

// LIBS -- brandnamic
var cookiebannerInit = require('bn-cookiebanner').default;

// CUSTOM SCRIPTS
var dynFileLoader = require('./lib/dynFileLoader');

// CONFIGS

// GLOBAL EXPORTS
window.$ = window.jQuery = $;
window.debug = window.debug || false;


/** ********************************
 *              CUSTOM
 ** ****************************** */

/**
 * variables
 * ------------------------------- */

var $body = $('body');

// TS CONSTANTS & XLIFF LOCALES
var constants = BN.lang.constants || {};

// LANGUAGE
var language = $body.data('lang');
var lang_int = $body.data('lang-id');

// set current language for moment.js
moment.locale(language);

// CSS FILE-PATHS
var some_css = {
    // EXAMPLE WITHOUT CALLBACK
    // newsletter: 'path/to/source.css',
};

// JS FILE-PATHS
var some_js = {
    // EXAMPLE WITH CALLBACK
    // newsletter: {
    //     src: 'path/to/source.js',
    //     callback: callbackTestFunction()
    // }
};

/**
 * general
 * ------------------------------- */

// load some css
dynFileLoader.load(some_css, 'css');

// load some js
dynFileLoader.load(some_js, 'js');

/**
 * cookiebanner
 * ------------------------------- */

cookiebannerInit({
    language: language,
    url: "/index.php?id=5&L=" + lang_int,
    options: {
        excludes: '.cb-cookiebanner .cb-text,.cb-cookiebanner .cb-infolink'
    }
});


/**
 * header
 * ------------------------------- */

/**
 * vista
 * ------------------------------- */

/**
 * main
 * ------------------------------- */

/**
 * footer
 * ------------------------------- */




