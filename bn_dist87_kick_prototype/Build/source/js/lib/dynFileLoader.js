/* *************************************************** *
    load external resources dynamicly (js, css, ...)
    and with optional callback function
 * ***************************************************
    possible config:
    1) long variant -> with callback:
    '''
        var configObject = {
            nameOfSource: {
                src: 'path/to/source',
                callback: function () { *do something* }
            }
        };
    '''
    2) short variant -> without callback:
    '''
        var configObject = {
            nameOfSource: 'path/to/source'
        };
    '''
****************************************************** */

var exports = {};

/**
 * Function which creates the specific tag
 * and create event for a custom callback
 * (intended for internal use only)
 *
 * @param {Object} config - configuration for the tag
 */
exports.tagCreator = function (config) {
    var tag;
    // create the specific html tag
    if (config.type === 'js') {
        tag = document.createElement('script');
        tag.src = config.src;
        tag.type = 'text/javascript';
        tag.async = true;
    } else if (config.type === 'css') {
        tag = document.createElement('link');
        tag.href = config.src;
        tag.media = 'screen';
        tag.rel = 'stylesheet';
        tag.type = 'text/css';
    }
    // append tag in body area
    if (typeof tag !== 'undefined') {
        document.getElementsByTagName('body')[0].appendChild(tag);
        // trigger a callback if given
        if (typeof config.callback !== 'undefined') {
            // if 'addEventListener' doesn't exist -> fallback for IE
            if (!tag.addEventListener) {
                tag.attachEvent('onload', config.callback);
            } else {
                tag.addEventListener('load', config.callback);
            }
        }
    }
};

/**
 * This function should be used in
 * your code to load things
 *
 * @param {Object} obj
 * @param {string} type
 */
exports.load = function (obj, type) {
    for(var item in obj) {
        if(obj.hasOwnProperty(item)) {
            var src = '';
            var callback = function () {};

            if (typeof obj[item] === typeof '')
                src = obj[item];
            else {
                src = obj[item].src;
                callback = obj[item].callback || callback;
            }

            exports.tagCreator({
                src: src,
                type: type,
                callback: callback
            });
        }
    }
};

module.exports = exports;