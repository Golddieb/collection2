'use strict';

window.BN = window.brandnamic = (window.brandnamic || window.BN || {});

var hotel_id = $('body').data('ehid');

window.brandnamic.formDefaults = {
    all: {
        language: BN.lang.currLang,
        hotel_id: hotel_id,
        validation: 'live-noText-noIcon',
        // Uncomment clearcache: 1 during dynforms development
        // clearcache: 1,
    },
};

