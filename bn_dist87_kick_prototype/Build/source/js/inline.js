'use strict';

/** ********************************
 *         IMPORTS & EXPORTS
 ** ****************************** */

var dynFileLoader = require('./lib/dynFileLoader');


/** ********************************
 *              CUSTOM
 ** ****************************** */

// only proceed if DOM is fully loaded
document.addEventListener('DOMContentLoaded', function() {
    // CSS
    var global_css = {
        mainCSS: '/typo3conf/ext/bn_typo_dist/Resources/Public/admin/css/main.css',
    };
    // JS
    var global_js = {
        mainJS: '/typo3conf/ext/bn_typo_dist/Resources/Public/admin/js/main.js'
    };

    // load global css
    dynFileLoader.load(global_css, 'css');
    // load global js
    dynFileLoader.load(global_js, 'js');
});