
// -------------------------------------
//   require list
// -------------------------------------

var gulp = require('gulp'),
    browserSync = require('browser-sync');


//   browser-sync tool
gulp.task('tool:browser-sync', function() {
    browserSync.init({
        // proxy: 'http://bn****.develope.bnamic.com',

        // middleware: require('serve-static')('../Files'),
        middleware: [
            require('serve-static')('../Files', {
                'cacheControl': false
            }),
            function(req, res, next) {
                var conjunction;
                if (req.url.match(/\?/)) {
                    conjunction = '&';
                } else {
                    conjunction = '?';
                }
                req.url = req.url + conjunction + "no_cache=1";
                next();
            }
        ],
        files: '../Files/**/*.{js|css}',

        rewriteRules: [
            // matches all css files in the Files/css folder
            {
                match: /\/typo3conf\/ext\/bn_typo_dist\/Files\/css\/(\w*).css\?\d*/igm,
                replace: '/css/$1.css'
            },
            // matches all js files in the Files/js folder
            {
                match: /\/typo3conf\/ext\/bn_typo_dist\/Files\/js\/(\w*).js\?\d*/igm,
                replace: '/js/$1.js'
            }
        ],
    });
});


// watcher for browser-sync
gulp.task('watch-sync', ['tool:browser-sync'], function() {
    gulp.start('create:ts');
    gulp.watch('individual/**/*.{scss,css}', ['compile:scss:all']);
    gulp.watch('individual/**/*.js', ['compile:js:main']);
});