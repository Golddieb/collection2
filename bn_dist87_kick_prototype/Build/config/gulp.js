var path = require('path');

// absolut paths relativ from this file
var distroRootPath = "../../";
var distroCodeSourcePath = "../source/";

module.exports = {
    distroRootPath: distroRootPath,
    distroCodeSourcePath: distroCodeSourcePath,
    tasks: {
        js: {
            main : {
                entry: path.resolve(__dirname, distroCodeSourcePath, 'js/index.js'),
                absFileDest : path.resolve(__dirname, distroRootPath, 'Resources/Public/admin/js'),
                filename: 'main.js'
            },
            inline : {
                entry: path.resolve(__dirname, distroCodeSourcePath, 'js/inline.js'),
                absFileDest : path.resolve(__dirname, distroRootPath, 'Resources/Public/admin/js'),
                filename: 'inline.js'
            }
        },
        scss: {
            main : {
                entry: path.resolve(__dirname, distroCodeSourcePath, 'scss/main.scss'),
                absFileDest : path.resolve(__dirname, distroRootPath, 'Resources/Public/admin/css'),
                filename: 'main.css'
            },
            inline : {
                entry: path.resolve(__dirname, distroCodeSourcePath, 'scss/inline.scss'),
                absFileDest : path.resolve(__dirname, distroRootPath, 'Resources/Public/admin/css'),
                filename: 'inline.css'
            }
        },
        ts: {
            include: {
                js : [
                    "inline"
                ],
                css: [
                    "inline"
                ]
            },
            absFileDest : path.resolve(__dirname, distroRootPath, 'Resources/Private/typoscript/generated'),
            filename: 'dist.ts',
            tsOutputPath : 'bn_typo_dist/Resources/Public/admin/'
        }
    }
};