var webpack = require('webpack-stream');

module.exports = function(filename, additionalPlugins) {
    additionalPlugins = additionalPlugins || [];
    
    return {
        output: {
            filename: filename
        },
        module: {
            loaders: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    loader: "babel-loader?cacheDirectory=true",
                    options: {
                        presets: ['env']
                    }
                }
            ]
        },
        plugins: [
            new webpack.webpack.optimize.UglifyJsPlugin({
                parallel: true,
                uglifyOptions: {
                    ecma: 6,
                    mangle: true,
                    output: {
                        comments: true
                    }
                }
             }),
            new webpack.webpack.NoEmitOnErrorsPlugin()
        ].concat(additionalPlugins)
    };
};