/* ----------------------------------- *
 *    Brandnamic JS/(S)CSS Builder     *
 * ----------------------------------- */

// -------------------------------------
//   global require list
// -------------------------------------

// main plugin, which is needed for general functionality
var gulp = require('gulp');
// all plugins, which start with gulp-* are automatically loaded with
// the following module and saved into the 'plugins' variable.
var gulpLoadPlugins = require('gulp-load-plugins');
var plugins = gulpLoadPlugins();
// all the other modules have to be acquired via
// the 'require' command.
var webpack = require('webpack-stream');
var fs = require('fs');
var scssTildeImporter = require('node-sass-tilde-importer');


// -------------------------------------
//          GLOBAL FUNCTIONS
// -------------------------------------

function globalErrorHandler(err) {
    console.error(err);
    this.emit('end');
}

// -------------------------------------
//   Options for gulpfile (outsourced)
// -------------------------------------

var options = require('./config/gulp');
var webpackConfig = require('./config/webpack');

// -------------------------------------
//   Tasks
// -------------------------------------

// ALL-IN-ONE task for js
gulp.task('compile:js:all', function(cb) {
    plugins.util.log("starting js subtasks...");
    plugins.sequence(['compile:js:inline', 'compile:js:main'])(cb);
});

// task for custom js
gulp.task('compile:js:main', function() {
    return gulp.src(options.tasks.js.main.entry)
        .pipe(plugins.plumber({
            errorHandler: function (err) {this.emit('end');}
        }))
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('default'))
        .pipe(webpack(
            webpackConfig(options.tasks.js.main.filename)
        ))
        .pipe(gulp.dest(options.tasks.js.main.absFileDest));
});
// task for custom js
gulp.task('compile:js:inline', function() {
    return gulp.src(options.tasks.js.inline.entry)
        .pipe(plugins.plumber({
            errorHandler: function (err) {this.emit('end');}
        }))
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('default'))
        .pipe(webpack(
            webpackConfig(options.tasks.js.inline.filename)
        ))
        .pipe(gulp.dest(options.tasks.js.inline.absFileDest));
});

// ALL-IN-ONE task for scss
gulp.task('compile:scss:all', function(cb) {
    plugins.util.log("starting scss subtasks...");
    plugins.sequence(['compile:scss:inline', 'compile:scss:main'])(cb);
});

// task for main css
gulp.task('compile:scss:main', function() {
    return gulp.src(options.tasks.scss.main.entry)
        .pipe(plugins.plumber({ errorHandler: globalErrorHandler }))
        .pipe(plugins.sass({ importer: scssTildeImporter }))
        .on('error', plugins.sass.logError)
        .pipe(plugins.autoprefixer({ browsers: ['last 5 versions'], cascade: false }))
        .pipe(plugins.concat(options.tasks.scss.main.filename))
        .pipe(plugins.cleanCss({ compatibility: 'ie8' }))
        .pipe(gulp.dest(options.tasks.scss.main.absFileDest))
});

// task for inline css (header, navi, season + print)
gulp.task('compile:scss:inline', function() {
    return gulp.src(options.tasks.scss.inline.entry)
        .pipe(plugins.plumber({ errorHandler: globalErrorHandler }))
        .pipe(plugins.sass({ importer: scssTildeImporter }))
        .on('error', plugins.sass.logError)
        .pipe(plugins.autoprefixer({ browsers: ['last 5 versions'], cascade: false }))
        .pipe(plugins.concat(options.tasks.scss.inline.filename))
        .pipe(plugins.cleanCss({ compatibility: 'ie8' }))
        .pipe(gulp.dest(options.tasks.scss.inline.absFileDest))
});

// task for typoscript - writes correct typo-includes
gulp.task('create:ts', function() {
    var content = [];
    var path = "";

    Object.keys(options.tasks.ts.include).forEach(function(filetype, i) {
        ++i;
        content.push('# ------------------------------------');
        content.push('page.headerData.20.' + i+'0' + ' = COA');
        options.tasks.ts.include[filetype].forEach(function (name, j) {
            filetype = (filetype === "css" ) ? "scss" : filetype;
            ++j;
            var conf = options.tasks[filetype][name];

            if (filetype === 'scss') {
                if (name === 'main') {
                    // add path of css file
                    path = 'EXT:'+options.tasks.ts.tsOutputPath+'css/' + conf.filename;
                    content.push('# main.css');
                    content.push('page.includeCSS.css' + j + '=' + path);
                    content.push('page.includeCSS.css' + j + '.media = screen');
                } else if (name === 'inline') {
                    path = 'EXT:'+options.tasks.ts.tsOutputPath+'css/' + conf.filename;
                    content.push('# inline.css');
                    content.push('page.headerData.20.'+ i+'0.' + j+'0' + ' = FILE');
                    content.push('page.headerData.20.'+ i+'0.' + j+'0' + '.file = ' + path);
                    content.push('page.headerData.20.'+ i+'0.' + j+'0' + '.wrap = <style type="text/css" media="all">|</style>');
                }
            } else if (filetype === "js") {
                if (name === "inline") {
                    // add path of js file
                    path = "EXT:"+options.tasks.ts.tsOutputPath+"js/" + conf.filename;
                    content.push('# inline.js');
                    content.push('page.headerData.20.'+ i+'0.' + j+'0' + ' = FILE');
                    content.push('page.headerData.20.'+ i+'0.' + j+'0' + '.file = ' + path);
                    content.push('page.headerData.20.'+ i+'0.' + j+'0' + '.wrap = <script type="text/javascript">|</script>');
                }
            }
        });
    });

    // write to ts file
    plugins.util.log(
        "writing file '"+ options.tasks.ts.filename +
        "' to '" + options.tasks.ts.absFileDest + "'"
    );
    return plugins.file(options.tasks.ts.filename, content.join("\n"), { src: true })
        .pipe(gulp.dest(options.tasks.ts.absFileDest));
});

// ALL-IN-ONE task for building entire project.
// (with task order)
gulp.task('build:all', function(cb) {
    plugins.util.log("building entire project");
    // run JS and scss async, wait till finished then start typoscript
    plugins.sequence(['compile:js:all', 'compile:scss:all'], 'create:ts')(cb);
});


// -------------------------------------
//   Watchers
// -------------------------------------

// default watcher for individual .scss and .js files.
// They trigger their corresponding tasks.
gulp.task('watch:individual', function() {
    gulp.watch('source/scss/**/*.{scss,css}', function () {
        plugins.sequence('compile:scss:all', 'create:ts') (function () {});
    });
    gulp.watch('source/js/**/*.js', function () {
        plugins.sequence('compile:js:all', 'create:ts') (function () {});
    });
});


// -------------------------------------
//   Tools
// -------------------------------------
// add some tools here if needed. The kickstarter
// have maybe some usable tasks already installed or
// preconfigured. Look for the task subfolder.
// Install them like this:
// e.g.: require("./tasks/browser-sync.js");


// -------------------------------------
//   Custom tasks, watchers, etc ...
// -------------------------------------
// place your code here and do whatever you want...