# ##########################################
#            GLOBAL OVERRIDES
# ##########################################

<INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/system/config/global.ts">


# ##########################################
#       MAIN PAGE SETUP (HEAD SECTION)
# ##########################################

<INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/system/head.ts">


# ##########################################
#        EXTENSIONS CONFIGURATION
# ##########################################

# distro config
<INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/system/config/distro.ts">
# min config
<INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/extensions/min/config.ts">


# ##########################################
#           ADDITIONAL PAGETYPES
# ##########################################

# infract config
<INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/system/pagetypes/infract.ts">
# robots config
<INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/system/pagetypes/robots.ts">


# ##########################################
#      GENERATED TS FILE(S) FROM GULP
# ##########################################

<INCLUDE_TYPOSCRIPT:source="DIR:EXT:bn_typo_dist/Resources/Private/typoscript/generated/" extensions="ts">


# ##########################################
#    FLUIDTEMPLATE CONFIG (BODY SECTION)
# ##########################################

<INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/system/body.ts">

