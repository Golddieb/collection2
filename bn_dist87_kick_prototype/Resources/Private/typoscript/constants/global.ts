### GLOBAL CONSTANTS ###

# Protocol Switch
protocol = http://

[globalString = ENV:HTTPS=on]
    protocol = https://
[global]

# add query string exclude for typolinks
queryString_exclude_params = id,utm_source,utm_medium,utm_campaign,c,gclid,utm_term,utm_content,PHPSESSID,c[ids_hotels][],ADMCMD_cooluri

# Brandnamic Infos
bn {
    name = Brandnamic
    companyType = GmbH
    slogan = Hotel & Destination Marketing
    copyright = {$bn.name} {$bn.companyType} | {$bn.slogan}
    mail = info@brandnamic.com

    # Infract
    # customcategory=infract=Infract
    infract.data {
        # cat=infract/1; type=string; label= Berater
        berater =
        # cat=infract/2; type=string; label= PM
        pm =
        # cat=infract/3; type=string; label= Projektverantwortlicher Programmierer
        dev =
        # cat=infract/4; type=string; label= Beteiligte Programmierer
        devs =
    }
}

# customcategory=companydata=Betrieb Informationen
company {
    # cat=companydata; type=string; label= Name
    name = Betriebsname
    # cat=companydata; type=string; label= Logo-Pfad
    logo = /typo3conf/ext/bn_typo_dist/Resources/Public/admin/img/backend/LOGO_brandnamic_356x356.png
    # cat=companydata; type=string; label= Email
    mail = betriebs@mail.com
    # cat=companydata; type=string; label= Telefonnummer
    tel = +39 1234 567 890
    # cat=companydata; type=string; label= Telefonnummer (fuer 'tel' Link)
    telLink = 00391234567890
    # cat=companydata; type=string; label= Faxnummer
    fax = +39 1234 567 891
    # cat=companydata; type=string; label= MwSt-Nummer
    vat = IT00000000000
    # cat=companydata; type=string; label= Postleitzahl
    zip = I-00000
    # cat=companydata; type=string; label= Breitengrad
    coordLat = 00.00000
    # cat=companydata; type=string; label= Laengengrad
    coordLong = 00.00000

    # cat=companydata; type=string; label= Ehotelier TextID
    ehotelier_id = 001_BRANDNAMIC_1

    description {
        # cat=companydata; type=string; label= Beschreibung DE
        de =
        # cat=companydata; type=string; label= Beschreibung IT
        it =
        # cat=companydata; type=string; label= Beschreibung EN
        en =
    }

    keywords {
        # cat=companydata; type=string; label= Keywords DE
        de =
        # cat=companydata; type=string; label= Keywords IT
        it =
        # cat=companydata; type=string; label= Keywords EN
        en =
    }
}

# lang globals (language independent)
lang {
    global {
        0.locale = DE
        1.locale = IT
        2.locale = EN
    }
}
# Metas Fallback
author = {$company.name}

# LANG OVERRIDES -- DE
<INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/constants/lang/de.ts">

# LANG OVERRIDES -- IT
[globalVar = GP:L = 1]
    <INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/constants/lang/it.ts">
[global]
# LANG OVERRIDES -- EN
[globalVar = GP:L = 2]
    <INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/constants/lang/en.ts">
[global]



