## LANGUAGE SYSTEM CONFIG ##
# (DON'T USE THIS FOR TRANSLATIONS)
sys_language_uid = 2
language = en
locale_all = en_GB

# Brandnamic Infos
bn.link = https://www.brandnamic.com/en

# Language translations
lang {
    cur {
        label = English
        uid = {$sys_language_uid}
        locale = {$language}
        locale_all = {$locale_all}
    }
    global {
        0.label = German
        1.label = Italian
        2.label = English
    }
}

# Metas Fallback
description = {$company.description.en}
keywords = {$company.keywords.en}


# Default phrases
default {
    no_news = Unfortunately there are no news at the moment.
    no_offers = Currently there are no packages available.
    no_weather = Current meteorological data is available from 11:00 AM.
}

# Custom things...

