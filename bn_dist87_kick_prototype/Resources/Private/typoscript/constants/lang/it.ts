## LANGUAGE SYSTEM CONFIG ##
# (DON'T USE THIS FOR TRANSLATIONS)
sys_language_uid = 1
language = it
locale_all = it_IT

# Brandnamic Infos
bn.link = https://www.brandnamic.com/it

# Language translations
lang {
    cur {
        label = Italiano
        uid = {$sys_language_uid}
        locale = {$language}
        locale_all = {$locale_all}
    }
    global {
        0.label = Tedesco
        1.label = Italiano
        2.label = Inglese
    }
}

# Metas Fallback
description = {$company.description.it}
keywords = {$company.keywords.it}


# Default phrases
default {
    no_news = Purtroppo non ci sono novità al momento.
    no_offers = Al momento non ci sono pacchetti disponibili.
    no_weather = I dettagli meteo aggiornati sono disponibili dalle ore 11.00.
}

# Custom things...
