## LANGUAGE SYSTEM CONFIG ##
# (DON'T USE THIS FOR TRANSLATIONS)
sys_language_uid = 0
language = de
locale_all = de_DE

# Brandnamic Infos
bn.link = https://www.brandnamic.com

# Language translations
lang {
    cur {
        label = Deutsch
        uid = {$sys_language_uid}
        locale = {$language}
        locale_all = {$locale_all}
    }
    global {
        0.label = Deutsch
        1.label = Italienisch
        2.label = Englisch
    }
}

# Metas Fallback
description = {$company.description.de}
keywords = {$company.keywords.de}


# Default phrases
default {
    no_news = Leider gibt es zur Zeit keine Neuigkeiten.
    no_offers = Derzeit sind keine Angebote verfügbar.
    no_weather = Aktuelle Wetterdaten sind erst ab 11:00 Uhr verfügbar.
}

# Custom things...
