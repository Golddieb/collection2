### GLOBAL OVERRIDES ###

## RTE ##
# wrap each table with a div tag
lib.parseFunc_RTE {
    externalBlocks {
        table.stdWrap.wrap = <div class="responsive-table">|</div>
    }
}

## NEW CONTENT ELEMENTS ##
# define rendering definition
tt_content {
    bn_seasonswitch < lib.contentElement
    bn_seasonswitch {
        templateName = SeasonSwitch
        templateRootPaths.100 = EXT:bn_typo_dist/Resources/Private/fluidtmpl/seasons/frontend/
        partialRootPaths.100 = EXT:bn_typo_dist/Resources/Private/fluidtmpl/seasons/frontend/partials
    }
    bn_seasonelement < lib.contentElement
    bn_seasonelement {
        templateName = SeasonElement
        templateRootPaths.100 = EXT:bn_typo_dist/Resources/Private/fluidtmpl/seasons/frontend/
        partialRootPaths.100 = EXT:bn_typo_dist/Resources/Private/fluidtmpl/seasons/frontend/partials
    }
}