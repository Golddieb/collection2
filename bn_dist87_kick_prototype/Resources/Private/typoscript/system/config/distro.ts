## Configuraton for distro plugin ##
plugin.tx_bn_typo_dist {
    settings {
        headings {
            tags {
                1 = h1
                2 = h2
                3 = h3
                4 = h4
                5 = h5
                last = h6
                default = span
            }
            types {
                header = 1
                subheader = 2
                thirdheader = 3
            }
            increment {
                by = 1
                begin = 2
            }
        }
        wrapper {
            tags {
                first = div
                ifNoHeaderSet = div
                default = section
            }
        }
    }
}