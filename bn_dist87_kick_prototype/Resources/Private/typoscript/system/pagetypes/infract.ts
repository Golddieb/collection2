infract = PAGE
infract {
    typeNum = 666
    config {
        disableAllHeaderCode = 1
        disableCharsetHeader = 1
        additionalHeaders.10.header = Content-Type:application/json
        xhtml_cleaning = 0
        admPanel = 0
        debug = 0
        no_cache = 1
    }
    10 = COA
    10 {
        10 = TEXT
        10.value = {$bn.infract.data.berater}
        10.wrap = "berater": "|",

        20 = TEXT
        20.value = {$bn.infract.data.pm}
        20.wrap = "pm": "|",

        30 = TEXT
        30.value = {$bn.infract.data.dev}
        30.wrap = "dev": "|",

        40 = TEXT
        40.value = {$bn.infract.data.devs}
        40.wrap = "devs": "|"

        wrap = {|}
    }
}