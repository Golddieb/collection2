robots = PAGE
robots {
    typeNum = 1000
    config {
        disableAllHeaderCode = 1
        renderCharset = utf-8
        metaCharset = utf-8
        additionalHeaders.10.header = Content-Type:text/plain;charset=utf-8
        # disable cache
        no_cache = 1
        # don't clean xhtml tags
        xhtml_cleaning = 0
        # disable admin panel
        admPanel = 0
        # cooluri settings
        tx_cooluri_enable = 0
    }
    10 = COA
    10 {
        ## STATIC -- Add static code here
        10 = TEXT
        10 {

            value (
User-Agent: *
Allow: /
Disallow: /t3lib/
Disallow: /typo3/
Disallow: /*&type=98
)
            append = TEXT
            append.char = 10
        }

        ## GENERATED -- Allow installed extensions resources
        # the public resource directory from each installed extension should
        # be allowed explicitly
        20 = USER
        20.userFunc = BN\BnTypoDist\Helper\FileLookUpHelper->generateRobotsAllowedExtensions

        ## STATIC -- Add static code here
        30 = TEXT
        30 {
            value (
Disallow: /typo3conf/ext/
            )
            append = TEXT
            append.char = 10
        }

        ## GENERATED -- Sitemaps (add additional languages by yourself)
        40 = HMENU
        40 {
            special = language
            special.value = 0,1,2
            special.normalWhenNoLanguage = 0

            1 = TMENU
            1 {
                noBlur = 1
                NO {
                    doNotLinkIt = 1
                    stdWrap {
                        cObject = COA
                        cObject {
                            10 = TEXT
                            10 {
                                value = Sitemap:
                                noTrimWrap = || |
                            }
                            20 = TEXT
                            20 {
                                data = getEnv:HTTP_HOST
                                prepend = TEXT
                                prepend {
                                    value = {$protocol}
                                    insertData = 1
                                }
                            }
                            30 = TEXT
                            30 {
                                noTrimWrap = ||sitemap.xml|

                                value = / || /it/ || /en/
                                append = TEXT
                                append.char = 10
                            }
                        }
                    }
                }

                # Blendet Sprachen aus wenn nicht verfügbar
                USERDEF1 = 1
                USERDEF1.doNotLinkIt = 1
                USERDEF1.doNotShowLink = 1

                USERDEF2 = 1
                USERDEF2.doNotLinkIt = 1
                USERDEF2.doNotShowLink = 1
            }
        }
    }
}