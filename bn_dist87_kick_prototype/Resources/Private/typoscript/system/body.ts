# Custom lib includes
#<INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/libs/lib.navi.ts">
<INCLUDE_TYPOSCRIPT:source="FILE:EXT:bn_typo_dist/Resources/Private/typoscript/libs/lib.elements.ts">

# MAIN-TEMPLATE
page.10 = FLUIDTEMPLATE
page.10 {
    format = html
    file = EXT:bn_typo_dist/Resources/Private/fluidtmpl/page/Main.html

    layoutRootPath = EXT:bn_typo_dist/Resources/Private/fluidtmpl/page/layouts/
    partialRootPath = EXT:bn_typo_dist/Resources/Private/fluidtmpl/page/partials/

    dataProcessing {
        # Main Menu
        10 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
        10 {
            special = directory
            special.value = 1
            levels = 5
            includeSpacer = 1
            excludeUidList = 3
            as = mainmenu
        }
        # Lang Menu
        20 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
        20 {
            special = language
            special.value = 0,1,2
            special.normalWhenNoLanguage = 0
            as = langmenu
            dataProcessing {
                10 = BN\BnTypoDist\DataProcessing\GlobalSysLanguageProcessor
            }
        }
        # Breadcrumb
        30 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
        30 {
            special = rootline
            special.range= 0|-1
            as = breadcrumb
        }
        # Legal Menu
        40 = TYPO3\CMS\Frontend\DataProcessing\MenuProcessor
        40 {
            special = list
            special.value = 4,5,6
            levels = 1
            as = legalmenu
        }
    }
}

# FALLBACK-TEMPLATE
[globalVar = TSFE:id=15]
    page.10 {
        file = EXT:bn_typo_dist/Resources/Private/fluidtmpl/page/Fallback.html
        dataProcessing >
        variables >
    }
[global]