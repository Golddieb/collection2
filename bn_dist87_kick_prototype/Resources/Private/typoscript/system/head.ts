# main configuration of the "page"-object, for further info's look here:
# https://docs.typo3.org/typo3cms/TyposcriptReference/Setup/Index.html
page = PAGE
page {
    typeNum = 0
    config {
        # general settings
        doctype = html5
        xmlprologue = none
        disablePrefixComment = false

        # cache settings
        no_cache = 0
        cache_clearAtMidnight = true
        sendCacheHeaders = 1

        # html-tag settings
        htmlTag_setParams = lang="{$language}" xml:lang="{$language}"

        # title tag settings
        pageTitleFirst = 1
        pageTitle.field = subtitle // title

        # language settings
        linkVars = L(0-4)
        sys_language_uid = {$sys_language_uid}
        language = {$language}
        locale_all = {$locale_all}
        sys_language_mode = ignore
        sys_language_overlay = hideNonTranslated

        # resources settings
        absRefPrefix = /
        removeDefaultJS = 1
        inlineStyle2TempFile = 0

        # typolink settings
        intTarget = _top
        extTarget = _blank

        # email settings
        # it's recommended to set the value of 'spamProtectEmailAddresses in the
        # range from -5 to 1, values outside of this range can make problems, for further infos look here:
        # https://docs.typo3.org/typo3cms/TyposcriptReference/Setup/Config/Index.html#spamprotectemailaddresses
        spamProtectEmailAddresses = -3
        spamProtectEmailAddresses_atSubst = @<span class="hidden">no-spam.</span>
        spamProtectEmailAddresses_lastDotSubst = .<span class="hidden">no-spam.</span>

        # extensions settings
        tx_cooluri_enable = 1
        redirectOldLinksToNew = 1

        # development settings (don't use in production!)
        admPanel = 0
        contentObjectExceptionHandler = 0
    }

    # META-Tag configuration
    # you can place everthing you want here, no need for headerData objects...
    # for further info's, look here:
    # https://docs.typo3.org/typo3cms/TyposcriptReference/Setup/Meta/Index.html
    meta {
        # default tags
        robots = index, follow, noodp
        viewport = width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=yes
        referrer = origin

        # with possible overrides from the backend
        author = {$author}
        author.override.field = author
        description = {$description}
        description.override.field = description
        keywords = {$keywords}
        keywords.override.field = keywords

        # additional tags
        format-detection = telephone=no

        # Facebook open graphs
        og:title {
            field = title
        }
        og:site_name = {$company.name}
        og:url {
            typolink {
                parameter.field = uid
                forceAbsoluteUrl = 1
                returnLast = url
            }
        }
        og:description = {$description}
        og:description {
            override.field = description
            override.if.isTrue.field = description
        }
        og:type = website
        og:locale = de_DE
        og:locale:alternate {
            value {
                1 = it_IT
                2 = en_GB
            }
        }
        ## uncomment and adjust the following if
        ## you have an image set somewhere
        # og:image {
        #     cObject = CONTENT
        #     cObject {
        #         table = tt_content
        #         select {
        #             max = 1
        #             pidInList = 1
        #             languageField = sys_language_uid
        #             where = colPos=0
        #         }
        #     }
        # }
        # og:image:type = image/jpeg
        # og:image:width = 1280
        # og:image:height = 720
    }

    # header-Tag configuration
    headerData {

        # static
        10 = COA
        10 {
            # copyright Info (dynamic year)
            1 = COA
            1 {
                5 = TEXT
                5.value = <!-- Copyright:
                10 = TEXT
                10 {
                    data = date : U
                    strftime = %Y
                    noTrimWrap = | | |
                }
                15 = TEXT
                15.value = {$bn.copyright} -->
            }

            # add alternate tags for the language
            2 = HMENU
            2 {
                stdWrap.append = TEXT
                stdWrap.append.char = 10

                special = language
                special.value = 0,1,2
                special.normalWhenNoLanguage = 0

                1 = TMENU
                1 {
                    noBlur = 1
                    NO {
                        doNotLinkIt = 1
                        stdWrap {
                            cObject = COA
                            cObject {
                                wrap = <link rel="alternate"|/>

                                10 = TEXT
                                10 {
                                    noTrimWrap = | hreflang="|" |

                                    value = de || it || en
                                }

                                20 = TEXT
                                20 {
                                    wrap = href="|"

                                    typolink {
                                        parameter.data = page:uid
                                        additionalParams = &L=0 || &L=1 || &L=2
                                        returnLast = url
                                        forceAbsoluteUrl = 1
                                    }
                                }
                            }
                        }
                    }

                    # hide not available languages
                    USERDEF1 = 1
                    USERDEF1.doNotLinkIt = 1
                    USERDEF1.doNotShowLink = 1

                    USERDEF2 = 1
                    USERDEF2.doNotLinkIt = 1
                    USERDEF2.doNotShowLink = 1
                }
            }

            # icons --> use http://realfavicongenerator.net/
            # add the generated code lines here
            3 = TEXT
            3.value (
            <!-- insert generated icon paths here -->
            )

            # cononical link to prevent duplicate content (SEO)
            4 = TEXT
            4 {
                wrap = <link rel="canonical" href="|" />
                typolink {
                    parameter = {page:uid}
                    parameter.insertData = 1
                    # add all parameter of the current URL
                    addQueryString = 1
                    addQueryString.method = GET
                    # specific params will be ignored so that they can't be included twice
                    addQueryString.exclude = {$queryString_exclude_params}
                    forceAbsoluteUrl = 1
                    returnLast = url
                }
            }

            # Typo3 e-mail decrypt
            # @TODO: search for alternative, maybe: spamProtectEmailAddresses = ascii
            5 = TEXT
            5.value (
            <script type="text/javascript">
                /* decrypt helper function */
                function decryptCharcode(n,start,end,offset) {
                    n = n + offset;
                    if (offset > 0 && n > end) {
                        n = start + (n - end - 1);
                    } else if (offset < 0 && n < start) {
                        n = end - (start - n - 1);
                    }
                    return String.fromCharCode(n);
                }
                /* decrypt string */
                function decryptString(enc,offset) {
                    var dec = "";
                    var len = enc.length;
                    for(var i=0; i < len; i++) {
                        var n = enc.charCodeAt(i);
                        if (n >= 0x2B && n <= 0x3A) {
                            dec += decryptCharcode(n,0x2B,0x3A,offset); /* 0-9 . , - + / : */
                        } else if (n >= 0x40 && n <= 0x5A) {
                            dec += decryptCharcode(n,0x40,0x5A,offset); /* A-Z @ */
                        } else if (n >= 0x61 && n <= 0x7A) {
                            dec += decryptCharcode(n,0x61,0x7A,offset); /* a-z */
                        } else {
                            dec += enc.charAt(i);
                        }
                    }
                    return dec;
                }
                /* decrypt spam-protected emails */
                function linkTo_UnCryptMailto(s) {
                    location.href = decryptString(s,3);
                }
            </script>
            )

            # ts2js implementation
            6 = COA
            6 {
                10 = USER
                10.userFunc = BN\BnTypoDist\Converter\Ts2JsConverter->getData
            }

            # google structured data
            7 = COA
            7 {
                1 = TEXT
                1.value (
                    <!-- Google Structured Data -->
                    <script type="application/ld+json">
                        {
                            "@context": "https://schema.org",
                            "@type": "LocalBusiness",
                            "name": "{$company.name}",
                )
                2 = TEXT
                2 {
                    wrap = "description": "|",
                    value = {$description}
                    override.field = description
                    override.if.isTrue.field = description
                }
                3 = TEXT
                3 {
                    wrap = "url": "|",
                    typolink {
                        parameter.field = uid
                        forceAbsoluteUrl = 1
                        returnLast = url
                    }
                }
                4 = TEXT
                4 {
                    wrap = "logo": "https://|{$company.logo}",
                    data = getIndpEnv:HTTP_HOST
                }
                5 = TEXT
                5.value (
                            "email":"{$company.mail}",
                            "telephone": "{$company.tel}",
                            "faxNumber": "{$company.fax}",
                            "address": {
                                "@type": "PostalAddress",
                                "postalCode": "{$company.zip}"
                            },
                            "geo":{
                                "@type": "GeoCoordinates",
                                "latitude": "{$company.coordLat}",
                                "longitude": "{$company.coordLong}"
                            },
                            "contactPoint": [{
                                "@type": "ContactPoint",
                                "telephone": "{$company.tel}",
                                "contactType": "reservations",
                                "availableLanguage": ["German", "Italian", "English"]
                            }]
                        }
                    </script>
                )
            }
        }

        # DYNAMIC: ONLY USED IN GULP,
        # DON'T USE OR DELETE THIS HERE!
        20 = COA

        # very useful for debug purposes -
        # for production, delete the following lines
        # 30 = USER
        # 30.userFunc = BN\BnTypoDist\Helper\DebugHelper->debug
    }

    # configuration of the body-tag
    bodyTagCObject = TEXT
    bodyTagCObject {
        dataWrap = <body id="uid{field:uid}"| data-lang="{$language}" data-lang-id="{$sys_language_uid}" data-ehid="{$company.ehotelier_id}">

        # extend classes
        cObject = COA
        cObject {
            # if no classes set, the "class"-attribut will be hidden
            stdWrap {
                noTrimWrap = | class="|" |
                required = 1
            }

            # !!!!
            # ATTENTION: "5" not usable! Will add a specific class if an backend user is logged in (see below!)
            # !!!!

            # lang-locale as class (for css-override possibility)
            10 = TEXT
            10 {
                value = {$language}
                insertData = 1
            }

            # page-uid as class (for css-override possibility)
            20 = TEXT
            20 {
                noTrimWrap = | ||
                value = uid{field:uid}
                insertData = 1
            }
        }
    }
}

# body-class and set 'no_cache' if an backend user is logged in
[globalVar = TSFE:beUserLogin = 1]
    page.config.no_cache = 1
    page.bodyTagCObject.cObject {
        5 = TEXT
        5 {
            noTrimWrap = || |
            value = be_user
        }
    }
[global]

# noindex settings for dev-domains
[globalString = IENV:HTTP_HOST = *bnamic.com]
    page.meta.robots = noindex, nofollow
[global]

# noindex settings for special pages (data-rootline)
[PIDinRootline  = 7]
    page.meta.robots = noindex, nofollow
[global]

# set 404 header for calling 404 page
[globalVar = TSFE:id = 8]
    config.additionalHeaders.10.header = HTTP/1.0 404
[global]

# check browser and (in case of an old one) redirect to specific fallback page
# (if you are not on the fallback site already)
[BN\BnTypoDist\Conditions\BrowserDetection] AND [globalVar = TSFE:id != 15]
    page.config.additionalHeaders.10 {
        header = Location: index.php?id=15
    }
[global]