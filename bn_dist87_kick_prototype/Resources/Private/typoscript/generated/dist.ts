# ------------------------------------
page.headerData.20.10 = COA
# inline.js
page.headerData.20.10.10 = FILE
page.headerData.20.10.10.file = EXT:bn_typo_dist/Resources/Public/admin/js/inline.js
page.headerData.20.10.10.wrap = <script type="text/javascript">|</script>
# ------------------------------------
page.headerData.20.20 = COA
# inline.css
page.headerData.20.20.10 = FILE
page.headerData.20.20.10.file = EXT:bn_typo_dist/Resources/Public/admin/css/inline.css
page.headerData.20.20.10.wrap = <style type="text/css" media="all">|</style>