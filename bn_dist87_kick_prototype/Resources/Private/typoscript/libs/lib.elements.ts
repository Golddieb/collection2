# -------------------- #
#       CONTENT        #
# -------------------- #
# Call it via "<f:cObject typoscriptObjectPath="lib.CONTENT"/>"
# (!) Mind the new field quoting of the where clause (not necessary, but recommended)
# see: https://docs.typo3.org/typo3cms/extensions/core/8.7/Changelog/8.7/Important-80506-DbalCompatibleFieldQuotingInTypoScript.html
lib.CONTENT < styles.content.get
lib.CONTENT {
    select.where = {#colPos} = 0


}

# -------------------- #
#        SEASON        #
# -------------------- #
lib.SEASON = FLUIDTEMPLATE
lib.SEASON {
    templateName = SeasonElement
    templateRootPaths.100 = EXT:bn_typo_dist/Resources/Private/fluidtmpl/seasons/frontend/
    partialRootPaths.100 = EXT:bn_typo_dist/Resources/Private/fluidtmpl/seasons/frontend/partials/
    dataProcessing {
        10 = BN\BnTypoDist\DataProcessing\Elements\SeasonsProcessor
        10 {
            # pid of the page where the switch element is placed
            switchPid = 19

            # the colpos id where the season elements are placed
            colPos = 1

            # define if the season processor should slide up to rootline
            # till he can find at least one record
            slide = -1

            # additional configuration for slide, like 'collecting' mode and reversed order
            #slide {
            #    collect = -1
            #    collectReverse = true
            #}
        }
    }
}