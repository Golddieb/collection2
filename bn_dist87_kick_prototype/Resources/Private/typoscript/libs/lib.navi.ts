#################################################
# Menüs & Navigationen
# (OUTDATED!!! use fluid templates instead!!)
# it is still in the kickstarter for the first
# months only
#################################################

# Beispiel: Sprachmenü
lib.LANG_MENU = HMENU
lib.LANG_MENU {
    wrap = <ul class="lang nav">|</ul>

    special = language
    special.value = 0,1,2
    special.normalWhenNoLanguage = 0

    1 = TMENU
    1 {
        noBlur = 1
        NO {
            wrapItemAndSub = <li>|</li>
            doNotLinkIt = 1
            stdWrap {
                setCurrent = DE || IT || EN
                current = 1

                typolink {
                    parameter.field = uid
                    additionalParams = &L=0 || &L=1 || &L=2

                    addQueryString = 1
                    addQueryString.method = GET
                    addQueryString.exclude = {$queryString_exclude_params}

                    title.field = subtitle
                }
            }
        }

        ACT < .NO
        ACT.wrapItemAndSub = <li class="active">|</li>
        ACT = 1

        # Blendet Sprachen aus wenn nicht verfügbar
        USERDEF1 = 1
        USERDEF1.doNotLinkIt = 1
        USERDEF1.doNotShowLink = 1

        USERDEF2 = 1
        USERDEF2.doNotLinkIt = 1
        USERDEF2.doNotShowLink = 1
    }
}

# Beispiel: Hauptnavigation
lib.MAIN_NAV = HMENU
lib.MAIN_NAV {
    wrap = <nav class="mainmenu">|</nav>

    special = directory
    special.value = 1
    ### zum Ausschliessen einzelner Seiten untenstehendes Attribut dekommentieren und Seiten-IDs eintragen
    excludeUidList = 5

    1 = TMENU
    1 {
        wrap =  <ul class="lvl0">|</ul>
        noBlur = 1
        expAll = 1

        NO {
            wrapItemAndSub = <li class="first">|</li> |*| <li>|</li> |*| <li class="last">|</li>
            ATagTitle.field = title
        }

        ACT < .NO
        ACT = 1
        ACT.ATagParams = class="act"

        CUR < .NO
        CUR = 1
        CUR.ATagParams = class="cur"
   }

    2 < .1
    2.wrap = <ul class="lvl1 sublvl">|</ul>

    3 < .1
    3.wrap = <ul class="lvl2 sublvl">|</ul>
}

# Beispiel: Krümelpfad
lib.BREADCRUMB = HMENU
lib.BREADCRUMB {
    wrap = <div class="breadcrumb">|</div>

    special = rootline
    special.range= 0 | -1

    1 = TMENU
    1 {
        noBlur = 1

        NO {
            allWrap = |<span> > </span> |*||*||
            ATagTitle {
                field = subtitle//title//alias//nav_title
            }
        }

        CUR = 1
        CUR.doNotLinkIt = 1
    }
}

# Beispiel: Footermenü
temp.LEGAL_MENU = HMENU
temp.LEGAL_MENU {
    wrap = <div id="legalM" class="legal-menu">|</div>

    special = list
    special.value = 4,5,6

    1 = TMENU
    1 {
        noBlur = 1

        NO.ATagTitle.field = title
    }
}