You can change the preview image to your prefered image or icon. Here you can set the path to the preview images.

> **Attention**: mask has a hardcoded image naming format and it is forcing you to use .png! 
For each(!) mask-element the format has to be: "ce_" + elementkey + ".png".

The **elementkey** is the unique string you give during the mask-element creation process.

So, e.g. if your element has the key **std-element** the image should have the name **ce_std-element.png**.