## EXT: min (Minifier for TYPO3)
# CSS + JS
page {
    config {
        compressCss = 0
        concatenateCss = 1
        compressJs = 0
        concatenateJs = 1
    }
}

# HTML
plugin.tx_min.tinysource {
    enable = 1
    head {
        stripTabs = 1
        stripNewLines = 1
        stripDoubleSpaces = 1
        stripTwoLinesToOne = 1
    }
    body {
        stripComments = 1
        stripTabs = 1
        stripNewLines = 1
        stripDoubleSpaces = 1
        stripTwoLinesToOne = 0
        preventStripOfSearchComment = 1
        protectCode {
            10 = /(<textarea.*?>.*?<\/textarea>)/is
            20 = /(<pre.*?>.*?<\/pre>)/is
        }
    }
    oneLineMode = 1
}


# deactivate if be user is logged in GET parameter "?debug=1" is given
[globalVar = TSFE:beUserLogin > 0] && [globalVar = GP:debug = 1]
    # CSS + JS
    page {
        config {
            compressCss = 0
            concatenateCss = 0
            compressJs = 0
            concatenateJs = 0
        }
    }
    # HTML
    plugin.tx_min.tinysource.enable = 0
[global]