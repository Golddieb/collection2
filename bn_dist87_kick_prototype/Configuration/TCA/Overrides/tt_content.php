<?php
defined('TYPO3_MODE') or die();

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func( function () {
    // Adds the content element to the "Type" dropdown
    ExtensionManagementUtility::addPlugin(
        [
            'LLL:EXT:bn_typo_dist/Resources/Private/Language/locallang.xlf:season.switch.label',
            'bn_seasonswitch',
            'EXT:bn_typo_dist/Resources/Public/admin/img/backend/backend_favicon_45x45.png'
        ],
        'CType',
        'bn_typo_dist'
    );
    ExtensionManagementUtility::addPlugin(
        [
            'LLL:EXT:bn_typo_dist/Resources/Private/Language/locallang.xlf:season.element.label',
            'bn_seasonelement',
            'EXT:bn_typo_dist/Resources/Public/admin/img/backend/backend_favicon_45x45.png'
        ],
        'CType',
        'bn_typo_dist'
    );


    $TCAforSeasonSwitch = [
        'types' => [
            'bn_seasonswitch' => [
                'showitem' => '
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                    selected_categories;LLL:EXT:bn_typo_dist/Resources/Private/Language/locallang.xlf:season.switch.categories_selection,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended
                ',
                'columnsOverrides' => [
                    'selected_categories' => [
                        'config' => [
                            'size' => 15,
                        ],
                    ],
                ],
            ],
        ],
    ];

    $TCAforSeasonElement = [
        'types' => [
            'bn_seasonelement' => [
                'showitem' => '
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
                    categories;LLL:EXT:bn_typo_dist/Resources/Private/Language/locallang.xlf:season.element.category_selection,
                    assets;LLL:EXT:bn_typo_dist/Resources/Private/Language/locallang.xlf:season.element.items,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
                    --palette--;;language,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                    --palette--;;hidden,
                    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,rowDescription,
                --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended
                ',
                'columnsOverrides' => [
                    'categories' => [
                        'config' => [
                            'maxitems' => 1,
                            'size' => 1,
                            'type' => 'select',
                            'renderType' => 'selectSingle',
                            'foreign_table_where' => 'AND sys_category.sys_language_uid IN (-1, 0) AND sys_category.pid = 19 ORDER BY sys_category.sorting ASC',
                        ],
                    ],
                    'assets' => [
                        'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('assets', [
                            'overrideChildTca' => [
                                'columns' => [
                                    'uid_local' => [
                                        'config' => [
                                            'appearance' => [
                                                'elementBrowserAllowed' => 'gif,jpg,jpeg,bmp,png,pdf,svg,ai,mp4,webm,youtube,vimeo'
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ], $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext'],'mp3,wav,ogg,flac,opus')
                    ]
                ],
            ],
        ],
    ];

    // add the config to the global TCA array
    $GLOBALS['TCA']['tt_content'] = array_replace_recursive($GLOBALS['TCA']['tt_content'], $TCAforSeasonSwitch);
    $GLOBALS['TCA']['tt_content'] = array_replace_recursive($GLOBALS['TCA']['tt_content'], $TCAforSeasonElement);

});