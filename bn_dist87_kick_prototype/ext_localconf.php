<?php
defined('TYPO3_MODE') or die();

use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func(function () {

    /**
     * Current Extension Key
     */
    $_EXTKEY = "bn_typo_dist";

    /**
     * Add userTsConfig
     */
    ExtensionManagementUtility::addUserTSConfig(
        '<INCLUDE_TYPOSCRIPT:source="FILE:EXT:' . $_EXTKEY . '/Resources/Private/typoscript/system/config/user.tsconfig">'
    );

    /**
     * Add pageTsConfig
     */
    ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT:source="FILE:EXT:' . $_EXTKEY . '/Resources/Private/typoscript/system/config/page.tsconfig">'
    );

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['bn_typo_dist'] =
        \BN\BnTypoDist\Hooks\PageLayoutView\CustomElementsPreviewRenderer::class;

});



