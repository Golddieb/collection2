<?php
defined('TYPO3_MODE') or die();

/**
 * Extension Manager/Repository config file for BN distribution.
 */

$EM_CONF[$_EXTKEY] = array(
    'title' => 'BN Kickstarter',
    'description' => '',
    'category' => 'distribution',
    'author' => 'Julian Mair',
    'author_email' => 'julian.mair@brandnamic.com',
    'state' => 'stable',
    'clearCacheOnLoad' => 1,
    'uploadfolder' => 0,
    'createDirs' => '',
    'version' => '',
    'constraints' => array(
        'depends' => array(
            // Third party extensions (versions are handled in composer.json)
            'cooluri' => '',
            'mask' => '',
            'min' => '',
            'vhs' => ''
        )
    ),
);
