<?php
defined('TYPO3_MODE') or die();
	
call_user_func(function () {

	/**
	 * Current Extension Key
	 */
	$_EXTKEY = "bn_typo_dist";

	/**
	 * Make the extension configuration accessible
	 */
	if (!is_array($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY])) {
		$GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY] = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][$_EXTKEY]);
	}

    /**
     * Icon Registry for BN Icons
     */
    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
        \TYPO3\CMS\Core\Imaging\IconRegistry::class
    );
    $iconRegistry->registerIcon(
        "tx-bntypodist-default", // Icon-Identifier, z.B. tx-myext-action-preview
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        ['source' => 'EXT:' . $_EXTKEY . '/Resources/Public/admin/img/backend/backend_favicon.svg']
    );

    /*
     * CUSTOMIZE BACKEND LOGIN
     * http://www.visionbites.de/blog/artikel/typo3-7-lts-den-login-screen-anpassen.html
     * */
    // get unserialized value (array) of the configuration
    $tmpLoginConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend']);

    // main logo from cdn
    $tmpLoginConf['loginLogo'] = "//cdn.bnamic.com/brandnamic_files/logo_bn/BN_Logo_2016_RGB_pos.svg";

    // color for the button and border (brandnamic-red)
    $tmpLoginConf['loginHighlightColor'] = "#ae173b";

    // static background image | disapear when width < 768px | img source is local due to picture size
    $tmpLoginConf['loginBackgroundImage'] = "EXT:".$_EXTKEY."/Resources/Public/admin/img/backend/backend_bg.jpg";

    // icon for the backend (top left corner)
    $tmpLoginConf['backendLogo'] = "EXT:".$_EXTKEY."/Resources/Public/admin/img/backend/backend_favicon_45x45.png";

    // serialize and save the previous temporary values into the global config array
    $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['backend'] = serialize($tmpLoginConf);

    // END - CUSTOMIZE BACKEND LOGIN
});
