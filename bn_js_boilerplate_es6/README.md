# Brandnamic JS module boilerplate

This is the boilerplate for ES6 JS modules.

## Development

1. Clone the repository
1. Install dependencies using [Yarn Package Manager](https://yarnpkg.com/en/) running `yarn`.
    If you prefer NPM instead, just run `npm install`
1. Run development server with `yarn run watch` (or `npm run watch`)
1. For running the production build, run `yarn run build`

## Contributing

Contributions are welcome! Please open a pull request.