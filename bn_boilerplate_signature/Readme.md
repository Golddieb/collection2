# Signaturen

Dieses Repo dient als **"Kickstarter"** für Signaturen. Die hier schon angelegten Dummy-Dateien (in Ordner ```Template\Microsoft\Signatures```) dienen nur zur Veranschaulichung der Ordnerstruktur und sollen gelöscht werden.

### Anwendnungsfälle

Für die Erstellung von Signaturen gibt es hauptsächlich zwei Anwendungsfälle

1. Webspace liegt bei Brandnamic:
    * Bilder werden auf dem Webspace gespeichert und in der Signatur - mit absoluten Pfaden - verlinkt.   
        Die Signatur wird als ```.htm``` **(!Achtung: ohne ```l```)** abgespeichert und in den ```%APPDATA%/Microsoft/Signatures``` hinterlegt. Man beachte die Namenskonventionen, siehe unten.   
        Die 2 Dateien (```.rtf``` und ```.txt```) müssen neben der ```.htm``` noch manuell angelegt werden (müssen den selben Namen besitzen). 

2. Webspace liegt **nicht** bei Brandnamic:
    * \(optimal\) HTML Datei im Browser öffnen \> ```STRG + A``` und ```STRG + C``` \> in Outlook Signatur Editor einfügen \> Auf "OK" drücken.
    * HTML Datei als ```.docm``` abspeichern \> in Outlook Signatur Editor einfügen \> Auf "OK" drücken.

    In beiden Fällen muss die Namenskonventionen beachtet werden, siehe unten.

### Konventionen

Bei der Signatur Erstellung wird eine Namenskonvention verfolgt:

```BN_{Kundenname}_{evtl. weitere Zusätze}```

* Der Kundenname muss nicht vollständig ausgeschrieben sein, kann man auch kürzen.   
    Beispiel: ```Mondi Holiday Hotel Bellevue``` \> ```Mondi-Bellevue```
    
* Die evtl. Zusätze können bei jeden Auftrag variieren. z.B.:
    * Signaturen für Sommer oder Winter
    * Signaturen für verschiedene Abteilungen
    * etc...
    
    Beispiele: ```BN_Schmiedhans```, ```BN_Mondi-Bellevue_Sommer```, ```BN_SHI_Praxis```

### Abschluss

Egal welcher Fall eintritt, am Ende müssen die 3 Dateien und 1 Ordner aus den ```%APPDATA%/Microsoft/Signatures``` Ordner kopiert werden (haben alle dem selben Namen).

Diese Daten werden dann:

1. In die vorgegebene Ordnerstruktur kopiert, siehe Ordner ```Template```.
2. Falls nicht schon vorhanden, ein README.txt File hinzufügen, welches Installationsanweisungen beinhaltet.
3. Ein Zip daraus erstellt (der Microsoft-Ordner soll zusammen mit dem Readme-File, im Zip als erste Ebene erscheinen).
4. Die erstellte Zip muss zwingend getestet werden, ob der automatische Enpack-Mechanismus ordnungsgemäß funktioniert.
5. Signatur testen (primär in Outlook, zusätzliche Mailclients wäre natürlich optimal aber nicht erforderlich)
6. TLWEB bzw. den zuständigen PM informieren, damit die Signatur an Kunden bzw. Haustechniker geschickt werden kann.