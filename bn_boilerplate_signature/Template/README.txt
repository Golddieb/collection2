# Einrichten der Brandnamic Signatur #
# Windows Vista, 7, 8, 8.1, 10 --- Outlook 2007, 2010, 2013, 2016 #

## Automatische Enpackung der Signatur ##

1. Rechtsklick auf die ".zip"-Datei -> "Öffnen mit..." -> "Windows Explorer" auswählen.
2. "Alle extrahieren..." auswählen.
3. Klick auf "Durchsuchen...".
4. In Adressleiste "%APPDATA%" (ohne Anführungszeichen) eingeben -> Enter -> "Ordner auswählen".
5. Entpacken.


## Ausweichlösung (nur relevant, falls die obere Methode nicht fehlerfrei funktioniert) ##

1. Den Windows Explorer öffnen und in die Addresszeile "%APPDATA%" (ohne Anführungszeichen) eingeben.
2. Den Ordner "Mircosoft" > "Signatures" suchen und öffnen und Fenster offen lassen.
3. Die Signatur an einen beliebigen Ort entpacken und den entpackten Ordner öffnen.
4. Den enthaltenen "Signatures" Ordner suchen und öffnen.
5. Alle darin enthaltenden Dateien kopieren, zum vorherigen Explorer-Fenster wechseln und sie einfügen.
